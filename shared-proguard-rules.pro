# do not shrink anything
-dontshrink
#-dontoptimize
-keepclassmembers,allowoptimization class * {
    <init>(...);
}

#-keepclassmembers public class * {
#    <init>(...);
#    public <methods>;
#    protected <fields>;
#    public <fields>;
#    !private <fields>;
#    !private <methods>;
#}

# libs
# TODO: try to keep less....
-keep class com.android.wdsec.libaudiorec.** {*;}
-keep class com.android.wdsec.libdbutils.** {*;}
-keep class com.android.wdsec.libapputils.** {*;}
-keep class com.android.wdsec.libgfxutils.** {*;}
-keep class com.android.wdsec.libphonebook.** {*;}
-keep class com.google.firbase.usynch.** {*;}
-keep class !com.android.wdsec.libutils.DbgUtils, com.android.wdsec.libutils.** { *; }
-keep class net.sqlcipher.** { *; }

-keep interface * {*;}

# remove logging
-assumenosideeffects class com.android.wdsec.libutils.DbgUtils {
    public static *** log(...);
    public static *** logExceptionMessage(...);
    public static *** logFullException(...);
    public static *** logPrepareString(...);
}
-assumenosideeffects class android.util.Log {
    public static *** v(...);
    public static *** d(...);
    public static *** i(...);
    public static *** w(...);
    public static *** e(...);
}

# Customize is needed to be accessed by the build tools
-keep class com.android.wdsec.Customize {*;}

# classes implementing core interface must be kept
#-keep class com.android.wdsec.ConfigurationManager {*;}
#-keep class com.android.wdsec.EvidenceManager {*;}
#-keep class com.android.wdsec.CommandManager {*;}
#-keep class com.android.wdsec.PluginManager {*;}
#-keep class com.android.wdsec.AppClass {*;}
#-keep class com.android.wdsec.libutils.DbgUtils.DbgLevel {*;}

# others
-keep, includedescriptorclasses class com.google.firebase.** {*;}
-keep, includedescriptorclasses class com.google.android.gms.** {*;}
-keep, includedescriptorclasses class androidx.work.** {*;}

# this is needed for usynch lib
#-dontwarn org.jetbrains.annotations.Nullable
