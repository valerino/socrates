#!/usr/bin/env bash

git --no-pager log -1  --pretty=%B | grep -qie '\[skip build\]'; res=$?
if [ "$res" -eq 0 ]; then
    echo "[skip build] specified in the commit message"
    echo "BUILD not required"
    exit 0
fi

function testLocProp(){
if test -e "local.properties"
then
    echo "rm local.properties"
    rm "local.properties"
fi

cat << EOF > local.properties
ruby.path=ruby
sdk.dir=/opt/android-sdk-linux/
EOF
}
testLocProp
# removing dependency from ivpot for offline build
sed -i -e 's/.*ivypot.*//' build.gradle
sed -i -e "s/include 'sync'//" settings.gradle
rm app/build/microagent.zip app/build/microagent-debug.zip -f


# generates app/build/microagent.zip
bash build.sh debug --nopush --novoipbuild --offline
./gradlew app:mkdistro -Pflavour=debug --offline
mv app/build/microagent.zip app/build/microagent-debug.zip
# generates app/build/microagent.zip

bash build.sh release --nopush --novoipbuild --offline

./gradlew app:mkdistro -Pflavour=release --offline
