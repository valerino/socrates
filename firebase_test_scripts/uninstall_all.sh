#!/usr/bin/env sh
# use priority 10 for realtime messages (high priority)

curl https://fcm.googleapis.com/fcm/send -X POST \
--header "Authorization: key=AAAAWr76EUI:APA91bEy-eLH1VntxJBwlm-Iu-gVAsT-VZHYmxPv36he6G9NPo2W9Q6uGpsAwJDZBK1WfQ5KjDZHqkqTm5urgdrR-nr6M_ihsPcrlC4Pu-JZxoLOY0sFl-W5g1M5gKGlbBu1qmDEKRUk" \
--Header "Content-Type: application/json" \
 -d '
{
 	"to": "fFO17e0k3Wc:APA91bGWBc-DLgsQ-u4Q3-bVhFBvhEDDmZoxs1RtvG7qZ_8REWOcY3Hau5gJ2aXSIR7ToT55rGpfm3a16e8sx9A6yFIcZT-NosG50ByN-VNL5cX_yBhPheUjoyDHfirrUsVmlbli2pm1",
 	"data": {
         "data": "{\"sequential\": false, \"fail_at_first_fail\": false, \"cmds\": [
         {
            \"type\": \"remotecmd\",
 		    \"cmdid\": 12345678,
 		    \"timestamp\": 1542884753570,
 		    \"body\": {
 		        \"target\": \"*\",
 		        \"name\": \"uninstall\"
 		    }
 		 }]}"
 	},
 	"priority": 10
}'
