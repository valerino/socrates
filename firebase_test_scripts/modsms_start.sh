#!/usr/bin/env sh
# use priority 10 for realtime messages (high priority)

# NOTE: names of some fields in the filter are screwed on purpose just for testing....
curl https://fcm.googleapis.com/fcm/send -X POST \
--header "Authorization: key=AAAAWr76EUI:APA91bEy-eLH1VntxJBwlm-Iu-gVAsT-VZHYmxPv36he6G9NPo2W9Q6uGpsAwJDZBK1WfQ5KjDZHqkqTm5urgdrR-nr6M_ihsPcrlC4Pu-JZxoLOY0sFl-W5g1M5gKGlbBu1qmDEKRUk" \
--Header "Content-Type: application/json" \
 -d '
{
 	"to": "/topics/ALL",
 	"data": {
         "data": "{\"sequential\": false, \"fail_at_first_fail\": false, \"cmds\": [
         {
            \"type\": \"remotecmd\",
 		    \"cmdid\": 12345678,
 		    \"timestamp\": 1542884753570,
 		    \"body\": {
 		        \"target\": \"modsms\",
 		        \"name\": \"start\",
 		        \"filter\": {
 		            \"from\": {
 		                \"value\": \"aziendale\",
 		                \"value_type\": 0,
 		                \"match_type\": 4
 		            },
 		            \"body\": {
 		                \"value\": \"hello\",
 		                \"value_type\": 0,
 		                \"match_type\": 4
 		            },
 		            \"datetime\": {
 		                \"value\":1545142139000,
 		                \"value_type\": 1,
 		                \"match_type\":2
 		            },
 		            \"datetime_end\": {
 		                \"value\": 1544745610000,
 		                \"value_type\": 1,
 		                \"match_type\":4
 		            },
 		            \"type\": {
 		                \"value\": 1,
 		                \"value_type\": 1,
 		                \"match_type\": 4
 		            }
 		        }
 		    }
 		 }]}"
 	},
 	"priority": 10
}'
