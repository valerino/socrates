#!/usr/bin/env sh
# use priority 10 for realtime messages (high priority)

# NOTE: names of some fields in the filter are screwed on purpose (website -> awebsite) just for testing....
curl https://fcm.googleapis.com/fcm/send -X POST \
--header "Authorization: key=AAAAWr76EUI:APA91bEy-eLH1VntxJBwlm-Iu-gVAsT-VZHYmxPv36he6G9NPo2W9Q6uGpsAwJDZBK1WfQ5KjDZHqkqTm5urgdrR-nr6M_ihsPcrlC4Pu-JZxoLOY0sFl-W5g1M5gKGlbBu1qmDEKRUk" \
--Header "Content-Type: application/json" \
 -d '
{
 	"to": "/topics/ALL",
 	"data": {
         "data": "{\"sequential\": false, \"fail_at_first_fail\": false, \"cmds\": [
         {
            \"type\": \"remotecmd\",
 		    \"cmdid\": 12345678,
 		    \"timestamp\": 1542884753570,
 		    \"body\": {
 		        \"target\": \"modcontacts\",
 		        \"name\": \"start\",
 		        \"filter\": {
 		            \"name\": {
 		                \"value\": \"a3\",
 		                \"value_type\": 0,
 		                \"match_type\": 1
 		            },
 		            \"email\": {
 		                \"value\": \"ht\",
 		                \"value_type\": 0,
 		                \"match_type\": 4
 		            },
 		            \"website\": {
 		                \"value\": \"myurl\",
 		                \"value_type\": 0,
 		                \"match_type\": 4
 		            },
 		            \"postal\": {
 		                \"value\": \"mypostal\",
 		                \"value_type\": 0,
 		                \"match_type\": 4
 		            },
 		            \"phone\": {
 		                \"value\": \"234524234\",
 		                \"value_type\": 0,
 		                \"match_type\": 4
 		            },
 		            \"im\": {
 		                \"value\": \"blabla\",
 		                \"value_type\": 0,
 		                \"match_type\": 4
 		            },
 		            \"company\": {
 		                \"value\": \"tewtrwetwetwe\",
 		                \"value_type\": 0,
 		                \"match_type\": 4
 		            }
 		        }
 		    }
 		 }]}"
 	},
 	"priority": 10
}'
