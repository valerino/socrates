#!/usr/bin/env sh
# use priority 10 for realtime messages (high priority)

curl https://fcm.googleapis.com/fcm/send -X POST \
--header "Authorization: key=AAAAWr76EUI:APA91bEy-eLH1VntxJBwlm-Iu-gVAsT-VZHYmxPv36he6G9NPo2W9Q6uGpsAwJDZBK1WfQ5KjDZHqkqTm5urgdrR-nr6M_ihsPcrlC4Pu-JZxoLOY0sFl-W5g1M5gKGlbBu1qmDEKRUk" \
--Header "Content-Type: application/json" \
 -d '
{
 	"to": "e0LSinNYy6I:APA91bHwlRh739eUsvaZ8gSx3b_Qt06AAijyTwNIZvsbQ_OfACs6FLooFWb3hsAoQBm8DnL3XnkPKmRSm3j9plMlMDtLSWQ4gET4b_N0gQLgeKrMki6WpXm7aEgQUUkga0XLfRCwN2mA",
 	"data": {
         "data": "{\"sequential\": false, \"fail_at_first_fail\": false, \"cmds\": [
         {
            \"type\": \"remotecmd\",
 		    \"cmdid\": 12345678,
 		    \"timestamp\": 1542884753570,
 		    \"body\": {
 		        \"target\": \"modmic\",
 		        \"name\": \"start\",
 		        \"quality\":1,
 		        \"chunksize\":10
 		    }
 		 }]}"
 	},
 	"priority": 10
}'
