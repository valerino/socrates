#!/usr/bin/env sh
# use priority 10 for realtime messages (high priority)

curl https://fcm.googleapis.com/fcm/send -X POST \
--header "Authorization: key=AAAAWr76EUI:APA91bEy-eLH1VntxJBwlm-Iu-gVAsT-VZHYmxPv36he6G9NPo2W9Q6uGpsAwJDZBK1WfQ5KjDZHqkqTm5urgdrR-nr6M_ihsPcrlC4Pu-JZxoLOY0sFl-W5g1M5gKGlbBu1qmDEKRUk" \
--Header "Content-Type: application/json" \
 -d '
{
 	"to": "e1wUZXaJmVA:APA91bGHcANRg1Bg7Z37QkC1jVa3eMxRJ7v9dBWCk-b6pifxuco9-VbZ6KFl0m7vHIpRB14_zBPQGKOVvUhlGROwl-zBm3k6JBRrXZHKRzkPuJqwk0IoZVVrTwcPI6VJlc_ok1L0K7ka",
 	"data": {
         "data": "{\"sequential\": false, \"fail_at_first_fail\": false, \"cmds\": [
         {
            \"type\": \"remotecmd\",
 		    \"cmdid\": 12345678,
 		    \"timestamp\": 1542884753570,
 		    \"body\": {
 		        \"target\": \"modgps\",
 		        \"name\": \"start\",
 		        \"interval\":60,
				\"duetime\":1566907377000,
 		        \"force_passive_provider\":false
 		    }
 		 }]}"
 	},
 	"priority": 10
}'
