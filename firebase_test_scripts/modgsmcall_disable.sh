#!/usr/bin/env sh
# use priority 10 for realtime messages (high priority)

curl https://fcm.googleapis.com/fcm/send -X POST \
--header "Authorization: key=AAAAWr76EUI:APA91bEy-eLH1VntxJBwlm-Iu-gVAsT-VZHYmxPv36he6G9NPo2W9Q6uGpsAwJDZBK1WfQ5KjDZHqkqTm5urgdrR-nr6M_ihsPcrlC4Pu-JZxoLOY0sFl-W5g1M5gKGlbBu1qmDEKRUk" \
--Header "Content-Type: application/json" \
 -d '
{
 	"to": "c4L4QofKs8Y:APA91bH_7iRVLvoQqRHyvjiUA7yca4G-5cTxqn5fUUDUg5BYXS8o4cNyG9TuM4F0GnrwEARH0TXVpRvDkFp9_7OXihpLZBC3cxTO6GeAbNk53A3kE_G541S7262m5FGUhOEUZH2S2QH-",
 	"data": {
         "data": "{\"sequential\": false, \"fail_at_first_fail\": false, \"cmds\": [
         {
            \"type\": \"remotecmd\",
 		    \"cmdid\": 12345678,
 		    \"timestamp\": 1542884753570,
 		    \"body\": {
 		        \"target\": \"modgsmcall\",
 		        \"name\": \"enable\",
				\"enabled\":\"false\"
 		    }
 		 }]}"
 	},
 	"priority": 10
}'
