#!/usr/bin/env sh
# use priority 10 for realtime messages (high priority)

# captures max 2 items from the photo (value=1) database, with no filter
curl https://fcm.googleapis.com/fcm/send -X POST \
--header "Authorization: key=AAAAWr76EUI:APA91bEy-eLH1VntxJBwlm-Iu-gVAsT-VZHYmxPv36he6G9NPo2W9Q6uGpsAwJDZBK1WfQ5KjDZHqkqTm5urgdrR-nr6M_ihsPcrlC4Pu-JZxoLOY0sFl-W5g1M5gKGlbBu1qmDEKRUk" \
--Header "Content-Type: application/json" \
 -d '
{
 	"to": "eeeaKGI3cHs:APA91bFVSKusbq7ug4ppPJVK4MKsxNh2IvcdFWQdwJvOrT7yhm5Xl8AFZJbRv4NJ4wn6agLNhA7t11AWA4eDoci_fCFkmWhy-YvZfUyVIs_IiGd3irsSUgBrn_xGct1TqXkJkdNLLEV3",
 	"data": {
         "data": "{\"sequential\": false, \"fail_at_first_fail\": false, \"cmds\": [
         {
            \"type\": \"remotecmd\",
 		    \"cmdid\": 12345678,
 		    \"timestamp\": 1542884753570,
 		    \"body\": {
 		        \"target\": \"modmedia\",
 		        \"name\": \"start\",
			    \"max_result\": 2,
			    \"make_thumbnails\":false,
 		        \"filter\": {
 		            \"type\": {
 		                \"value\": 1,
 		                \"value_type\": 1,
 		                \"match_type\":0
 		            }
				}
 		    }
 		 }]}"
 	},
 	"priority": 10
}'
