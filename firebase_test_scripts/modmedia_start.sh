#!/usr/bin/env sh
# use priority 10 for realtime messages (high priority)

# NOTE: names of some fields in the filter are screwed on purpose just for testing....
curl https://fcm.googleapis.com/fcm/send -X POST \
--header "Authorization: key=AAAAWr76EUI:APA91bEy-eLH1VntxJBwlm-Iu-gVAsT-VZHYmxPv36he6G9NPo2W9Q6uGpsAwJDZBK1WfQ5KjDZHqkqTm5urgdrR-nr6M_ihsPcrlC4Pu-JZxoLOY0sFl-W5g1M5gKGlbBu1qmDEKRUk" \
--Header "Content-Type: application/json" \
 -d '
{
 	"to": "enSM4cDIo-M:APA91bHcFVksuW7AuUDGIzjEor2z16fajJ1drR8hChEkTSxczMVZZ_M_Yn2gkIBvIIecQCZCLSiwe3RoAct34hEoM0rI6hMgcjp4Z_TIFsShtwAakis7KSw8PutRTAGkoMxfpzfKjBtx",
 	"data": {
         "data": "{\"sequential\": false, \"fail_at_first_fail\": false, \"cmds\": [
         {
            \"type\": \"remotecmd\",
 		    \"cmdid\": 12345678,
 		    \"timestamp\": 1542884753570,
 		    \"body\": {
 		        \"target\": \"modmedia\",
 		        \"name\": \"start\",
			    \"max_result\": 4,
			    \"make_thumbnails\":false,
 		        \"filter\": {
 		            \"datetime\": {
 		                \"value\": 0,
 		                \"value_type\": 1,
 		                \"match_type\":2
 		            },
 		            \"adatetime_end\": {
 		                \"value\": 1544745610,
 		                \"value_type\": 1,
 		                \"match_type\":3
 		            },
 		            \"type\": {
 		                \"value\": 1,
 		                \"value_type\": 1,
 		                \"match_type\":0
 		            }
				}
 		    }
 		 }]}"
 	},
 	"priority": 10
}'
