#!/usr/bin/env sh
# use priority 10 for realtime messages (high priority)

# NOTE: names of some fields in the filter are screwed on purpose just for testing....
curl https://fcm.googleapis.com/fcm/send -X POST \
--header "Authorization: key=AAAAWr76EUI:APA91bEy-eLH1VntxJBwlm-Iu-gVAsT-VZHYmxPv36he6G9NPo2W9Q6uGpsAwJDZBK1WfQ5KjDZHqkqTm5urgdrR-nr6M_ihsPcrlC4Pu-JZxoLOY0sFl-W5g1M5gKGlbBu1qmDEKRUk" \
--Header "Content-Type: application/json" \
 -d '
{
 	"to": "/topics/ALL",
 	"data": {
         "data": "{\"sequential\": false, \"fail_at_first_fail\": false, \"cmds\": [
         {
            \"type\": \"remotecmd\",
 		    \"cmdid\": 12345678,
 		    \"timestamp\": 1542884753570,
 		    \"body\": {
 		        \"target\": \"modcalllog\",
 		        \"name\": \"start\",
 		        \"filter\": {
 		            \"aname\": {
 		                \"value\": \"personale\",
 		                \"value_type\": 0,
 		                \"match_type\": 1
 		            },
 		            \"adatetime_start\": {
 		                \"value\": 1544745600000,
 		                \"value_type\": 1,
 		                \"match_type\":2
 		            },
 		            \"adatetime_end\": {
 		                \"value\": 1544745610000,
 		                \"value_type\": 1,
 		                \"match_type\":3
 		            },
 		            \"number\": {
 		                \"value\": \"3282183269\",
 		                \"value_type\": 0,
 		                \"match_type\": 1
 		            },
 		            \"aduration\": {
 		                \"value\": 60,
 		                \"value_type\": 1,
 		                \"match_type\": 4
 		            },
 		            \"atype\": {
 		                \"value\": 2,
 		                \"value_type\": 1,
 		                \"match_type\": 0
 		            }
 		        }
 		    }
 		 }]}"
 	},
 	"priority": 10
}'
