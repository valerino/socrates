import argparse
import firebase_admin
from firebase_admin import credentials
from firebase_admin import db
import google
from google.oauth2 import service_account
# --uid "4dafc31e-46d1-403432c7-951963423430f3432" --secret "Ac9BcH7zx9GHF13hS0yU6rJzBXFiEsdJAxHsP2hf"
parser = argparse.ArgumentParser('get_token - acquire a new token')
parser.add_argument('--uid', nargs='?', help='application uid for reading the db', default=None)
parser.add_argument('--secret', nargs='?', help='database legacy secret', default=None)
args = parser.parse_args()

if args.secret :

    from firebase_token_generator import create_token

    auth_payload = {"uid": args.uid, }
    auth = create_token(args.secret, auth_payload)
    print ("auth:\n" + str(auth) +"\n expires in 24h")
else:

    # Fetch the service account key JSON file contents, this is equivalents to service_account.Credentials.from_service_account_file
    '''
    cred = credentials.Certificate('key.json')

    # Initialize the app with a custom auth variable, limiting the server's access
    firebase_admin.initialize_app(cred, {
        'databaseURL': 'https://testagt-9cee3.firebaseio.com',
        'databaseAuthVariableOverride': {
            'uid': '4dafc31e-46d1-403432c7-951963423430f3432'
        }
    })

    # The app only has access as defined in the Security Rules
    ref = db.reference('/presence')
    print(ref.get())
    print ("token:" + str(cred.get_access_token()))
    '''
    # Define the required scopes
    scopes = [
      "https://www.googleapis.com/auth/userinfo.email",
      "https://www.googleapis.com/auth/firebase.database"
    ]

    # Authenticate a credential with the service account
    credentials = service_account.Credentials.from_service_account_file(
        "key.json", scopes=scopes)

    request = google.auth.transport.requests.Request()
    credentials.refresh(request)
    access_token = credentials.token
    print ("token:\n" + str(access_token))
    print ("utc expiration:" + str(credentials.expiry))
