#!/usr/bin/env python2
import argparse
import binascii
import datetime
import time
import google
import json
from google.oauth2 import service_account

def xorra(b, bytes):
    bytes = list(bytes)
    b = list(b)
    i = len(bytes) - 1
    while i >= 0:
        a = i % len(b)
        bytes[i] = ord(bytes[i]) ^ ord(b[a])
        i -= 1
    return bytes


def extract_id(name, value):
    div = "-"
    if "+" in value:
        div = "+"
    segments = value.split(div)
    app_id = None
    token = segments[0]

    if len(segments) > 1:
        app_id = segments[0]
        token = segments[1]
    instanceId = name[(-20 * 2):]
    enc_name = name[:(-20 * 2)]
    if app_id:
        print ("app_id is : " + app_id)
    print ("decoding token : " + token)
    b_nonce = binascii.a2b_hex(token[:16 * 2])
    print ("nonce : " + binascii.b2a_hex(b_nonce))
    b_instance = binascii.a2b_hex(instanceId)
    print ("instance : " + binascii.b2a_hex(b_instance))
    b_name = binascii.a2b_hex(enc_name)
    print ("xname : " + binascii.b2a_hex(b_name))
    b_value = binascii.a2b_hex(token[16 * 2:])
    print ("value : " + binascii.b2a_hex(b_value))

    # b_instance = bytearray(b_instance)
    xored =  xorra(b_instance, b_name)
    xored = [chr(c) for c in xored]
    name = "".join(xored)
    for_xor_token = "".join([chr(c) for c in xorra(b_nonce, b_instance)])
    print ("forXor : " + binascii.b2a_hex(for_xor_token))
    jsonData = json.loads( "".join([chr(c) for c in xorra(for_xor_token, b_value)]))
    print ("json=" + str(jsonData) )
    print ("id = {}\n name={} \n name_str={}\n token={}".format(instanceId,
                                                                binascii.b2a_hex(name),name, jsonData['token']))
    return jsonData['token']


import requests


def select_device(args):
    address = 'https://{}.firebaseio.com/presence.json'.format(args.projid)
    data = {
        'auth': args.auth
    }
    print (address)
    r = requests.get(address, params=data)
    devices = r.json()

    devices = devices[devices.keys()[0]]
    i = 1
    arr_devices = []
    for k in devices:
        print ("{}) {} = {}".format(i, k, devices[k]))
        if "error" in k:
            raise Exception(devices[k])
        i += 1
        arr_devices.append((k, devices[k]))
    i = int(input("select the device:"))
    return arr_devices[i - 1]


def ping(key, token_id):
    address = 'https://fcm.googleapis.com/fcm/send'
    header = {
        'Authorization': 'key={}'.format(key),
        'Content-Type': 'application/json'
    }

    data = {
        'to': token_id,
        'data': {
            'data': {
                'type': 'remotecmd',
                'cmdid': '{}'.format(key),
                'name': '{}'.format(datetime.datetime.fromtimestamp(time.time()).strftime('%c')),
                'timestamp': '{}'.format(time.time()),
                'body': {
                    "target": "core",
                    "name": "ping",
                }
            },
            'priority': 10,
        }
    }
    r = requests.post(address, headers=header, json=data)

    print(r.text)


if __name__ == '__main__':
    #extract_id('DF1D72024BF9B3087890871CC6E17A52486278BCA76D05647BC9833848A0B72CE82441647BC9833848A0B72CF6D57A52092C3CEE',
    #              '+06E7EAFA228CDB1EDFAEE6FEA6004C9D8DF0F1EC2E0D7511F56461')
    # --uid "4dafc31e-46d1-403432c7-951963423430f3432" --secret "Ac9BcH7zx9GHF13hS0yU6rJzBXFiEsdJAxHsP2hf"
    parser = argparse.ArgumentParser('ping an online device')




    parser.add_argument('--auth', '-a', help='auth token for reading the db',
                        default='eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1NDg2NjQwNzAsImQiOnsidWlkIjoiNGRhZmMzMWUtNDZkMS00MDM0MzJjNy05NTE5NjM0MjM0MzBmMzQzMiJ9LCJ2IjowfQ.4FNZ5Q1ZhvW3uW9M-KSopEjiczFJvTHrd9rMfMgLqes')
    #    parser.add_argument('--key', nargs='?', help='api key for cloud messaging',
    #                    default='AAAAWr76EUI:APA91bEy-eLH1VntxJBwlm-Iu-gVAsT-VZHYmxPv36he6G9NPo2W9Q6uGpsAwJDZBK1WfQ5KjDZHqkqTm5urgdrR-nr6M_ihsPcrlC4Pu-JZxoLOY0sFl-W5g1M5gKGlbBu1qmDEKRUk')

    projects_info = {
        1:
            {
                'projid': 'testme-c6588',
                'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1NDg2NjQ0ODYsImQiOnsidWlkIjoiNGRhZmMzMWUtNDZkMS00MDM0MzJjNy05NTE5NjM0MjM0MzBmMzQzMiJ9LCJ2IjowfQ.J9iGuWUUH5KmcuNCtT8vag1x5Ksimg5dIqNeNS74RXo',
                'key': 'AAAAWr76EUI:APA91bEy-eLH1VntxJBwlm-Iu-gVAsT-VZHYmxPv36he6G9NPo2W9Q6uGpsAwJDZBK1WfQ5KjDZHqkqTm5urgdrR-nr6M_ihsPcrlC4Pu-JZxoLOY0sFl-W5g1M5gKGlbBu1qmDEKRUk',
            },
        2:
            {
                'projid': 'testagt-9cee3',
                'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1NDg2NjQwNzAsImQiOnsidWlkIjoiNGRhZmMzMWUtNDZkMS00MDM0MzJjNy05NTE5NjM0MjM0MzBmMzQzMiJ9LCJ2IjowfQ.4FNZ5Q1ZhvW3uW9M-KSopEjiczFJvTHrd9rMfMgLqes',
                'key': 'AAAAWr76EUI:APA91bEy-eLH1VntxJBwlm-Iu-gVAsT-VZHYmxPv36he6G9NPo2W9Q6uGpsAwJDZBK1WfQ5KjDZHqkqTm5urgdrR-nr6M_ihsPcrlC4Pu-JZxoLOY0sFl-W5g1M5gKGlbBu1qmDEKRUk',
            }
    }

    # parser.add_argument('--secret', nargs='?', help='database legacy secret', default=None)
    args = parser.parse_args()
    i = 1
    for k in projects_info:
        print ("{}) {}".format(i, projects_info[i]['projid']))
        i += 1
    i = int(input("select the project:"))
    args.projid = projects_info[i]['projid']
    if not hasattr(args,'auth'):
        args.auth = projects_info[i]['auth']
    args.key = projects_info[i]['key']
    device = select_device(args)
    print ("extracting token for {}".format(device))
    if isinstance(device[1], dict):
        device = list(device[1].items())[0]
    token_id = extract_id(device[0], device[1])
    ping(args.key, token_id)
