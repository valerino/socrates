#!/usr/bin/env sh

: << 'COMMENT'
metodo 1:
usando un secure token , legacy token:
https://www.firebase.com/docs/rest/guide/user-auth.html
1) leggere il database secret in https://console.firebase.google.com/u/0/project/testagt-9cee3/settings/serviceaccounts/databasesecrets
2) creare una regola di lettura sul db di destinazione per un uid in particolare , del tipo:
{
  "rules": {
    ".read": "auth.uid === '4dafc31e-46d1-403432c7-951963423430f3432'",
    ".write": true
  }
}
3) Eseguire lo script :
firebase_test_scripts/admin_tools/get_token.py --uid 4dafc31e-46d1-403432c7-951963423430f3432 --secret Ac9BcH7zx9GHF13hS0yU6rJzBXFiEsdJAxHsP2hf
4) Usare il token che ha validita' per 24h
COMMENT
echo "Metodo 1"

curl https://testagt-9cee3.firebaseio.com/presence.json?auth=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1NDgyNDEwNDcsImQiOnsidWlkIjoiNGRhZmMzMWUtNDZkMS00MDM0MzJjNy05NTE5NjM0MjM0MzBmMzQzMiJ9LCJ2IjowfQ.WNvZSI5Tv30PiFw2TfIr3RN4F2mjzaWymx32yeYxWSs\&print=pretty

: << 'COMMENT'
metodo 2:
usando un access_token
https://www.firebase.com/docs/rest/guide/user-auth.html
1) generare e salvare una nuova chiave privata:
https://console.firebase.google.com/u/0/project/testagt-9cee3/settings/serviceaccounts/adminsdk
firebase_test_scripts/admin_tools/key.json

2) Eseguire lo script :
firebase_test_scripts/admin_tools/get_token.py
3) Usare il token che ha validita' per 1h

NOTA: access_token funziona anche con una regola del timpo :
{
  "rules": {
    ".read": false,
    ".write": true
  }
}

COMMENT
echo "Metodo 2"
curl https://testagt-9cee3.firebaseio.com/presence.json?access_token=ya29.c.Elt4Bj5kSyMzPWefzFwRNavQN0l6KWNTdZ0Z_ZBgwzsvgwDMDfIquVTE4J9V_y6mIZtqRbAu68UQXP3k4PWCzSvMVlkgqYxjEN7ybILtouKZsvE_1JTguAq_AoL4\&print=pretty -X GET

