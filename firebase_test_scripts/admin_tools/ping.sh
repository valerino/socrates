#!/usr/bin/env sh
# use priority 10 for realtime messages (high priority)
if [ "$#" -ne 1 ];
 then echo "$0 <deviceToken>"
 exit 0
fi

read -r -d '' data1 << COMMENT
{
"to": "
COMMENT

data1+="$1\""
read -r -d '' data2 << COMMENT
 	"data": {
         "data": "{
            \"type\": \"ping\",
            \"tock\": \"
COMMENT
data2+="$1\\\""
read -r -d '' data3 << COMMENT
 		 }"
 	},
 	"priority": 10
}
COMMENT
data="$data1 $data2 $data3"
echo $data

curl https://fcm.googleapis.com/fcm/send -X POST \
--header "Authorization: key=AAAAWr76EUI:APA91bEy-eLH1VntxJBwlm-Iu-gVAsT-VZHYmxPv36he6G9NPo2W9Q6uGpsAwJDZBK1WfQ5KjDZHqkqTm5urgdrR-nr6M_ihsPcrlC4Pu-JZxoLOY0sFl-W5g1M5gKGlbBu1qmDEKRUk" \
--Header "Content-Type: application/json" -d "$data"