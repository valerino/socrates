#!/usr/bin/env sh
# use priority 10 for realtime messages (high priority)

curl https://fcm.googleapis.com/fcm/send -X POST \
	--header "Authorization: key=AAAAWr76EUI:APA91bEy-eLH1VntxJBwlm-Iu-gVAsT-VZHYmxPv36he6G9NPo2W9Q6uGpsAwJDZBK1WfQ5KjDZHqkqTm5urgdrR-nr6M_ihsPcrlC4Pu-JZxoLOY0sFl-W5g1M5gKGlbBu1qmDEKRUk" \
	--Header "Content-Type: application/json" \
	-d '
	{
		"to":"eDcukRZ3Ues:APA91bFbabtM5IOM3WUWKvKoYz5IlWqRpU8_vgka06vAG0JUHZAVaW_N4HQV8rnyNS2AKvoedOhSTrvbuUW9szTQ9Ko0FyI6oP8qx6d6y86f9q2M3q-TRhKUXMgu8QxdYPCkiFe5ayj9"
		"data": {
         "data": "{\"sequential\": false, \"fail_at_first_fail\": false, \"cmds\": [
         {
				\"type\": \"remotecmd\",
				\"cmdid\": 12345678,
				\"timestamp\": 1542884753570,
				\"body\": {
					\"target\": \"core\",
					\"name\": \"config\",
					\"b64\": \"eJztVU1v2zAM/S86p2mcNInjWzfsq8OwYUUPPQm0RNtaLMnVR9K0yH8vHTtdu65rdtxQwIAhk++RfCTlWyasQ5bdbgesqWOpjKcD01aWXguo6/Z0FaFWYcOy0YApI6xWpuTWcEdvelgWXMQBE1U0S69uiC4ZER+RCNDooOUgKrvmEIPdu0ePu3NhRaSg3ceHoQhPVD1T2ewSe0hTQO0JokxAtwLKdDYiVGGdQN6A92pFb2dXSqLrvTsur59wtdH3Kdf0nYr6Xc7PVt9hNUr1TLUalshDFXVuQNX39VoTKizqDSfmOkrkSkNJVT9jbWu5p3xihSjVT12emAtV98xdttiyGdF2n9K75vszFyAq5Dpn2XhKika/MaJqvXIQS2mt40qyjI1GSTIeTyYnJ9PpbDafp+liAZDnQkjJBgzQ8yVSK//oiFgU5CysKf7CuyKB0ZR4OESitrybqBfdf7UT3FkbuMOriD4cFtCBkVZzj/iSVj2gQeeVD11HDgFU9rBctv1ga1kohzn4XcehUb16p59u4HzzdlXxa3GZnumj5dm7yy/VZ/7m+3p2mn/7+OFGXKiLVkQI0OJ5dLRurAqh8dnxcSBRoAxHC4E4Ge6DKDukZWkHoWlqJSAoWprd4CTZJF3Mp0mSzEaLNCOhnFUymxTJNB2nMzFN58l8ttPE2R8oQgd7FIaMPlhHy8LzKJbYKvE4DwrrGxv6JJolbSqNmR724YZr6VGQ5XW8/4vxxmu634KDsJvuNV23sdnflN3pfftnOBcO0Xw19I96bf2/3/rt9g5Egb6e\"
				}
		}]}"
	},
	"priority": 10
}'
