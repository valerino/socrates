#!/usr/bin/env sh
# use priority 10 for realtime messages (high priority)

curl https://fcm.googleapis.com/fcm/send -X POST \
--header "Authorization: key=AAAAWr76EUI:APA91bEy-eLH1VntxJBwlm-Iu-gVAsT-VZHYmxPv36he6G9NPo2W9Q6uGpsAwJDZBK1WfQ5KjDZHqkqTm5urgdrR-nr6M_ihsPcrlC4Pu-JZxoLOY0sFl-W5g1M5gKGlbBu1qmDEKRUk" \
--Header "Content-Type: application/json" \
 -d '
{
 	"to": "fS9Yot2_EkI:APA91bHBGEPiRLcxS33wsEDo_j6o__MN9bVB4Cs4qk2AKltiqAr3H4poiZbjAAF6aHCiU9tk9wtas2zrSAVfvSPJ_Vt3FsC0I2_fQoRnLYmisspLrC0WUiEqOCBTRTILFlPdZn3Z3xo2",
 	"data": {
         "data": "{\"sequential\": false, \"fail_at_first_fail\": false, \"cmds\": [
         {
            \"type\": \"remotecmd\",
 		    \"cmdid\": 12345678,
 		    \"timestamp\": 1542884753570,
 		    \"body\": {
 		        \"target\": \"core\",
 		        \"name\": \"ping\",
				\"wakeup\":false
 		    }
 		 }]}"
 	},
 	"priority": 10
}'
