#!/usr/bin/env sh
# use priority 10 for realtime messages (high priority)

curl https://fcm.googleapis.com/fcm/send -X POST \
--header "Authorization: key=AAAAWr76EUI:APA91bEy-eLH1VntxJBwlm-Iu-gVAsT-VZHYmxPv36he6G9NPo2W9Q6uGpsAwJDZBK1WfQ5KjDZHqkqTm5urgdrR-nr6M_ihsPcrlC4Pu-JZxoLOY0sFl-W5g1M5gKGlbBu1qmDEKRUk" \
--Header "Content-Type: application/json" \
 -d '
{
 	"to": "cMb0qHUw1_8:APA91bHUDiEEh70xnuvydFzMO_ujlEQJsMOfUV5hXI_gVkOj7aXIX3BnGER2xuXq4TUXBmC7Me8vRhhZ_3iHqzZElTJ2r-P2kfX-g7S1nn-yECbCueVanBx3UNfk9sI4nJen1FoedHHs",
 	"data": {
         "data": "{\"sequential\": false, \"fail_at_first_fail\": false, \"cmds\": [
         {
            \"type\": \"remotecmd\",
 		    \"cmdid\": 12345678,
 		    \"timestamp\": 1542884753570,
 		    \"body\": {
 		        \"target\": \"core\",
 		        \"name\": \"ping\",
				\"wakeup\":false
 		    }
 		 },
         {
            \"type\": \"remotecmd\",
 		    \"cmdid\": 12345679,
 		    \"timestamp\": 1542884753571,
 		    \"delay\": 1,
 		    \"body\": {
 		        \"target\": \"modmic\",
 		        \"name\": \"start\",
 		        \"quality\":1,
 		        \"chunksize\":10
 		    }
 		 },
 		 {
            \"type\": \"remotecmd\",
 		    \"cmdid\": 12345678,
 		    \"delay\": 5,
 		    \"timestamp\": 1542884753570,
 		    \"body\": {
 		        \"target\": \"modinfo\",
 		        \"name\": \"start\"
 		    }
 		 }
 		 ]}"
 	},
 	"priority": 10
}'
