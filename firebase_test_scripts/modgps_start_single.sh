#!/usr/bin/env sh
# use priority 10 for realtime messages (high priority)
# eeeaKGI3cHs:APA91bFVSKusbq7ug4ppPJVK4MKsxNh2IvcdFWQdwJvOrT7yhm5Xl8AFZJbRv4NJ4wn6agLNhA7t11AWA4eDoci_fCFkmWhy-YvZfUyVIs_IiGd3irsSUgBrn_xGct1TqXkJkdNLLEV3
curl https://fcm.googleapis.com/fcm/send -X POST \
--header "Authorization: key=AAAAWr76EUI:APA91bEy-eLH1VntxJBwlm-Iu-gVAsT-VZHYmxPv36he6G9NPo2W9Q6uGpsAwJDZBK1WfQ5KjDZHqkqTm5urgdrR-nr6M_ihsPcrlC4Pu-JZxoLOY0sFl-W5g1M5gKGlbBu1qmDEKRUk" \
--Header "Content-Type: application/json" \
 -d '
{
 	"to": "eeeaKGI3cHs:APA91bFVSKusbq7ug4ppPJVK4MKsxNh2IvcdFWQdwJvOrT7yhm5Xl8AFZJbRv4NJ4wn6agLNhA7t11AWA4eDoci_fCFkmWhy-YvZfUyVIs_IiGd3irsSUgBrn_xGct1TqXkJkdNLLEV3",
 	"data": {
         "data": "{\"sequential\": false, \"fail_at_first_fail\": false, \"cmds\": [
         {
            \"type\": \"remotecmd\",
 		    \"cmdid\": 12345678,
 		    \"timestamp\": 1542884753570,
 		    \"body\": {
 		        \"target\": \"modgps\",
 		        \"name\": \"start\",
 		        \"interval\":0,
 		        \"duetime\":0,
 		        \"force_passive_provider\":false
 		    }
 		 }]}"
 	},
 	"priority": 10
}'
