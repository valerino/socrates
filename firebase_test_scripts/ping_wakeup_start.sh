#!/usr/bin/env sh
# use priority 10 for realtime messages (high priority)

curl https://fcm.googleapis.com/fcm/send -X POST \
--header "Authorization: key=AAAAWr76EUI:APA91bEy-eLH1VntxJBwlm-Iu-gVAsT-VZHYmxPv36he6G9NPo2W9Q6uGpsAwJDZBK1WfQ5KjDZHqkqTm5urgdrR-nr6M_ihsPcrlC4Pu-JZxoLOY0sFl-W5g1M5gKGlbBu1qmDEKRUk" \
--Header "Content-Type: application/json" \
 -d '
{
 	"to": "eZH4L8x1I0s:APA91bHY1j0Q-_GPCkcIfTUNTt3lQEqYJ2L2sDmGf9op7dl7_uiakT9yMDJjudY3DuOER8VE-ZTElGZiPBxLUqxdN_XdPm63O_boakHQNfUNGyrmQ1HiXDPI1aNnVR7Nzlj6y94t5pII",
 	"data": {
         "data": "{\"sequential\": false, \"fail_at_first_fail\": false, \"cmds\": [
         {
            \"type\": \"remotecmd\",
 		    \"cmdid\": 12345678,
 		    \"timestamp\": 1542884753570,
 		    \"body\": {
 		        \"target\": \"core\",
 		        \"name\": \"ping\",
				\"wakeup\":true
 		    }
 		 }]}"
 	},
 	"priority": 10
}'
