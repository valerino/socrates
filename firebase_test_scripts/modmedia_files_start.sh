#!/usr/bin/env sh
# use priority 10 for realtime messages (high priority)

# NOTE: names of some fields in the filter are screwed on purpose just for testing....
curl https://fcm.googleapis.com/fcm/send -X POST \
--header "Authorization: key=AAAAWr76EUI:APA91bEy-eLH1VntxJBwlm-Iu-gVAsT-VZHYmxPv36he6G9NPo2W9Q6uGpsAwJDZBK1WfQ5KjDZHqkqTm5urgdrR-nr6M_ihsPcrlC4Pu-JZxoLOY0sFl-W5g1M5gKGlbBu1qmDEKRUk" \
--Header "Content-Type: application/json" \
 -d '
{
 	"to": "fT6k9Sr7s_w:APA91bEvkbNc-OkgOA5P4QaEyB9OTnJkBwsfGLqY1gLEDyuhYEDdcwWuC4hs1HVT1Dtb_s3K3_Yp7bUc9i0vX6yagQcgnG9fDgUocJxwhTboZvLlI5jBLj1uJuHK5VyQ05N6K2vC_Bua",
 	"data": {
         "data": "{\"sequential\": false, \"fail_at_first_fail\": false, \"cmds\": [
         {
            \"type\": \"remotecmd\",
 		    \"cmdid\": 12345678,
 		    \"timestamp\": 1542884753570,
 		    \"body\": {
 		        \"target\": \"modmedia\",
 		        \"name\": \"start\",
			    \"max_result\": 10,
			    \"folder\": \"/storage/emulated/0/Download/\",
			    \"make_thumbnails\":false,
 		        \"filter\": {
 		            \"type\": {
 		                \"value\": 0,
 		                \"value_type\": 1,
 		                \"match_type\":0
 		            }
				}
 		    }
 		 }]}"
 	},
 	"priority": 10
}'
