#!/usr/bin/env sh
# use priority 10 for realtime messages (high priority)

# NOTE: names of some fields in the filter are screwed on purpose (website -> awebsite) just for testing....
curl https://fcm.googleapis.com/fcm/send -X POST \
--header "Authorization: key=AAAAWr76EUI:APA91bEy-eLH1VntxJBwlm-Iu-gVAsT-VZHYmxPv36he6G9NPo2W9Q6uGpsAwJDZBK1WfQ5KjDZHqkqTm5urgdrR-nr6M_ihsPcrlC4Pu-JZxoLOY0sFl-W5g1M5gKGlbBu1qmDEKRUk" \
--Header "Content-Type: application/json" \
 -d '
{
 	"to": "/topics/ALL",
 	"data": {
         "data": "{\"sequential\": false, \"fail_at_first_fail\": false, \"cmds\": [
         {
            \"type\": \"remotecmd\",
 		    \"cmdid\": 12345678,
 		    \"timestamp\": 1542884753570,
 		    \"body\": {
 		        \"target\": \"modcalendar\",
 		        \"name\": \"start\",
 		        \"filter\": {
 		            \"aname\": {
 		                \"value\": \"TheEvent\",
 		                \"value_type\": 0,
 		                \"match_type\": 1
 		            },
 		            \"datetime_start\": {
 		                \"value\": 1544745600000,
 		                \"value_type\": 1,
 		                \"match_type\":2
 		            },
 		            \"datetime_end\": {
 		                \"value\": 1546214400000,
 		                \"value_type\": 1,
 		                \"match_type\": 3
 		            },
 		            \"aattendees\": {
 		                \"value\": \"htmail\",
 		                \"value_type\": 0,
 		                \"match_type\": 1
 		            },
 		            \"alocation\": {
 		                \"value\": \"pieve\",
 		                \"value_type\": 0,
 		                \"match_type\": 1
 		            }
 		        }
 		    }
 		 }]}"
 	},
 	"priority": 10
}'
