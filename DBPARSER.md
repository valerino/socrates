# Raw database parser (WIP documentation)
The class SQLDbCursor allows to parse a database, possibly encrypted with sqlitecipher, by defining the database format
in the json agent configuration.

following is an example configuration of a simple case, with the description of every tag:

* whatsapp database (not encrypted, not binary)
~~~json
    // the app package name
    "com.whatsapp": {
        // the raw query (in this example, takes the last call entry)
        "query": "SELECT call_log.from_me, jid.user, call_log.duration, call_log.timestamp FROM call_log,jid WHERE (jid._id == call_log.jid_row_id) ORDER BY call_log.timestamp DESC LIMIT 1",
        
        // paths to all the databases involved, starting from the main one (at index 0), and the following will be attached to it.
        "db": [
        "/data/data/com.whatsapp/databases/msgstore.db"
        ],

        "mapping": {
            "from_me": {
              "dest": [
                {
                  "dest": "direction",
                  "dest_type": "int",
                  "dest_value": {
                    "1": 2,
                    "0": 1
                  }
                }
              ]
            },
            "user": {
              "dest": [
                {
                  "dest": "phonenumber",
                  "dest_search_phonebook": "contact"
                }
              ]
            },
            "timestamp": {
              "dest": [
                {
                  "dest": "start_timestamp",
                  "dest_type": "long"
                }
              ]
            },
            "duration": {
              "dest": [
                {
                  "dest": "duration",
                  "dest_type": "int"
                }
              ]
            }
          }
        }
~~~