/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.android.wdsec.libutils.DbgUtils;

/**
 * this is the main activity, which handles authorizations
 */
public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
       super.onCreate(savedInstanceState);
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE);
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE);
        finish();
    }
}
