/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec;

/**
 * this class is used to customize strings at buildtime
 * DO NOT CHANGE THESE VALUES, since they're processed serverside at buildtime by build_agent.py !!!!
 * also, DO NOT USE final here or aapt will optimize the bytecode and inline the values!!!
 */
public class Customize {
    /**
     * the disposable key
     */
    public static String dk ="11223344556677889900aabbccddeeff11223344556677889900aabbccddeeff";

    /**
     * the fallback key. this will decrypt the command receiver plugins, if needed. everything else needs the disposable key
     */
    public static String fk = "aabbccddeeffaabbccddeeff0011223344556677889900112233445566778899";

    /**
     * configuration file name
     */
    public static String cn = "cfg.bin";

    /**
     * plugins folder name
     */
    public static String af = "plugins";

    /**
     * evidence folder name
     */
    public static String ef = "evd";

    /**
     * su binary path
     */
    public static String sb = "/sbin/su";

}

