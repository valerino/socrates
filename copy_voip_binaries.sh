#!/usr/bin/env sh
# preliminary script to copy voip binaries from hkvoip repository to plugins/modvoip/.../assets
# usage: copy_voip_binaries.sh <path/to/hkvoip_repo>
if [ "$#" -ne 1 ]; then
	echo "usage: $0 <path/to/hkvoip_repo>"
	exit 1
fi

# copy!
rm -rf plugins/modvoip/src/main/assets
mkdir -p plugins/modvoip/src/main/assets
cp $1/android/libs/arm64-v8a/loader plugins/modvoip/src/main/assets/loader
if [ $? -ne 0 ]; then
    exit 1
fi
cp $1/android/libs/arm64-v8a/libhkvoip.so plugins/modvoip/src/main/assets/libhkvoip.so
if [ $? -ne 0 ]; then
    exit 1
fi
cp $1/android/libs/armeabi-v7a/loader plugins/modvoip/src/main/assets/loader32
if [ $? -ne 0 ]; then
    exit 1
fi
cp $1/android/libs/armeabi-v7a/libhkvoip.so plugins/modvoip/src/main/assets/libhkvoip32.so
if [ $? -ne 0 ]; then
    exit 1
fi

echo "voip binaries copied to plugins/modvoip/src/main/assets!"
ls -l plugins/modvoip/src/main/assets
