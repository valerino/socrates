/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec.modcalllog;

import android.app.Activity;
import android.os.Bundle;

/**
 * dummy class, never used
 */
public class MainActivity extends Activity {

    private void test() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                test();
            }
        });
        t.start();
    }
}
