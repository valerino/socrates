/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec.modmic;

import android.app.Activity;
import android.os.Bundle;

/**
 * dummy class, never used
 */
public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
