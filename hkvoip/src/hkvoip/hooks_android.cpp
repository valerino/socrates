#include <libutils.h>
#include <frida-gum.h>
#include "android_audio_internal.h"
#include "android_audio_buffer.h"
#include "hkvoip.h"
#include "hooks_android.h"
#include "encoder.h"

gpointer ptrAudioRecordObtainBuffer = NULL;
typedef status_t (*PTR_AudioRecord_ObtainBuffer)(void* thisPtr, Buffer* audioBuffer, const struct timespec * requested, struct timespec *elapsed, unsigned long *nonContig);
gpointer ptrAudioRecordReleaseBuffer = NULL;
typedef void (*PTR_AudioRecord_ReleaseBuffer)(void* thisPtr, Buffer* audioBuffer);
gpointer ptrAudioTrackReleaseBuffer = NULL;
typedef void (*PTR_AudioTrack_ReleaseBuffer)(void* thisPtr, Buffer* audioBuffer);

/*
 * AudioRecord::obtainBuffer()
 */
status_t hookAudioRecordObtainBuffer(void* thisPtr, Buffer* audioBuffer, const struct timespec * requested, struct timespec *elapsed, unsigned long *nonContig) {
  //LOGD("hook AudioRecord::obtainBuffer()\n");

  // call original
  PTR_AudioRecord_ObtainBuffer p = (PTR_AudioRecord_ObtainBuffer)ptrAudioRecordObtainBuffer;
  status_t res = p(thisPtr, audioBuffer, requested, elapsed, nonContig);
  if (res != EWOULDBLOCK) {
    // log buffer
    audio_collect_data(thisPtr, AUDIO_SOURCE_MICROPHONE, (unsigned char*)audioBuffer->raw, audioBuffer->size);
  }
  return res;
}

/*
 * AudioRecord::releaseBuffer()
 */
void hookAudioRecordReleaseBuffer(void* thisPtr, Buffer* audioBuffer) {
  //LOGD("hook AudioRecord::releaseBuffer()\n");

  // log buffer
  audio_collect_data(thisPtr, AUDIO_SOURCE_MICROPHONE, (unsigned char*)audioBuffer->raw, audioBuffer->size);

  // call original
  PTR_AudioRecord_ReleaseBuffer p = (PTR_AudioRecord_ReleaseBuffer)ptrAudioRecordReleaseBuffer;
  p(thisPtr, audioBuffer);
}

/*
 * AudioTrack::releaseBuffer()
 */
void hookAudioTrackReleaseBuffer(void* thisPtr, Buffer* audioBuffer) {
  //LOGD("hook AudioTrack::releaseBuffer()\n");

  audio_collect_data(thisPtr, AUDIO_SOURCE_SPEAKERS, (unsigned char*)audioBuffer->raw, audioBuffer->size);

  // call original
  PTR_AudioTrack_ReleaseBuffer p = (PTR_AudioTrack_ReleaseBuffer)ptrAudioTrackReleaseBuffer;
  p(thisPtr, audioBuffer);
}

int hooks_install_android(GumInterceptor* interceptor) {
  // get AudioRecord pointers
#if defined(__aarch64__)
  ptrAudioRecordObtainBuffer = (gpointer)gum_module_find_export_by_name(NULL, "_ZN7android11AudioRecord12obtainBufferEPNS0_6BufferEPK8timespecPS3_Pm");
  if (!ptrAudioRecordObtainBuffer) {
    LOGE("Can't find AudioRecord::obtainBuffer()\n");
    return ENOENT;
  }
  LOGI("found AudioRecord::obtainBuffer() at %p", ptrAudioRecordObtainBuffer);
#else
  ptrAudioRecordReleaseBuffer = (gpointer)gum_module_find_export_by_name(NULL, "_ZN7android11AudioRecord13releaseBufferEPNS0_6BufferE");
  if (!ptrAudioRecordReleaseBuffer) {
    // try this, for different android versions
    ptrAudioRecordReleaseBuffer = (gpointer)gum_module_find_export_by_name(NULL, "_ZN7android11AudioRecord13releaseBufferEPKNS0_6BufferE");
    if (!ptrAudioRecordReleaseBuffer) {
      LOGE("Can't find AudioRecord::releaseBuffer()\n");
      return ENOENT;
    }
  }
  LOGI("found AudioRecord::releaseBuffer() at %p", ptrAudioRecordReleaseBuffer);
#endif

  // get AudioTrack pointers
  ptrAudioTrackReleaseBuffer = (gpointer)gum_module_find_export_by_name(NULL, "_ZN7android10AudioTrack13releaseBufferEPNS0_6BufferE");
  if (!ptrAudioTrackReleaseBuffer) {
    // try this, for different android versions
    ptrAudioTrackReleaseBuffer = (gpointer)gum_module_find_export_by_name(NULL, "_ZN7android10AudioTrack13releaseBufferEPKNS0_6BufferE");
    if (!ptrAudioTrackReleaseBuffer) {
      LOGE("Can't find AudioTrack::releaseBuffer()\n");
      return ENOENT;
    }
  }
  LOGI("found AudioTrack::releaseBuffer() at %p", ptrAudioTrackReleaseBuffer);

  // hook AudioRecord
#if defined(__aarch64__)
  GumReplaceReturn res = gum_interceptor_replace_function(interceptor, ptrAudioRecordObtainBuffer, (gpointer)hookAudioRecordObtainBuffer, NULL);
#else
  GumReplaceReturn res = gum_interceptor_replace_function(interceptor, ptrAudioRecordReleaseBuffer, (gpointer)hookAudioRecordReleaseBuffer, NULL);
#endif
  if (res != GUM_REPLACE_OK) {
    LOGE("Can't hook AudioRecord, res=%d\n", res);
    return res;
  }

  // hook AudioTrack
  res =  gum_interceptor_replace_function(interceptor, ptrAudioTrackReleaseBuffer, (gpointer)hookAudioTrackReleaseBuffer, NULL);
  if (res != GUM_REPLACE_OK) {
    LOGE("Can't hook AudioTrack, res=%d\n", res);
#if defined(__aarch64__)
    gum_interceptor_revert_function(interceptor, ptrAudioRecordObtainBuffer);
#else
    gum_interceptor_revert_function(interceptor, ptrAudioRecordReleaseBuffer);
#endif
    return res;
  }

  // done
  LOGI("AudioTrack and AudioRecord paths correctly hooked!\n");
  return 0;
}

