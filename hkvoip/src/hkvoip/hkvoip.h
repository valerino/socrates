#ifndef __HKVOIP_H__
#define __HKVOIP_H__

// sets the encoder with quality 5, vbr off, complexity 3
#define AUDIO_ENCODER_DEFAULT "dummy"

#ifdef __cplusplus
extern "C" {
#endif
  /**
   * @brief called at each AudioRecord/AudioTrack releaseBuffer() and such, with pointer to the audio data
   *
   * @param thisPtr 'this' instance from AudioRecord/AudioTrack methods
   * @param path path to the folder where to store the collected data
   * @param type AUDIO_SOURCE_MICROPHONE or AUDIO_SOURCE_SPEAKERS
   * @param buffer raw buffer
   * @param size size of the raw buffer
   */
  void audio_collect_data(void* thisPtr, int type, unsigned char* buffer, size_t size);

  /**
   * @brief initializes the audio data collector
   *
   * @param path path to the folder where to store the collected data
   * @param options a string like "q=5,v=0,c=3,s=15" (quality 0-10, vbr=0|1, complexity 1-10, splitseconds 15-300) to configure the encoder, or AUDIO_ENCODER_DEFAULT
   */
  void audio_capture_initialize(const char* path, const char* options);
#ifdef __cplusplus
}
#endif

#endif
