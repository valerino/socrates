#ifndef __HOOKS_ANDROID_H__
#define __HOOKS_ANDROID_H__

#ifdef __cplusplus
extern "C" {
#endif
  /**
   * @brief install hooks on the Android audio APIs
   *
   * @param interceptor an instance of frida's GumInterceptor
   * @return 0 on success
   */
  int hooks_install_android(GumInterceptor* interceptor);

#ifdef __cplusplus
}
#endif

#endif


