#include <libutils.h>
#include <frida-gum.h>
#include "hooks.h"
#include "hkvoip.h"

void __attribute__((visibility("default"))) run(const gchar * data, gboolean * stay_resident) {

  // initialize gum
  *stay_resident = TRUE;
  gum_init_embedded ();
  GumInterceptor * interceptor = gum_interceptor_obtain ();

  // install hooks
  LOGD("run() called, library injected into pid %d", getpid());
  int res = hooks_install(interceptor);
  if (res == 0) {
    // initializing audio collection
#ifdef __ANDROID__
    char tmp[] = "/data/local/tmp";
#else
    char tmp[] = "/tmp";
#endif
    audio_capture_initialize(tmp, (const char*)data);
  }

  gum_interceptor_flush(interceptor);
  g_object_unref (interceptor);
}

