#include <libutils.h>
#include <frida-gum.h>

#ifdef __ANDROID__
#include "hooks_android.h"
#endif

int hooks_install(GumInterceptor* interceptor) {
  if (!interceptor) {
    return EINVAL;
  }
  gum_interceptor_begin_transaction (interceptor);
#ifdef __ANDROID__
  int res = hooks_install_android(interceptor);
#endif
  gum_interceptor_end_transaction (interceptor);
  return res;
}

