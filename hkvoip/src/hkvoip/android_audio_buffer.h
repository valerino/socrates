/**
 * the following is from android audiosystem sources, AudioRecord/AudioTrack.h
 */

#ifndef __ANDROID_AUDIOBUFFER__
#define __ANDROID_AUDIOBUFFER__

class Buffer {
				public:
								// FIXME use m prefix
								size_t frameCount;  // number of sample frames corresponding to size;
								// on input it is the number of frames available,
								// on output is the number of frames actually drained
								// (currently ignored but will make the primary field in future)
								size_t size;        // input/output in bytes == frameCount * frameSize
								// on output is the number of bytes actually drained
								// FIXME this is redundant with respect to frameCount,
								// and TRANSFER_OBTAIN mode is broken for 8-bit data
								// since we don't define the frame format
								union {
												void* raw;
												short* i16;  // signed 16-bit
												int8_t* i8;  // unsigned 8-bit, offset by 0x80
								};
};

#endif
