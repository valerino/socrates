#ifndef __encoder_h__
#define __encoder_h__

#define AUDIO_SOURCE_MICROPHONE 1 // mic
#define AUDIO_SOURCE_SPEAKERS 2   // speakers

/*
 * @brief describes an audio chunk file
 */
struct audio_chunk_context {
    LIST_ENTRY chain;          // internal
    double seconds_from_start; // tracks how many seconds from the start of the
                               // call
    int num;                   // this chunk number
    time_t session;            // unique call/session id
    int type;                  // AUDIO_SOURCE_MICROPHONE, AUDIO_SOURCE_SPEAKERS
    bool last;                 // is this the last audio chunk for the call ?
    char raw_path[260];        // path to raw audio file (input)
    char encoded_path[260];    // path to the encoded audio file (output)
    char encoded_path_alt[260]; // alternative path to the encoded audio file
                                // (output)
    char base_path[260];        // base path for the collected data
    size_t raw_written;         // raw bytes written to path
    FILE *f;                    // stream backing raw file at path
    uint32_t format;            // raw audio format
    uint32_t sample_rate;       // raw audio sample rate
    uint32_t channels;          // raw audio channels #
    time_t timestamp;           // this chunk timestamp (seconds from 1/1/1970)
    int vbr;                    // vbr is enabled ? default 0
    int quality;       // audio quality for speex encoding (0-10), default 5
    int complexity;    // encoder complexity (1-10), default is 3
    bool use_alt_path; // this chunk is saved in the alternative path
};

/*
 * @brief encode a raw audio file identified by the chunk context
 * @param ctx pointer to an audio_chunk_context referring to a raw mic/spk file
 *
 * @return bool
 */
bool encode_file(struct audio_chunk_context *ctx);
#endif
