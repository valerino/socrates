#include <dlfcn.h>
#include <frida-core.h>
#include <libutils.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

/**
 * @brief purge unused files in temporary folder (TODO: try to avoid creation,
 * probably there's a frida api about it...)
 */
void purge_frida_tmp() {
#ifdef __ANDROID__
    char tmp[] = "/data/local/tmp";
#else
    char tmp[] = "/tmp";
#endif

    // locate any frida folder in tmp
    DIR *dir;
    struct dirent *entry;
    dir = opendir(tmp);
    if (dir == NULL) {
        return;
    }

    int count = 0;
    while ((entry = readdir(dir)) != NULL) {
        if (strstr(entry->d_name, "frida")) {
            // found a frida folder
            char full_path[260];
            sprintf(full_path, "%s/%s", tmp, entry->d_name);
            LOGI("purging frida folder: %s\n", full_path);
            file_recursive_delete(full_path);
        }
    }
    closedir(dir);
}

/*
 * @brief inject into the given process (internal)
 *
 * @param s process name to inject into
 * @param argc from main
 * @param argv from main
 * argv layout: argv[0] <process|process_csv>
 * </path/to/64bit_so[,/path/to/32bit_so]>
 * [q(uality)=0-10,v(br)=0|1,c(omplexity)=1-10]);
 * @return 0 on success
 */
int inject_internal(char *s, int argc, char **argv) {
    pid_t pids[128] = {0};
    int num_pids = 0;
    int res = process_get_pid(s, pids, 128, &num_pids);
    if (res != 0) {
        return res;
    }

    // get the paths from argv[2]
    char to_inject_path_32[1024] = {0};
#if defined __aarch64__ || defined __x86_64__
    char to_inject_path_64[1024] = {0};
    strncpy(to_inject_path_64, argv[2], sizeof(to_inject_path_64));
    char *comma = strstr(to_inject_path_64, ",");
    if (comma) {
        // copy path 32bit
        *comma = '\0';
        comma++;
        strncpy(to_inject_path_32, comma, sizeof(to_inject_path_32) - 1);
    }
#else
    // argv[2] must be a single path
    strncpy(to_inject_path_32, argv[2], sizeof(to_inject_path_32) - 1);
    if (strstr(to_inject_path_32, ",")) {
        // not supported
        LOGE("argv[2] must be a single path in 32bit build.\n");
        return EPERM;
    }
#endif

    // there may be more than a process in argv[1]
    for (int i = 0; i < num_pids; i++) {
        bool is_64bit = false;
        char *to_inject = to_inject_path_32;
#if defined __aarch64__ || defined __x86_64__
        // on 64bit, we run the 64bit injector and we may need to inject 32bit
        // apps with the 32bit .so
        is_64bit = process_is_64bit(pids[i]);
        if (!is_64bit) {
            // we need to use the 32bit .so
            if (strlen(to_inject_path_32) == 0) {
                // 32bit .so not provided
                LOGE("can't inject in 32bit process %s, pid=%d,  32bit dll "
                     "must be provided in argv[2] "
                     "(/path/to/64bit_so,/path/to/32bit_so)\n",
                     s, pids[i]);
                continue;
            }
            to_inject = to_inject_path_32;
        } else {
            // 64bit process
            to_inject = to_inject_path_64;
        }
#endif

        // check if it's already mapped first...
        if (process_find_module(pids[i], to_inject, NULL, NULL, NULL, 0) == 0) {
            // process is already mapped
            LOGW(
                "%s already mapped in process %s, pid=%d, skipping injection\n",
                to_inject, s, pids[i]);
            continue;
        }

        // check if the file exists first, or frida will crash the application
        // during injection!
        if (access(to_inject, F_OK) != -1) {
            // inject
            GError *err = NULL;
            LOGI("injecting %s into process %s, pid=%d, 64bit=%d", to_inject, s,
                 pids[i], is_64bit);
            FridaInjector *injector = frida_injector_new_inprocess();
            guint id = frida_injector_inject_library_file_sync(
                injector, pids[i], to_inject, "run",
                argc == 4 ? argv[3] : "dummy", &err);
            if (err != NULL) {
                LOGE("error injection into process %s, pid=%d: %s", s, pids[i],
                     err->message);
                g_error_free(err);
            } else {
                frida_injector_close_sync(injector);
            }
            g_object_unref(injector);

            // do a purge run...
            purge_frida_tmp();
        } else {
            LOGE("error, can't find library to be injected: %s\n", to_inject);
        }
    }
    return 0;
}

/*
 * @brief inject into the given processes at argv[1]
 *
 * @param argc from main
 * @param argv from main
 * argv layout: argv[0] <process|process_csv>
 * </path/to/64bit_so[,/path/to/32bit_so]> <receiver>
 * [q(uality)=0-10,v(br)=0|1,c(omplexity)=1-10]);
 */
void inject(int argc, char **argv) {
    // get the process csv
    char processes[1024] = {0};
    strncpy(processes, argv[1], sizeof(processes) - 1);

    char *s = strtok(processes, ",");
    if (s == NULL) {
        // single process
        inject_internal(processes, argc, argv);
        return;
    }

    // loop injecting into processes
    while (s != NULL) {
        // inject into this process
        inject_internal(s, argc, argv);
        // next
        s = strtok(NULL, ",");
    }
}

int main(int argc, char **argv) {
    if (argc < 3) {
#ifndef NDEBUG
        printf("usage: %s <process|process_csv> "
               "</path/to/64bit_so[,/path/to/32bit_so]>"
               "[q(uality)=0-10,v(br)=0|1,c(omplexity)=1-10,s(econds)=15-300,ss(creenshot)=0|1,r(eceiver)=Android BroadcastReceiver name,b(su binary)=/path/to/android/su]\n",
               argv[0]);
#endif
        return 1;
    }

    // init frida
    frida_init();

    while (1) {
        // loop
        inject(argc, argv);

        // wait 10 seconds
        sleep(10);
    }

    // never reached directly ....
    frida_deinit();
    return 0;
}
