#ifndef __log_api_h__
#define __log_api_h__

#define LOG_TAG "SECDBG"

#ifdef NDEBUG
#define LOGE(...)
#define LOGW(...)
#define LOGD(...)
#define LOGI(...)
#else
#ifdef __ANDROID__
#include <android/log.h>
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)
#define LOGW(...) __android_log_print(ANDROID_LOG_WARN, LOG_TAG, __VA_ARGS__)
#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__)
#define LOGI(...) __android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__)
#endif // __ANDROID__
#endif // NDEBUG

#endif
