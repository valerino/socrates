#ifndef __fileapi_h__
#define __fileapi_h__

/*
 * @brief get file size from file pointer
 *
 * @param f the file
 * @param size on successful return, file size
 * @return 0 on success
 */
int file_get_size_fp(FILE* f, size_t* size);

/*
 * @brief get file size
 *
 * @param path the file
 * @param size on successful return, file size
 * @return 0 on success
 */
int file_get_size(const char* path, size_t* size);

/**
 * @brief recursive delete a folder
 *
 * @param dir path to the directory to be deleted
 * @return 0 on success
 */
int file_recursive_delete(const char *dir);

#endif
