#ifndef __process_api_h__
#define __process_api_h__

/**
 * @brief execute process
 * @param argv commandline as array of strings
 * @param wait true to wait for completion
 * @return 0 on success
 */
int process_exec(const char **argv, bool wait);

/**
 * @brief get process pid/s by name
 *
 * @param process_name name of the process to get pid of
 * @param pids on successful return, array of pids returned (may be more than
 * one pid with the same name!)
 * @param pids_size max pid_t in the pids array
 * @param on successful return, number of returned pids in the pids array
 * @return 0 on success
 */
int process_get_pid(const char *process_name, pid_t *pids, int pids_size,
                    int *num_pids);

/**
 * @brief get process name by pid
 *
 * @param pid pid of the process to get the name of
 * @param process on successful return, the process name
 * @param process_size size of the process buffer
 * @return 0 on success
 */
int process_get_name(pid_t pid, char *process, int process_size);

/*
 * @brief checks if the specified process is 64bit
 *
 * @param pid the process id
 * @return bool
 */
bool process_is_64bit(pid_t pid);

/**
 * @brief check if a module is mapped into the process space
 *
 * @param pid process to check into
 * @param module the module name (i.e. something.so)
 * @param start if not NULL, on successful return is the start address of the
 * module in the pid process space
 * @param end if not NULL, on successful return is the end address of the module
 * in the pid process space
 * @param path if not NULL, on successful return is filled with the module path
 * @param path_size size of the path buffer
 * @return 0 on success
 */
int process_find_module(pid_t pid, const char *module, uintptr_t *start,
                        uintptr_t *end, char *path, int path_size);
#endif
