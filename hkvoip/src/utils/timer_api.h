#ifndef __TIMER_API__
#define __TIMER_API__

#ifndef __linux__
typedef uintptr_t timer_t;
#endif

typedef void (*timer_callback)(void* data);

/**
 * @brief create a timer which ticks at the desired interval
 *
 * @param initial_expiration interval before initial timer activation, in seconds
 * @param interval timer interval between ticks, in seconds (0 for one-shot timer)
 * @param callback pointer to a callback to be called at each tick
 * @param callback_data optional
 * @param id on succesful return, the timer id
 * @TODO: Darwin
 * @return 0 on success
 */
int timer_initialize(int delay, int period, timer_callback callback, void* callback_data, timer_t* id);

/**
 * @brief destroy a timer
 *
 * @param timer a timer created by timer_create()
 * @TODO: Darwin
 * @return 0 on success
 */
int timer_destroy(timer_t timer);

#endif
