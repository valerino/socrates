#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>
#include <time.h>
#include "timer_api.h"
#ifdef __linux__
#include <signal.h>
#endif

int timer_initialize(int initial_expiration, int interval, timer_callback callback, void* callback_data, timer_t* id) {
  if (!callback || !id) {
    return EINVAL;
  }

  *id = 0;
#ifdef __linux__
  // create the timer
  struct sigevent se = {0};
  se.sigev_notify = SIGEV_THREAD;
  se.sigev_notify_function = (void*)callback;
  se.sigev_value.sival_ptr=callback_data;
  timer_t timer = 0;
  int res = timer_create(CLOCK_REALTIME, &se, &timer);
  if (res != 0) {
    res=errno;
    return res;
  }

  // setup and set timer
  struct itimerspec ts = {0};
  // initial expiration
  ts.it_value.tv_sec = abs(initial_expiration);
  ts.it_value.tv_nsec = (initial_expiration - abs(initial_expiration)) * 1e09;

  if (interval) {
    // tick interval
    ts.it_interval.tv_sec = abs(interval);
    ts.it_interval.tv_nsec = (interval - abs(interval)) * 1e09;
  }
  res = timer_settime(timer, CLOCK_REALTIME, &ts, NULL);
  if (res != 0) {
    res = errno;
    timer_destroy(timer);
    return res;
  }

  // done
  *id = timer;
#endif // __linux__
  return 0;
}

int timer_destroy(timer_t timer) {
  if (timer) {
#ifdef __linux__
    return timer_delete(timer);
#endif
  }
  return EINVAL;
}



