#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fts.h>
#include <fcntl.h>

int file_get_size_fp(FILE *f, size_t *size) {
    if (!size || !f) {
        return EINVAL;
    }
    *size = 0;
    struct stat st;
    if (fstat(fileno(f), &st) != 0) {
        return errno;
    }
    *size = st.st_size;
    return 0;
}

int file_get_size(const char *path, size_t *size) {
    if (!size || !path) {
        return EINVAL;
    }

    *size = 0;
    FILE *f = fopen(path, "rb");
    if (!f) {
        return errno;
    }
    int err = file_get_size_fp(f, size);
    fclose(f);
    if (err != 0) {
        return err;
    }
    return 0;
}

int file_recursive_delete(const char *dir) {
    int ret = 0;
    FTS *ftsp = NULL;
    FTSENT *curr;
    char *files[] = {(char *)dir, NULL};

    ftsp = fts_open(files, FTS_NOCHDIR | FTS_PHYSICAL | FTS_XDEV, NULL);
    if (!ftsp) {
        ret = errno;
        goto finish;
    }

    while ((curr = fts_read(ftsp))) {
        switch (curr->fts_info) {
        case FTS_NS:
        case FTS_DNR:
        case FTS_ERR:
            break;

        case FTS_DC:
        case FTS_DOT:
        case FTS_NSOK:
            break;

        case FTS_D:
            break;

        case FTS_DP:
        case FTS_F:
        case FTS_SL:
        case FTS_SLNONE:
        case FTS_DEFAULT:
            if (remove(curr->fts_accpath) < 0) {
                ret = errno;
            }
            break;
        }
    }

finish:
    if (ftsp) {
        fts_close(ftsp);
    }

    return ret;
}
