# VOIP hooking framework
* generic VOIP hooking framework
* modular and designed to be multiplatform (just add another *hooks_<platform>.cpp* with the hook definitions)
* based on Frida, implemented using a native agent (*no javascript!*)
* (c) wolf/ht, 2018

[Android version](./android/README.md)

iOS version (TBD, refer to *freeda* repo for js implementation, trivial to port)

OSX version (TBD, refer to *freeda* repo for js implementation, trivial to port)

Linux version (TBD)

Windows version (TBD)

