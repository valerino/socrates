#!/usr/bin/env sh

__LOADER=loader
__LIBHKVOIP=libhkvoip.so
__LIBHKVOIP32=libhkvoip32.so

# copy to device
adb push ./libs/armeabi-v7a/$__LOADER /data/local/tmp/$__LOADER32
adb push ./libs/armeabi-v7a/$__LIBHKVOIP /data/local/tmp/$__LIBHKVOIP32
adb push ./libs/arm64-v8a/$__LOADER /data/local/tmp/$__LOADER
adb push ./libs/arm64-v8a/$__LIBHKVOIP /data/local/tmp/$__LIBHKVOIP

adb shell chmod 777 /data/local/tmp
adb shell chmod 777 /data/local/tmp/$__LOADER
adb shell chmod 777 /data/local/tmp/$__LOADER32
adb shell chmod 777 /data/local/tmp/$__LIBHKVOIP
adb shell chmod 777 /data/local/tmp/$__LIBHKVOIP32


