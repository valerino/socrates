#!/usr/bin/env sh

__LOADER=loader
__LIBHKVOIP=libhkvoip.so
__LIBHKVOIP32=libhkvoip32.so

if [ ! -d "./devkit-android-arm64" ]; then
    # download frida devkits for arm and arm64
    cd ..
    ./download_frida.sh android-arm,android-arm64 12.6.10 ./android
    cd android
fi

# build
__CWD=$(pwd)
cd jni
ndk-build $1 $2 $3 $4 $5 $6 $7 $8 $9
[ $? -eq 0 ] || exit $?;
if [ "$1" == "clean" ]; then
	exit 0
fi
cd $__CWD
