#!/usr/bin/env sh
if [ "$1" == "" ]; then
	echo "usage: $0 <frida_root> <--clean>"
	exit 1
fi

if [ "$ANDROID_NDK_ROOT" == "" ]; then
	echo "ANDROID_NDK_ROOT must be set!"
	exit 1
fi

__CWD=$(pwd)
if [ "$2" == "--clean" ]; then
	echo ". cleaning devkit"
	rm -rf ./devkit-android-arm64
	rm -rf ./devkit-android-arm
	echo ". cleaning frida"
	cd $1
	make clean
	git reset --hard --recurse-submodules
	cd $__CWD
fi

echo ". replacing config.mk"
cp $__CWD/frida_android_config.mk $1/config.mk
cp $__CWD/capstone_android_config.mk $1/capstone/config.mk

echo ". building frida core"
cd $1
make core-android
if [ ! $? -eq 0 ]; then
    exit 1
fi

echo ". assembling frida libs (arm64)"
mkdir -p $__CWD/devkit-android-arm64/lib
mkdir -p $__CWD/devkit-android-arm64/include
./releng/devkit.py frida-core android-arm64 ./build/frida-android-arm64
./releng/devkit.py frida-gum android-arm64 ./build/frida-android-arm64
cp ./build/frida-android-arm64/libfrida-core.a $__CWD/devkit-android-arm64/lib/
cp ./build/frida-android-arm64/libfrida-gum.a $__CWD/devkit-android-arm64/lib/
cp ./build/frida-android-arm64/frida-core.h $__CWD/devkit-android-arm64/include
cp ./build/frida-android-arm64/frida-gum.h $__CWD/devkit-android-arm64/include

echo ". assembling frida libs (arm)"
mkdir -p $__CWD/devkit-android-arm/lib
mkdir -p $__CWD/devkit-android-arm/include
./releng/devkit.py frida-core android-arm ./build/frida-android-arm
./releng/devkit.py frida-gum android-arm ./build/frida-android-arm
cp ./build/frida-android-arm/libfrida-core.a $__CWD/devkit-android-arm/lib/
cp ./build/frida-android-arm/libfrida-gum.a $__CWD/devkit-android-arm/lib/
cp ./build/frida-android-arm/frida-core.h $__CWD/devkit-android-arm/include
cp ./build/frida-android-arm/frida-gum.h $__CWD/devkit-android-arm/include

cd $__CWD

