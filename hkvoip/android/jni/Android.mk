LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
FRIDA_LIB_PATH := ../devkit-android-$(TARGET_ARCH)/lib
FRIDA_INCLUDE_PATH := ../devkit-android-$(TARGET_ARCH)/include
UTILS_INCLUDE_PATH := ../../src/utils

# prebuilt frida core
LOCAL_MODULE := frida-core
LOCAL_C_INCLUDES :=$(LOCAL_PATH)/$(FRIDA_INCLUDE_PATH)
LOCAL_SRC_FILES := $(LOCAL_PATH)/$(FRIDA_LIB_PATH)/libfrida-core.a
include $(PREBUILT_STATIC_LIBRARY)

# prebuilt frida gum
include $(CLEAR_VARS)
LOCAL_MODULE := frida-gum
LOCAL_C_INCLUDES :=$(LOCAL_PATH)/$(FRIDA_INCLUDE_PATH)
LOCAL_SRC_FILES := $(LOCAL_PATH)/$(FRIDA_LIB_PATH)/libfrida-gum.a
include $(PREBUILT_STATIC_LIBRARY)

# utility api
LOCAL_PATH := ../../src/utils
include $(CLEAR_VARS)
LOCAL_MODULE := utils
LOCAL_C_INCLUDES := $(LOCAL_PATH)
LOCAL_SRC_FILES := file_api.c \
	process_api.c \
	timer_api.c 
LOCAL_CFLAGS += -Os -s -fvisibility=hidden -fvisibility-inlines-hidden
LOCAL_CPPFLAGS += $(LOCAL_CFLAGS)
include $(BUILD_STATIC_LIBRARY)

# loader
LOCAL_PATH := ../../src/loader
include $(CLEAR_VARS)
LOCAL_MODULE := loader
LOCAL_C_INCLUDES := $(LOCAL_PATH) \
	$(FRIDA_INCLUDE_PATH) \
	$(UTILS_INCLUDE_PATH) 
LOCAL_STATIC_LIBRARIES := frida-core \
	utils 
LOCAL_SRC_FILES := loader.c
LOCAL_CFLAGS += -Os -s -fvisibility=hidden -fvisibility-inlines-hidden
LOCAL_CPPFLAGS += $(LOCAL_CFLAGS)
LOCAL_LDLIBS:= -llog
include $(BUILD_EXECUTABLE)

# voip hook so
LOCAL_PATH := ../../src/hkvoip
include $(CLEAR_VARS)
LOCAL_MODULE := hkvoip
LOCAL_C_INCLUDES := $(LOCAL_PATH) \
	$(FRIDA_INCLUDE_PATH) \
	$(UTILS_INCLUDE_PATH) 
LOCAL_STATIC_LIBRARIES := frida-gum \
	utils
LOCAL_SRC_FILES := hkvoip.c \
	encoder.c \
	main.c \
	hooks.c \
	hooks_android.cpp

LOCAL_C_INCLUDES += $(LOCAL_PATH)/ogg/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/speex/include
LOCAL_SRC_FILES += $(wildcard $(LOCAL_PATH)/ogg/*.c)
LOCAL_SRC_FILES += $(wildcard $(LOCAL_PATH)/speex/*.c)
LOCAL_CFLAGS += -Os -s -DFIXED_POINT -DUSE_KISS_FFT -DEXPORT="" -UHAVE_CONFIG_H -fvisibility=hidden -fvisibility-inlines-hidden
LOCAL_CPPFLAGS += $(LOCAL_CFLAGS)
LOCAL_LDLIBS := -llog
include $(BUILD_SHARED_LIBRARY)
