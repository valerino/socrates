## Docker Image Builder for building Android implant build Environment

This project is used to build a docker image with all the necessary tools and library dependencies needed to
fully build and android core.

## Requirements

- Docker
- wget
- Internet connection

## Motivation

Create a docker image in order to standardize the build environmet.

## Installation

Cd into the build directory and run the ./prepareEnvWithInternet.sh script.

If rcs build is needed call the script ./prepareEnvWithInternetRcs.sh

If correctly executed, the script would produce an image named : android-build:<version>.tar within the main project directory.
After that, is possible to load the produced image on the build server with the fllowing command:
docker load < <dockerImage>

To check the image avalaibility run:
docker images

Once present the image can be used with docker:
docker run --tty --interactive  -v /dev/urandom:/dev/random  -v=$(pwd)/:/home android-build:<version>

the -v map a volume :
$(pwd)/:/home this maps the directory where the command has been executed into /home
/dev/urandom:/dev/random this map host machine /dev/urandom in the docker run /dev/random
The last mapping directive is important if you need to generate random number within docker.


## NOTES

Remember to **change the version** inside the build script
build/prepareEnvWithInternet.sh when modification are committed.


##### Updating an image without rebuild it:
1) Run a docker image without the option -rm , which will remove the container when exiting
docker run -i -t  -v=$(pwd)/:/home -v /dev/urandom:/dev/random android-build:0.0.0-alpha

2) Inside the container make the modifications you need and then exit

3) Identify via docker ps output the sha of the container
docker ps -a
CONTAINER ID        IMAGE                       COMMAND             CREATED             STATUS              PORTS               NAMES
ac6bce6c2cd6        microandroid:0.0.0   "/bin/bash"         About a minute ago   Exited (127) 1 seconds ago                       amazing_volhard

4) merge the changes in the image by doing a commit:
docker commit ac6bce6c2cd6 microandroid:0.0.0

5) rm the container
docker rm ac6bce6c2cd6

6) eventually save the update image to a tar ready to be installed:
docker image save -o image.tar microandroid:0.0.0