#!/usr/bin/env sh
# encrypt with deflate/AES, decrypt with AES/inflate
# microandroid test tool

if [ $# != 3 ]; then
	# in=input file to be encrypted
	# out=encrypted output path
	echo "usage: $0 <in> <out> <--decrypt|--encrypt>"
	exit 1
fi

# the default key/iv
KEY=11223344556677889900aabbccddeeff11223344556677889900aabbccddeeff
IV=11223344556677889900aabbccddeeff
MODE=encrypt
if [ "$3" = "--decrypt" ]; then
	MODE=decrypt
fi
backendtools/mrcs_tool.py --infile $1 --outfile $2 --key $KEY --iv $IV --mode $MODE
