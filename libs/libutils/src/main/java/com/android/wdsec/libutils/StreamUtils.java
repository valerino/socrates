/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec.libutils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;

/**
 * StreamUtils contains stream utilities
 */
public class StreamUtils {

    /**
     * shortcut to close the specified closeable without throwing exception
     * @param c the stream
     */
    public static void close(Closeable c) {
        if (c == null) {
            return;
        }
        try {
            c.close();
        } catch (IOException e) {
        }
    }

    /**
     * copy input to output stream
     * @param src stream with content
     * @param dst the destination stream
     * @throws IOException
     */
    static void toOutputStream(InputStream src, OutputStream dst) throws IOException {
        while (true) {
            byte[] buf = new byte[1024];
            int len = src.read(buf);
            if (len < 0) {
                break;
            }
            dst.write(buf, 0, len);
        }
    }

    /**
     * read stream to bytes
     * @param src stream with content
     * @throws IOException
     */
    public static byte[] toBytes(InputStream src) throws IOException {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        toOutputStream(src,os);
        os.close();
        return os.toByteArray();
    }

    /**
     * read stream to bytes
     * @param path path to file to be read
     * @throws IOException
     */
    public static byte[] toBytes(final String path) throws IOException {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        FileInputStream is = new FileInputStream(path);
        toOutputStream(is,os);
        os.close();
        is.close();
        return os.toByteArray();
    }

    /**
     * copy stream content to file (will be created if not existent, or overwritten)
     * @param src stream with content
     * @param dst the destination path
     * @throws IOException
     */
    public static void toFile(InputStream src, String dst) throws IOException {
        FileOutputStream out = new FileOutputStream(dst);
        toOutputStream(src, out);
        out.close();
    }

    /**
     * copy byte array to file (will be created if not existent, or overwritten)
     * @param src a byte array
     * @param dst the destination path
     * @param append true for appending, false to overwrite
     * @throws IOException
     */
    private static void toFile(final byte[] src, String dst, boolean append) throws IOException {
        FileOutputStream out = new FileOutputStream(dst, append);
        ByteArrayInputStream bis = new ByteArrayInputStream(src);
        toOutputStream(bis, out);
        out.close();
        bis.close();
    }

    /**
     * copy byte array to file (will be created if not existent, or overwritten)
     * @param src a byte array
     * @param dst the destination path
     * @throws IOException
     */
    public static void toFile(final byte[] src, String dst) throws IOException {
        toFile(src,dst,false);
    }

    /**
     * decompress a byte array using Inflate
     * @param data data compressed with Deflate
     * @return
     * @throws IOException
     * @throws DataFormatException
     */
    public static byte[] decompress(byte[] data) throws IOException, DataFormatException {
        Inflater inflater = new Inflater();
        inflater.setInput(data);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
        byte[] buffer = new byte[1024];
        while (!inflater.finished()) {
            int count = inflater.inflate(buffer);
            outputStream.write(buffer, 0, count);
        }
        outputStream.close();
        byte[] output = outputStream.toByteArray();
        return output;
    }

    /**
     * copy byte array to file (appending if existent)
     * @param src a byte array
     * @param dst the destination path
     * @throws IOException
     */
    public static void toFileAppend(final byte[] src, String dst) throws IOException {
        toFile(src,dst,true);
    }

    /**
     * copy stream content to file (will be created if not existent, or overwritten)
     * @param src stream with content
     * @param dst the destination File
     * @throws IOException
     */
    public static void toFile(InputStream src, File dst) throws IOException {
        toFile(src, dst.getAbsolutePath());
    }

    /**
     * copy file (will be created if not existent, or overwritten)
     * @param src the source File
     * @param dst the destination File
     * @throws IOException
     */
    public static void copy(File src, File dst) throws IOException {
        InputStream is = new FileInputStream(src);
        toFile(is, dst.getAbsolutePath());
    }

    /**
     * copy file (will be created if not existent, or overwritten)
     * @param src the source path
     * @param dst the destination path
     * @throws IOException
     */
    public static void copy(String src, String dst) throws IOException {
        InputStream is = new FileInputStream(src);
        toFile(is, dst);
    }
}
