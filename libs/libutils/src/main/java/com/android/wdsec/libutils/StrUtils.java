/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec.libutils;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * StrUtils contains string utilities
 */
public class StrUtils {
    /**
     * gets String message from an Exception/Throwable, including stack trace
     * @param ex the exception
     * @return
     */
    static String strFromExceptionWithStackTrace(Throwable ex) {
        // get the message
        final ByteArrayOutputStream bos = new ByteArrayOutputStream();
        final PrintWriter pw = new PrintWriter(bos);
        pw.print(ex.getMessage());

        // and the stack trace
        pw.print("\n");
        ex.printStackTrace(pw);
        pw.close();

        // to string
        final String s = bos.toString();
        StreamUtils.close(bos);
        return s;
    }

    /**
     * byte array to hex string  (0x00\0xab\0xcd\0xef -> "00abcdef")
     * @param b a byte array
     * @return
     */
    public static String strFromHex (byte[] b) {
        return strFromHex(b, '\0');
    }

    /**
     * byte array to hex string  (0x00\0xab\0xcd\0xef -> "00abcdef")
     * @param b a byte array
     * @param separator a separator between each byte, or 0
     * @return
     */
    public static String strFromHex (byte[] b, char separator) {
        int l = b.length;
        final StringBuilder s = new StringBuilder();
        for (int i=0; i < l; i++) {
            s.append(String.format("%02x", b[i]&0xff));
            if (separator != '\0' && i != (l-1)) {
                s.append(separator);
            }
        }
        return s.toString();
    }

    /**
     * hex string to byte array ("00abcdef" -> 0x00\0xab\0xcd\0xef)
     * @param str the hex string
     * @return
     */
    public static byte[] strToHex(String str)  {
        int arLen = str.length() / 2;
        byte[] b = new byte[arLen];
        int i = 0;
        for (int j = 0; j < arLen; j++ ) {
            final String s = str.substring(i, i+2);
            b[j] = (byte)Integer.parseInt(s,16);
            i+=2;
        }
        return b;
    }

    /**
     * enclose a string with the given character (i.e. 'TheString' -> '"TheString"')
     * @param s string to be enclosed
     * @param encloseWith the enclose char, i.e. ' " '
     * @return
     */
    public static String enclose(String s, Character encloseWith) {
        return (encloseWith + s + encloseWith);
    }

    /**
     * convert a string list to a string array
     * @param l a list of string
     * @return a string array, or null if list is empty
     */
    public static String[] listToStringArray(List<String> l) {
        if (l.size() == 0) {
            return null;
        }
        String[] out = new String[l.size()];
        int i = 0;
        for (String p : l) {
            out[i] = p;
            i++;
        }
        return out;
    }
}
