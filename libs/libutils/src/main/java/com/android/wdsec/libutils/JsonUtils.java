/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec.libutils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * json utilities
 */
public class JsonUtils {

    /**
     * convenience method to add to a json node without handling exception
     * (the string is added ONLY if it's not empty)
     * @param n the node
     * @param name node name
     * @param v node value
     */
    public static void jsonPutString(JSONObject n, final String name, final String v) {
        if (v == null || v.isEmpty() || n == null) {
            return;
        }

        try {
            n.put(name, v);
        } catch (JSONException e) {
            // can't fail
        }
    }

    /**
     * convenience method to add to a json node without handling exception
     * @param n the node
     * @param name node name
     * @param v node value
     */
    public static void jsonPutInt(JSONObject n, final String name, int v) {
        if (n == null) {
            return;
        }

        try {
            n.put(name, v);
        } catch (JSONException e) {
            // can't fail
        }
    }

    /**
     * convenience method to add to a json node without handling exception
     * @param n the node
     * @param name node name
     * @param v node value
     */
    public static void jsonPutLong(JSONObject n, final String name, long v) {
        if (n == null) {
            return;
        }
        try {
            n.put(name, v);
        } catch (JSONException e) {
            // can't fail
        }
    }

    /**
     * convenience method to add to a json node without handling exception
     * @param n the node
     * @param name node name
     * @param v node value
     */
    public static void jsonPutDouble(JSONObject n, final String name, double v) {
        if (n == null) {
            return;
        }
        try {
            n.put(name, v);
        } catch (JSONException e) {
            // can't fail
        }
    }

    /**
     * convenience method to add to a json node without handling exception
     * @param n the node
     * @param name node name
     * @param v node value
     */
    public static void jsonPutBoolean(JSONObject n, final String name, boolean v) {
        if (n == null) {
            return;
        }

        try {
            n.put(name, v);
        } catch (JSONException e) {
            // can't fail
        }
    }

    /**
     * convenience method to add to a json node without handling exception
     * @param n the node
     * @param name node name
     * @param v node value
     */
    public static void jsonPutJSONObject(JSONObject n, final String name, JSONObject v) {
        if (n == null || v == null) {
            return;
        }

        try {
            n.put(name, v);
        } catch (JSONException e) {
            // can't fail
        }
    }

    /**
     * convenience method to add to a json array without handling exception
     * (the array is added ONLY if it's not empty)
     * @param n the node
     * @param name node name
     * @param v node value
     */
    public static void jsonPutJSONArray(JSONObject n, final String name, JSONArray v) {
        if (v == null || v.length() == 0 || n == null) {
            return;
        }

        try {
            n.put(name, v);
        } catch (JSONException e) {
            // can't fail
        }
    }

    /**
     * convenience method to add to a json node without handling exception (must implement Serializable!)
     * @param n the node
     * @param name node name
     * @param v node value
     */
    public static void jsonPutObject(JSONObject n, final String name, Object v) {
        if (n == null) {
            return;
        }

        try {
            n.put(name, v);
        } catch (JSONException e) {
            // can't fail
        }
    }

    /**
     * return pretty indented string
     * @param js the json object
     * @return
     */
    public static String jsonToStringIndented(final JSONObject js) {
        if (js != null) {
            try {
                return js.toString(2);
            } catch (JSONException e) {
            }
        }
        return "";
    }

    /**
     * convert a string list to a string array
     * @param l a list of string
     * @return a string array, or null if list is empty
     */
    public static String[] JsonArrayToStringArray(JSONArray ar) {
        if (ar.length() == 0) {
            return null;
        }

        // allocate an output array of the required size
        int l = ar.length();
        String[] out = new String[l];

        // fill the array
        for (int i = 0; i < l; i++) {
            out[i] = ar.optString(i);
        }
        return out;
    }
}
