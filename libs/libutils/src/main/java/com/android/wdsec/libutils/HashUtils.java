/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */

package com.android.wdsec.libutils;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * hashing utilities (SHA1)
 */
public class HashUtils {
    /**
     * SHA1 hash the given buffer
     * @param b the buffer
     * @return
     */
    public static String hashBufferToString(final byte b[]) {
        BigInteger i = hashBuffer(b);
        return StrUtils.strFromHex(i.toByteArray());
    }

    /**
     * SHA256 hash the given buffer
     * @param b the buffer
     * @return
     */
    static byte[] hashBufferSha256(final byte b[]) {
        if (b == null) {
            return null;
        }

        MessageDigest d = null;
        try {
            d = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            // can't happen
            return null;
        }
        d.update(b, 0, b.length);
        byte[] sha = new BigInteger(1, d.digest()).toByteArray();

        // ensure it returns 32 bytes (on a sony 5.1, this returns 33 bytes!!!!!!!!)
        ByteArrayOutputStream bos = new ByteArrayOutputStream(32);
        bos.write(sha,0,32);
        StreamUtils.close(bos);
        return bos.toByteArray();
    }

    /**
     * SHA1 hash the given buffer
     * @param b the buffer
     * @return
     */
    public static BigInteger hashBuffer(final byte b[]) {
        if (b == null) {
            return null;
        }

        MessageDigest d = null;
        try {
            d = MessageDigest.getInstance("SHA-1");
        } catch (NoSuchAlgorithmException e) {
            // can't happen
            return null;
        }
        d.update(b, 0, b.length);
        return new BigInteger(1, d.digest());
    }

    /**
     * SHA1 hash the given file
     * @param f the file to be hashed
     * @return
     */
    public static BigInteger hashFile(File f) {
        MessageDigest d = null;
        boolean error = false;
        try {
            d = MessageDigest.getInstance("SHA-1");
        } catch (NoSuchAlgorithmException e) {
            // can't happen
            return null;
        }

        FileInputStream ff;
        try {
            ff = new FileInputStream(f);
        } catch (FileNotFoundException e) {
            //DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR, e);
            return null;
        }
        long remaining = f.length();

        // read file in 1mb chunks
        int chunkSize = 1024*1000;
        byte[] readBuf = new byte[chunkSize];
        while (remaining != 0) {
            int toRead = chunkSize;
            if (remaining < chunkSize) {
                // last chunk
                toRead = (int)remaining;
            }
            int readBytes;
            try {
                // read a chunk
                readBytes = ff.read(readBuf,0,toRead);

                // update hash
                d.update(readBuf,0,readBytes);
            } catch (IOException e) {
                error = true;
                break;
            }

            // next chunk
            remaining -= readBytes;
        }
        StreamUtils.close(ff);
        if (error) {
            return null;
        }

        // done
        return new BigInteger(1, d.digest());
    }

    /**
     * SHA1 hash the given file
     * @param f the file to be hashed
     * @return
     */
    public static String hashFileToString(File f) {
        BigInteger i = hashFile(f);
        if (i == null) {
            return "";
        }
        return StrUtils.strFromHex(i.toByteArray());
    }
}
