/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec.libutils;

import org.json.JSONException;
import org.json.JSONObject;

// import io.michaelrocks.paranoid.Obfuscate;

// @Obfuscate
public class AgentMsgUtils {
    /**
     * initialize an empty common json
     * @param cmdId the cmd id string (may be 0)
     * @param type the evidence type, i.e. 'audio'
     * @param context if not 0, an evidence specific number used to mantain context
     * @return
     */
    private static JSONObject agentMsgInitJson(long cmdId, final String type, long context) {
        JSONObject n = new JSONObject();
        if (cmdId != 0) {
            JsonUtils.jsonPutLong(n, "cmdid", cmdId);
        }
        JsonUtils.jsonPutString(n, "type", type);
        JsonUtils.jsonPutLong(n, "timestamp", System.currentTimeMillis());
        if (context != 0) {
            JsonUtils.jsonPutLong(n, "context", context);
        }
        return n;
    }

    /**
     * send a command execution notification evidence to core
     * @param evmgr an object implementing the IEvidenceMgr interface
     * @param cmdId the command id, if coming from a command
     * @param error one of the CmdResult costants
     * @param errorString if not null, a string better describing the error
     * @param deviceUid if not null, a device unique id (key to be used for further communication) to include
     */
    public static void agentMsgSendCmdExecNotifyToCore(IEvidenceMgr evmgr, long cmdId, int error, final String errorString, final String deviceUid) {
        JSONObject b = new JSONObject();
        JsonUtils.jsonPutInt(b, "error", error);
        if (errorString != null && error != 0) {
            // add only in case of error
            JsonUtils.jsonPutString(b,"msg",errorString);
        }
        if (deviceUid != null) {
            // add the unique id
            JsonUtils.jsonPutString(b,"device_unique_key",deviceUid);
        }
        AgentMsgUtils.agentMsgSendEvidenceToCore(evmgr, cmdId, "cmdresult", 0, b);
    }

    /**
     * send a command execution notification evidence to core
     * @param evmgr an object implementing the IEvidenceMgr interface
     * @param cmdId the command id, if coming from a command
     * @param error one of the CmdResult costants
     * @param errorString if not null, a string better describing the error
     */
    public static void agentMsgSendCmdExecNotifyToCore(IEvidenceMgr evmgr, long cmdId, int error, final String errorString) {
        agentMsgSendCmdExecNotifyToCore(evmgr, cmdId, error, errorString, null);
    }

    /**
     * get the command id from a message (command) json
     * @param msg the message
     * @return
     */
    public static long agentMsgGetCmdId(JSONObject msg) {
        final long id = msg.optLong("cmdid", 0);
        return id;
    }

    /**
     * get the command id from a message (command) json
     * @param msg the message
     * @return null if not found
     */
    public static JSONObject agentMsgGetBody(JSONObject msg) {
        if (msg == null) {
            return null;
        }
        try {
            return msg.getJSONObject("body");
        } catch (JSONException e) {

        }
        return null;
    }

    /**
     * send a collected evidence to core
     * @param evmgr an object implementing the IEvidenceMgr interface
     * @param cmdId the command id, if coming from a command
     * @param type  the evidence type
     * @param context evidence context, or 0
     * @param body the evidence body
     */
    public static void agentMsgSendEvidenceToCore(IEvidenceMgr evmgr,long cmdId, final String type, long context, JSONObject body) {
        // generate full json
        JSONObject n = AgentMsgUtils.agentMsgInitJson(cmdId, type, context);
        JsonUtils.jsonPutJSONObject(n, "body", body);

        // add the evidence
        try {
            evmgr.evidenceAdd(n);
        }
        catch (Throwable e) {
            // shouldn't happen....
            DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR, e);
        }
    }
}
