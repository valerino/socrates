/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec.libgfxutils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;

import com.android.wdsec.libutils.DbgUtils;
import com.android.wdsec.libutils.StreamUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

/**
 * graphic utilities
 */
public class GfxUtils {
    /**
     * internal, creates a JPG thumbnail
     * @param ctx a Context
     * @param b a thumbnail Bitmap
     * @return
     */
    private static byte[] makeThumbnailCommon(Context ctx, Bitmap b) {
        byte[] bytes;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        // compress and obtain a buffer
        b.compress(Bitmap.CompressFormat.JPEG, 60, bos);
        bytes = bos.toByteArray();
        if (BuildConfig.DEBUG) {
            // on debugging, dump to examine content....
            try {
                File dbgThumb = new File(String.format("%s/thumb_%d",ctx.getExternalFilesDir(null).getAbsolutePath(), System.currentTimeMillis()));
                DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"dumping thumbnail to %s", dbgThumb.getAbsolutePath());
                StreamUtils.toFile(bytes, dbgThumb.getAbsolutePath());
            } catch (IOException e) {
                DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR, e);
            }
        }
        StreamUtils.close(bos);
        return bytes;
    }

    /**
     * generates a JPG 640x480 thumbnail for image or video
     * @oaran ctx a Context
     * @param path path to the original image/video
     * @param type MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE or MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO
     * @return
     */
    public static byte[] makeThumbnail(Context ctx, final String path, int type) {
        Bitmap b;
        if (type == MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE) {
            // generate thumbnail for image
            b = ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(path), 640, 480);
        }
        else {
            // generate thumbnail for video
            b = ThumbnailUtils.createVideoThumbnail(path, MediaStore.Video.Thumbnails.MINI_KIND);
        }
        return makeThumbnailCommon(ctx, b);
    }

    /**
     * generates a JPG 640x480 thumbnail for image
     * @oaran ctx a Context
     * @param buf a buffer with image content
     * @return
     */
    public static byte[] makeImageThumbnailFromBuffer(Context ctx, byte[] buf) {
        Bitmap b = ThumbnailUtils.extractThumbnail(BitmapFactory.decodeByteArray(buf,0,buf.length), 640, 480);
        return makeThumbnailCommon(ctx, b);
    }

}
