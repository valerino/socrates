/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec.libutils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * StreamUtils contains stream utilities
 */
public class StreamUtils {

    /**
     * shortcut to close the specified closeable without throwing exception
     * @param c the stream
     */
    public static void close(Closeable c) {
        if (c == null) {
            return;
        }
        try {
            c.close();
        } catch (IOException e) {
        }
    }

    /**
     * copy input to output stream
     * @param src stream with content
     * @param dst the destination stream
     * @throws IOException
     */
    public static void toOutputStream(InputStream src, OutputStream dst) throws IOException {
        while (true) {
            byte[] buf = new byte[1024];
            int len = src.read(buf);
            if (len < 0) {
                break;
            }
            dst.write(buf, 0, len);
        }
    }

    /**
     * read stream to bytes
     * @param src stream with content
     * @throws IOException
     */
    public static byte[] toBytes(InputStream src) throws IOException {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        toOutputStream(src,os);
        os.close();
        return os.toByteArray();
    }

    /**
     * read stream to bytes
     * @param path path to file to be read
     * @throws IOException
     */
    public static byte[] toBytes(final String path) throws IOException {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        FileInputStream is = new FileInputStream(path);
        toOutputStream(is,os);
        os.close();
        is.close();
        return os.toByteArray();
    }

    /**
     * copy stream content to file (will be created if not existent, or overwritten)
     * @param src stream with content
     * @param dst the destination path
     * @throws IOException
     */
    public static void toFile(InputStream src, String dst) throws IOException {
        FileOutputStream out = new FileOutputStream(dst);
        toOutputStream(src, new FileOutputStream(dst));
        out.close();
    }

    /**
     * copy byte array to file (will be created if not existent, or overwritten)
     * @param src a byte array
     * @param dst the destination path
     * @throws IOException
     */
    public static void toFile(final byte[] src, String dst) throws IOException {
        FileOutputStream out = new FileOutputStream(dst);
        ByteArrayInputStream bis = new ByteArrayInputStream(src);
        toOutputStream(bis, new FileOutputStream(dst));
        out.close();
        bis.close();
    }

    /**
     * copy stream content to file (will be created if not existent, or overwritten)
     * @param src stream with content
     * @param dst the destination File
     * @throws IOException
     */
    public static void toFile(InputStream src, File dst) throws IOException {
        toFile(src, dst.getAbsolutePath());
    }

    /**
     * copy file (will be created if not existent, or overwritten)
     * @param src the source File
     * @param dst the destination File
     * @throws IOException
     */
    public static void copy(File src, File dst) throws IOException {
        InputStream is = new FileInputStream(src);
        toFile(is, dst.getAbsolutePath());
    }

    /**
     * copy file (will be created if not existent, or overwritten)
     * @param src the source path
     * @param dst the destination path
     * @throws IOException
     */
    public static void copy(String src, String dst) throws IOException {
        InputStream is = new FileInputStream(src);
        toFile(is, dst);
    }
}
