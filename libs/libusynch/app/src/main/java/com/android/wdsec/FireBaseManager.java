package com.android.wdsec;

import android.content.Context;
import android.support.annotation.NonNull;

import com.android.wdsec.libutils.DbgUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firbase.usynch.crypto.Keys;
import com.google.firbase.usynch.crypto.SHA1Digest;
import com.google.firbase.usynch.uZProtocol;
import com.google.firbase.usynch.util.Utils;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import heartb.google.com.usynch.R;

public class FireBaseManager {
    private static volatile FireBaseManager singleton;
    private Context context;
    private HashMap<String, FirebaseOptions> fb_options = new HashMap<String, FirebaseOptions>();
    private HashMap<String, FirebaseApp> fb_apps = new HashMap<String, FirebaseApp>();
    private ArrayList<String> appIndexer = new ArrayList<>();
    private int selected_option = 0;
    private String selected_app = "";
    private FirebaseApp current_fbapp = null;
    private ValueEventListener info_connected = null;


    private FireBaseManager() {
        appIndexer.add(appIndexer.size(),"com.android.wdsec");
        fb_options.put("com.android.wdsec",new FirebaseOptions.Builder()
                .setApiKey("AIzaSyCvh_xcY8Jm-kJEYMhK_BRw6AbPHGzcUiU")
                .setDatabaseUrl("https://testagt-9cee3.firebaseio.com")
                .setApplicationId("1:389751116098:android:3f158286c587176f")
                .setProjectId("testagt-9cee3")
                //.setGcmSenderId("389751116098-sdbnskfrtegmlpambqrtp8gc838n5bh4.apps.googleusercontent.com")
               // .setStorageBucket("testagt-9cee3.appspot.com")
                .build());
/*
        appIndexer.add(appIndexer.size(),"com.valerino.test");

        fb_options.put("com.valerino.test",new FirebaseOptions.Builder()
                .setApiKey("AIzaSyCvh_xcY8Jm-kJEYMhK_BRw6AbPHGzcUiU")
                .setDatabaseUrl("https://testagt-9cee3.firebaseio.com")
                .setApplicationId("1:389751116098:android:2c00fe92ee5b369d")
                .setProjectId("testagt-9cee3")
                //.setGcmSenderId("389751116098-sdbnskfrtegmlpambqrtp8gc838n5bh4.apps.googleusercontent.com")
                //.setStorageBucket("testagt-9cee3.appspot.com")
                .build());
                */
        appIndexer.add(appIndexer.size(),"com.great.firebase");
        fb_options.put("com.great.firebase",new FirebaseOptions.Builder()
                .setApiKey("AIzaSyAXvvIff0RQ679UiqY4CJZm3zoTCm2aGHU")
                .setDatabaseUrl("https://testme-c6588.firebaseio.com")
                .setApplicationId("1:911991300449:android:2402743fb16a63c8")
                .setProjectId("testme-c6588")
                //.setGcmSenderId("389751116098-sdbnskfrtegmlpambqrtp8gc838n5bh4.apps.googleusercontent.com")
                //.setStorageBucket("testagt-9cee3.appspot.com")
                .build());
    }


    public static FireBaseManager self(Context context) {
        if (singleton == null) {
            synchronized (FireBaseManager.class) {
                if (singleton == null) {
                    singleton = new FireBaseManager();
                    singleton.useAuth = context.getResources().getBoolean(R.bool.USEFBAUTH);
                }
            }
        }
        singleton.context = context;
        return singleton;
    }
//
    public String getToken() {
        return _token[1];
    }

    public String getUser() {
        return _token[0];
    }



    public synchronized void generateIDs(String s) {
        this._token = uZProtocol.getUid(context,s);
    }
    /*
     * Firebase realtime database, handlers and data
     */

    /**
     * the Realtime DB handler
     */
    private FirebaseDatabase _db;

    /**
     * the app instance token
     */
    private String[] _token = {null,null};

    /**
     * Reference to "firebaserealtime/presence/" DB
     */
    private DatabaseReference _userRef = null;


    /**
     * State for to "firebaserealtime/.info/connected"
     */
    private boolean _firebase_online = false;

    /**
     * Reference for to "firebaserealtime/.info/connected"
     */
    private DatabaseReference _onlineRef = null;

    private boolean useAuth = false;
    /**
     * Initialize firebase realtime database with the application ID
     * and handler
     *
     */
    public boolean initFireBase() {
        // Firebase Realtime DB
        // get connected info
        DbgUtils.log(DbgUtils.DbgLevel.INFO, "initFireBase");
        _db = FirebaseDatabase.getInstance(current_fbapp);
        info_connected = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // get the connection status for the current token
                DbgUtils.log(DbgUtils.DbgLevel.INFO, "data change:");
                _firebase_online = (boolean)dataSnapshot.getValue();
                refreshPresence();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };

        try {
            Task<InstanceIdResult> task = FirebaseInstanceId.getInstance(current_fbapp).getInstanceId();

            task.addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {

                @Override
                public void onSuccess(InstanceIdResult instanceIdResult) {

                    String s =instanceIdResult.getToken();
                    DbgUtils.log(DbgUtils.DbgLevel.INFO, "Result available:\n" + s);
                    if (! s.isEmpty()) {
                        generateIDs(s);
                        _onlineRef = _db.getReference(".info/connected");
                        try {
                            makeReference(useAuth);
                            _onlineRef.addValueEventListener(info_connected);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }
            });
            // TODO: 12/19/18 cache the processed token, in case of change there is a notification handled by FirebaseMsgService
        }catch (Exception e){
            DbgUtils.log(DbgUtils.DbgLevel.INFO, "Exception getting the token");
            e.printStackTrace();
            return false;
        }
        return true;


    }


    private void makeReference(boolean useUid) {
        String uid = FirebaseAuth.getInstance(_db.getApp()).getUid();
        String subdir = Utils.bytesToHex(SHA1Digest.get(_db.getApp().getOptions().getProjectId().getBytes()));
        if(!useUid) {
            uid = getUser();
        }
        if(uid!=null) {
            DbgUtils.log(DbgUtils.DbgLevel.INFO, String.format("presence/%s/%s/%s",subdir, uid, getUser()));
            _userRef = _db.getReference(String.format("presence/%s/%s/%s", subdir, uid, getUser()));
        }
    }


    protected synchronized String refreshPresence() {
        String s = "";
        if (_userRef ==null){
            DbgUtils.log(DbgUtils.DbgLevel.INFO, "skipping refresh because _db not initialized");
        }else {
            s = ((_firebase_online) ? "+" : "-") + getToken();
            _userRef.setValue(s);
            if (_firebase_online) {
                DbgUtils.log(DbgUtils.DbgLevel.INFO, "app went ONLINE");
            }
            DbgUtils.log(DbgUtils.DbgLevel.INFO, "refresh :" + s);
            _userRef.onDisconnect().setValue("-" + getToken());

        }
        return s;
    }


    public void initFireBaseApplication(int appIndex) {
        if (appIndex <0 || appIndex>= fb_options.size() ){
            appIndex = 0;
        }

        selected_app = appIndexer.get(appIndex);
        DbgUtils.log(DbgUtils.DbgLevel.INFO, "connecting to  " + selected_app );
        FirebaseOptions options = fb_options.get(selected_app);
        if(!fb_apps.containsKey(selected_app)) {
            fb_apps.put(selected_app, FirebaseApp.initializeApp(context, options, selected_app));
        }
        current_fbapp = fb_apps.get(selected_app);
        FirebaseDatabase.getInstance(current_fbapp).goOnline();
        //login
        FirebaseAuth mAuth = FirebaseAuth.getInstance(current_fbapp);
        if(useAuth && mAuth.getUid() == null) {
            mAuth.signInAnonymously()
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // Sign in success, update UI with the signed-in user's information
                                DbgUtils.log(DbgUtils.DbgLevel.INFO ,"signInAnonymously:success");
                                makeReference(useAuth);
                                refreshPresence();
                            } else {
                                // If sign in fails, display a message to the user.
                                DbgUtils.log(DbgUtils.DbgLevel.INFO ,"signInAnonymously:failure", task.getException());
                            }
                        }
                    });
        }
        selected_option = appIndex;
    }
    public void close(){
        FirebaseDatabase.getInstance(current_fbapp).goOffline();
        _onlineRef.removeEventListener(info_connected);
        if(useAuth){
            _userRef = null;
        }
        info_connected = null;
        try {
            FirebaseInstanceId.getInstance(current_fbapp).deleteInstanceId();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public int getNextOption() {
        int i = selected_option;
        i++;
        i = i%fb_options.size();
        return i;
    }


    public void newToken(String s) {
        generateIDs(s);
        refreshPresence();
    }
}
