# Micro synch library and sample app
## Library 
usynch/usynchlib

## Installation
Add the debug or release arr file in the main application libs directory

Then add the following line to the dependency section of the application build.gradle
compile(name:'usynchlib', ext:'aar')

i.e:
```commandline

dependencies {
    compile fileTree(dir: 'libs', include: ['*.jar'])
    compile(name:'usynchlib', ext:'aar')

}
```

# Usage 
Inside your application just call the static method uZProtocol.microsync to run a sync
with the passed payload and target host ip.
in case the host is null, the one binarypatched inside the library is used.
```commandline
JSONObject jsonObj = new JSONObject();
jsonObj.put(S.e("microagent"), S.e("gagliardo"));
return uZProtocol.microsync(getApplicationContext(), null, jsonObj, true, true);

```

# Sample application
usynch/app

The application just execute a sync when the button is pressed.

# Build
./gradlew usynchlib:assembleDebug

or 

./gradlew usynchlib:assembleRelease