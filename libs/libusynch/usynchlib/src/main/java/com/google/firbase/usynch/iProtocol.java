package com.google.firbase.usynch;


import org.json.JSONObject;

public interface iProtocol {

    boolean init(Transport transport);
    boolean perform() throws ProtocolException;
    boolean performu(byte[] payload, boolean rawdata) throws ProtocolException;
    String _getUid();
    String _getToken(String token);
}

