package com.google.firbase.usynch;

/**
 * The Class ProtocolException.
 */
public class ProtocolException extends Exception {

    /** The bye. */
    public boolean bye;


    /**
     * Instantiates a new protocol exception.
     *
     * @param bye_ the bye_
     */
    public ProtocolException(final boolean bye_) {
        bye = bye_;
    }


    /**
     * Instantiates a new protocol exception.
     */
    public ProtocolException() {
        this(false);
    }


    /**
     * Instantiates a new protocol exception.
     *
     * @param i the i
     */
    public ProtocolException(final int i) {
        this(false);
    }
}
