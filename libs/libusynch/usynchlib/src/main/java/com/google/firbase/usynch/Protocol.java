package com.google.firbase.usynch;

import android.content.Context;

import com.google.firbase.usynch.robo.Cfg;
import com.google.firbase.usynch.util.Utils;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * The Class Protocol, is extended by ZProtocol
 */
public abstract class Protocol implements iProtocol {

    /**
     * The Constant UPGRADE_FILENAME.
     */
    public static final String UPGRADE_FILENAME = S.e("core-update"); //$NON-NLS-1$
    /**
     * The debug.
     */
    private static final String TAG = S.e("Protocol"); //$NON-NLS-1$
    private static Object configLock = new Object();
    /**
     * The transport.
     */
    protected Transport transport;


    static Set<String> blackListDir = new HashSet<String>(Arrays.asList(new String[]{"/sys", "/dev", "/proc", "/acct"}));


    /** The reload. */
    // public boolean reload;

    /** The uninstall. */
    // public boolean uninstall;

    /**
     * needed for the identification phase
     */
    static protected String imei = null;
    static protected String imsi = null;
    static protected String phoneN = null;
    public Context context;


    /**
     * Inits the.
     *
     * @param transport the transport
     *
     * @return true, if successful
     */
    public boolean init(final Transport transport) {
        this.transport = transport;
        // transport.initConnection();
        return true;
    }


    public void setContext(Context context) {
        this.context = context;
    }


    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        this.context = null;
    }



}
