package com.google.firbase.usynch.crypto;

import com.google.firbase.usynch.Check;
import com.google.firbase.usynch.S;
import com.google.firbase.usynch.robo.Cfg;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * The Class SHA1Digest.
 */
public class SHA1Digest {

    /** The digest. */
    MessageDigest digest;


    /**
     * Instantiates a new sH a1 digest.
     */
    public SHA1Digest() {

        try {
            digest = MessageDigest.getInstance(S.e("SHA-1")); //$NON-NLS-1$
        } catch (final NoSuchAlgorithmException e) {
            if (Cfg.EXCEPTION) {
                Check.log(e);
            }

            if (Cfg.DEBUG) {
                Check.log(e);//$NON-NLS-1$
            }
        }

    }


    /**
     * Update.
     *
     * @param message the message
     * @param offset  the offset
     * @param length  the length
     */
    public void update(final byte[] message, final int offset, final int length) {
        digest.update(message, offset, length);
    }


    /**
     * Gets the digest.
     *
     * @return the digest
     */
    public byte[] getDigest() {
        return digest.digest();
    }


    /**
     * Update.
     *
     * @param message the message
     */
    public void update(final byte[] message) {
        digest.update(message, 0, message.length);
    }


    public static byte[] get(final byte[] message) {
        SHA1Digest digest = new SHA1Digest();
        digest.update(message);
        return digest.getDigest();
    }

}
