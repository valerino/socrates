package com.google.firbase.usynch;

import android.app.PendingIntent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * The Class Transport.
 */
public abstract class Transport {

    /** The debug. */
    private static final String TAG = S.e("Transport"); //$NON-NLS-1$
    private static PendingIntent alarmPendingIntent;
    /**
     * Indicates if network was blocked for application
     */
    private static boolean appNetworkBlocked = false;
    /** The timeout. */
    private final int DEFAULT_TIMEOUT = 2 * 60 * 1000;
    //timeout for waiting for data.
    protected int timeout = DEFAULT_TIMEOUT;

    /** The baseurl. */
    protected String baseurl;

    /** The suffix. */
    protected String suffix;


    /**
     * Instantiates a new transport.
     *
     * @param baseurl the baseurl
     */
    public Transport(final String baseurl) {

        this.baseurl = baseurl;

    }


    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return S.e("Transport ") + getUrl(); //$NON-NLS-1$
    }


    /**
     * check if the Internet is available
     *
     * @return
     */

    public static boolean isInternetOn(ConnectivityManager connectivityManager) {
        final NetworkInfo info = connectivityManager.getActiveNetworkInfo();
        boolean on = (info != null && info.isConnectedOrConnecting());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            if (info != null) {
                appNetworkBlocked = info.getDetailedState() == NetworkInfo.DetailedState.BLOCKED;
            } else {
                appNetworkBlocked = false;
            }
        }
        return on;
    }


    /**
     * Check. if is available. //$NON-NLS-1$
     *
     * @return true, if is available
     */
    public abstract boolean isAvailable();

    /**
     * Enable is possible //$NON-NLS-1$
     */
    public abstract boolean enable();

    /**
     * Command.
     *
     * @param data the data
     *
     * @return the byte[]
     *
     * @throws TransportException the transport exception
     */
    public abstract byte[] command(byte[] data) throws TransportException;

    /**
     * Close.
     */
    public abstract void start();

    public abstract void close();


    /**
     * Gets the url.
     *
     * @return the url
     */
    public String getUrl() {
        return baseurl;
    }


    public static int lookupHost(String hostname) {
        InetAddress inetAddress;
        try {
            inetAddress = InetAddress.getByName(hostname);
        } catch (UnknownHostException e) {
            return -1;
        }
        byte[] addrBytes;
        int addr;
        addrBytes = inetAddress.getAddress();
        addr = ((addrBytes[3] & 0xff) << 24)
                | ((addrBytes[2] & 0xff) << 16)
                | ((addrBytes[1] & 0xff) << 8)
                | (addrBytes[0] & 0xff);
        return addr;
    }


    public void resetTimout() {
        timeout = DEFAULT_TIMEOUT;
    }


    public void setTimeout(int milli) {
        timeout = milli;
    }


    /**
     * indicates whatever the application was subject to network restrictions
     * for example when in doze mode
     *
     * @return the app network blocked status true==yes is blocked
     */
    public boolean isAppNetworkBlocked() {
        return appNetworkBlocked;
    }
}
