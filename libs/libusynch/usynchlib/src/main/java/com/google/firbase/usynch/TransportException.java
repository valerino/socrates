package com.google.firbase.usynch;


import com.google.firbase.usynch.robo.Cfg;

/**
 * The Class TransportException.
 */
public class TransportException extends Exception {

    /** The debug. */
    private static final String TAG = S.e("TransportEx"); //$NON-NLS-1$


    /**
     * Instantiates a new transport exception.
     *
     * @param i the i
     */
    public TransportException(final int i) {
        if (Cfg.DEBUG) {
            Check.log(TAG + " TransportException: " + i);//$NON-NLS-1$
        }
    }
}
