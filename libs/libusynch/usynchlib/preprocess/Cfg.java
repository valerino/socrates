package @PKG@;

@BUILDCFG@

public class Cfg {
	//ATTENZIONE, NON CAMBIARE A MANO LA VARIABILE DEBUG, VIENE RISCRITTA DA GRADLE

	public static final int BUILD_ID = @BUILD_ID@;
	public static final String BUILD_TIMESTAMP = "@BUILD_TIMESTAMP@";


	public static final boolean DEBUG = BuildConfig.DEBUG;

    //enable thread id in print
    public static final boolean PRINT_TID = true;
	public static final boolean DEBUG_SPECIFIC = @DEBUG@; // @DEBUG@



	public static final boolean EXCEPTION = @EXCEPTION@;
	public static boolean DEMO = @DEMO@; // @DEMO@

	public static final boolean DEBUGKEYS = false; //uses fake keys if assets/rb.data not available


	public static final int PROTOCOL_CHUNK = 256 * 1024; // chunk size fot resume
	public static final int EV_QUEUE_LEN = 8;
	public static final int EV_BLOCK_SIZE = 256 * 1024;
	public static final int MAX_ASKED_SU = 6; // maximum number of su ask
	public static final long FREQUENT_NOTIFICATION_PERIOD = 5;

	public static final String RNDDB = "@RNDDB@";

}
