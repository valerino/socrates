/**
 * micro-rcs-android
 * jrf, ht, 2k18
 */

package com.android.wdsec.libphonebook;

import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.provider.ContactsContract;

import com.android.wdsec.libdbutils.FilterUtils;
import com.android.wdsec.libdbutils.SQLDbCursor;
import com.android.wdsec.libutils.DbgUtils;
import com.android.wdsec.libutils.JsonUtils;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * utilities to extract data from the address book.
 *
 * the filter supported by these function has the following format:
 * "filter": {
 *      "name": {
 *          "value": "TheContactName",
 *          //  0|1|4 (exact/partial match/ignore)
 *
 *          "match_type": 0,
 *
 *          // 0: string
 *          "value_type": 0
 *      },
 *      "email": {
 *          "value": "bla@bla.com",
 *
 *          //  0|1|4 (exact/partial match/ignore)
 *          "match_type": 0,
 *
 *          // 0: string
 *          "value_type": 0
 *      },
 *      "website": {
 *          "value": "http://bla.bla.com",
 *
 *          //  0|1|4 (exact/partial match/ignore)
 *          "match_type": 0,
 *
 *          // 0: string
 *          "value_type": 0
 *      },
 *      "phone": {
 *          "value": "328 2122234",
 *
 *          //  0|1|4 (exact/partial match/ignore)
 *          "match_type": 0,
 *
 *          // 0: string
 *          "value_type": 0
 *      },
 *      "postal": {
 *          "value": "22, acacia avenue",
 *
 *          //  0|1|4 (exact/partial match/ignore)
 *          "match_type": 0,
 *
 *          // 0: string
 *          "value_type": 0
 *      },
 *      "im": {
 *          "value": "MyImAccount",
 *
 *          //  0|1|4 (exact/partial match/ignore)
 *          "match_type": 0,
 *
 *          // 0: string
 *          "value_type": 0
 *      },
 *      "company": {
 *          "value": "MyCompany",
 *
 *          //  0|1|4 (exact/partial match/ignore)
 *          "match_type": 0,
 *
 *          // 0: string
 *          "value_type": 0
 *      }
 * }
 */
public class PhonebookUtils {
    /**
     * the call direction
     */
    public enum CallDirection {
        None,
        Incoming,
        Outgoing
    }


    /**
     * get contact name from number, if it's present in the address book
     * @param ctx a Context
     * @param number a phone number
     * @return empty string if not found
     */
    public static final String contactsDisplayNameFromNumber(Context ctx, final String number) {
        final String s = SQLDbCursor.contactsDisplayNameFromNumber(ctx, number);
        return s;
    }

    /**
     * gets a cursor capable of returning one contact per row.
     * the returned cursor must be closed with close() once done.
     * @param ctx a Context
     * @param filter optional, a filter with the format described in the class definition
     * @return
     * @throws SQLiteException on query failed
     * @throws SecurityException on permission deny
     * @throws IllegalArgumentException item not found/empty result
     */
    public static SQLDbCursor contactsGetCursor(Context ctx, JSONObject filter) throws SQLiteException, SecurityException, IllegalArgumentException {
        SQLDbCursor c = FilterUtils.filterQueryGeneric(ctx, filter, "name",
                ContactsContract.Contacts.CONTENT_URI,
                null,
                new String[] {ContactsContract.Contacts.DISPLAY_NAME, ContactsContract.Contacts.NAME_RAW_CONTACT_ID},
                ContactsContract.Contacts.DISPLAY_NAME);
        return c;
    }

    /**
     * query emails for the given contact
     * @param ctx a Context
     * @param item returned by contactsGetCursor()
     * @param filter optional, a filter with the format described in the class definition
     * @return
     * @throws SQLiteException on query failed
     * @throws SecurityException on permission deny
     * @throws IllegalArgumentException item not found/empty result
     */
    public static JSONArray contactQueryEmails(Context ctx, SQLDbCursor item, JSONObject filter) throws SQLiteException, SecurityException, IllegalArgumentException {
        JSONArray ar = new JSONArray();

        String selection = String.format("%s=\"%s\" AND %s=\"%s\"",
                ContactsContract.CommonDataKinds.Email.RAW_CONTACT_ID,
                item.getString(ContactsContract.Contacts.NAME_RAW_CONTACT_ID),
                ContactsContract.CommonDataKinds.Email.MIMETYPE,
                ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE);

        final String[] columns = {ContactsContract.CommonDataKinds.Email.ADDRESS, ContactsContract.CommonDataKinds.Email.LABEL,
                ContactsContract.CommonDataKinds.Email.TYPE, ContactsContract.CommonDataKinds.Email.MIMETYPE};

        SQLDbCursor c = FilterUtils.filterQueryGeneric(ctx, filter, "email",
                ContactsContract.CommonDataKinds.Email.CONTENT_URI, selection, columns,
                ContactsContract.CommonDataKinds.Email.ADDRESS);

        // fill the emails array
        while (c.advance() != null) {
            JSONObject n = new JSONObject();
            JsonUtils.jsonPutString(n,"address", c.getString(ContactsContract.CommonDataKinds.Email.ADDRESS));
            int type = c.getInt(ContactsContract.CommonDataKinds.Email.TYPE);
            final String label = ContactsContract.CommonDataKinds.Email.getTypeLabel(ctx.getResources(),type,
                    c.getString(ContactsContract.CommonDataKinds.Email.LABEL)).toString();
            JsonUtils.jsonPutString(n,"type", label);
            ar.put(n);
        }
        c.close();
        return ar;
    }

    /**
     * query emails for the given contact
     * @param ctx a Context
     * @param item returned by contactsGetCursor()
     * @param filter optional, a filter with the format described in the class definition
     * @return
     * @throws SQLiteException on query failed
     * @throws SecurityException on permission deny
     * @throws IllegalArgumentException item not found/empty result
     */
    public static JSONObject contactQueryOrganization(Context ctx, SQLDbCursor item, JSONObject filter) throws SQLiteException, SecurityException, IllegalArgumentException {

        String selection = String.format("%s=\"%s\" AND %s=\"%s\"",
                ContactsContract.CommonDataKinds.Organization.RAW_CONTACT_ID,
                item.getString(ContactsContract.Contacts.NAME_RAW_CONTACT_ID),
                ContactsContract.CommonDataKinds.Organization.MIMETYPE,
                ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE);

        final String[] columns = {ContactsContract.CommonDataKinds.Organization.COMPANY, ContactsContract.CommonDataKinds.Organization.TITLE,
                ContactsContract.CommonDataKinds.Organization.OFFICE_LOCATION, ContactsContract.CommonDataKinds.Organization.DEPARTMENT,
                ContactsContract.CommonDataKinds.Email.MIMETYPE};

        SQLDbCursor c = FilterUtils.filterQueryGeneric(ctx, filter, "company",
                ContactsContract.Data.CONTENT_URI, selection, columns,
                ContactsContract.CommonDataKinds.Organization.COMPANY);
        if (c.advance() == null) {
            // no company info
            c.close();
            return null;
        }

        JSONObject n = new JSONObject();
        JsonUtils.jsonPutString(n,"name", c.getString(ContactsContract.CommonDataKinds.Organization.COMPANY));
        JsonUtils.jsonPutString(n,"location", c.getString(ContactsContract.CommonDataKinds.Organization.OFFICE_LOCATION));
        JsonUtils.jsonPutString(n,"title", c.getString(ContactsContract.CommonDataKinds.Organization.TITLE));
        JsonUtils.jsonPutString(n,"department", c.getString(ContactsContract.CommonDataKinds.Organization.DEPARTMENT));
        c.close();
        return n;
    }

    /**
     * query websites for the given contact
     * @param ctx a Context
     * @param item returned by contactsGetCursor()
     * @param filter optional, a filter with the format described in the class definition
     * @return
     * @throws SQLiteException on query failed
     * @throws SecurityException on permission deny
     * @throws IllegalArgumentException item not found/empty result
     */
    public static JSONArray contactQueryWebsites(Context ctx, SQLDbCursor item, JSONObject filter) throws SQLiteException, SecurityException, IllegalArgumentException {
        JSONArray ar = new JSONArray();

        String selection = String.format("%s=\"%s\" AND %s=\"%s\"",
                ContactsContract.CommonDataKinds.Website.RAW_CONTACT_ID,
                item.getString(ContactsContract.Contacts.NAME_RAW_CONTACT_ID),
                ContactsContract.CommonDataKinds.Website.MIMETYPE,
                ContactsContract.CommonDataKinds.Website.CONTENT_ITEM_TYPE);

        final String[] columns = {ContactsContract.CommonDataKinds.Website.URL, ContactsContract.CommonDataKinds.Website.MIMETYPE};

        SQLDbCursor c = FilterUtils.filterQueryGeneric(ctx, filter, "website",
                ContactsContract.Data.CONTENT_URI, selection, columns,
                ContactsContract.CommonDataKinds.Website.URL);

        // fill the websites array
        while (c.advance() != null) {
            JSONObject n = new JSONObject();
            JsonUtils.jsonPutString(n,"url", c.getString(ContactsContract.CommonDataKinds.Website.URL));
            ar.put(n);
        }
        c.close();
        return ar;
    }

    /**
     * query phone numbers for the given contact
     * @param ctx a Context
     * @param ctx a Context
     * @param item returned by contactsGetCursor()
     * @param filter optional, a filter with the format described in the class definition
     * @return
     * @throws SQLiteException on query failed
     * @throws SecurityException on permission deny
     * @throws IllegalArgumentException item not found/empty result
     */
    public static JSONArray contactQueryPhoneNumbers(Context ctx, SQLDbCursor item, JSONObject filter) throws SQLiteException, SecurityException, IllegalArgumentException {
        JSONArray ar = new JSONArray();

        String selection = String.format("%s=\"%s\" AND %s=\"%s\"",
                ContactsContract.CommonDataKinds.Phone.RAW_CONTACT_ID,
                item.getString(ContactsContract.Contacts.NAME_RAW_CONTACT_ID),
                ContactsContract.CommonDataKinds.Phone.MIMETYPE,
                ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE);

        final String[] columns = {ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.CommonDataKinds.Phone.LABEL,
                ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.MIMETYPE};

        SQLDbCursor c = FilterUtils.filterQueryGeneric(ctx, filter, "phone",
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI, selection, columns,
                ContactsContract.CommonDataKinds.Phone.NUMBER);

        // fill the phonenumbers array
        while (c.advance() != null) {
            JSONObject n = new JSONObject();
            JsonUtils.jsonPutString(n,"number", c.getString(ContactsContract.CommonDataKinds.Phone.NUMBER));
            int type = c.getInt(ContactsContract.CommonDataKinds.Phone.TYPE);
            final String label = ContactsContract.CommonDataKinds.Phone.getTypeLabel(ctx.getResources(),type,
                    c.getString(ContactsContract.CommonDataKinds.Phone.LABEL)).toString();
            JsonUtils.jsonPutString(n,"type", label);
            ar.put(n);
        }
        c.close();
        return ar;
    }

    /**
     * query postal addresses for the given contact
     * @param ctx a Context
     * @param item returned by contactsGetCursor()
     * @param filter optional, a filter with the format described in the class definition
     * @return
     * @throws SQLiteException on query failed
     * @throws SecurityException on permission deny
     * @throws IllegalArgumentException item not found/empty result
     */
    public static JSONArray contactQueryPostalAddresses(Context ctx, SQLDbCursor item, JSONObject filter) throws SQLiteException, SecurityException, IllegalArgumentException {
        JSONArray ar = new JSONArray();

        String selection = String.format("%s=\"%s\" AND %s=\"%s\"",
                ContactsContract.CommonDataKinds.StructuredPostal.RAW_CONTACT_ID,
                item.getString(ContactsContract.Contacts.NAME_RAW_CONTACT_ID),
                ContactsContract.CommonDataKinds.StructuredPostal.MIMETYPE,
                ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE);

        final String[] columns = {ContactsContract.CommonDataKinds.StructuredPostal.FORMATTED_ADDRESS, ContactsContract.CommonDataKinds.StructuredPostal.LABEL,
                ContactsContract.CommonDataKinds.StructuredPostal.TYPE, ContactsContract.CommonDataKinds.StructuredPostal.MIMETYPE};

        SQLDbCursor c = FilterUtils.filterQueryGeneric(ctx, filter, "postal",
                ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_URI, selection, columns,
                ContactsContract.CommonDataKinds.StructuredPostal.FORMATTED_ADDRESS);

        // fill the postal addresses array
        while (c.advance() != null) {
            JSONObject n = new JSONObject();
            JsonUtils.jsonPutString(n,"address", c.getString(ContactsContract.CommonDataKinds.StructuredPostal.FORMATTED_ADDRESS));
            int type = c.getInt(ContactsContract.CommonDataKinds.StructuredPostal.TYPE);
            final String label = ContactsContract.CommonDataKinds.StructuredPostal.getTypeLabel(ctx.getResources(),type,
                    c.getString(ContactsContract.CommonDataKinds.StructuredPostal.LABEL)).toString();
            JsonUtils.jsonPutString(n,"type", label);
            ar.put(n);
        }
        c.close();
        return ar;
    }

    /**
     * query IM addresses for the given contact
     * @param ctx a Context
     * @param item returned by contactsGetCursor()
     * @param filter optional, a filter with the format described in the class definition
     * @return
     * @throws SQLiteException on query failed
     * @throws SecurityException on permission deny
     * @throws IllegalArgumentException item not found/empty result
     */
    public static JSONArray contactQueryIM(Context ctx, SQLDbCursor item, JSONObject filter) throws SQLiteException, SecurityException, IllegalArgumentException {
        JSONArray ar = new JSONArray();

        String selection = String.format("%s=\"%s\" AND %s=\"%s\"",
                ContactsContract.CommonDataKinds.Im.RAW_CONTACT_ID,
                item.getString(ContactsContract.Contacts.NAME_RAW_CONTACT_ID),
                ContactsContract.CommonDataKinds.Im.MIMETYPE,
                ContactsContract.CommonDataKinds.Im.CONTENT_ITEM_TYPE);

        final String[] columns = {ContactsContract.CommonDataKinds.Im.PROTOCOL,
                ContactsContract.CommonDataKinds.Im.DATA, ContactsContract.CommonDataKinds.Im.MIMETYPE};

        SQLDbCursor c = FilterUtils.filterQueryGeneric(ctx, filter, "im",
                ContactsContract.Data.CONTENT_URI, selection, columns,
                ContactsContract.CommonDataKinds.Im.DATA);

        // fill the im addresses array
        while (c.advance() != null) {
            JSONObject n = new JSONObject();
            JsonUtils.jsonPutString(n,"address", c.getString(ContactsContract.CommonDataKinds.Im.DATA));
            int type = c.getInt(ContactsContract.CommonDataKinds.Im.PROTOCOL);
            final String label = ContactsContract.CommonDataKinds.Im.getProtocolLabel(ctx.getResources(),type,
                    c.getString(ContactsContract.CommonDataKinds.Im.LABEL)).toString();
            JsonUtils.jsonPutString(n,"type", label);
            ar.put(n);
        }
        c.close();
        return ar;
    }
}
