#!/usr/bin/env sh
rm ./dbg_out_minimal_nodisposablekey.apk
./build_agent.py --core_apk ../app/build/outputs/apk/debug/app-debug.apk --out_apk ./dbg_out_minimal_nodisposablekey.apk --plugins ./plugins_minimal.txt \
--cfg ../app/configuration.json --firebase_cfg ../app/google-services.json --pkg_name com.valerino.test --ks ../testkeystore.jks --kn test --kp 123456 --ksp 123456 \
--disposable_key 11223344556677889900aabbccddeeff11223344556677889900aabbccddeeff --clear_disposable_key --hide_tools_output
