#!/usr/bin/env sh
rm ./rls_out_minimal_nodisposablekey.apk
./build_agent.py --core_apk ../app/build/outputs/apk/release/app-release.apk --out_apk ./rls_out_minimal_nodisposablekey.apk --plugins ./plugins_minimal_rls.txt \
--cfg ../app/configuration.json --firebase_cfg ../app/google-services.json --pkg_name com.valerino.test --ks ../testkeystore.jks --kn test --kp 123456 --ksp 123456 \
--disposable_key 11223344556677889900aabbccddeeff11223344556677889900aabbccddeeff --clear_disposable_key --hide_tools_output
