#!/usr/bin/env python3
# this tool generates encrypted/compressed assets to be included in the built APK at compile time.
# may be also used as a helper tool on the backend to generate updates to be used with 'modupdate' plugin
import argparse
import binascii
import zlib
from Crypto.Cipher import AES
from Crypto.Util import Counter
from Crypto import Random
import os
import sys

def encrypt(buf, args):
    """
    zlib compress -> aes encryption
    """
    outBuf = bytearray()

    # compress
    ori_len = len(buf)
    compressed = zlib.compress(buf, 9)
    
    # encrypt
    nonce = Counter.new(128,  initial_value=int.from_bytes(args.ivbytes, 'big'))
    crypter = AES.new(args.keybytes, AES.MODE_CTR, counter=nonce)
    encrypted = crypter.encrypt(compressed)
    
    if args.random_iv:
        # prepend the iv
        outBuf.extend(args.ivbytes)
    outBuf.extend(encrypted)

    # write to file
    f = open(args.outfile[0], 'wb')
    f.write(outBuf)
    f.close()
    print('[-] original len=%d, compressed/encrypted len=%d, has_iv=%d' % (ori_len, len(encrypted), args.random_iv))
    return


def decrypt(buf, args):
    """
    aes decryption -> zlib decompress
    """
    ori_len = len(buf)
    
    #decrypt
    nonce = Counter.new(128,  initial_value=int.from_bytes(args.ivbytes, 'big'))
    decrypter = AES.new(args.keybytes, AES.MODE_CTR, counter=nonce)
    decrypted = decrypter.decrypt(buf)

    # decompress
    decompressed = zlib.decompress(decrypted)

    # write to file
    f = open(args.outfile[0], 'wb')
    f.write(decompressed)
    f.close()
    print('[-] original len=%d, decrypted/decompressed len=%d, has_iv=%d' % (ori_len, len(decompressed), args.random_iv))
   
    return


def process_file(args):
    # reading input
    f = open(args.infile[0], 'rb')
    buf = f.read()
    f.close()
    print ('[-] infile=%s' % args.infile[0])

    # output is same as input if not specified
    if args.outfile == None:
        args.outfile = args.infile
    print ('[-] outfile=%s' % args.outfile[0])
    print ('[-] mode=%s' % args.mode[0])

    # check if we use random iv
    if args.random_iv:
        if args.iv is None and args.mode[0] == 'encrypt':
            # generate random iv
            args.ivbytes = Random.get_random_bytes(16)
            print('[-] random iv=%s' % str(binascii.hexlify(args.ivbytes), 'utf-8'))
        if args.mode[0] == 'decrypt':
            # use first 16 bytes as iv
            args.ivbytes = buf[:16]
        
            # skip first 16 bytes
            buf = buf[16:]
            print('[-] using first 16 bytes of infile as iv=%s' % str(binascii.hexlify(args.ivbytes), 'utf-8'))
    else: 
        # use provided iv
        print ('[-] iv=%s' % args.iv[0])
        args.ivbytes=binascii.unhexlify(args.iv[0])
    
    # use provided key
    print('[-] key=%s' % args.key[0])
    args.keybytes = binascii.unhexlify(args.key[0])

   # process
    if args.mode[0] == 'encrypt':
        encrypt(buf, args)
    else:
        decrypt(buf,args)

    print ('[-] DONE!')
    return    

def main():
    parser = argparse.ArgumentParser('microandroid - compressed/encrypted fileformat tool')
    parser.add_argument('--infile', nargs=1, help='path to the input file', required = True)
    parser.add_argument('--outfile', nargs=1, help='path to the output file', default = None)
    parser.add_argument('--key', nargs=1, help='the 32-byte hex string key', required=True)
    parser.add_argument('--iv', nargs=1, help='the 16-byte hex string initialization vector. if random_iv=True and mode="encrypt", it is randomly generated.if random_iv=True and mode="decrypt" the iv is read from the first 16 bytes of infile, and the rest is used as data')
    parser.add_argument('--mode', nargs=1, help='encrypt(in --> zlib deflate --> aes encrypt --> out) or decrypt(in --> aes decrypt --> zlib deflate --> out)', required=True)
    parser.add_argument('--random_iv', action='store_const', const=True, help='if specified, 16 bytes random iv is prepended on encryption or read from first 16 bytes of --infile on decryption.')
    args = parser.parse_args()
    process_file(args)

if __name__ == "__main__":
    main()
