#!/usr/bin/env sh
./build_agent.py --core_apk ../app/build/outputs/apk/debug/app-debug.apk --out_apk ./out.apk --plugins ./plugins.txt --cfg ../app/configuration.json --firebase_cfg ../app/google-services.json --ks ../testkeystore.jks --kn test --kp 123456 --ksp 123456 \
--pkg_name com.valerino.test --disposable_key 11223344556677889900aabbccddeeff11223344556677889900aabbccddeeff --hide_tools_output
