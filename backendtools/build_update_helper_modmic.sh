#!/usr/bin/env sh
# helper test script to build an encrypted binary suitable to be sent through modupdate
# do not modify these directly, make a copy and hardcode your values!
CORE_APK_PACKAGE=com.android.wdsec
PLUGIN_INTERNAL_NAME=modmic
IN_APK=../plugins/modmic/build/outputs/apk/debug/modmic-debug.apk
OUT_APK=./modmic_update_encrypted.apk
# this is the DEVICE UNIQUE KEY (changes with device!)
KEY=19cca8eba0a8e2df29698f7bb0a8fc3e55ee90a3bd43c1904b3d9d7824dfe83d

./build_update.py --in_apk $IN_APK --out_apk $OUT_APK --device_key $KEY --plugin_internal_name $PLUGIN_INTERNAL_NAME \
 --core_apk_pkgname $CORE_APK_PACKAGE --ks ../testkeystore.jks --kn test --kp 123456 --ksp 123456 --hide_tools_output
