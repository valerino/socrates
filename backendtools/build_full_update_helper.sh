#!/usr/bin/env sh
# helper test script to build a full apk update  suitable to be sent through modupdate
# do not modify these directly, make a copy and hardcode your values!
# we are using the vanilla apk here, just to test .... just, it must be the SAME name used for the pre-existing APK AND SIGNED WITH THE SAME KEY/KEYSTORE, or the update won't work!
IN_APK=../app/build/outputs/apk/debug/app-debug.apk
OUT_APK=./app_encrypted.apk
# this is the DEVICE UNIQUE KEY (changes with device!)
KEY=545c5b6dccbc8980aaa0f148d16c1247f188638a60edf8f37228ffdf2695f257

# prepare a full apk as usual with build_agent.py, then simply encrypt it using a random iv
./encrypt_decrypt.py --infile $IN_APK --outfile $OUT_APK --key $KEY --random_iv --mode encrypt
