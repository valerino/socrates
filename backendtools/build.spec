# -*- mode: python -*-

block_cipher = None


a = Analysis(['build_agent.py'],
             pathex=['Z:\\work\\rcs\\microandroid\\backendtools', 'Z:\\work\\rcs\\microandroid\\backendtools\\buildutils'],
             binaries=[],
             datas=[

                    ('Z:\\work\\rcs\\microandroid\\backendtools\\buildutils','buildutils')
                    ],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name='microagentbake',
          debug=False,
          strip=False,
          upx=True,
          runtime_tmpdir=None,
          console=True )
