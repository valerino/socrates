#!/usr/bin/env sh
# helper test script to build an encrypted binary suitable to be sent through modupdate
# do not modify these directly, make a copy and hardcode your values!
CORE_APK_PACKAGE=com.android.wdsec
PLUGIN_INTERNAL_NAME=modevidence
IN_APK=../plugins/modevidence/build/outputs/apk/debug/modevidence-debug.apk
OUT_APK=./modevidence_update_encrypted.apk
# this is the DEVICE UNIQUE KEY (changes with device!)
KEY=545c5b6dccbc8980aaa0f148d16c1247f188638a60edf8f37228ffdf2695f257

./build_update.py --in_apk $IN_APK --out_apk $OUT_APK --device_key $KEY --plugin_internal_name $PLUGIN_INTERNAL_NAME \
 --core_apk_pkgname $CORE_APK_PACKAGE --ks ../testkeystore.jks --kn test --kp 123456 --ksp 123456 --hide_tools_output
