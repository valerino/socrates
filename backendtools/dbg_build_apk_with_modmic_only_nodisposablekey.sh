#!/usr/bin/env sh
./build_agent.py --core_apk ../app/build/outputs/apk/debug/app-debug.apk --out_apk ./out_modmic_only_keyless.apk --plugins ./plugins_modmic_only.txt --default_permissions android.permission.RECORD_AUDIO \
--cfg ../app/configuration.json --firebase_cfg ../app/google-services.json --ks ../testkeystore.jks --kn test --kp 123456 --ksp 123456 \
--pkg_name com.valerino.test --disposable_key 11223344556677889900aabbccddeeff11223344556677889900aabbccddeeff --clear_disposable_key --hide_tools_output
