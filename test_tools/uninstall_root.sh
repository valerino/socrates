# this is an helper script to quickly uninstall the agent and kill the
# relevant processes
# needs root, and also handles killing injected processes

# uninstall agent
pm uninstall com.android.wdsec

# kill loader and voip apps, add other apps when needed...
pkill -f -l 9 loader
pkill -f -l 9 loader32
pkill -f -l 9 com.whatsapp
pkill -f -l 9 org.thoughtcrime.securesms
pkill -f -l 9 com.viber.voip
pkill -f -l 9 com.skype.raider
pkill -f -l 9 org.telegram.messenger
pkill -f -l 9 com.facebook.orca
pkill -f -l 9 com.facebook.orca
pkill -f -l 9 jp.naver.line.android

# reenable magisk
pm enable com.topjohnwu.magisk/a.c

# cleanup (but leave this script)
rm /sdcard/*.bin
rm /data/local/tmp/*.bin
rm /data/local/tmp/loader*
rm /data/local/tmp/*.so
rm /data/local/tmp/patch_selinux.sh
