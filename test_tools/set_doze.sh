#!/usr/bin/env sh
# set/unset doze mode on the connected device
# query: query state, reset: reset battery(plug/unplug)
# usage: set_doze.sh <on|off|query|reset>

runGradle() {
	echo "[-] building core APK with plugins ..."
	./gradlew $_GRADLE_ASSEMBLE
	if [ $? -ne 0 ]; then
    	exit 1
	fi

    if [ "$2" != "--nopush" ]; then
        # install
        echo "[-] installing APK ..."
        adb install -r app/build/outputs/apk/$1/app-$1.apk
        if [ $? -ne 0 ]; then
            exit 1
        fi

        # run
        echo "[-] starting Activity for BootReceiver to work at reboot ..."
        adb shell am start -n com.android.wdsec/.MainActivity
    fi
}

if [ "$1" = "on" ]; then
	# setting doze mode on
	echo "[-] setting doze on"
	adb shell dumpsys battery reset
	adb shell dumpsys deviceidle force-idle
elif [ "$1" = "off" ]; then
	# setting doze mode off
	echo "[-] setting doze off"
	adb shell dumpsys deviceidle unforce
	adb shell dumpsys battery reset
elif [ "$1" = "query" ]; then
	# query device state
	echo "[-] querying deep-idle(doze) status"
	adb shell dumpsys deviceidle get deep
elif [ "$1" = "reset" ]; then
	# reset battery
	echo "[-] resetting battery"
	adb shell dumpsys battery reset
else
	echo "usage: $0 <on|off|query|reset>"
	exit 1
fi


