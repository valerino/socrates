#!/usr/bin/env sh
# switch adb to tcpip (device must be connected over usb and have a valid wifi connection)

adb kill-server

# get device wlan ip
_RES=$(adb shell ip -f inet addr show wlan0)
if [[ "$?" -ne 0 ]]; then
        exit 1
fi

_IP=$(echo "$_RES" | grep -oE "\b((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b" | head -n1)
echo "[.] device ip = $_IP"
if [[ -z "$_IP" ]]; then
        echo "[x] cannot get device ip!"
        exit 1
fi

# connect
_PORT=5555
echo "[.] connecting adb to $_IP:$_PORT...."
adb tcpip "$_PORT"
sleep 1
adb connect "$_IP:$_PORT"

